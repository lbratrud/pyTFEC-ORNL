#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os

version = "2.0 9/28/2017"

sys.stdout.write("\nCapture TPC version {}\n\n".format(version))
from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_IO as fec_io
import waveGen
import pulseReConstruct

CmdExecutionQueue = []
make_raw_capture=0
fec = fec_io.FEC(CmdExecutionQueue,[version])
fec.DoInitProc()

fname = time.strftime('%Y%b%d_%H%M%S')
fpath = "{}/ReconPulse/{}".format(fec.capturePath, fname)
os.makedirs(fpath)
os.chdir(fpath)

#aList = [5,10,15,20,25,30,35]
aList = [50]
#fList = [15,25,50,100,150,200]
#fList = [4,10,15,20,25,50]
fList = [4]
rList = [10000]
freq = 33300

sys.stdout.write("Path = {}\n".format(fpath))
amplitude = 25

for P0 in ["on","off"]:
   for P1 in ["on","off"]:
      for amplitude in aList:
          for fall in fList:
              for rise in rList:
                 desc = "amp_{}mVpp_f{}ns_r{}ns_{}hz_{}_{}".format(amplitude,fall,rise,freq,P0,P1)
                 sys.stdout.write("Doing {}\n".format(desc))
                 PulseInfo1 = waveGen.setupFullSignal(0, [freq, amplitude * 0.001, fall*1e-9, rise*1e-9,P0,10e-6])
                 PulseInfo2 = waveGen.setupFullSignal(1, [freq, amplitude * 0.001, fall*1e-9, rise*1e-9,P1,10e-6])
                 time.sleep(25)
                 cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir . --mask 0x1".format(pulseReConstruct.numFrames)
                 code = fec_io.ExecuteCmdParse(cmd)
                 try:
                    os.rename("run000000_trorc00_link00.bin", "run0_{}.bin".format(desc))
                    os.rename("run000000_trorc00_link01.bin", "run1_{}.bin".format(desc))
                 except:
                    pass

exit()
