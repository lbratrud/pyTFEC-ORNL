#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
import FEC_DATA as fecDATA

import projpath as mpath

mpath.projName = "TPC-FEC"
import errwarn as errwarn

errwarn.ErrWarn.setup(0)
import pulseReConstruct

sys.stdout.write("\nReconBinData version {}\n\n".format(pulseReConstruct.version))

FREQINPUT = 33300
resolution = 16
make_summary = 0
i = 0
do_channel_gain_plot = 0
do_recon_plot = 0
xminDef="None"
xmaxDef="None"

do_help = 0
while i < len(sys.argv):
    cmd = sys.argv[i].lower()
    if cmd in  ["-h","-help"]:
        do_help = 1
    elif cmd == "-freq":
        FREQINPUT = int(sys.argv[i + 1])
        i += 1
    elif cmd == "-freqk":
        FREQINPUT = int(1000 * float(sys.argv[i + 1]))
        i += 1
    elif cmd == "-res":
        resolution = int(sys.argv[i + 1])
        i += 1
    elif cmd == "-plot":
        do_recon_plot = 1
    elif cmd == "-gainplot":
        do_channel_gain_plot = 1
    elif cmd == "-xmin":
        xminDef = float(sys.argv[i + 1])
        i += 1
    elif cmd == "-xmax":
        xmaxDef = float(sys.argv[i + 1])
        i += 1
    elif cmd in ["-s", "-sum", "-summary"]:
        make_summary = 1
        i += 1
    i += 1

if do_help==1:
    sys.stdout.write("This program has several functions. The primary one is to reprocess the .reconX files in a directory\n")
    sys.stdout.write("It handles gain or crosstalk files based on the file naming conversions.\n")
    sys.stdout.write("Files ending in _on_off or _off_on represent crosstalk files.\n")
    sys.stdout.write("Files ending in _g20 or _g30 represent gain files.\n")
    sys.stdout.write("")
    sys.stdout.write("-freq <value>    Define the input frequency of the input. Default 33.300KHz.")
    sys.stdout.write("-freqk <value>   Define the input frequency of the input in KHz. Default 33.300KHz.")
    sys.stdout.write("-res <value>     Define the reconstruction resolution. Default 16.")
    sys.stdout.write("-plot            Generate plots for each channel with the reconstructed data")
    sys.stdout.write("-gainplot        Generate a linear gain information plots for each channel.")
    sys.stdout.write("")
    sys.stdout.write("For reconstructed plots, there are two addition parameters to control the amount of data.")

    sys.stdout.write("-xmin            Define a start time point in microseconds")
    sys.stdout.write("-xmax            Define a stop time point in microseconds")

    exit(0)
pwd = os.getcwd()
pwdRef = pwd.replace("/home/daquser/TPC-FEC/data/", "")
words = pwdRef.split("/")

fecname = words[0]
pulseInfoList = []
pulseReConstruct.DoSetup(resolution, setFREQINPUT=FREQINPUT)
if xminDef=="None":
   xminDef = 0
else:
   xminDef = int(xminDef*pulseReConstruct.USTOPNTS)

if xmaxDef == "None":
   xmaxDef = pulseReConstruct.num_bins
else:
   xmaxDef = int(xmaxDef*pulseReConstruct.USTOPNTS)

fec = fecDATA.FEC_data()
if fecname[0:3]=="fec":
    fec.BoardID = int(fecname.replace("fec",""),16)
else:
    fec.ScaID = int(fecname.replace("fid",""),16)
fec.Path = pwd
#fec.BoardID = int("0x{}".format(i.replace("fec", "")), 0)

pulseReConstruct.ProcessFiles(fec,fecname,force_update=1,xminDef=xminDef,xmaxDef=xmaxDef,do_channel_gain_plot=do_channel_gain_plot,do_recon_plot=do_recon_plot)
exit()
