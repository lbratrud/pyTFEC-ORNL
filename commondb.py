# commondb.py (cdb)
#
# Define ProjParams, ColorList, and TextObjLists. All files should reference
# this one for getting color, parameter, or text information.
#
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.com)
# 5/27/2016

import math
import sys
import string
import sys
import os
import errwarn

from projbase import *
import projdb as db
import projpath as mpath

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------
# There are no common Parameters for a system. Each one is different!

ProjParams = db.ParamObj("SysParameters",0)

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------
# 
def GetTextWL(msg,font,fontsize):
    w = 0.85*fontsize*len(msg)
    h = 1.2*fontsize
    return (w,h)

def StripUnderScore(name):
    return name.translate(string.maketrans("_", " "))

def getSubDesc(words,group):
    cname = ""
    for i in words:
         if i!=group:
              if len(cname) > 0:
                 cname += " "
              cname += i
    return cname
# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------
class TextConfig:
    def __init__(self):
       self.name = 0
       self.color = "#ffffff"
       self.size = 12
       self.font = "Arial"
       self.justify = "center"

       self.Master = 0
       self.myformat=0
       self.enable = 1
       self.xpos = 0
       self.ypos = 0
       self.modeX = 0
       self.modeY = 0
       self.anchor = "c"
       self.textpos = -1

    def doc(self):
       sys.stdout.write("TextConfig {} master={}\n".format(self.name,self.Master))
       sys.stdout.write("=> Color={} Size={} Font={} Justify={}\n".format(self.color,self.size,self.font,self.justify))
       sys.stdout.write("=> Format={} Enable={} anchor={} pos={}\n".format(self.myformat,self.enable,self.anchor,self.textpos))
       sys.stdout.write("=> x,y={},{} mode={},{}\n".format(self.xpos,self.ypos,self.modeX,self.modeY))

    def parse(self,subcmd,words,index):
       if subcmd in ["x"]:
           self.setX(float(words[index+1]))
           return 1
       elif subcmd in ["y"]:
           self.setY(float(words[index+1]))
           return 1
       elif subcmd in ["absx","ax"]:
           self.setAbsX(float(words[index+1]))
           return 1
       elif subcmd in ["absy","ay"]:
           self.setAbsY(float(words[index+1]))
           return 1
       elif subcmd in ["xc","xcenter"]:
           self.setXCenter(float(words[index+1]))
           return 1
       elif subcmd in ["yc","ycenter"]:
           self.setYCenter(float(words[index+1]))
           return 1
       elif subcmd in ["txtpos","textpos","textposition"]:
           self.setTextPos(words[index+1])
           return 1
       elif subcmd in ["anchor"]:
           self.setAnchor(words[index+1])
           return 1
       elif subcmd in ["size","fontsize"]:
           self.size = int(words[index+1])
           return 1
       elif subcmd in ["text","txt"]:
           self.setEnable(words[index+1])
           return 1
       elif subcmd in ["clear"]:
           self.Clear()
           return 0
       elif subcmd in ["font"]:
           self.font = words[index+1].translate(strmaketrans("+", " "))
           return 1
       elif subcmd == "color":
           self.color  = words[index+1]
           return 1
       elif subcmd == "x":
           self.setX(float(words[index+1]))
           return 1
       elif subcmd == "y":
           self.setY(float(words[index+1]))
           return 1
       elif subcmd == "format":
           fval = words[index+1].lower()
           if fval in ["nospace","wrap","normal"]:
              self.myformat = fval
           else:
              sys.stdout.write("{}: Bad format {}\n".format(self.name,fval))
           return 1
       elif subcmd == "justify":
           fval = (words[index+1]).lower()
           if fval in ["center","left","right"]:
              self.justify = fval
           else:
              sys.stdout.write("{}: Bad justify {}\n".format(self.name,fval))
           return 1
       elif subcmd in ["pos","position"]:
           self.setX(float(words[index+1]))
           self.setY(float(words[index+2]))
           return 2
       return -1

    def formatText(self,text):
       ftext = text.translate(strmaketrans("^","\n"))
       if self.myformat=="nospace":
          return ftext.translate(strmaketrans(" ^", "\n\n"))
       else:
          return ftext

    def createText(self,window,x,y,msg,ID,anchor,ftype):
       xi = x
       yi = y
       x = window.xconvert(x)
       y = window.yconvert(y)
       font = self.getFont()
       textmsg = self.formatText(msg)
       (w,h) = GetTextWL(textmsg,font,self.size)
       args = []
       args.append(x)
       args.append(y)

       adj = 0.1
       xadj1 = xadj2 = 0
       if anchor in ["ne","se","e"]:
         xadj1 =  -1-adj
       elif anchor in ["nw","sw","w"]:
         xadj2 =  1+adj
       else:
         xadj1 = 0.5+adj
         xadj2 = -0.5-adj

       yadj1 = yadj2 = 0
       if anchor in ["ne","nw","n"]:
         yadj1 =  -1-adj
       elif anchor in ["se","sw","s"]:
         yadj2 =  1+adj
       else:
         yadj1 = 0.5+adj
         yadj2 = -0.5-adj

       window.definebb(x+xadj1*w,y+yadj1*h)
       window.definebb(x+xadj2*w,y+yadj2*h)
       text = window.canvas.create_text(*args)
       window.TextListAdd(text,self.name)
       fsize = int(self.size*window.scale_factor*SCALE_FONTSIZE_RATIO)
       window.canvas.itemconfig(text,tags=ID,justify=self.justify,text=textmsg,fill=self.getColor(),font=(font,fsize,ftype),anchor=anchor)
       return text

    def setParam(self,parmname,value):
       return

    def setup(self,paramlist):
       self.color = paramlist[0]
       self.font = paramlist[1].translate(strmaketrans("+", " "))
       self.size=paramlist[2]

    def getColor(self):
       if self.Master == 0:
          return self.color
       else:
          return self.Master.color
    def getFont(self):
       if self.Master == 0:
          return self.font
       else:
          return self.Master.font
    def getSize(self):
       if self.Master == 0:
          return self.size
       else:
          return self.Master.size
       
    def saveInfo(self,f):
       font = self.font.translate(strmaketrans(" ", "+"))
       f.write("textconfig name={} font={} size={} color={}\n".format(self.name,font,self.size,self.color))

    def Print(self):
       return
#       sys.stdout.write("TextConfig E={} set={},{} use={} anc={} C={},{} color={}\n".format(self.enable,self.xpos,self.ypos,self.useXY,self.anchor,self.xcenter,self.ycenter,self.color))

    def setName(self,name):
       self.name = name

     
    def CopyN(self,other):
       self.color = other.color
       self.font = other.font
       self.size = other.size
       self.myformat = other.myformat
       self.justify = other.justify

       self.xpos = other.xpos
       self.ypos = other.ypos
       self.anchor = other.anchor
       self.enable = other.enable
       self.modeX = other.modeX
       self.modeY = other.modeY
       self.textpos = other.textpos

    def Copy(self,other):
       self.name = other.name
       self.CopyN(other)
       
    def MasterCopy(self,other):
       self.name = other.name
       self.CopyN(other)
       if other.name != 0:
          self.Master = TextConfigList.seek(other.name)
       
    def setX(self,x):
       self.modeX = "set"
       self.xpos = x

    def setAbsX(self,x):
       self.modeX = "abs"
       self.xpos = x

    def setY(self,y):
       self.modeY = "set"
       self.ypos = y
    def setAbsY(self,y):
       self.modeY = "abs"
       self.ypos = y

    def setEnable(self,enable):
       if enable in ["off","OFF"]:
            self.enable  = 0
       else:
            self.enable  = 1

    def checkAnchor(self,anchor):
       if anchor in ["c","ne","se","sw","nw","n","s","e","w","en","es","wn","ws"]:
         return anchor
       elif anchor in ["t","top"]:
         return "n"
       elif anchor in ["b","bottom","bot"]:
         return "s"
       elif anchor in ["l","left"]:
         return "w"
       elif anchor in ["r","right"]:
         return "e"
       else:
          errwarn.ErrWarn.RecErr(1,"Unknown anchor setting {}".format(anchor))
          return -1

    def setAnchor(self,anchor):
       self.anchor = self.checkAnchor(anchor)
       if self.anchor==-1:
          self.anchor = "c"

    def setTextPos(self,anchor):
       self.textpos = self.checkAnchor(anchor)

    def Clear(self):
       self.modeX = self.modeY = 0
       
    def setXCenter(self,y):
       self.modeX  = "ctr"
       self.setY(y)

    def setAbsXCenter(self,y):
       self.modeX  = "ctr"
       self.AbsY(y)

    def setYCenter(self,x):
       self.modeY  = "ctr"
       self.setX(x)

    def setAbsYCenter(self,x):
       self.modeY = "ctr"
       self.AbsX(x)
       
class TextConfigListObj(db.ListObj):
   def __init__(self):
      db.ListObj.__init__(self,"TextConfig","upper","S",[])
       
   def create(self,name):
      i = TextConfig()
      i.name = name.upper()
      self.MyList.append(i)
      return i

TextConfigList = TextConfigListObj()
TextConfigList.setupWDesc("winbase_text", ["#ffffff","liberation sans",15],"WINDOWS")
TextConfigList.setupWDesc("report", ["#ffffff","Times-Roman",14],"REPORTS")
TextConfigList.setupWDesc("title", ["#ffffff","Times-Roman",20],"REPORTS")

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------
# Color have some defaults!
class ProjColors(db.ParamObj):
   def __init__(self,name,MainList):
      db.ParamObj.__init__(self,name.upper(),MainList)
      self.DescList = []
      self.defaults()

   def getParam(self,param):
      pname = self.ParamTrans(param)
      v = self.getV(pname)
      if v == "NONE<>":
          return param
      else:
          return v

   def getDesc(self,param):
      pname = self.ParamTrans(param)
      for i in self.DescList:
          if i[0]==pname:
              return i[1]
      return 0
   
   def setDesc(self,param,desc):
      pname = self.ParamTrans(param)
      for i in self.DescList:
          if i[0]==pname:
              i[1]=desc
              return
      self.DescList.append([pname,desc])

   def seek(self,param):
      pname = self.ParamTrans(param)
      v = self.getV(pname)
      if v != "NONE<>":
         return v
      return 0

   def add(self,param,value):
      pname = self.ParamTrans(param)
      return self.setV(pname,value)

   def addwDesc(self,param,value,desc):
      pname = self.ParamTrans(param)
      self.setV(pname,value)
      self.setDesc(pname,desc)

   def defaults(self):
      # Define color defaults so the program does not crash!      
      self.addwDesc("BACKGROUND","#000000","DISWIN")

      self.addwDesc("WINBASE_BG","#404040","WINBASE")
      self.addwDesc("WINBASE_TEXT_FG","#FFFFFF","WINBASE")
      self.addwDesc("WINBASE_TEXT_BG","#191919","WINBASE")
      self.addwDesc("WINBASE_PROMPT_FG","#ffffff","WINBASE")
      self.addwDesc("WINBASE_PROMPT_BG","#303060","WINBASE")
      self.addwDesc("WINBASE_MENU_FG","#ffffff","WINBASE")
      self.addwDesc("WINBASE_MENU_BG","#4040c0","WINBASE")
      self.addwDesc("WINBASE_SELECT","#7878a8","WINBASE")

      self.addwDesc("VISIBLE_YES","#ffffff","ENGINE")
      self.addwDesc("VISIBLE_NO","#909090","ENGINE")

ColorList = ProjColors("ProjColors",0)

def SaveDB():
    f = mpath.openFile("sys","sys","basedefs",'w')
    if f==-1:
       return
    ProjParams.saveList(f,"setparm")
    ColorList.saveList(f,"colorsetup")
    for t in TextConfigList.MyList:
       t.saveInfo(f)
    f.close()

def ReadDB():
  f = mpath.openFile("sys","sys","basedefs",'r')
  if f==-1:
     return

  for line in f:
   nline = line.translate(strmaketrans("\n,=", "   "))
   words = nline.split()
   if len(words) > 0:
#       sys.stdout.write("ReadDB: {}\n".format(nline))
       cmd = words[0].lower()
       if cmd in ["parameter"]:
          if len(words) == 3:
             ProjParams.add(words[1].lower(),float(words[2]))
       elif cmd in ["colorsetup"]:
           ColorList.add(words[1],words[2])
       elif cmd in ["textconfig"]:
           txt = TextConfigList.find(words[1])
           mlen = len(words)
           if mlen > 2:
              i = 2
              while i < mlen:
                 subcmd = words[i].lower()
                 inc = txt.parse(subcmd,words,i)
                 if inc < 0:
                    errwarn.ErrWarn.RecErr(1,"Unknown TEXTCONFIG keyword {}- {}\n".format(subcmd,line))
                 else:
                    i += inc
                 i += 1
       else:
           errwarn.ErrWarn.RecErr(1,"Unknown keyword {}- {}\n".format(cmd,line))
  f.close()

def SaveBaseDefs():
    f = mpath.openFile("sys","sys","basedefs",'w')
    if f==-1:
        return
    ProjParams.saveList(f,"parameter")
    ColorList.saveList(f,"colorsetup")
    for t in TextConfigList.MyList:
       t.saveInfo(f)
    f.close()

