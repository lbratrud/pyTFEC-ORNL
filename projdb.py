# mtcsdb.py
#
# Create a List of objects verses having different lists for each object and
# different search routines! It is my version of a database, but it would be good
# idea to convert it to mongoDB in the future!
#
# --------------------------------------------------------------------------------
# Created 1/1/2015 by Lloyd Clonts clontslg@yahoo.com
# Project started December 2013 for Gary W. Turner
# for implementing CTC system

import string
import sys
import os
import time
from projbase import *
import projtime as mtime
import projpath as mpath
import errwarn

AllLists = []
# --------------------------------------------------------------------------------      
# ListObj
#
# Define a list of objects and give a common means of referencing them.
#
# listtype:
#     Name of list and how the algorithms output data
#
# case:
#     Formating for name of listtype.
#     Options are: upper, lower, none
#
# valtype:
#     Define what the listtype is.
#     Options are: S (string), I (int)
#
# InputParamList:
#     Define the parameters for each object in a particular list.
#
class ListObj:
   def __init__(self,listtype,case,valtype,InputParamList):
      self.MyList = []
      self.Modified = 0
      self.writable = 1
      self.nameasID = 0
      self.case = case
      self.listtype = listtype.lower()
      self.valtype = valtype
      self.index = 1
      AllLists.append(self)
      self.ParamList = []
      self.DynamicList = []
      self.InitParamList = []
      self.GSconvert = []
      self.user_option = 0
      self.DescList = []

      for s in InputParamList:
         words = s.lower().split()
         mlen = len(words)
         mytype = "string"
         myvalue = "none"
         
         index = 1
         mode = 0
         code = 0
         while index < mlen:
            if words[index] == "dynamic":
               mode = 1
            elif code==0:
               mytype = words[index]
               code += 1
            elif code==1:
               myvalue = words[index]
               code += 1
            index += 1

         if mode==1:
            if words[0] not in self.ParamList:
                self.DynamicList.append(words[0])
            self.defineParamGS(words[0],mytype,myvalue)
         else:
            self.ParamListSetup(words[0],mytype,myvalue)

   def addGSconvert(self,keyword,paramtype):
      for i in self.GSconvert:
         if i[0]==keyword:
            return
      self.GSconvert.append([keyword,paramtype])

   def defineParamGS(self,keyword,paramtype,defaultvalue):
       if paramtype in ["int","string","char","date","datetime","color","timemeasure"]:
          self.addGSconvert(keyword,paramtype)
          if paramtype == "int":
            if defaultvalue=="none":
               defaultvalue = 0
          elif paramtype in ["date","datetime"]:
             if defaultvalue =="none":
                defaultvalue = "none"
          elif paramtype == "char":
             if defaultvalue =="none":
                defaultvalue = "N"
          elif paramtype == "color":
             if defaultvalue =="none":
                defaultvalue = "#ffffff"
          elif paramtype == "timemeasure":
             if defaultvalue =="none":
                defaultvalue = 0

       elif self.addConvert(keyword,paramtype)==0:
          self.addGSconvert(keyword,0)

       if keyword not in self.InitParamList:
            self.InitParamList.append([keyword,defaultvalue])

   def ParamListSetup(self,keyword,paramtype,defaultvalue):
       if keyword not in self.ParamList:
          self.ParamList.append(keyword)
       self.defineParamGS(keyword,paramtype,defaultvalue)

   def addConvert(self,keyword,listtype):
      for i in AllLists:
         if i.listtype == listtype:
            self.addGSconvert(keyword,i)
            return 1
      return 0

   def convertIDName(self,mode,keyword,value):
      global trainsys_time
      for i in self.GSconvert:
         if i[0] == keyword:
            if i[1] in ["int","string","char","color","timemeasure",0]:
                return value
               
            if i[1] in ["date","datetime"]:
               if value in NULL_DEF:
                  return value
               if mode=="ID":
                   return mtime.String2Time(value,i[1])
               else:
                  return mtime.Time2String(value,i[1])

            if value in NULL_DEF or i[1]==0:
               return "none"
            if mode=="ID":
               obj = i[1].seek(value)
               if obj!=0:
                  return obj.objID
               obj = i[1].IDseek(value)
               if obj!=0:
                  return obj.objID
            else:
               obj = i[1].IDseek(value)
               if obj!=0:
                  return obj.name
               obj = i[1].seek(value)
               if obj!=0:
                  return obj.name
#            sys.stdout.write("Failed to find {} {}={}\n".format(i[1].listtype,keyword,value))
            return "none"
      return value

   def convertIDToName(self,keyword,value):
      return self.convertIDName("Name",keyword,value)

   def convertNameToID(self,keyword,value):
      return self.convertIDName("ID",keyword,value)

   def saveInfo(self,f):
      f.write("{} ListObjIdNum = {}\n#*****************************\n".format(self.listtype,self.index))
      for c in self.MyList :
        c.saveInfo(f,self.listtype,self.ParamList)

   def saveDynamic(self,f):
      if len(self.DynamicList)==0:
         return
      for c in self.MyList :
         c.saveInfo(f,self.listtype,self.DynamicList)

   def save(self):
      if self.writable==0:
         return
      f = mpath.openFile("sys","sys",self.listtype,'w')
      self.saveInfo(f)
      f.close()

   def backup(self):
      if self.writable==0:
         return
      datef = time.strftime('%Y_%b_%d_%I_%M')
      f = mpath.openFile("backup",self.listtype,datef,'w')
      self.saveInfo(f)
      f.close()

   def getName(self,name):
      trans = strmaketrans("^"," ")
      if self.valtype=="I":
         try:
            v = int(name)
            return v
         except ValueError:
            return "badInt {}\n".format(name)

      elif self.case == "upper":
         return name.translate(trans).upper()
      
      elif self.case == "lower":
         return name.translate(trans).lower()
      return name

   def TimerProc(self,traintimediff):
      for i in self.MyList:
         i.TimerProc(traintimediff)
   def MsgProc(self):
      for i in self.MyList:
         i.MsgProc()

   def create(self,name):
      sys.stdout.write("ListObj::Listtype={} is unknown\n".format(self.listtype))
      return 0

   def IDseek(self,ID):
      if self.valtype!="I":
         uID = ID.upper()
      else:
         uID = ID
      for i in self.MyList:
         if i.objID == uID:
            return i
      return 0

   def ParmValueSeek(self,parm,value):
      for i in self.MyList:
         if i.getParam(parm) == value:
            return i
      return 0
   
   def IDfind(self,ID):
      if self.valtype!="I":
         uID = ID.upper()
      else:
         uID = ID
      for i in self.MyList:
         if i.objID == uID:
            return i
      i = self.create("NEWONE")
      i.objID = uID
      return i

   def seek(self,name):
      sname = self.getName(name)
      for i in self.MyList:
         if i.name == sname:
            return i
      return 0

   def find(self,name):
      sname = self.getName(name)
      for i in self.MyList:
         if i.name == sname:
            return i

      i = self.create(sname)
      i.setParam("name",sname)
      if self.nameasID==0:
         i.objID = "{}{}".format(self.listtype[0].upper(),self.index)
      else:
         i.objID = sname
      self.index += 1
      return i

   def ObjSetup(self):
      for i in self.MyList:
         i.Setup()

   def setup(self,name,info):
      n = self.find(name)
      n.setup(info)

   def setupWDesc(self,name,info,desc):
      self.setup(name,info)
      self.setDesc(name,desc)
      
   def IDdelete(self,deleteID):
      index = 0
      for i in self.MyList:
         if i.objID == deleteID:
            i.deleteBase()
            self.MyList.pop(index)
            self.save()
            return 1
         index += 1
      errwarn.ErrWarn.RecErr(1,"DB {} Delete ID={} FAILED".format(self.listtype,deleteID))
      return 0
   def delete(self,name):
      index = 0
      sname = self.getName(name)
      for i in self.MyList:
         if self.getName(i.name) == sname:
            i.deleteBase()
            self.MyList.pop(index)
            self.save()
            return 1
         index += 1
      errwarn.ErrWarn.RecErr(1,"DB {} Delete {} FAILED".format(self.listtype,name))
      return 0

   def parse(self,words):
      if  words[1]=="ListObjIdNum":
         self.index = int(words[2])
         return
      param = words[2]
      if param.lower()=="name":
         myObj = self.IDfind(words[1])
      else:
         myObj = self.IDseek(words[1])
         if myObj == 0:
            return
      i = 3
      value = ""
      donefirst = 0
      while i < len(words):
         if donefirst==1:
            value += " "
         donefirst = 1
         value += words[i]
         i+=1
      myObj.setBase(param,value)

   def getDesc(self,param):
      for i in self.DescList:
          if i[0]==param:
              return i[1]
      return 0
   
   def setDesc(self,inparam,desc):
      param = inparam.upper()
      for i in self.DescList:
          if i[0]==param:
              i[1]=desc
              return
      self.DescList.append([param,desc])

   def ReadDB(self):
      f = mpath.openFile("sys","sys",self.listtype,'r')
      if f==-1:
         return
    
      for line in f:
    #   sys.stdout.write("line={}".format(line))
       nline = line.translate(strmaketrans("\n,=", "   "))
       words = nline.split()
       if len(words) > 0:
           cmd = words[0].lower()
           if cmd in ["#","!","*"]:
              pass
           if cmd in [self.listtype]:
               self.parse(words)
           else:
               errwarn.ErrWarn.RecErr(1,"Unknown keyword {}- {}\n".format(cmd,line))
      f.close()
      
   def SaveDB(self):
        f = mpath.openFile("sys","sys",self.listtype,'w')
        if f==-1:
            return
        self.saveInfo(f)
        f.close()

# ParamObj
#
# Basic operations of parameter control of objects in the list.
#
# name:
#     Name of object being created
#
# MainList:
#     Reference back to ListObj.
#
class ParamObj:
   def __init__(self,name,MainList):
      self.objID = -1
      self.name = name
      self.objList = []
      self.MainList = MainList
      if MainList!=0:
         for s in MainList.InitParamList:
            self.objList.append([s[0],s[1],s[1]])

   def getParamList(self,param):
      return []
      
   def deleteBase(self):
      # Delete any relationships
      return

   def MsgProc(self):
      return

   def TimerProc(self,traintimediff):
      return


   def addNewParam(self,param,ptype,defvalue):
      if self.MainList!=0:
         self.MainList.ParamListSetup(param,ptype,defvalue)
      for i in self.objList:
         if i[0]==param:
            return
      self.addparam(param,defvalue)

   def addparam(self,param,defvalue):
      for i in self.objList:
         if i[0]==param:
            i[1] = i[2] = defvalue
            return
      self.objList.append([param,defvalue,defvalue])

   def createObjList(self,KeyWords):
      for s in KeyWords:
         words = s.lower().split()
         self.addparam(words[0],"none")
         
   def getColor(self):
      return "white"

   def doc(self):
      sys.stdout.write("ParamListObj {}\n".format(self.name))
      for o in self.objList:
         sys.stdout.write(" {}={}\n".format(o[0],o[1]))

   def saveList(self,f,ref):
      for o in self.objList:
         f.write("{} {}={}\n".format(ref,o[0],o[1]))

   def getBase(self,param):
      if param=="name":
         return self.name

      for i in self.objList:
         if i[0] == param:
            return i[1]
      return "NONE<>"

   def setBase(self,param,val):
      if param=="name":
         self.name = val

      for i in self.objList:
         if i[0] == param:
#            sys.stdout.write("setBase i={}\n".format(i))
            if i[1] != val:
               if self.MainList!=0:
                  if param in self.MainList.ParamList:
                     self.MainList.Modified = 1
            if len(i) != 3:
               sys.stdout.write("param={} i={}\n".format(param,i))
            i[2] = i[1]
            i[1] = val
            return 0
      self.objList.append([param,val,val])
      return -1

   def incrV(self,param,val):
      for i in self.objList:
         if i[0] == param:
            i[2] = i[1]
            i[1] = float(i[1]) + val
            if val != 0 and self.MainList!=0:
               self.MainList.Modified = 1
            return i[1]
      return "none"

   def getV(self,param):
      if param=="name":
         return self.name

      for i in self.objList:
         if i[0] == param:
            if self.MainList!=0:
               return self.MainList.convertIDToName(i[0],i[1])
            else:
               return i[1]
      return "NONE<>"

   def setV(self,param,val):
      if param=="name":
         self.name = val

      elif self.MainList!=0:
         val = self.MainList.convertNameToID(param,val)

      return self.setBase(param,val)

   def getParam(self,param):
      pname = self.ParamTrans(param)
      return self.getV(pname)

   def setParamModify(self,param,value,DoModify):
      if self.MainList!=0 and DoModify==1:
         self.MainList.Modified=1
      return self.setParam(param,value)

   def setParam(self,param,value):
      pname = self.ParamTrans(param)
      v = self.setV(pname,value)
      if v != 0:
         sys.stdout.write("Cannot find parameter {} for {}. Adding it!\n".format(param,self.name))
      return 0

   def add(self,param,val):
      if self.setV(param,val)==-1:
         return 1
      return 0
   
   def ParamTrans(self,param):
      words = param.translate(strmaketrans("\n/","  ")).lower().split()
      eff = ""
      for i in words:
         eff += "{}".format(i)
      return eff

   def Update(self):
      for i in self.objList:
         if i[1] != i[2]:
            if self.MainList!=0:
               self.MainList.Modified=1
      return

   def Setup(self):
      return

   def saveInfo(self,f,ref,keywords):
      for s in keywords:
         v = self.getBase(s)
         f.write("{} {} {}={}\n".format(ref,self.objID,s,v))

   def IsInUse(self,ignore):
      return 0
