# gbtxconfigIO.py
#
# Python file for reading and writing GBTx configuration files used by CERN
# GBTxprogrammer JAVAscript.
#
# The GBTx format is a list of the HEX values for GBTx register.
#
# I created a format (RegDef) for defining the registers of the GBTx. The format is:
# CONFIG_REGISTER_DEFINITIONS = [
#    ["ckCtr0",0,0x00],
#    ["ckCtr1",1,0x00],
#    ["ckCtr2",2,0x00],
#    ["ckCtr3",3,0x00],
#    ["ttcCtr0",4,0x00],
#    ["ttcCtr1",5,0x00],
#    ["ttcCtr2",6,0x00],
#    ["ttcCtr3",7,0x00],
#    ["ttcCtr4",8,0x00],
#    ["ttcCtr5",9,0x00],
#    ["ttcCtr6",10,0x00],
#    ["ttcCtr7",11,0x00],
#    ["ttcCtr8",12,0x00],
#    ["ttcCtr9",13,0x00],
#    ["ttcCtr10",14,0x00],
#    ["ttcCtr11",15,0x00],
#    ["ttcCtr12",16,0x03],
#    ["ttcCtr13",17,0x03],
#    ["ttcCtr14",18,0x03],
#    ["ttcCtr15",19,0x03],
#    ["ttcCtr16",20,0x03],
#   etc
# ]
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import string
import sys
import os

version = "1.0 10/24/2016"

#
# ReadConfigFile
#
# Load giving file of sequence of HEX numbers as a List. 
#
# Returns: The 365 numbers that were read!
def ReadConfigFile(filename):
  mList = []
  f = open(filename,'r')
  for line in f:
     mList.append(int(line,16))
  f.close()
  return mList


# WriteRegDefConfigFile
#
# Write out RegDef to a configuration file 
#
# Returns:
#
def WriteRegDefConfigFile(filename,configList):
  f = open(filename,'w')
  for i in configList:
      f.write("{}\n".format(hex(i[2])[2:]))
  f.close()
  sys.stdout.write("CreateConfigFile {}\n".format(filename))


# CompareRefDefConfigFile
#
# Compare a RegDef to a configuration file 
#
# Returns: number of differences
#
def CompareRefDefConfigFile(RegDef,cFileList,f):
    addr = 0
    diffs = 0
    for i in cFileList:
        j = RegDef[addr]
        if i!=j:
            if f!=0:
                f.write("  @{}={} mList={}\n".format(addr,hex(j),hex(i)))
            diffs += 1
        addr += 1
    return diffs
