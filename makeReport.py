#!/usr/bin/env python3

# captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
import pwrtest

import projpath as mpath
mpath.projName = "TPC-FEC"

import projtime as mtime
import FEC_email
import glob
version = "1.2 1/05/2018"

from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_DATA as fecDATA

tDef = time.strftime('%Y%b%d_%H%M%S')
windex = 1
all_data = 0
email_data = 0
while windex < len(sys.argv):
    if sys.argv[windex]=="-email":
       email_data = 1
    else:
       sys.stdout.write("Unknow option {}\n".format(sys.argv[windex]))
    windex += 1


def makeSingleReport(path):
    global tDef, email_data

    d1 = fecDATA.FEC_data()
    fecDATA.readSummary(d1, path)
    sID = d1.getFID().lower()
    bID = d1.getBID().lower()
#    bID = "{:0>3}".format(hex(d1.BoardID)[2:])
#    sID = "{:0>6}".format(hex(d1.ScaID)[2:])

    email_body = "FEC{} fID={} Summary\n\n".format(bID, sID) + d1.MakeTextReport()
    filename = "autosum_fec{}_{}_{}.pdf".format(bID.lower(), sID.lower(), tDef)
    d1.MakePDF(filename)
    if email_data == 1:
        mList = glob.glob("autosum*.pdf")
        mList += glob.glob("*_fid*.txt")
        sendList = ['readkf@ornl.gov', 'clontslg@ornl.gov']
#        sendList = ['readkf@ornl.gov', 'clontslg@ornl.gov', 'glassel@physi.uni-heidelberg.de']
        FEC_email.SendEmail(sendList, "FEC{} fID={} Final Report".format(bID, sID), email_body, mList)

if all_data==0:
    makeSingleReport(os.getcwd())
else:
    SumList = glob.glob("{}/fid*/Summary".format(fecDATA.DATAPATH))
    for i in SumList:
        os.chdir(i)
        makeSingleReport(i)
exit(0)



exit()
