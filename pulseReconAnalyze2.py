#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
version = "2.0 9/28/2017"

sys.stdout.write("\nCapture TPC version {}\n\n".format(version))
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import pulseReConstruct
import projmkrpt as mkrpt
import FEC_IO as fec_io

FREQINPUT = -1
resolution=16
i = 0
while  i < len(sys.argv):
   cmd = sys.argv[i].lower()
   if i == "-freq":
	   pulseReConstruct.FREQINPUT = int(sys.argv[i + 1])
	   i += 1
   elif i == "-freqk":
	   pulseReConstruct.FREQINPUT = 1000 * int(sys.argv[i + 1])
	   i += 1
   elif i == "-res":
	   resolution = int(sys.argv[i + 1])
	   i += 1
   i+=1

sys.stdout.write("INPUTFREQ={}\n".format(pulseReConstruct.FREQINPUT))
#
if len(files)==0:
	for gbtxNum in [0,1]:
		files = glob.glob("run{}_*.bin".format(gbtxNum))
		if len(files) > 0:
			for i in files:
				cmd = "decoder_gbtx -f {} -i {}".format(pulseReConstruct.numFrames,i)
				nData = i.replace(".bin","")
				cmd += " -o {} -s {} -p".format(nData,gbtxNum)
				sys.stdout.write("Decoding GBTx file {}\n".format(nData))
				fec_io.ExecuteCmdParse(cmd)
	files = glob.glob("*GBTX0.dat")

pulseReConstruct.DoSetup(16)
dataList = []
for i in files:
	sys.stdout.write("Reading {}\n".format(i))
	data = pulseReConstruct.LoadPulseData(i)
	data.loadFile(i,maxitr=200000)
	data.construct()
	data.infofind()
	data.Print()
	data.removeBaseLine()
	dataList.append(data)

pdfgen = mkrpt.makeReport("analyze2.pdf", "Optimization")
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]

myData = []
myZeroData = []
lineList = []
cindex = 0
maxpnts=125000
for i in range(0,8):
	lineList.append(["Ch{}".format(i),ColorList[cindex],None])
	cindex += 1
	if cindex >= 8:
	   cindex = 0

myData = dataList[0].ReconList[0:8]

xmax = 0
ymax = 0
for i in myData:
	for k in i:
		if k[0] > xmax:
		   xmax = k[0]
		if k[1] > ymax:
			ymax = k[1]
xmax = 0.1*int(10.5*xmax)
ymax = 10*(int(0.1*ymax)+1)

pdfgen.addBasicMultiPlot(xmax,ymax,myData,"Samples=125k Ts={}ns".format(pulseReConstruct.TIMERESOLUTION),lineList,xpnts=10,plotsize=600)

pdfgen.DoExit()

exit()
