# FEC_IO.py
#
# This file handles the interactions with the FEC board. Internal operations
# handlethe accessing of different registers within the x-RORC.
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@yahoo.com)


import math
import sys
import time
import string
import sys
import os
import array
import ctypes
import subprocess
import serial
import serial.tools.list_ports

for i in serial.tools.list_ports.comports():
	sys.stdout.write("ComPort={}\n".format(i))
