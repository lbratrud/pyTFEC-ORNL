#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
version = "2.0 9/28/2017"

sys.stdout.write("\npulsePrint version {}\n\n".format(version))
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import pulseReConstruct
import projmkrpt as mkrpt
import FEC_IO as fec_io

FREQINPUT = 33300
resolution=16
i = 0
while  i < len(sys.argv):
   cmd = sys.argv[i].lower()
   if cmd == "-freq":
	   FREQINPUT = int(sys.argv[i + 1])
	   i += 1
   elif cmd == "-freqk":
	   FREQINPUT = int(1000 * float(sys.argv[i + 1]))
	   i += 1
   elif cmd == "-res":
	   resolution = int(sys.argv[i + 1])
	   i += 1
   i+=1

pulseReConstruct.DoSetup(resolution,setFREQINPUT=FREQINPUT)

def getFiles():
	return glob.glob("run0_*GBTX0.dat")


path = os.getcwd()
sys.stdout.write("INPUTFREQ={} path={}\n".format(pulseReConstruct.FREQINPUT,path))
files = getFiles()
#
if len(files)==0:
	for gbtxNum in [0,1]:
		files = glob.glob("run{}_*.bin".format(gbtxNum))
		if len(files) > 0:
			for i in files:
				cmd = "decoder_gbtx -f {} -i {}".format(pulseReConstruct.numFrames,i)
				nData = i.replace(".bin","")
				cmd += " -o {} -s {} -p".format(nData,gbtxNum)
				sys.stdout.write("Decoding GBTx file {}\n".format(nData))
				fec_io.ExecuteCmdParse(cmd)
	files = getFiles()

#pdfgen = mkrpt.makeReport("analyze2.pdf", "Optimization")
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]

DataList = []
for f in files:
	fileDef0 = f.replace("_GBTX0.dat", "")
	fileDef = fileDef0.replace("run0_", "")
#	fileDef = fileDef.replace("_GBTX1.dat", "")#
	#fileDef = fileDef.replace("run0_", "")
	#fileDef = fileDef.replace("run1_", "")
	data = pulseReConstruct.LoadPulseData(fileDef)
	sys.stdout.write("Reading {}\n".format(fileDef))
	data.loadFile(f,maxitr=200000)
	f1 = "run1_{}_GBTX1.dat".format(fileDef)
#	data.loadFile(f1,maxitr=200000)
	data.construct()
	of = open("{}_PD{}.dat".format(fileDef,pulseReConstruct.BINSPERSAMPLEFREQ),'w')
	of.write("# Directory {}\n".format(path))
	data.Print(of)
	of.close()
	data.infofind()
	data.removeBaseLine()
	DataList.append(data)
	# of = open("{}_PDBLC{}.dat".format(fileDef,pulseReConstruct.BINSPERSAMPLEFREQ),'w')
	# of.write("# Directory {}\n".format(path))
	# data.Print(of)
	# of.close()

pdfgen = mkrpt.makeReport("pulsePrint{}.pdf".format(resolution), "PulsePrint Output by Channels")
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]

cpnt = int(DataList[0].size * 0.25)
spnt = int(cpnt - 5e-7/pulseReConstruct.TIMERESOLUTION)
epnt = int(cpnt + 5e-7/pulseReConstruct.TIMERESOLUTION)
MaxList = []
for index in range(0, 16):
	sys.stdout.write("Creating Channel {} plot\n".format(index))
	myData = []
	myDataZoom = []
	lineList = []
	cindex = 0
	myMax = []
	for dl in DataList:
		got_data = 0
		ymax = 0
		for dt in dl.ReconList[index]:
			if dt[1] > 5:
			   got_data = 1
			   break
		if got_data==1:
			myData.append(dl.ReconList[index])
			myDataZoom.append(dl.ReconList[index][spnt:epnt])
			lineList.append(["{}".format(dl.FileDef),ColorList[cindex],None])
			cindex += 1
			if cindex >= len(ColorList):
			   cindex = 0
	if len(myData) > 0:
		xmax = 0
		ymax = 0
		for i in myData:
			for k in i:
				if k[0] > xmax:
					xmax = k[0]
				if k[1] > ymax:
					ymax = k[1]
		xmax = 0.1 * int(10.5 * xmax)
		ymax = 10 * (int(0.1 * ymax) + 1)
		pdfgen.addBasicMultiPlot(xmax,ymax,myData,"Channel {} vs {}".format(index,index+1),lineList,xpnts=10,plotsize=300)
		xmax = 0
		ymax = 0
		xmin = 5000
		for i in myDataZoom:
			for k in i:
				if k[0] < xmin:
				   xmin = k[0]
				if k[0] > xmax:
					xmax = k[0]
				if k[1] > ymax:
					ymax = k[1]
		xmin = 0.1 * int(9.5 * xmin)
		xmax = 0.1 * int(10.5 * xmax)
		ymax = 10 * (int(0.11 * ymax) + 1)
		pdfgen.addBasicMultiPlot(xmax,ymax,myDataZoom,"Zoomed Channel {} vs {} ".format(index,index+1),lineList,xpnts=10,plotsize=300,xmin=xmin)
		pdfgen.addComponent([-2])

pdfgen.DoExit()

exit()
