# FEC_IO.py
#
# This file handles the interactions with the FEC board. Internal operations
# handlethe accessing of different registers within the x-RORC.
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@yahoo.com)


import math
import sys
import time
import string
import sys
import os
import array
import ctypes
import subprocess
import projpath as mpath
import errwarn as errwarn
import serial
import serial.tools.list_ports
from projbase import *
import supplyCtrl
import bpgbtx
import GBTx0cnfg as Cnfg0
import GBTx1cnfg as Cnfg1
import pwrtest
import glob
import waveGen
import pulseReConstruct
import FEC_DATA
import zebra
import FEC_email
import shutil
import commondb as cdb

ENABLE_GBTX1_PROGRAMMING = 1
DO_POWER_SEQUENCE = 0
DO_POWER_SEQUENCE_TEST = 0
FEC_REFERENCE_MODE = "SCA"
ENABLE_ELINK_TERMINATION = 1
# EMAIL_OPTION:
#  archive = Wait for MIN_FECS_TO_ARCHIVE FEC directories, then send them to Peter and Ken (FEC_DATA.receivers)
#  sendnow = Send data files immediately to Ken and Lloyd (testing mode)
#  otherwise = Do nothing
EMAIL_OPTION = "none"
version = "3.2 05/03/2019"
MIN_FECS_TO_ARCHIVE = 120
try:
    qrscan = zebra.ZebraDs2278.from_connect()
except:
    qrscan = 0

TMP_BARCODE_FILE = "/tmp/barcode_tmp.txt"

try:
    myCompID = os.environ["teststation"]
except:
    myCompID = "<re-login please>"

# import gbtxconfigIO as gbtxconfig
# mlist = gbtxconfig.ReadConfigFile("GBTX1_pc.txt")
# gbtxconfig.CompareRefDefConfigFile(Cnfg1.CONFIG_REGISTER_DEFINITIONS,mlist,sys.stdout)

def ExecuteCmdParse(cmd, add_cmd=0):
    mList = []
    if add_cmd == 1:
        mList.append("** ==========")
        mList.append("** ExecuteCmd {}".format(cmd))
        mList.append("** ==========")
        mList.append("")
    p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    for i in p.communicate():
        if i != None:
            if sys.version_info[0] > 2:
                text = i.decode("utf-8")
                lines = text.translate(strmaketrans("\r", " ")).split('\n')
            else:
                lines = i.translate(strmaketrans("\r", " ")).split('\n')
            for j in lines:
                if len(j) > 0:
                    mList.append(j)
                    #    sys.stdout.write("ExecuteCmdParse {}\n".format(mList))
    return mList


ERROR_MISSING_SAMPA = 0x1



def getPortInfo():
    return serial.tools.list_ports.comports()

# for i in getPortInfo():
#    sys.stdout.write("PortInfo = {}\n".format(i))
# checkForPort
#
# See if a Port exists in the system
# 1 = Success
# 0 = Not available
# 
def checkForPort(portname):
    for i in getPortInfo():
        name = "{}".format(i).split()
        if len(name) > 1:
            if portname == name[0]:
                return 1
    return 0


def ScanPowerSupplies():
    SupplyList = []
    m = getPortInfo()
    for i in m:
        words = ("{}".format(i)).split()
        Supply = supplyCtrl.Keysight3646A(words[0])
        if Supply.ValidDevice() == 1:
#            sys.stdout.write("Found Voltage Suppy:: {} as {}\n".format(Supply.myIDN, Supply.PortName))
            SupplyList.append([Supply, 0])
        else:
#            sys.stdout.write("Port {} is not PowerSupply\n".format(i))
            Supply.close()
    return SupplyList


# for i in getUSBInfo():
#    sys.stdout.write("USB={}\n".format(i))
Start_Time = time.time()

# BarCodeScanList = ["SAMPA0","SAMPA1","SAMPA2","SAMPA3","SAMPA4","ManufactID",]
BarCodeScanList = ["BoardID","ManufactID", "SAMPA0", "SAMPA1", "SAMPA2", "SAMPA3", "SAMPA4"]

TestStepInfo = [ ]
TestStepInfo.append([0,"WAITING","NONE","NONE"])

BarScanDetails = []
BarScanDetails.append("A. Get a board.")
BarScanDetails.append("B. Add FEC board ID BARCODE.")
BarScanDetails.append("C. Hit button to start SCANNING barcodes.")

PwrUpDetails = []
PwrUpDetails.append("A. Place board in the NOISE fixture.")
PwrUpDetails.append("B. Connect CERN Dongle to FEC board.")
PwrUpDetails.append("C. Change SWITCH positions from NORMAL to OFF.")
PwrUpDetails.append("D. Modifiy DIP switches to match FEC board ID.")
PwrUpDetails.append("E. Either enable output on PWR supply <or> click the EXECUTE button.")

AdjustFecDetails = []
AdjustFecDetails.append("FEC board ID does not match BARCDOE.")
AdjustFecDetails.append("Fix the FEC board ID!")

RemoveDongleDetails = []
RemoveDongleDetails.append("A. Remove CERN Dongle from FEC board")
RemoveDongleDetails.append("B. Change SWITCH positions from OFF to NORMAL")

SwitchToNormalDetails = []
SwitchToNormalDetails.append("Change SWITCH positions from OFF to NORMAL")

RunBasicTests = []
RunBasicTests.append("Run Basic Tests")

RunPulseTests = []
RunPulseTests.append("A. Move FEC board from NOISE fixture to PULSE fixture")
RunPulseTests.append("B. Connect FLEX cables to ERNIs in fixture")
RunPulseTests.append("C. Connect GND to board")

ArchiveDetails = []
ArchiveDetails.append("Once the FEC board is powered down:")
ArchiveDetails.append("A. Remove the FEC board from the PULSE fixture.")
ArchiveDetails.append("B. Put FEC board back into its antistatic bag")
ArchiveDetails.append("C. Place FEC board into appropriate box.")

ErrorInitDetails = []
ErrorInitDetails.append("Initialization failed.")
ErrorInitDetails.append("A. Make certain PWR connector is attached properly.")
ErrorInitDetails.append("B. Is Supply voltage and current in range.")
ErrorInitDetails.append("C. Make certain optical cables are connected.")
ErrorInitDetails.append("D. Make certain CERN dongle is connected.")
ErrorInitDetails.append("E. Make certain SWITCH positions are OFF.")

DeviceErrorDetails = []
DeviceErrorDetails.append("External Device Initialization failed.")
DeviceErrorDetails.append("See the internal log using ViewMode.")
DeviceErrorDetails.append("Restart this application when the problem is fixed.")
DeviceErrorDetails.append("Contact Support if the problem cannot be resolved.")

AchieveErrorDetails = []
AchieveErrorDetails.append("Archive Process failed.")
AchieveErrorDetails.append("Expected data archvie did not exists!")
AchieveErrorDetails.append("Either the operator attempt to advance to this state")
AchieveErrorDetails.append("before it existed or the filesystem has a problem.")
AchieveErrorDetails.append("Contact Support if the problem cannot be resolved.")

CernDongleDetails = []
CernDongleDetails.append("The CERN dongle had an error!")
CernDongleDetails.append("1. Make certain dongle is connected to the FEC board.")
CernDongleDetails.append("2. Make certain SWITCH positions are OFF.")
CernDongleDetails.append("")
CernDongleDetails.append("If case 1 and 2 have been checked, then")
CernDongleDetails.append("A. Unplug the DONGLE from the USB extension cable.")
CernDongleDetails.append("B. Wait 5-10 seconds")
CernDongleDetails.append("C. Plug the DONGLE back into the USB extension cable.")
CernDongleDetails.append("If problem persists, then contact support.")

FinalSwitchToNormalDetails = []
FinalSwitchToNormalDetails.append("Operator Failure. SWITCH position was not properly changed earlier.")
FinalSwitchToNormalDetails.append("Change SWITCH positions from OFF to NORMAL")

# Error States
TEST_STEP_INIT_ERROR = -1
TEST_STEP_DEVICE_ERROR = -2
TEST_STEP_ARCHIVE_ERROR = -3
TestStepInfo.append([TEST_STEP_INIT_ERROR,"ERROR-INIT","waiting",ErrorInitDetails])
TestStepInfo.append([TEST_STEP_DEVICE_ERROR,"DEVICE ERROR","waiting",DeviceErrorDetails])
TestStepInfo.append([TEST_STEP_ARCHIVE_ERROR,"ARHCIVE ERROR","waiting",AchieveErrorDetails])

# Normal States
TEST_STEP_BARCODE = 1
TEST_STEP_POWERUP = 2
TEST_STEP_MODIFYID = 3
TEST_STEP_WAITBASIC = 4
TEST_STEP_TAKE_DONGLE = 5
TEST_STEP_SWITCH_BACK = 6
TEST_STEP_BASIC_TESTS = 7
TEST_STEP_PULSE_TESTS = 8
TEST_STEP_ARCHIVE = 9
TEST_STEP_DONGLE_ISSUE = 10
TEST_STEP_FINAL_FAILED_SWITCH = 11
TestStepInfo.append([TEST_STEP_BARCODE,"Do BARSCAN","barscan",BarScanDetails,1])
TestStepInfo.append([TEST_STEP_POWERUP,"Do POWERUP", "pwrup", PwrUpDetails,1])
TestStepInfo.append([TEST_STEP_MODIFYID,"Modify ID","boardid",AdjustFecDetails,0])
TestStepInfo.append([TEST_STEP_WAITBASIC,"ID Tests","waitbasic",RemoveDongleDetails,1])
TestStepInfo.append([TEST_STEP_TAKE_DONGLE,"Take Dongle","testdongle",RemoveDongleDetails,0])
TestStepInfo.append([TEST_STEP_SWITCH_BACK,"SWITCH BACK","testscaswitch",SwitchToNormalDetails,0])
TestStepInfo.append([TEST_STEP_BASIC_TESTS,"Basic Tests","basictest",RunBasicTests,1])
TestStepInfo.append([TEST_STEP_PULSE_TESTS,"Pulse Tests","pulsetest",RunPulseTests,1])
TestStepInfo.append([TEST_STEP_ARCHIVE,"!FINISH!","archive",ArchiveDetails,1])
TestStepInfo.append([TEST_STEP_DONGLE_ISSUE,"Dongle Issue","fuse",CernDongleDetails,0])
TestStepInfo.append([TEST_STEP_FINAL_FAILED_SWITCH,"SWITCH BACK","failedswitch",FinalSwitchToNormalDetails,0])
ATTR_INDEX = 0
ATTR_NAME = 1
ATTR_EXESTATE = 2
ATTR_DESCLIST = 3
ATTR_STEPTO = 4

#START_TEST_POINT = TEST_STEP_POWERUP
START_TEST_POINT = TEST_STEP_BARCODE

DEVICE_CODE_PWR = 0x1
DEVICE_CODE_WAVE = 0x2
DEVICE_CODE_DONGLE = 0x4
DEVICE_CODE_ZEBRA = 0x8


TRORC_CONFIG_DIR = "/home/daquser/trorc/dist/trorc-utils/config/"
class FEC:
    def __init__(self, winmain, opticalChannel=1):
        self.winmain = winmain
        self.CmdExecuteList = []
        self.FEC_mask = opticalChannel
        self.FEC_binnum = 2 * (opticalChannel - 1)
        self.PowerSupplyName = 0
        self.Supply = 0
        self.specify_tfec_flags = "--v2"
        self.scan_ready = 0
        self.MaxState = 0
        self.device_code = 0
        for i in TestStepInfo:
            if i[ATTR_INDEX] > self.MaxState:
                self.MaxState = i[ATTR_INDEX]
        self.Clear()
        self.ReadSetup(opticalChannel)
        #      self.DoInitProc()

    def ClearBarCodeScan(self):
        self.scannedCodes = []
        self.ScanIndex = 0
        for i in BarCodeScanList:
            self.scannedCodes.append(0)

    def Clear(self):
        self.num_BCs = len(BarCodeScanList)
        self.pwrState = -100
        self.InitLog = []
        self.sampa_active = 0
        self.SupplyInfo = []
        self.ScaID = -1
        self.InitErrors = 0
        self.start_time = time.time()
        self.ClearBarCodeScan()
        self.fecData = FEC_DATA.FEC_data()
        self.fecData.setLog(self.InitLog)
        self.pwrState = -2
        self.problem_num = 1
        self.fuseCnt = 0
        self.see_pdf = 1
        self.fid = -1
        self.TestMode = "waiting"
        self.TestStep = START_TEST_POINT
        self.WatchTime = time.time()
        self.WatchEnable = 0
        self.NewMode = 1
        self.boardID = 0
        self.CmdExecuteList.append("updatelog")
        self.fuseCode = -1

    def ReadSetup(self, opticalChannel):
        filename = "/home/daquser/FecTesterSetup/Station{}".format(opticalChannel)
        self.TestStep = TEST_STEP_BARCODE
        if self.ReadBarCodes()==1:
            self.TestStep = TEST_STEP_POWERUP

        self.device_code = 0
        if qrscan!=0:
            self.device_code |= DEVICE_CODE_ZEBRA
            self.addToLog("Zebra Scanner cannot be found!", doprint=1, color="#ff4040")
            self.addToLog("A. Check USB connections between USB hub and Scanner.", doprint=1, color="#ff4040")
            self.addToLog("NOTE: Scanner needs to charge 10 minutes before it will work properly.", doprint=1, color="#ff4040")
            self.addToLog("B. Contact Support Immediately.", doprint=1, color="#ff4040")
        f = open(filename, 'r')
        for line in f:
            words = line.split()
            if len(words) > 0:
                if words[0]=="supply":
                    self.Supply = supplyCtrl.Keysight3646A(words[1])
                    code = self.Supply.ValidDevice()
                    if code == 1:
                        self.addToLog("Found Voltage Suppy:: {} as {}".format(self.Supply.myIDN, self.Supply.PortName))
                        self.Supply.Off()
                        self.device_code |= DEVICE_CODE_PWR
                    else:
                        self.expected_device_error = 1
                        if code == -1:
                            self.addToLog("Power Supply cannot be found!",doprint=1,color="#ff4040")
                            self.addToLog("A. Check USB connections between USB hub and Power Supply.",doprint=1,color="#ff4040")
                            self.addToLog("B. Contact Support Immediately.",doprint=1,color="#ff4040")
                        else:
                            self.addToLog("A foreign device is on the Power Supply USB port!",doprint=1,color="#ff4040")
                            self.addToLog("A. Remove unknown USBs from the computer and USB hub.",doprint=1,color="#ff4040")
                            self.addToLog("B.Contact Support Immediately.",doprint=1,color="#ff4040")
                        self.addToLog("Internal NOTE: Port={}".format(words[1]), doprint=1, color="#8080ff")
                        self.Supply.close()
                if words[0] == "wavegen":
                    self.wavegen = waveGen.Keysight33622A()
                    if self.wavegen.valid==1:
                        self.device_code |= DEVICE_CODE_WAVE
                    else:
                        self.addToLog("Excepted waveform generator cannot be found.".format(words[1]), doprint=1, color="#ff4040")
                        self.addToLog(self.wavegen.Error, doprint=1, color="#ff4040")
                        self.addToLog("A. Check USB connections between USB hub and Waveform Generator.", doprint=1, color="#ff4040")
                        self.addToLog("B. Contact Support Immediately.", doprint=1, color="#ff4040")
        f.close()
        if self.device_code != DEVICE_CODE_WAVE | DEVICE_CODE_PWR | DEVICE_CODE_ZEBRA:
            self.TestStep = TEST_STEP_DEVICE_ERROR

    def getSupplyInfo(self, measurename, add_log=1, add_list=0):
        if self.Supply == 0:
            self.addToLog("Supply Undefined",color="#ff4040")
            return
        vs1, is1 = self.Supply.getInfo(0)
        vs2, is2 = self.Supply.getInfo(1)
        if add_list == 1:
            self.SupplyInfo.append([measurename, vs1, is1, vs2, is2])
        # if add_log == 1:
        #     self.addToLog("measurement {} V1={} I1={} V2={} I2={}".format(measurename, vs1, is1, vs2, is2))

    def BadDevice(self,devMask):
        self.device_code &= ~devMask
        self.TestStep = TEST_STEP_DEVICE_ERROR
        self.setNewMode("waiting", setNewMode=1)

    def PowerSupplyOn(self):
        if self.Supply == 0:
            return -1

        if self.Supply.ValidDevice() == 1:
            self.Supply.On()
            self.Supply.Channel[0].setV(supplyCtrl.V2r3, supplyCtrl.CURRENT_LIMIT_2r3)
            self.Supply.Channel[1].setV(supplyCtrl.V3r3, supplyCtrl.CURRENT_LIMIT_3r3)
            return 1
        else:
            self.BadDevice(DEVICE_CODE_PWR)
            return 0

    # def CheckPorts(self):
    #    m = getPortInfo()
    #    self.BusPirateDevName = 0
    #    for i in m:
    #       words = i.split()
    #       check_next_device = 1
    #       if self.Supply==0 and check_next_device == 1:
    #          self.Supply = supplyCtrl.Keysight3646A(words[0])
    #          if self.Supply.ValidDevice()==1:
    #             self.addToLog("Found Voltage Suppy:: {} as {}\n".format(self.Supply.myIDN,self.Suppy.PortName))
    #             check_next_device = 0
    #          else:
    #             self.Supply.close()
    #             self.Supply=0
    #
    #       if self.Supply==0 and check_next_device == 1:
    #          self.Supply = supplyCtrl.Keysight3646A(words[0])
    #          if self.Supply.ValidDevice()==1:
    #             self.addToLog("Found Voltage Suppy:: {} as {}\n".format(self.Supply.myIDN,self.Suppy.PortName))
    #             check_next_device = 0
    #          else:
    #             self.Supply.close()
    #             self.Supply=0
    #
    #
    def checkFastInit(self):
        self.InitErrors = 0
        self.InitLog = []
        self.getSupplyInfo("Init")
        self.SAMPA_Scan()
        if self.ScaID == -1:
            return -1
        self.setCapturePath()
        self.getSupplyInfo("PRESENT", add_log=0, add_list=1)

    def IsBoardPowered(self):
        if self.Supply == 0:
            return -1
        vs1, is1 = self.Supply.getInfo(0)
        vs2, is2 = self.Supply.getInfo(1)
        if vs1 > 1 and vs2 > 1:
            return 1
        return 0

    def getGBTX0Cnfg(self):
        if ENABLE_ELINK_TERMINATION == 1:
            return "{}/fec-gbtx0-cfg-wb-wd-to-et.txt".format(TRORC_CONFIG_DIR)
        else:
            return "{}/fec-gbtx0-cfg-wb-wd-to.txt".format(TRORC_CONFIG_DIR)

    def DongleCheck(self):
        try:
            mlist = ExecuteCmdParse("gbt_usb_dongle.py --i2c-scan")
            if mlist[0] == "No GBTx detected":
                return 0
            else:
                return 1
        except:
            return -1

    def Fuse(self):
        self.setModeCond("check")
        code = self.DongleCheck()
        if code!=1:
#            sys.stdout.write("Fusing Exiting\n")
            return code

        # mlist = ExecuteCmdParse("gbt_usb_dongle.py --read 365")
        # if mlist[0]=="0xaa":
        #     ExecuteCmdParse("gbt_usb_dongle.py --dump-cfg /tmp/gbt-dump")
        #     return 2
        fname = self.getGBTX0Cnfg()
        self.setModeCond("burn")
        try:
            ExecuteCmdParse("gbt_usb_dongle.py --fuse-cfg {} ".format(fname))
        except Exception as e:
            self.addToLog("Fuse program failed. Exception =>", color="#ffff00")
            self.addToLog(" => {}".format(e), color="#888800")
        #        ExecuteCmdParse("gbt_usb_dongle.py --write-cfg {} ".format(fname))
        return 1
    def TestScaGbtxSwitch(self):
        code = ExecuteCmdParse(
            "tfec --mask {} --gbtx-scan".format(self.FEC_mask))
        cmpwords = "Found GBTx0 [I2C addr 15, SCA CH3]".split()
        for i in code:
#            sys.stdout.write("tfec <= {}\n".format(i))
            if i.split()==cmpwords:
                return 1
        return 0

    def SetupGBTX0(self):
        cfg_file = self.getGBTX0Cnfg()
        if ENABLE_ELINK_TERMINATION == 1:
            cfg_file = "{}fec-gbtx0-cfg-wb-wd-to-et.txt".format(TRORC_CONFIG_DIR)
        else:
            cfg_file = "{}/fec-gbtx0-cfg-wb-wd-to.txt".format(TRORC_CONFIG_DIR)
        self.program_GBTx0(cfg_file)

    def SetupGBTX1(self):
        if ENABLE_ELINK_TERMINATION == 1:
            cfg_file = "{}/fec-gbtx1-cfg-wb-wd-to-et.txt".format(TRORC_CONFIG_DIR)
        else:
            cfg_file = "{}/fec-gbtx1-cfg-wb-wd-to.txt".format(TRORC_CONFIG_DIR)

        self.ExecuteCmdParse(
            "tfec -v --mask {} --gbtx1-cfg {}".format(self.FEC_mask, cfg_file))


    def setCapturePath(self):
        if self.boardID > 0:
            path = mpath.makePath(FEC_DATA.DATAPATH, self.getFecReference())
        else:
            path = mpath.makePath(FEC_DATA.DATAPATH, "junk")
        self.capturePath = path

    def DoInitProc(self):
        self.InitErrors = 0
        self.InitLog = []
        self.SetupGBTX1()
        self.SAMPA_Scan()
        if self.sampa_active != 0:
            self.setCapturePath()
            self.CmdExecuteList.append("initdone")
            self.addToLog("DoInitProc Step 0F: Time={:.2f}".format(time.time() - Start_Time))
            return
        self.addToLog("DoInitProc Step 1: Time={:.2f}".format(time.time() - Start_Time))
        self.ExecuteCmdParse("trorc_status_dump")
        # Get the code version!!!
        self.ExecuteCmdParse("tfec --version")
        if self.sampa_active == 0:
            self.InitializeTRORC()
        self.FEC_Setup()
        #      time.sleep(1)
        #      self.ExecuteCmdParse("trorc_fec_readout_control -P 0x1f -T 0x3 -p -t -S")
        #      time.sleep(5)
#        sys.stdout.write("")
        self.SAMPA_Scan()
        if self.sampa_active != 0:
            self.SAMPA_Setup(self.sampa_active)
        self.resetSAMPA()
        self.SetupGBTX1()
        #      if self.sampa_active == 0:
        #         return
        self.setCapturePath()
        self.addToLog("DoInitProc Step 2: Time={:.2f}".format(time.time() - Start_Time))

        LogName = "{}/pyTFEC_init_{}.log".format(self.capturePath, time.strftime('%Y%b%d_%H%M%S'))
        self.addToLog("Log File = {}".format(LogName))
        #      sys.stdout.write("LogName={}\n".format(LogName))
        #      for j in self.InitLog:
        #          sys.stdout.write("{}\n".format(j))
        #      return
        Init_Log_file = open(LogName, 'w')
        for j in self.InitLog:
            Init_Log_file.write("{}\n".format(j[0]))
        Init_Log_file.close()
        self.CmdExecuteList.append("initdone")
        self.readPowerFile(self.capturePath)
        self.getSupplyInfo("PRESENT", add_log=0, add_list=1)
        Prs = pwrtest.findPowerMeasure(self.SupplyInfo, "PRESENT")
        if Prs != 0:
            mList = pwrtest.TestPresentPwr(Prs)
            for i in mList:
                self.addToLog(i[1], color=i[0])
        self.addToLog("Initialization time {:.2f}s".format(time.time() - self.start_time), color="#0000ff")
        self.addToLog("DoInitProc Step 3F: Time={:.2f}".format(time.time() - Start_Time))

    def getFecReference(self):
        if FEC_REFERENCE_MODE == "SCA" and self.ScaID >= 0:
            return "fid{:0>6}".format(hex(self.ScaID)[2:])
        else:
            return "fec{:0>3}".format(hex(self.BoardID)[2:])

    def getFecName(self):
        if FEC_REFERENCE_MODE == "SCA" and self.ScaID >= 0:
            return "{:0>6}".format(hex(self.ScaID)[2:])
        else:
            return "{:0>3}".format(hex(self.BoardID)[2:])

    def ExecuteCmdParse(self, cmd):
        code = ExecuteCmdParse(cmd, add_cmd=1)
        for i in code:
            self.addToLog(i)
        self.addToLog("")
        return code

    def resetSAMPA(self):
        return
        # cmd = "treadout -S --mask 0x1"
        sys.stdout.write("Start readout_control\n")
        time.sleep(1)
        self.ExecuteCmdParse("trorc_fec_readout_control -P 0xff -T 0x3 -p -t -S")
        time.sleep(1)
        sys.stdout.write("Done readout_control\n")

    def program_GBTx0(self, filename, do_update=0):
        value = self.BusPirateDev.readRegister(365)
        if do_update == 0:  # Check to see if we need to do this!
            if value == 0xcc:
                self.addToLog("GBTx0 has already been programmed")
                return
        if value == 0xcc:
            value = self.BusPirateDev.writeRegister(365, 0)
        self.addToLog("Programming GBTx0 with ".format(filename))
        self.BusPirateDev.writeConfig(filename)

    def POWER_Scan_test_variation(self):
        self.SupplyInfo = []
        num_pnts = 200
        for i in range(0, num_pnts):
            self.addToLog("Taking current measurement {}".format(i))
            self.getSupplyInfo("T{}".format(i), add_list=1, add_log=0)
        src1 = [0, 0]
        src2 = [0, 0]
        for S in self.SupplyInfo:
            val1 = S[2]
            val2 = S[4]
            src1[0] += val1
            src1[1] += val1 * val1
            src2[0] += val2
            src2[1] += val2 * val2
        src1[0] /= num_pnts
        self.addToLog(
            "SRC1 2.25V avg={} derv={}\n".format(src1[0], math.sqrt(src1[1] / num_pnts - src1[0] * src1[0])))
        src2[0] /= num_pnts
        self.addToLog(
            "SRC2 3.25V avg={} derv={}\n".format(src2[0], math.sqrt(src2[1] / num_pnts - src2[0] * src2[0])))

    def SavePwrInfo(self,setPath):
        filename = "pwrInfo_fid{}.txt".format(self.fecData.getFID().lower())
        pwrfilename = "{}/{}".format(setPath, filename)
        sys.stdout.write("PwrInfo = {}\n".format(pwrfilename))
        pwrFile = open(pwrfilename, 'w')
        pwrFile.write("# {}\n".format(pwrfilename))
        pwrFile.write("time {}\n".format(time.strftime('%Y%b%d_%H%M%S')))
#        pwrFile.write("sampa_2r2_current {} tol={},{}\n".format(SAMPA_2V2_CR, *SAMPA_2V2_CR_TOL))
#        pwrFile.write("sampa_3r2_current {} tol={},{}\n".format(SAMPA_3V2_CR, *SAMPA_3V2_CR_TOL))
#        pwrFile.write("fec_2r2_current {} tol={},{}\n".format(FEC_2V2_CR, *FEC_2V2_CR_TOL))
#        pwrFile.write("fec_3r2_current {} tol={},{}\n".format(FEC_3V2_CR, *FEC_3V2_CR_TOL))
        for i in self.SupplyInfo:
            pwrFile.write("pwrtest {} SRC1 {} {} SRC2 {} {}\n".format(i[0], i[1], i[2], i[3], i[4]))
        pwrFile.close()
        return filename

    def POWER_Scan(self, setPath):
        self.SupplyInfo = []
        self.setModeCond("PWR-1/8")
        self.getSupplyInfo("INIT", add_list=1)
        self.ExecuteCmdParse("tsampa -v --mask {} --sampa-mask 0x1f --pwr-off".format(self.FEC_mask))
        self.getSupplyInfo("OFF", add_list=1)
        self.setModeCond("PWR-2/8")
        WAIT_TIME = 0.25
        time.sleep(WAIT_TIME)
        for i in range(0, 5):
            self.setModeCond("PWR-{}/8".format(i+3))
            self.addToLog("Testing SAMPA #{}".format(i))
            self.ExecuteCmdParse("tsampa -v --mask {} --sampa-mask {} --pwr-on".format(self.FEC_mask, 1 << i))
            time.sleep(WAIT_TIME)
            self.getSupplyInfo("ON {}".format(i), add_list=1)
            self.ExecuteCmdParse("tsampa -v --mask {} --sampa-mask 0x1f --pwr-off".format(self.FEC_mask))
            time.sleep(WAIT_TIME)
        self.ExecuteCmdParse("tsampa -v --mask {} --sampa-mask 0x1f --pwr-on".format(self.FEC_mask))
        self.getSupplyInfo("ON ALL", add_list=1)
        self.FEC_Setup()
        self.SAMPA_Scan()
        if self.sampa_active != 0:
            self.SAMPA_Setup(self.sampa_active)
        self.getSupplyInfo("ON NORMAL", add_list=1)
        filename = self.SavePwrInfo(setPath)
        mList = pwrtest.testPower(self.SupplyInfo)
        for i in mList:
            self.addToLog(i[1], color=i[0])
        return filename

    def GetBoardID(self):
        code = self.ExecuteCmdParse("tsampa -v --mask {} --scan".format(self.FEC_mask))
        self.sampa_active = 0
        self.boardID = -1
        self.ScaID = -1
        for i in code:
            words = i.split()
            if len(words) > 1:
                cmd = words[0].upper()
                if cmd == "FOUND":
                    self.sampa_active |= 1 << (int(words[1][5]))
                if words[0] == "FEC" and len(words)==3:
                    bID = int(words[1][1:])
                    if self.boardID==-1:
                        self.boardID = bID
                        self.CmdExecuteList.append("boardid")
                    elif bID!= self.boardID:
                        self.addToLog("Got BoardID conflict {} vs {}".format(bID,self.boardID), color="#00ff00")

                if words[0] == "parameter" and words[1] == "ScaID":
                    val = int(words[2][2:], 16)
                    if val > 0:
                        self.ScaID = val 
                    self.fid =  "{:0>6}".format(hex(self.ScaID)[2:]).upper()
                    self.CmdExecuteList.append("scaid")

        for i in range(0, 5):
            if (self.sampa_active & (1 << i)) != 0:
                self.addToLog("SAMPA{} is ACTIVE".format(i), color="#00ff00")
            else:
                self.addToLog("SAMPA{} is MISSING".format(i), color="#ff0000")
                self.InitErrors |= ERROR_MISSING_SAMPA
                #      sys.stdout.write("BoardID={}\n".format(hex(self.boardID)))
    def SAMPA_Scan(self, pwr_on=1):
        self.ExecuteCmdParse("tsampa -v --mask {} --sampa-mask 0x1f --pwr-on".format(self.FEC_mask))
        time.sleep(0.025)
        code = self.ExecuteCmdParse("tsampa -v --mask {} --scan".format(self.FEC_mask))
        self.sampa_active = 0
        self.boardID = -1
        self.ScaID = -1
        for i in code:
            words = i.split()
            if len(words) > 1:
                cmd = words[0].upper()
                if cmd == "FOUND":
                    self.sampa_active |= 1 << (int(words[1][5]))
                if words[0] == "FEC" and len(words)==3:
                    bID = int(words[1][1:])
                    if self.boardID==-1:
                        self.boardID = bID
                        self.CmdExecuteList.append("boardid")
                    elif bID!= self.boardID:
                        self.addToLog("Got BoardID conflict {} vs {}".format(bID,self.boardID), color="#00ff00")

                if words[0] == "parameter" and words[1] == "ScaID":
                    val = int(words[2][2:], 16)
                    if val > 0:
                        self.ScaID = val
                    self.fid =  "{:0>6}".format(hex(self.ScaID)[2:]).upper()
                    self.CmdExecuteList.append("scaid")

        for i in range(0, 5):
            if (self.sampa_active & (1 << i)) != 0:
                self.addToLog("SAMPA{} is ACTIVE".format(i), color="#00ff00")
            else:
                self.addToLog("SAMPA{} is MISSING".format(i), color="#ff0000")
                self.InitErrors |= ERROR_MISSING_SAMPA
                #      sys.stdout.write("BoardID={}\n".format(hex(self.boardID)))

    def InitializeTRORC(self):
        self.ExecuteCmdParse("trorc_init")
        self.getSupplyInfo("TRORC-Init", add_log=1, add_list=0)

    def SAMPA_setGain(self, gain):
        cg0 = 0
        cg1 = 0
        desc = "3mV/fC"
        if gain == 1:
            cg1 = 1
            desc = "20mV/fC"
        elif gain == 2:
            cg0 = 1
            desc = "30mV/fC"
        msg = self.ExecuteCmdParse("tfec -v --mask {} --cg0 {} --cg1 {}".format(self.FEC_mask, cg0, cg1))
#        sys.stdout.write("setGain={}\n".format(desc))

    #      sys.stdout.write("Response{}\n".format(msg))

    def SAMPA_setShaping(self, shaping):
        self.ExecuteCmdParse("tfec --mask {} --cts {}".format(self.FEC_mask, shaping))

    def SAMPA_setPolarity(self, polarity):
        self.ExecuteCmdParse("tfec --mask {} --pol {}".format(self.FEC_mask, polarity))

    def FEC_Setup(self):
        self.ExecuteCmdParse(
            "tsca -v --mask {} --hdlc-tx-bitswap 1 --hdlc-rx-bitswap 1 --hdlc-tx-mux 1".format(self.FEC_mask))
        self.ExecuteCmdParse(
            "tfec -v --mask {} --init -s --cg0 0 --cg1 1 --cts 0 {}".format(self.FEC_mask, self.specify_tfec_flags))
        self.SetupGBTX1()

    #      self.ExecuteCmdParse("trorc_fec_readout_control -P 0xff -T 0x3 -p -t -S")
    #      time.sleep(1.5)

    def SAMPA_Power(self, sampa_mask):
        self.ExecuteCmdParse("tsampa -v --mask {} --sampa-mask {} --pwr-on".format(self.FEC_mask, sampa_mask))
        self.ExecuteCmdParse("trorc_reset")

    def SAMPA_Setup(self, sampa_mask):
        self.ExecuteCmdParse("tsampa -v --mask {} --sampa-mask {} --elinks 11".format(self.FEC_mask, hex(sampa_mask)))

    def readPowerFile(self,path):
        self.SupplyInfo = pwrtest.readPowerFile(path, self.ScaID)
        if len(self.SupplyInfo) == 0:
            return
        mList = pwrtest.testPower(self.SupplyInfo)
        if mList == 0:
            return
        for i in mList:
            self.addToLog(i[1], color=i[0])

# ========================================================================================================================
    def GetFecIDFromBarCode(self):
#        sys.stdout.write("ScannedCodes = {}\n".format(self.scannedCodes))
        if self.scannedCodes[0]==0:
            return -1
        code = self.scannedCodes[0].split('-')
        return int(code[1])

    def addBarCode(self,btype,bval):
        index = 0
        for i in BarCodeScanList:
            if btype==i.lower():
                self.scannedCodes[index] = bval
                return
            index += 1

    def DeleteTmpBarCodeFile(self):
        if os.path.isfile(TMP_BARCODE_FILE) == False:
            return 0
        ExecuteCmdParse("rm {}".format(TMP_BARCODE_FILE))

    def ReadBarCodes(self,filename=0):
        if filename==0:
            if os.path.isfile(TMP_BARCODE_FILE)==False:
                return 0
            filename = TMP_BARCODE_FILE
        else:
            if os.path.isfile(filename) == False:
                return 0
        f = open(filename, 'r')
        for line in f:
            words = line.split()
            cmd  = words[0].lower()
            if cmd == "barcode":
                bval = ""
                for i in range(2,len(words)):
                    if i!=2:
                        bval += " "
                    bval += words[i]
                self.addBarCode(words[1].lower(),bval)
        f.close()
        return 1

    def SaveBarCodes(self,do_tmp=0):
        if self.fid==-1:
            do_tmp = 1
        if do_tmp==0:
            path = "{}/fid{}".format(FEC_DATA.DATAPATH,self.fid.lower())
            if not os.path.exists(path): os.makedirs(path)
            path += "/Summary"
            if not os.path.exists(path): os.makedirs(path)
            fname = "{}/barcode_fid{}.txt".format(path, self.fid.lower())
            if os.path.exists(fname): return
        else:
            fname = TMP_BARCODE_FILE
        f = open(fname, 'w')
        try:
            f.write("BoardID {}\n".format(self.boardID))
            if do_tmp==0:
               f.write("ScaID 0x{}\n".format(self.fid.lower()))
        except:
            pass
        for index in range(0,self.num_BCs):
            if self.scannedCodes[index] != 0:
                f.write("barcode {} {}\n".format(BarCodeScanList[index],self.scannedCodes[index]))
        f.close()
        # if do_tmp==0:
        #     fecname = self.getFecReference()
        #     SummaryPath = mpath.makePath(FEC_DATA.AUTOSUMPATH, fecname)
        #     BasicCheckSummaryFile(SummaryPath, fname)

    def getTestStepInfo(self):
        for i in TestStepInfo:
            if i[ATTR_INDEX] == self.TestStep:
                return i
        return 0

    def nextStep(self):
        code = 0
        TestStepDef = self.getTestStepInfo()
        if self.TestMode in ["barscan"]:
            if TestStepDef[ATTR_EXESTATE]==self.TestMode:
                code = 1
                self.setNewMode("waitpwr")
        elif self.TestMode in ["waiting","waitpwr"]:
            self.setNewMode(TestStepDef[ATTR_EXESTATE])
            code = 1
        if self.WatchEnable == 0:
            self.WatchTime = time.time()
            self.WatchEnable = 1

        #
        # if code==1:
        #     if self.TestStep > 0:
        #         self.TestStep += 1
        #     if self.TestStep >= self.MaxState:
        #         self.TestStep = 1
        #         self.Clear()
        return code



    def ScanSetup(self):
        self.scan_ready = 0
        if qrscan == 0:
            self.addToLog("Zebra Scanner cannot be found!", doprint=1, color="#ff4040")
            self.addToLog("Check USB connections between USB hub and Scanner.", doprint=1, color="#ff4040")
            self.addToLog("Scanner needs to charge 10 minutes before it will work properly.", doprint=1, color="#ff4040")
            return
        try:
            qrscan.setDebug(0)
            qrscan.beeper('LOW', 'LOW')
            qrscan.aim(True)
            qrscan.triggerMode('LEVEL')
            qrscan.beep()
            qrscan.picklistMode('ON')
            self.scan_ready = 1
        except Exception as e:
            pass

    def addToLog(self,msg,color="#ffffff",doprint=0):
        self.InitLog.append([msg,color])
#        if doprint==1:
#            sys.stdout.write("{}\n".format(msg))

    def setModeCond(self,cond):
        self.CmdExecuteList.append("setmode {}-{}".format(self.TestMode,cond))

    def setNewMode(self,newMode,setNewMode=0):
        self.TestMode = newMode
        self.NewMode = setNewMode
        self.ScanIndex = 0
#        sys.stdout.write("create setmode {}\n".format(newMode))
        self.CmdExecuteList.append("setmode {}".format(newMode))

#    def setModeDone(self,DoneMark):
#        self.CmdExecuteList.append("setmode {}-{}%".format(self.TestMode,DoneMark))
    def findBarCodeIndex(self,name):
        index = 0
        for i in BarCodeScanList:
            if i == name:
                return index
            index += 1
        return -1
    def loadBarCodes(self):
        for i in self.fecData.barCodes:
            index = self.findBarCodeIndex(i[0])
            if index!=-1:
                self.scannedCodes[index] = i[1]

    def DoScan(self):
        try:
            if self.scan_ready == 0:
                self.ScanSetup()
            else:
                data = qrscan.getCode()
                found = 0
                if data not in self.scannedCodes and self.ScanIndex < self.num_BCs:
                    added = 0
                    codekey = BarCodeScanList[self.ScanIndex][0:5]
                    datakey = data[0:5]
                    mlen = len(data)
                    self.addToLog("Scan BarCode <= {}".format(data))
                    if codekey == "SAMPA" or datakey == "SAMPA":
                        if codekey == datakey:
                            self.addToLog("Scanned " + BarCodeScanList[self.ScanIndex])
                            self.scannedCodes[self.ScanIndex] = data
                            self.ScanIndex += 1
                            found=1
                        else:
                            if codekey == "SAMPA":
                                self.addToLog("Invalid SAMPA barcode detected")
                            else:
                                self.addToLog("SAMPA was scanned instead of {}".format(
                                    BarCodeScanList[self.ScanIndex]))
                    else:
                        elen = 0
                        if codekey == "Manuf":
                            elen = 29
                        elif codekey == "Board":
                            elen = 11

                        if len(data) == elen:
                            self.addToLog("Scanned " + BarCodeScanList[self.ScanIndex])
                            self.scannedCodes[self.ScanIndex] = data
                            self.ScanIndex += 1
                            found=1
                        else:
                            self.addToLog("Detected bad {} (len={}, Expected={})".format(
                                BarCodeScanList[self.ScanIndex], mlen, elen))
                else:
                    if data in self.scannedCodes:
                        self.addToLog("Barcode already scanned")
                    if self.ScanIndex >= self.num_BCs:
                        self.addToLog("Finished Scanning!")
                if found==0:
                    self.addToLog("Scan BarCode <= {}".format(data))

                qrscan.aim(True)

        except Exception as e:
            pass


    def DoWork(self,Params):
        sleep_time = 0.2
        while 1:
            try:
                self.IterativeSequence()
            except Exception as e:
                self.addToLog("DO-WORK: Got Exception {}".format(e), doprint=1)

            time.sleep(sleep_time)
        #                if self.see_pdf==1:
        #                    ExecuteCmdParse("evince {}".format(filename))

    def IterativeSequence(self):
        if self.TestMode == "barscan":
            self.DoScan()
        elif self.TestMode == "waiting":
            pass
        elif self.TestMode == "waitpwr":
            pwrState = self.IsBoardPowered()
            if pwrState != self.pwrState:
                if pwrState==-1:
                    self.addToLog("No Power Supply", "PYFEC_STATUS_ERROR")
                elif pwrState==1:
                    self.setNewMode("fuse")
                    self.addToLog("FEC board is powered")
                self.pwrState = pwrState

        # elif self.TestMode == "pwrtest":
        #     pwrState = self.IsBoardPowered()
        #     if pwrState == -1:
        #         self.addToLog("No Power Supply", "PYFEC_STATUS_ERROR")
        #     elif pwrState == 0:
        #         self.addToLog("Waiting for Power Supply Turn-On", "PYFEC_STATUS_ATTN")
        #     elif pwrState == 1:
        #         self.setNewMode("fuse")
        #         self.addToLog("FEC board is powered")
        #     self.pwrState = pwrState

        elif self.TestMode == "pwrup":
            self.fuseCnt = 0
            self.itrcnt = 0
            code = self.PowerSupplyOn()
            if code != 1:
                self.addToLog("Problem {} Detected:".format(self.problem_num), color="#ff4040")
                self.addToLog("Power Supply connection was not established", color="#ff4040")
                self.addToLog("Make certain the power supply RS232 port is connected", color="#ff4040")
                self.addToLog("See Log: Problem {} Detected".format(self.problem_num), "PYFEC_STATUS_ATTN")
                self.setNewMode("waiting",setNewMode=1)
            else:
                self.setNewMode("waitpwr")

        # elif self.TestMode == "testfuse":
        #     self.fecIntf.SAMPA_Scan()
        #     if self.fecIntf.sampa_active > 0:
        #         self.addToLog("FEC has been previously fused")
        #         self.setNewMode("init")
        #     else:
        #         self.setNewMode("fuse")

        elif self.TestMode == "fuse":
            self.fuseCode = self.Fuse()
            self.itrcnt = 0
            if self.fuseCode == -1:
                self.addToLog("CERN Dongle could not be detected!",color="#ff4040")
                self.TestStep = TEST_STEP_DONGLE_ISSUE
                self.setNewMode("waiting", setNewMode=1)
            else:
                if self.fuseCode == 0:
                    self.addToLog("CERN Dongle could not detect FEC board!",color="#ff4040")
                    self.addToLog("1. CERN Dongle is not connected properly", color="#ff4040")
                    self.addToLog("2. SCA switch is not set properly", color="#ff4040")
                    self.addToLog("This procedure will continue and see if the board is already fused", color="#ff4040")
                self.setNewMode("init")

        elif self.TestMode == "init":
            self.DoInitProc()
            self.itrcnt += 1
            if self.sampa_active > 0:
                self.SaveBarCodes()
                self.CmdExecuteList.append("getfecsummary")
                if self.boardID == self.GetFecIDFromBarCode():
                    self.TestStep = TEST_STEP_WAITBASIC
                else:
                    self.TestStep = TEST_STEP_MODIFYID
                self.setNewMode("waiting", setNewMode=1)
            elif self.itrcnt > 5:
                self.TestStep = TEST_STEP_INIT_ERROR
                self.setNewMode("error",setNewMode=1)
            else:
                if self.itrcnt==3:
                    self.addToLog("Attemptting board power cycle to recover!",color="#4040ff")
                    self.Supply.Off()
                    time.sleep(2000)
                    self.Supply.On()
                self.setModeCond("{}".format(self.itrcnt))

        elif self.TestMode in ["boardid","waitbasic"]:
#                self.setNewMode("waiting",setNewMode=1)
            mybID = self.GetFecIDFromBarCode()
            if mybID==-1:
                self.setNewMode("testdongle")
            else:
                if self.boardID != mybID:
                    self.GetBoardID()
                if self.boardID == mybID:
                    self.setNewMode("testdongle")
                else:
                    self.TestStep = TEST_STEP_MODIFYID
                    self.setNewMode("waiting", setNewMode=1)

        elif self.TestMode == "testdongle":
            dongleCode = self.DongleCheck()
            if dongleCode == 1:
                self.TestStep = TEST_STEP_TAKE_DONGLE
                self.setNewMode("waiting",setNewMode=1)
            else:
                self.setNewMode("testscaswitch")

        elif self.TestMode == "testscaswitch":
            switchCode = self.TestScaGbtxSwitch()
            if switchCode==1:
                self.TestStep = TEST_STEP_BASIC_TESTS
                self.setNewMode("basictest")
            else:
                self.TestStep = TEST_STEP_SWITCH_BACK
                self.setNewMode("waiting",setNewMode=1)

        elif self.TestMode == "basictest":
            DoBasicTests(self)
            self.TestStep = TEST_STEP_PULSE_TESTS
            self.setNewMode("waiting",setNewMode=1)
            self.addToLog("Finished basic tests", color="#40ff40")
            self.CmdExecuteList.append("checkdata")

        elif self.TestMode == "pulsetest":
            DoPulseTests(self)
            self.TestStep = TEST_STEP_ARCHIVE
            self.setNewMode("waiting",setNewMode=1)
            self.addToLog("Finished pulse tests", color="#40ff40")
            self.CmdExecuteList.append("checkdata")

        elif self.TestMode == "failedswitch":
            switchCode = self.TestScaGbtxSwitch()
            if switchCode == 0:
                self.TestStep = TEST_STEP_FINAL_FAILED_SWITCH
                self.setNewMode("waiting", setNewMode=1)
            else:
                self.TestStep = TEST_STEP_ARCHIVE
                self.setNewMode("archive")
        elif self.TestMode == "archive":
            switchCode = self.TestScaGbtxSwitch()
            if switchCode==0:
                self.TestStep = TEST_STEP_FINAL_FAILED_SWITCH
                self.setNewMode("waiting", setNewMode=1)
            else:
                fID = self.fecData.getFID().lower()
                bID = self.fecData.getBID().lower()
    #            sys.stdout.write("archvie ID={},{}\n".format(fID,bID))
                fecname = "fid{}".format(fID)
                path = "{}/{}/Summary".format(FEC_DATA.DATAPATH,fecname)
    #                sys.stdout.write("path={}\n".format(path))

                if not os.path.exists(path):
                    self.TestStep = TEST_STEP_ARCHIVE_ERROR
                    self.setNewMode("waiting", setNewMode=1)
                else:
    #                    sys.stdout.write("path={}\n".format(path))
                    os.chdir(path)
                    dtmp = FEC_DATA.FEC_data()
                    FEC_DATA.readSummary(dtmp,path)
                    tDef = time.strftime('%Y%b%d_%H%M%S')
                    filename = "autosum_fec{}_{}_{}.pdf".format(bID,fID,tDef)
                    dtmp.MakePDF(filename)

                    SummaryPath = mpath.makePath(FEC_DATA.AUTOSUMPATH,fecname)
                    mList = [filename]
                    mList += glob.glob("*{}*.txt".format(fID))
                    for i in mList:
                        BasicCheckSummaryFile(SummaryPath, i)

                    if EMAIL_OPTION=="sendnow":
                        sendList = ['readkf@ornl.gov', 'clontslg@ornl.gov']
                        status = self.fecData.getBoardStatus()
                        email_body = "Total Test Time = {}s".format(int(time.time()-self.WatchTime))
                        #sys.stdout.write("Email FileList={}\n".format(mList))
                        FEC_email.SendEmail(FEC_DATA.receivers, "FEC{} fID={} {}".format(bID, self.fid, status), email_body, mList)
                    self.DeleteTmpBarCodeFile()
                    self.Supply.Off()
                    self.TestStep = TEST_STEP_BARCODE
                    self.setNewMode("waiting",setNewMode=1)
                    self.CmdExecuteList.append("clear")
                    self.Clear()


def ProcessBinData(fecData, numFrames):
    gfiles = glob.glob("*.stats")
    if len(gfiles) != 2:
        gfiles = glob.glob("*.bin")
        if len(gfiles) == 2:
            fecData.addToLog("Creating STATS and SYNC files")
            for i in [0, 1]:
                cmd = "decoder_gbtx -f {} -i run{}_1.bin".format(numFrames, i)
                cmd += " -o run{}_1 -s {} -p".format(i, i)
                mlist = ExecuteCmdParse(cmd)
                for m in mlist:
                    fecData.addToLog(m)
        else:
            fecData.addToLog("Missing BIN files")
            return -1

    for i in [0, 1]:
        fecData.Read("run{}_1.stats".format(i))
        fecData.Read("run{}_1.sync".format(i))
    return 1

def BasicCheckSummaryFile(SummaryPath, filename, force_update=1):
    if not os.path.exists(SummaryPath):
        os.makedirs(SummaryPath)
    else:
        if force_update == 0:
            sName = os.path.join(SummaryPath, filename)
            if os.path.isfile(sName) ==True:
                return
#    sys.stdout.write("Adding {}/{} to SUMMARY {}\n".format(os.getcwd(),filename,SummaryPath))
    cmd = "cp {} {}".format(filename, SummaryPath)
    ExecuteCmdParse(cmd)

def checkSummaryFile(fecname, filename, force_update=1):
    SummaryPath = mpath.makePath("{}/{}".format(FEC_DATA.DATAPATH,fecname), "Summary")
    BasicCheckSummaryFile(SummaryPath,filename)

#    SummaryPath = mpath.makePath(FEC_DATA.AUTOSUMPATH,fecname)
#    BasicCheckSummaryFile(SummaryPath,filename)


def addParam(pList, words):
#    sys.stdout.write("addParam {}\n".format(words))
    for i in pList:
        if i[0] == words[1]:
            i[1].append(words[2])
            return
    pList.append([words[1], [words[2]]])


def TestingInit(fec):
#    sys.stdout.write("Doing FAST FEC INIT\n")
    if fec.checkFastInit() == -1:
#        sys.stdout.write("Doing SLOW FEC INIT\n")
        if fec.DoInitProc() == -1:
            return -1
    return 0


def DoBasicTests(fec, force_update=1, winmain=0):
    fecData = fec.fecData
    fecData.ProcTimeLogInit()
    fecData.ClearBasicData()
    fecname = fec.getFecReference()
    fecData.ScaID = fec.ScaID
    fecData.BoardID = fec.boardID
    fecData.addParameter("teststation", myCompID)
    fecData.ProcTimeLog("Initialize")
    fec.setModeCond("PWR")
    fname = time.strftime('%Y%b%d_%H%M%S')
    fpath = "{}/BasicTests/{}".format(fec.capturePath, fname)
    os.makedirs(fpath)
    os.chdir(fpath)
    #
    # Do Power Testing
    #
    filename = fec.POWER_Scan(fpath)
    os.chdir(fpath)
    checkSummaryFile(fecname, filename, force_update=force_update)
    fecData.ProcTimeLog("Measured FEC power")
    fecData.supplyInfo = fec.SupplyInfo
    time.sleep(1.0)

    #
    # Do ADC measurements on the FEC board
    #
    pList = []
    for i in range(0, 5):
        fec.setModeCond("PWR-{}/5".format(i))
        cmd = "tsampa -v --mask {} --sampa-mask 0x1f --pwr-on".format(fec.FEC_mask)
        code = ExecuteCmdParse(cmd)
        for j in code:
            words = j.replace("=", " ").split()
            if words[0].lower() == "parameter":
                addParam(pList, words)

    for parm in pList:
        if parm[0] not in ["ScaID"]:
            sum = 0
            sumsq = 0
            cnt = 0
            for i in parm[1]:
                try:
                    val = float(i)
                    sum += val
                    sumsq += val * val
                    cnt += 1
                except:
                    pass
            if cnt > 0:
                sum /= cnt
                delta = math.sqrt(sumsq / cnt - sum * sum)
                fecData.addMeasurement(parm[0], sum, delta)
    #
    # Do the noise tests
    #
    fecData.ProcTimeLog("Measured SCA-ADCs")
    os.chdir(fpath)

    numFrames = int(8 * 5e5 + 300)
    cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir . --mask {}".format(numFrames, fec.FEC_mask)
    code = ExecuteCmdParse(cmd)
    fecData.addToLog("Exec={}".format(cmd))
    try:
        os.rename("run000000_trorc00_link0{}.bin".format(fec.FEC_binnum), "run0_1.bin")
        os.rename("run000000_trorc00_link0{}.bin".format(fec.FEC_binnum + 1), "run1_1.bin")
    except:
        pass
    fecData.ProcTimeLog("Capture Noise & Pedestal Data")
    fec.setModeCond("NS")
    ProcessBinData(fecData, numFrames)
    fecData.MakeNoisePDF("noise_{}.pdf".format(fecname))
    fecData.ProcTimeLog("Processing Data")
    filename = "basic_{}.txt".format(fecname)
    fecData.saveBasicTest(filename, version, fpath)
    checkSummaryFile(fecname, filename, force_update=force_update)
    fecData.ProcTimeLog("basicTests {}".format(fecname), do_total=1)
    fecData.addToLog("Output Directory {}".format(fpath))


def RunPulseTests(fec, freq=33300):
    fecData = fec.fecData
    fecData.ClearPulseData()
    fecData.ProcTimeLogInit()
    fecData.ScaID = fec.ScaID
    fecData.BoardID = fec.boardID
    fecData.ProcTimeLog("InitializeRun")
    procPathList = []

    # ---------------------------------------------------------
    # Cycle through the different amplitude to do the gain test
    # ---------------------------------------------------------
    fname = time.strftime('%Y%b%d_%H%M%S')

    for gain in [["20", 1], ["30", 2]]:
        gainDef = gain[0]
        fpath = "{}/GainTest{}/{}".format(fec.capturePath, gainDef, fname)
        os.makedirs(fpath)
        os.chdir(fpath)

        if gain[1] == 1:
            aList = [5, 10, 15, 20, 25, 30, 35, 40, 45, 50]
        else:
            aList = [5, 10, 15, 20, 25, 30, 35]
        fList = [4]
        rList = [10000]

        procPathList.append(fpath)

        P0 = "on"
        P1 = "on"
        #      fec.SAMPA_setGain(0)
        captureList = []
        fec.SAMPA_setGain(gain[1])
        for amplitude in aList:
            for fall in fList:
                for rise in rList:
                    desc = "amp_{}mVpp_f{}ns_r{}ns_{}hz_g{}".format(amplitude, fall, rise, freq, gainDef)
                    captureList.append(desc)

                    fecData.addToLog("Capturing {}".format(desc))
                    PulseInfo1 = fec.wavegen.setupFullSignal(0,
                                                         [freq, amplitude * 0.001, fall * 1e-9, rise * 1e-9, P0, 10e-6,
                                                          P0])
                    PulseInfo2 = fec.wavegen.setupFullSignal(1,
                                                         [freq, amplitude * 0.001, fall * 1e-9, rise * 1e-9, P1, 10e-6,
                                                          P1])
                    if fec.wavegen.valid==0:
                        fec.BadDevice(DEVICE_CODE_WAVE)
                        return
                    time.sleep(0.25)
                    cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir . --mask {}".format(
                        pulseReConstruct.numFrames, fec.FEC_mask)
                    code = ExecuteCmdParse(cmd)
                    try:
                        os.rename("run000000_trorc00_link0{}.bin".format(fec.FEC_binnum), "run0_{}.bin".format(desc))
                        os.rename("run000000_trorc00_link0{}.bin".format(fec.FEC_binnum + 1),
                                  "run1_{}.bin".format(desc))
                    except:
                        fecData.addToLog("Failed to move files",color="#ff0000")
                        pass

        fecData.SaveCapture(captureList)
        fecData.ProcTimeLog("Capture Gain Data @ {}".format(gainDef))

    # ---------------------------------------------------------
    # Do EVEN/ODD testing
    # ---------------------------------------------------------
    fpath = "{}/Crosstalk/{}".format(fec.capturePath, fname)
    os.makedirs(fpath)
    os.chdir(fpath)
    captureList = []
    procPathList.append(fpath)

    aList = [50]
    amplitude = 25

    #fList = [5]
    fec.SAMPA_setGain(1)  # 20mV/pC
    for P0 in ["on", "off"]:
        if P0 == "on":
            P1 = "off"
        else:
            P1 = "on"
        for amplitude in aList:
            for fall in fList:
                for rise in rList:
                    desc = "amp_{}mVpp_f{}ns_r{}ns_{}hz_{}_{}".format(amplitude, fall, rise, freq, P0, P1)
                    captureList.append(desc)
                    fecData.addToLog("Capturing {}".format(desc))
                    PulseInfo1 = fec.wavegen.setupFullSignal(0,
                                                         [freq, amplitude * 0.001, fall * 1e-9, rise * 1e-9, P0, 10e-6])
                    PulseInfo2 = fec.wavegen.setupFullSignal(1,
                                                         [freq, amplitude * 0.001, fall * 1e-9, rise * 1e-9, P1, 10e-6])
                    if fec.wavegen.valid==0:
                        fec.BadDevice(DEVICE_CODE_WAVE)
                        return
                    time.sleep(0.25)
                    cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir . --mask {}".format(
                        pulseReConstruct.numFrames, fec.FEC_mask)
                    code = ExecuteCmdParse(cmd)
                    try:
                        os.rename("run000000_trorc00_link0{}.bin".format(fec.FEC_binnum), "run0_{}.bin".format(desc))
                        os.rename("run000000_trorc00_link0{}.bin".format(fec.FEC_binnum + 1),
                                  "run1_{}.bin".format(desc))
                    except:
                        pass

    fec.wavegen.setupFullSignal(0, [freq, amplitude * 0.001, fall * 1e-9, rise * 1e-9, "off", 10e-6])
    fec.wavegen.setupFullSignal(1, [freq, amplitude * 0.001, fall * 1e-9, rise * 1e-9, "off", 10e-6])
    if fec.wavegen.valid == 0:
        fec.BadDevice(DEVICE_CODE_WAVE)
        return

    fecData.SaveCapture(captureList)
    fecData.ProcTimeLog("Capture CrossTest Data")
    return procPathList


# ---------------------------------------------------------
# Process the results
# ---------------------------------------------------------
def ProcessPulseTests(fecIntf, fecname,  procPathList, force_update=1, freq=33300):
    fecData = fecIntf.fecData
    pulseReConstruct.DoSetup(16, setFREQINPUT=freq)
    fecData.addParameter("crosstalkRCI", pulseReConstruct.reconInfo)

    for pwd in procPathList:
        os.chdir(pwd)
        pwdRef = pwd.replace(FEC_DATA.DATAPATH, "")
        fecData.Path = pwdRef
        pulseReConstruct.ProcessFiles(fecIntf,fecData, fecname, load_runinfo=0, force_update=force_update)
        fecData.ProcTimeLog("Reconstructing {}".format(pwdRef))
    fecData.ProcTimeLog("pulseTests {}".format(fecname), do_total=1)


def DoPulseTests(fecIntf, force_update=1, freq=33300):
    fecname = fecIntf.getFecReference()
    fecIntf.setModeCond("CP")
    procPathList = RunPulseTests(fecIntf, freq=freq)
    ProcessPulseTests(fecIntf, fecname, procPathList, force_update=force_update, freq=freq)
