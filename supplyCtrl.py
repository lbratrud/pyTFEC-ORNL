#
import sys
import math
import sys
import time
import string
import sys
import os
import array
import ctypes
import subprocess
import projpath as mpath
import errwarn as errwarn
import serial

import FEC_IO as fec_io
from projbase import *

V2r3 = 2.35
V3r3 = 3.4
CURRENT_LIMIT_2r3 = 1.2
CURRENT_LIMIT_3r3 = 3.0

class SupplyChannel:
    def __init__(self, instr, ID):
       self.ID = ID
       self.Instr = instr
       self.Status = [0, 0]

    def setV(self, voltage, climit):
        self.Instr.write("INST:SEL OUT{}".format(self.ID))
#        self.Instr.write("VOLT:RANG LOW")
#        self.Instr.write("VOLT:AMP {}".format(voltage))
#        self.Instr.write("CURR:LEV {}".format(climit))
#        sys.stdout.write("[{}]VOLT={}\n".format(self.ID,voltage))
        self.Instr.write("VOLT {}".format(voltage))
        self.Instr.write("CURR {}".format(climit))

    def Inquery(self):
        self.Instr.ask("INST:SEL OUT{}".format(self.ID))
        self.Instr.ask("INST:SEL?")
        vVal = self.Instr.ask("MEAS:VOLT?")
        iVal = self.Instr.ask("MEAS:CURR?")
        try:
            self.Status[0] = float(vVal)
            self.Status[1] = float(iVal)
        except:
            sys.stdout.write("Cannot convert V={},I={}\n".format(vVal,iVal))
#        sys.stdout.write("{} = {}\n".format(self.ID,self.Status))

class Keysight3646A:
    def __init__(self, portname):
        self.PortName = portname
        self.wait_time = 0.5
        self.read_timeout = 1.25
        self.baud = 9600
#        sys.stdout.write("Keysight3646A @ port={}\n".format(portname))
        try:
            self.Serial = serial.Serial(port=portname,baudrate=self.baud,timeout=0,
                                        parity=serial.PARITY_NONE,
                                        stopbits=serial.STOPBITS_TWO,
                                        bytesize=serial.EIGHTBITS)
        except:
            self.myIDN = "none"
            return
        self.ask("SYST::REM")
        self.myIDN = self.ask("*IDN?")
        if len(self.myIDN) == 0:
           return
        self.read_timeout = 0.5
        self.Status = [0,0]
        #self.Status[0] = self.ask("OUTPut:STATe?")
        self.Status[0] = self.ask("OUTPut?")

        self.Channel = [SupplyChannel(self,1),SupplyChannel(self,2)]
        for src in [0,1]:
            self.Channel[src].Inquery()

    def ValidDevice(self):
        name = "Agilent Technologies,E3648A"
        #sys.stdout.write("myIDN={} SPEC={}\n".format(self.myIDN,self.myIDN[0:len(name)]))
        if self.myIDN[0:len(name)] == name:
            return 1
        if self.myIDN  == "none":
            return -1
        return 0

    def On(self):
        self.write("OUTPut:STATe ON")
        #self.Status[0] = self.ask("OUTPut:STATe?")
        self.Status[0] = self.ask("OUTPut?")

    def Off(self):
        self.write("OUTPut:STATe OFF")
        #self.Status[0] = self.ask("OUTPut:STATe?")
        self.Status[0] = self.ask("OUTPut?")

    def ask(self,msg):
        self.write(msg)
        rsp = self.read()
        #sys.stdout.write("ask {} = {}\n".format(msg,rsp))
        return rsp

    def read(self):
        msg = ""
        timeout = time.time() + self.read_timeout
        code = 0
        while time.time() < timeout:
            sr = self.Serial.read(100)
            for j in sr:
                ch = chr(j)
                if ch in ["\r","\n"]:
                   if ch=="\r":
                      code |= 0x1
                   if ch=="\n":
                      code |= 0x2
                   if code==0x3: 
                      return msg
                else:
                   msg += ch
        return msg

    def write(self,msg):
        mList = []
        for i in msg:
            mList.append(ord(i))
        mList.append(ord("\r"))
        mList.append(ord("\n"))
        self.Serial.write(mList)
        time.sleep(10.0*len(msg)/self.baud)

    def close(self):
        self.Serial = 0

    def doc(self,ofile):
        for src in [0,1]:
            self.Channel[src].doc(ofile)
    def getInfo(self,channel):
        self.Channel[channel].Inquery()
        return self.Channel[channel].Status
