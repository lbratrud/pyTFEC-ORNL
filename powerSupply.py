#!/usr/bin/env python3
#
# powerSuppy.py
#
# This file handles the interactions with the FEC board. Internal operations
# handlethe accessing of different registers within the x-RORC.
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@yahoo.com)


import math
import sys
import time
import string
import sys
import os
import array
import ctypes
import subprocess
import errwarn as errwarn
import serial
import serial.tools.list_ports
from projbase import *
import supplyCtrl
import bpgbtx
import GBTx0cnfg as Cnfg0
import GBTx1cnfg as Cnfg1

ENABLE_GBTX1_PROGRAMMING = 1
DO_POWER_SEQUENCE=0
DO_POWER_SEQUENCE_TEST=0
version = "2.0 06/12/2017"

V1 = 2.25
V2 = 3.4
m = serial.tools.list_ports.comports()
Supply = 0
for i in m:
   words = "{}".format(i).split()
   if Supply==0:
      Supply = supplyCtrl.Keysight3646A(words[0])
      if Supply.ValidDevice()==1:
         sys.stdout.write("Found Voltage Suppy:: {} as {}\n".format(Supply.myIDN,Supply.PortName))
         break
      else:
         Supply.close()
         Supply=0

if Supply==0:
   sys.stdout.write("No PowerSuppy found\n")
   exit()

def Measure():
   vs1,is1 = Supply.getInfo(0)
   vs2,is2 = Supply.getInfo(1)
   sys.stdout.write("{}".format(Supply.PortName))
   try:
     if int(Supply.Status[0])==1:
        sys.stdout.write(" ON")
     else:
        sys.stdout.write(" OFF")
   except:
        sys.stdout.write(" UNK{}".format(Supply.Status[0]))
   sys.stdout.write(" V1={} I1={} V2={} I2={}\n".format(vs1,is1,vs2,is2))

Measure()
#Supply.On()
#Supply.Channel[0].setV(2.25, 1.0)
#Supply.Channel[1].setV(3.0, 3.0)
#time.sleep(0.5)
#Measure()

for i in sys.argv:
   cmd = i.lower()
   if i=="-off":
      Supply.Off()
      sys.stdout.write("Turning Supply Off\n")
      Measure()
   if i=="-on":
      sys.stdout.write("Here\n")
      Supply.On()
      Supply.Channel[0].setV(V1,1.1)
      Supply.Channel[1].setV(V2,3.8)
      Measure()
Supply.close()

exit()
