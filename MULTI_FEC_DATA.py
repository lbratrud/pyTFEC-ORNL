# MULTI_FEC_DATA.py
#
# This file combines data from SWEEP data and presents it in the same PDF file. This file is a variation
# of FEC_DATA!
#
# ORNL - 6/06/2017
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)


import math
import sys
import time
import string
import sys
import glob
import projmkrpt as mkrpt
import commondb as cdb
from projbase import *

from reportlab.graphics.shapes import Drawing, _DrawingEditorMixin, String
from reportlab.graphics.charts.lineplots import SimpleTimeSeriesPlot
from reportlab.lib.colors import purple, PCMYKColor, black, pink, green, blue
from reportlab.graphics import renderPDF

from reportlab.graphics.charts.lineplots import LinePlot, ScatterPlot
from reportlab.graphics.charts.legends import LineLegend
from reportlab.graphics.shapes import Drawing, _DrawingEditorMixin
from reportlab.graphics.widgets.markers import makeMarker
from reportlab.pdfbase.pdfmetrics import stringWidth, EmbeddedType1Face, registerTypeFace, Font, registerFont
from reportlab.graphics.charts.axes import XValueAxis, YValueAxis, AdjYValueAxis, NormalDateXValueAxis
from reportlab.graphics.widgets.markers import makeMarker
from reportlab.lib import colors
import FEC_DATA

class MULIT_data(mkrpt.makeReport):
   def __init__(self):
     self.markerindex = 0
     self.colorindex = 0
     self.FEC_Data_List = []

   def add_data(self,newFEC):
     self.FEC_Data_List.append(newFEC)

   def SetupColorMarkers(self):
     self.MarkerList = [None]
     
#     for i in ["Circle","Diamond","Square","Octagon","Heptagon","StarFive","StarSix"]:
#         self.MarkerList.append(makeMarker(i))
     self.ColorList = [colors.red,colors.blue,colors.green,colors.purple,colors.brown,colors.cyan,colors.orange,colors.darkolivegreen]
         
   def MakePDF(self, filename):
        self.TimeDef = time.strftime('%b %d %Y, %H:%M:%S')
        sys.stdout.write("FEC_DATA_LIST={}\n".format(self.FEC_Data_List))
        self.BoardID = int(self.FEC_Data_List[0].BoardID)
        mkrpt.makeReport.__init__(self,filename,"Multiple FEC plot")
        self.title2 = "????"
        self.addHeader()
        self.add_header = 1
        self.lineList = []
        for i in self.FEC_Data_List:
            color, marker = self.getColorMarker()
            self.lineList.append([i.PulseInfo, color, marker])
        for i in range(0,160):
           self.plotChannelData(i)

        #           for index in range(0,160):
#               if len(self.TimeDelta.Histo[index].YxList) > 1 or len(self.Energy.Histo[index].YxList) > 1:
#                  indexList.append(index)
#           self.EventDataAsSingleItemPlots(indexList,"")
#           self.EventDataAsMultiItemPlots(indexList,"")
        self.DoExit()

   def getColorMarker(self):
       cindex = self.colorindex
       mindex = self.markerindex
       self.colorindex += 1
       self.markerindex += 1
       if self.markerindex >= len(self.MarkerList):
          self.markerindex = 0
       if self.colorindex >= len(self.ColorList):
          self.colorindex = 0
       return [self.ColorList[cindex],self.MarkerList[mindex]]

   def addHeader(self):
       yf = y = mkrpt.letter[1] - mkrpt.margin
       xl = mkrpt.margin
       xh = mkrpt.letter[0] - mkrpt.margin
       x = 0.5 * (xl + xh)

       t = cdb.TextConfigList.find("title")
       size = t.getSize()

       self.canvas.setFont("Times-Roman", 0.5 * size)
       msg = "{} Page={}".format(self.TimeDef, self.page)
       self.page += 1
       msize = self.getWidth(msg, size)
       self.canvas.drawString(mkrpt.letter[0] - mkrpt.margin - 0.5 * msize, y + 0.1 * mkrpt.inch, msg)
#       self.canvas.drawString(xl, y + 0.1 * mkrpt.inch, "MODE={}".format(self.Mode))
       self.pageLine(y)
       y -= 1.1 * size

       title1 = "{} BOARD-ID {}({})".format(self.title1, self.BoardID, hex(self.BoardID))
       self.canvas.setFont("Times-Roman", size)
       x = xl + 0.5 * (xh - xl - self.getWidth(title1, size))
       if x < xl:
           x = xl
           #       mList.append(["DATE", time.strftime('%b %d %Y, %H:%M:%S')])
           #       mList.append(["BOARD-ID", "{}({})".format(self.BoardID, hex(self.BoardID)), "MODE", self.Mode])
       self.canvas.drawString(x, y, title1)
       y -= 0.5 * size
       x = xl + 0.5 * mkrpt.margin
#       self.canvas.setFont("Times-Roman", 0.5 * size)
#       self.canvas.drawString(x, y, self.PulseInfo)
#       y -= 0.5 * size
#       self.canvas.setFont("Times-Roman", 0.5 * size)
#       self.canvas.drawString(x, y, "Path = {}".format(self.Path))
#       y -= 0.5 * size

       self.pageLine(y)
       self.canvas.line(xl, yf, xl, y)
       self.canvas.line(xh, yf, xh, y)
       self.y = y

       t = cdb.TextConfigList.find("report")
       self.canvas.setFont("Times-Roman", t.getSize())

   def plotChannelData(self,index):
       data_cnt = 0
       for i in self.FEC_Data_List:
           if len(i.TimeDelta.Histo[index].YxList) > 0:
              data_cnt += 1
           if len(i.Energy.Histo[index].YxList) > 0:
              data_cnt += 1
       if data_cnt ==0:
          return
       self.addComponent([-2])
       Lindex = 0
       out_of_spec = 0
       for i in self.lineList:
           msg = i[0]
           if self.FEC_Data_List[Lindex].TimeDelta.Histo[index].Valid == 1:
              msg += " (IN-SPEC)"
           else:
               msg += " (OUT-OF-SPEC)"
               out_of_spec += 1
           self.addText(mkrpt.margin*2, self.y,msg, 10, [i[1].red, i[1].green, i[1].blue])
           self.y -= self.size
           Lindex += 1
       TimeDeltaData = []
       EnergyData = []
       for i in self.FEC_Data_List:
           TimeDeltaData.append(i.TimeDelta.Histo[index].YxList)
           EnergyData.append(i.Energy.Histo[index].YxList)

       plotsize = 320
       if out_of_spec > 0:
           self.addMultiPlotNoList(TimeDeltaData, "Time Delta Histogram (Channel {})".format(index))
       else:
           plotsize *= 2
       self.addMultiPlotNoList(EnergyData, "Energy Sum Histogram (Channel {})".format(index),plotsize=plotsize)

   def range2List(self, start, stop, ranges):
       step = int((stop - start) / ranges)
       if step * ranges + start < stop:
           step += 1

       mList = []
       for i in range(0, ranges + 1):
           mList.append(start + step * i)
       mList.sort(key=int, reverse=0)
       return [mList, start + step * i]

   def getDataMinMax(self,myData):
        ymin = xmin = 500000
        xmax = ymax = -500000
        for j in myData:
            for i in j:
                if i[0] > xmax:
                   xmax = i[0]
                if i[0] < xmin:
                   xmin = i[0]
                if i[1] > ymax:
                   ymax = i[1]
                if i[1] < ymin:
                   ymin = i[1]
        return [xmax,ymax]
                   
   def addMultiPlotNoList(self,myData,desc,plotsize=320):
        plot_ysep = int(plotsize/3)
        xmax,ymax = self.getDataMinMax(myData)
        plot_ysep = 60
        xList,xmax = self.range2List(0,xmax,10)
        yList,ymax = self.range2List(0,ymax,5)
      
        # In terms of the page, yinit is giving us the lower part of the page and we build UP toward the top. Same as other functions
        # in projmkrpt.py. LGC
        yinit = self.y-plotsize
        if yinit < 0:
           self.addComponent([-2])
           self.add_header = 1
           yinit = self.y-plotsize
        self.y = yinit
        
        drawing = Drawing(700,plotsize)
        lp = LinePlot()
        lp.x = 50
        lp.y = yinit+plot_ysep*0.25
        lp.width = 525
        lp.height = plotsize-plot_ysep
        lp.data = myData
#        lp.lineLabelFormat = '%2.0f'
        index = 0
        hsize = 0.5*(mkrpt.letter[0]-2*mkrpt.margin)
        yindex = 0
        self.addCenteredText(hsize+mkrpt.margin,lp.y+lp.height+plot_ysep*0.5,desc,15,[0,0,0])
        for i in self.lineList:
            lp.lines[index].strokeColor = i[1]
            lp.lines[index].symbol = i[2]
            index += 1
        lp.strokeColor = colors.black
        lp.xValueAxis.valueMin = 0
        lp.xValueAxis.valueMax = xmax
        lp.xValueAxis.valueSteps = xList
#        lp.xValueAxis.lineLabelFormat = '%2.0f'

        lp.yValueAxis.valueMin = 0
        lp.yValueAxis.valueMax = ymax
        lp.yValueAxis.valueSteps = yList
        drawing.add(lp)
        renderPDF.draw(drawing,self.canvas,0,0)

MD = MULIT_data()

dList = glob.glob("*/runinfo.txt")
index = 0
for i in dList:
    d1 = FEC_DATA.FEC_data()
    d1.Read(i)
    d1.ReadAll("{}".format(d1.Path))
    MD.add_data(d1)
    index += 1
#    if index > 2:
#       break
#MD.SetupColorMarkers()
#MD.MakePDF("Combo.pdf")

index = 0
mList = []
for data in MD.FEC_Data_List:
    index += 1
    mList.append("[{}] <= PulseInfo={}".format(index,data.PulseInfo))

for channel in range(0,160):
    mList.append("Chanenl[{}] Summary".format(channel))
    index = 0
    for data in MD.FEC_Data_List:
        for i in data.PulseList:
           if channel in i[2]:
              info = data.Energy.Histo[channel].getInfo()
              mList.append("[{:3d}] Ectr={:4.1f} Min={:4d} Max={:4d} {}".format(index,info[0],info[1],info[2],i[1]))
        index += 1
    mList.append("")

index = 0
for data in MD.FEC_Data_List:
    index += 1
    mList.append("[{}] PulseInfo={}".format(index,data.PulseInfo))
    mList.append("[{}] EAvg={:.1f} ERms={}".format(index,data.EAvg,data.ERms))
    for i in data.PulseList:
        mList.append("{} #channels={}".format(i[1],len(i[2])))
    mList.append("")

LogFile = open("runMulti.log", 'w')
for i in mList:
    LogFile.write("{}\n".format(i))
    sys.stdout.write("{}\n".format(i))
LogFile.close()
