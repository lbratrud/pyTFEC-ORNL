# captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import os
import time

MAXITR = 200
for i in range(0,MAXITR):
   sys.stdout.write("Itr {}/{}\n".format(i,MAXITR))
   #os.system("trorc_fec_readout_control -P 0xff -T 0x3 -p -t -S")
   os.system("trorc_fec_readout_control -P 0xff -T 0x3 -p -t")
   time.sleep(1)
exit(0)
