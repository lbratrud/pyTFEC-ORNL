# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import string
import sys
import os
import array
import projpath as mpath
import glob
import sampa_decoder as SParser

version = "1.0 11/03/2016"

mpath.projName = "TPC-FEC"
FreqMode = 160
tpcMode = 0

import sampa_decoder as SParser

valid_modes = ["CPD","TPC","SYNC"]
def GetFullList(FileRef):
    global valid_modes
    gList = glob.glob("/tmp/{}".format(FileRef))
#    sys.stdout.write("search={} glist={}\n".format(FileRef,gList))
    mList = []
    for i in gList:
        flist = i.replace(".log","").replace("/tmp/","").split("_")
        flen = len(flist) 
        if flist[1] in valid_modes and flist[2] in ["160","80"]:
           if flen == 7:
              dfile = "{}_{}_{}_{}_{}_{}".format(flist[0],flist[1],flist[2],flist[3],flist[4],flist[5])
              myRef = ["DS",dfile]
              if myRef not in mList:
                 mList.append(myRef)
           elif flen == 6:
              dfile = "{}_{}_{}_{}_{}_{}".format(flist[0],flist[1],flist[2],flist[3],flist[4])
              myRef = ["SF",dfile]
              if myRef not in mList:
                 mList.append(myRef)
    return mList

def GetMainList(FileRef):
    global valid_modes,tpcMode
    
    gList = glob.glob("/tmp/{}*".format(FileRef))
#    sys.stdout.write("search={} glist={}\n".format(FileRef,gList))
    mList = []
    for i in gList:
        flist = i.replace(".","_").split("_")
        flen = len(flist) 
#        sys.stdout.write("len={} flist={}\n".format(flen,flist))
        if flist[1] in valid_modes and flist[2] in ["160","80"]:
           if flen in [6,7]:
              FreqMode = int(flist[2])
              mList.append(i)
              if flist[1]=="TPC":
                 tpcMode = 1
    return mList

fList = []
if len(sys.argv) >= 2:
    FileReference = sys.argv[1]
    if os.path.isfile(FileReference):
       fList.append(FileReference)
       fnlist = FileReference.replace("/"," ").split()
       FileReference = fnlist[len(fnlist)-1]
       fname = FileReference.replace(".","_").replace("/tmp/","").split("_")
       if len(fname) > 3 and fname[1]=="TPC":
          tpcMode = 1
    else:
       fList = GetMainList(FileReference)
    if len(fList) > 0:
       sys.stdout.write("File List to Process:\n")
       index = 1
       for i in fList:
          sys.stdout.write("  [{}] {}\n".format(index,i))
          index += 1
       sys.stdout.write("\n")

if len(fList) == 0:
    sys.stdout.write("\npyFECdatard {}\n\nParse existing FEC data files\n".format(version))
    sys.stdout.write("Usuage: python pyFECdatard file_reference\n\n")
    mList = GetFullList("fec*")
    for i in mList:
        if i[0]=="DS":
           sys.stdout.write("DataSet {}\n".format(i[1]))
    for i in mList:
        if i[0]!="DS":
           sys.stdout.write("File {}\n".format(i[1]))
    sys.stdout.write("\n")

if len(fList) > 0:
    path = mpath.getPath("root")
    if tpcMode==1:
       resultsFileName = "{}/{}.tpc".format(path,FileReference)
    else:
       resultsFileName = "{}/{}.pedadc".format(path,FileReference)
    sys.stdout.write("Creating results file {}\n".format(resultsFileName))
    
    fout = open(resultsFileName,'w')
    for i in SParser.GBTx0_data:
      i.setRootFile(fout)
    for i in SParser.GBTx1_data:
      i.setRootFile(fout)
    rorcHandler = SParser.RorcRecieveHandler(tpcMode,FreqMode)
    rorcHandler.setOFile(fout)
    
    for CaptureFile in fList:
      rorcHandler.clearAll()
      fout.write("# ================ START {} ================\n".format(CaptureFile))
      
      LogFile = "{}.log".format(CaptureFile)
      if os.path.isfile(LogFile):
         flog = open(LogFileName,'r')
         for l in flog:
            fout.write(l)
         flog.close()
      fin = open(CaptureFile,'r')
      sys.stdout.write("Analyzing {}\n".format(CaptureFile))
      rorcHandler.AnalyzeFile(fin)
      fin.close()
      fout.write("# ================ STOP {} ================\n".format(CaptureFile))
    SParser.docSampaAddrInfo(fout)
    rorcHandler.doc()
    fout.close()
