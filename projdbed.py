# mtcsdbed.py
#
# Collection of GUIs for modifying / Editing/ viewing database components
#
# --------------------------------------------------------------------------------
# Created 1/1/2015 by Lloyd Clonts clontslg@yahoo.com
# Project started December 2013 for Gary W. Turner
# for implementing CTC system

import math
import sys
import time
import threading
import string
import sys

from projbase import *
import errwarn
import projdbb as dbb
import projwin as win

try:  # import as appropriate for 2.x vs. 3.x
   from tkinter import *
except:
   from Tkinter import *
# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
def StripUnderScore(name):
    return name.translate(str.maketrans("_", " "))

def getSubDesc(words,group):
    cname = ""
    for i in words:
         if i!=group:
              if len(cname) > 0:
                 cname += " "
              cname += i
    return cname

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class ParameterEditor(win.Base):
    def __init__(self):
         if win.Base.__init__(self,"parmeditor","Parameter Editor")==0:
            return

         self.size = 12
         self.myList = []
         myNames = []
         for t in dbb.TrainSysParms.objList:
            name = t[0]
            myNames.append(name)

         myNames.sort(key=str.lower)
         for n in myNames:
            for l in dbb.TrainSysParms.objList:
               if n == l[0]:
                  self.myList.append(l)
                  break
         row = 0
         col = 0
         self.currentParm = 0
         columnspan=4
         self.f1 = win.WinMenu(self,"Options",-1,0,1)
         self.f1.menub.grid(sticky=W) #columnspan=columnspan,
         if dbb.admin_mode==1:
            self.f1.menu.add_command(label="Save",command=dbb.ColorList.save)
            self.f1.menu.add_separator()
         self.f1.menu.add_command(label="Exit",command=self.DoExit)
         row += 1

         self.ObjList = []
         self.index = -1
         row = 1

         tick=32
         length = 300
         ssize=500

         col  = 0
         divide = 20
         ColumnList = []
         ColumnList.append(["Parameter",50,"V"])
         vtype = "E"
         if dbb.admin_mode == 0:
            vtype = "V"

         ColumnList.append(["Value",6,vtype])
            
         yScroll = Scrollbar(self.master)
         yScroll.grid(row=row+2,column=col,sticky=N+S,rowspan=divide)
         col+=1
         for c in ColumnList:
           mylabel = Label(self.master,text=c[0],font=self.font,bg=self.bgc,fg=self.fgc,width=c[1],relief=RAISED) #,relief=RAISED
           mylabel.grid(row=row,column=col,sticky=E+W)
           if c[2]=="V":
              myvalue = Label(self.master,text="",font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED,width=c[1])
           else:
              myvalue = Entry(self.master,font=self.font,bg=self.qbgc,fg=self.qfgc,relief=RAISED,width=c[1])
           myvalue.grid(row=row+1,column=col,sticky=E+W)

           xScroll = Scrollbar(self.master,orient=HORIZONTAL)

           mylist = Listbox(self.master,selectmode=SINGLE,font=self.font,bg=self.qbgc,fg=self.qfgc,width=c[1],
                            height=divide,relief=FLAT,yscrollcommand=yScroll.set,xscrollcommand=xScroll.set)
           xScroll.config(command=mylist.xview,bg=self.bgc)
           mylist.bind("<Double-Button-1>", self.getSelect)
           mylist.grid(row=row+2,column=col,rowspan=divide,sticky=E+W+N+S)
           xScroll.grid(row=row+divide+2,column=col,sticky=E+W)
           self.ObjList.append([myvalue,mylist,c[2]])
           col += 1
         yScroll.config(command=self.yview)
         self.updateList()
            
    def updateParm(self):
       if self.currentParm != 0 and dbb.admin_mode==1:
          name = self.currentParm[0].upper()
          value = self.ObjList[1][0].get()
          self.myList[self.index] = [name,value]
          dbb.TrainSysParms.setV(name.lower(),value)
          self.updateList()

    def saveDefaults(self):
       self.updateParm()
       dbb.SaveBaseDefs()

    def updateList(self):
       self.ObjList[0][1].delete(0,END)
       self.ObjList[1][1].delete(0,END)
       cnt = 0
       for c in self.myList:
          self.ObjList[0][1].insert(cnt,c[0].upper())
          self.ObjList[1][1].insert(cnt,c[1])
          cnt+=1
       
    def getSelect(self,event):
        self.updateParm()
        index = event.widget.nearest(event.y)
        self.currentParm = self.myList[index]
        self.index = index
#        self.ObjList[0][0].delete(0,END)
        self.ObjList[0][0].config(text=self.currentParm[0].upper())
        if dbb.admin_mode==1:
           self.ObjList[1][0].delete(0,END)
           self.ObjList[1][0].insert(0,self.currentParm[1])
        else:
           self.ObjList[1][0].config(text=self.currentParm[1])

    def yview(self,*args):
      for l in self.ObjList:
         l[1].yview(*args)

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class PersonelViewer(win.Viewer):
    def __init__(self):
      if win.Viewer.__init__(self,"Personel Manager (Draft)","personprofile","N")==0:
         return

      self.MainList = dbb.TrainPersonelList
      self.ColumnList.append([["Name","Initials"],20,"S"])
      self.ColumnList.append([["Contact Info","Email"],15,"S"])
      self.ColumnList.append([["Type","Status"],10,"S"])
      self.ColumnList.append([["Road Time","Dwell Time"],8,"S"])
      self.ColumnList.append([["Train","Call Time"],18,"S"])
      self.ColumnList.append([["Last Train","Last Call Time"],18,"S"])

      self.viewLists(16,3,0)
      self.updateLists()

    def DoAdd(self):
       PersonelEditor(0,self)
       
    def DoModify(self):
       if self.currentobj==0:
          return
       PersonelEditor(self.currentobj,self)

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class PersonelEditor(win.Editor):
    def __init__(self,objEdit,viewer):
      if win.Editor.__init__(self,"Personel",objEdit,viewer,dbb.TrainPersonelList)==0:
         return

      self.ColumnList.append(["Name",10,"ENTRY","S",3])
      self.ColumnList.append(["Contact Info",12,"ENTRY","S",10])
      self.ColumnList.append(["Email",12,"ENTRY","S",10])
      self.ColumnList.append(["Type",12,"MENU","S",3])
      self.ColumnList.append(["Status",10,"MENU","S",3])
      self.ColumnList.append(["Initials",10,"ENTRY","S",2])

      self.ColumnList.append(["Train",10,"LABEL","S",3])
      self.ColumnList.append(["Call Time",18,"LABEL","S",3])
      self.ColumnList.append(["Last Train",10,"LABEL","S",3])
      self.ColumnList.append(["Last Call Time",16,"LABEL","S",3])

      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
      self.DoCheck()


# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainPriorityViewer(win.Viewer):
    def __init__(self):
      if win.Viewer.__init__(self,"Train Priority Manager (Draft)","priorityprofile","N")==0:
         return

      self.MainList = dbb.TrainPriorityList
      self.ColumnList.append([["Name"],16,"S"])
      self.ColumnList.append([["Description"],26,"S"])
      self.ColumnList.append([["Priority"],5,"I"])

      self.viewLists(8,3,0)
      self.sortby.set("Priority")
      self.updateLists()

    def DoAdd(self):
       TrainPriorityEditor(0,self)
       
    def DoModify(self):
       if self.currentobj==0:
          return
       TrainPriorityEditor(self.currentobj,self)

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainPriorityEditor(win.Editor):
    def __init__(self,objEdit,viewer):
      if win.Editor.__init__(self,"Priority",objEdit,viewer,dbb.TrainPriorityList)==0:
         return

      self.ColumnList.append(["Priority",5,"LABEL","I",-1])
      self.ColumnList.append(["Name",8,"ENTRY","S",3])
      self.ColumnList.append(["Description",15,"ENTRY","S",5])

      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
      self.DoCheck()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class FormViewer(win.Viewer):
    def __init__(self):
      if win.Viewer.__init__(self,"Authority Form Manager (Draft)","formprofile","N") == 0:
         return
      self.MainList = dbb.AuthorityFormList

      self.ColumnList.append([["Name"],10,"I"])
      self.ColumnList.append([["FormType"],10,"S"])
      self.ColumnList.append([["Date"],10,"S"])
      self.ColumnList.append([["TrainID/\nIssue To"],10,"S"])
      self.ColumnList.append([["Engine/\nMilepost"],10,"S"])
      self.ColumnList.append([["Authority"],20,"S"])
      self.ColumnList.append([["General Info"],30,"S"])

      self.viewLists(16,3,0)
      self.updateLists()
      self.defineOptions()

    def DoFormExecList(self,mode):
       if self.currentobj==0:
          return

       if self.currentobj.IsValid()==1:
          formtype = self.currentobj.getParam("formtype").upper()
          if mode=="partial":
             force_done = 0
          else:
             force_done = 1
          if formtype=="A":
            tform.FormA(self.currentobj,dbb.DispatcherDef,force_done)
          elif formtype=="B":
            db = dbb.ProtoObjList.seek(self.currentobj.getParam("group"))
            tform.FormB(db,self.currentobj,dbb.DispatcherDef,force_done)

    def DoDeleteUnused(self):
      done = 0
      while done == 0:
         done = 1
         for i in self.MainList.MyList:
            if i.IsInUse(0)==0:
               self.MainList.delete(i.name)
               done = 0
      self.updateLists()

    def defineOptions(self):
      self.b1.menu.delete(0,END)
      found_unused = 0
      if self.MainList!=0:
         for i in self.MainList.MyList:
            if i.IsInUse(0)==0:
               found_unused = 1
               break
      if found_unused==1:
         self.b1.menu.add_command(label="Delete Unused",command=self.DoDeleteUnused)
         self.b1.menu.add_separator()
         
      if self.currentobj!=0:
         if self.currentobj.IsInUse(0)==0:
            self.b1.menu.add_command(label="Delete",command=self.DoDelete)
         else:
            if self.currentobj.getParam("finalrelease")==0:
                self.b1.menu.add_command(label="Partial",command= lambda md="partial": self.DoFormExecList(md))
            self.b1.menu.add_command(label="Final",command= lambda md="final": self.DoFormExecList(md))
         self.b1.menu.add_separator()
      self.b1.menu.add_command(label="Exit",command=self.DoExit)

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainViewer(win.Viewer):
    def __init__(self):

      self.yard = 0
      self.yardList = []
      for c in dbb.CtrlObjList.MyList:
        if c.mytype == "YARD":
           self.yardList.append(c)

      if win.Viewer.__init__(self,"Train Manager (Draft)","trainprofile","N") == 0:
         return

      self.MainList = dbb.TrainList
      self.ColumnList.append([["Name"],10,"S"])
      self.ColumnList.append([["Lead Engine","Engine Count"],12,"S"])
      self.ColumnList.append([["Status","HP"],8,"I"])
      self.ColumnList.append([["Call Time"],18,"S"])
      self.ColumnList.append([["Loads","Empties"],8,"I"])
      self.ColumnList.append([["Length","Tons"],8,"I"])
      self.ColumnList.append([["Priority","Priority Num"],10,"S"])
      self.ColumnList.append([["Conductor","Engineer"],15,"S"])
      self.ColumnList.append([["Route","Direction"],5,"S"])
      self.ColumnList.append([["General Info"],20,"X"])

      self.viewLists(16,3,0)
      self.updateLists()

    def defineOptions(self):
      self.b1.menu.delete(0,END)
      yardmenu = Menu(self.b1.menu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=0)
      for y in self.yardList:
         yardmenu.add_radiobutton(label="Yard {}".format(y.name),command=lambda obj=y: self.DoAdd(obj))

      self.b1.menu.add_cascade(label="Plan New Train",menu=yardmenu)
      self.b1.menu.add_separator()
      self.b1.menu.add_command(label="Fast Create",command=self.DoFastBuild)
      self.b1.menu.add_command(label="Fast Modify",command=self.DoFastModify)

      if self.currentobj!=0:
         self.b1.menu.add_separator()
         status = self.currentobj.getParam("status").lower()
         head = self.currentobj.getParam("headat")
         if head not in NULL_DEF:
            self.yard = dbb.CtrlObjList.IDseek(head)
         else:
            self.yard = 0
         
         if self.currentobj.IsInUse(0)==0:
            self.b1.menu.add_command(label="Delete",command=self.DoDelete)

         if status in ["planning"]:
            self.b1.menu.add_command(label="Build Train",command=self.DoBuild)
         elif status in ["building","called"]:
            self.b1.menu.add_command(label="Modify Train Build",command=self.DoBuild)

         if status in ["building","called","recrew/tieddown", "recrew/layeddown"]:
            self.b1.menu.add_command(label="Setup CallTime",command=self.DoFinal)

         if status not in ["planning","building","called"]:
            statusmenu = Menu(self.b1.menu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=0)
            for i in ["dwell","active","recrew","recrew/tieddown", "recrew/layeddown","terminate"]:
               if i != status:
                  statusmenu.add_command(label=i.upper(),command=lambda status=i: self.setStatus(status))
            self.b1.menu.add_cascade(label="Set Status",menu=statusmenu)

         if status in ["active","recrew","recrew/tieddown", "recrew/layeddown"]:
            defineTrainPersonelOptions(self,self.currentobj,self.b1)
            
      self.b1.menu.add_separator()
      self.b1.menu.add_command(label="Exit",command=self.DoExit)

    def setStatus(self,status):
       if self.currentobj!=0:
          self.currentobj.setParam("status",status)
    def DoFastBuild(self):
       self.currentobj = self.MainList.find("newone")
       self.currentobj.setParam("status","called")
       TrainEditor(self.currentobj,0,self,0)
    def DoFastModify(self):
       TrainEditor(self.currentobj,0,self,0)
    def DoAdd(self,obj):
       TrainPlanner(0,self,obj)
       self.defineOptions()
    def DoModify(self):
       TrainPlanner(self.currentobj,self,0)
    def DoBuild(self):
       TrainBuilder(self.currentobj,self,self.yard)
    def DoFinal(self):
       TrainCallSetup(self.currentobj,self)

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainPlanner(win.Editor):
    def __init__(self,objEdit,viewer,yard):
      if win.Editor.__init__(self,"Train",objEdit,viewer,dbb.TrainList)==0:
         return
      if objEdit==0:
         title = "Planning new Train at {}".format(yard.name)
      else:
         title = "Train Planner for {}".format(objEdit.name)
      self.master.title(title)

      if self.newone == 1:
         self.currentobj.setParam("headat",yard.name)
         yard.addTrain("LR",self.currentobj,0)

      menumode = "MENU"
      status = self.currentobj.getParam("status").lower()
      if status in ["active","onduty"]:
         menumode = "LABEL"

      self.ColumnList.append(["Status",8,"LABEL","S",-1])
      self.ColumnList.append(["Name",10,"ENTRY","S",3])
      self.ColumnList.append(["Route",15,menumode,"S",-1])

      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
      self.DoCheck()

    def DoSave(self):
       self.DoSaveInfo()
       route = dbb.TrainRouteList.seek(self.currentobj.getParam("route"))
       if route != 0:
          self.currentobj.setParam("direction",route.getParam("direction"))
          self.currentobj.setParam("priority",route.getParam("priority"))
          
       self.DoSaveFinish()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainBuilder(win.Editor):
    def __init__(self,objEdit,viewer,yard):
      if win.Editor.__init__(self,"Pickup/Setoff/Builder for {}".format(objEdit.name),objEdit,viewer,dbb.TrainList)==0:
         return
      self.yard = yard
      menumode = "MENU"
      menuentrymode = "MENUENTRY"
      entrymode = "ENTRY"

      self.ColumnList.append(["Status",8,"LABEL","S",-1])
      self.ColumnList.append(["Name",10,"LABEL","S",3])
      self.ColumnList.append(["Route",10,"LABEL","S",3])
      for i in range(int(dbb.TrainSysParms.getV("num_engines_max"))):
         if i==0:
            self.ColumnList.append(["Lead Engine",10,menuentrymode,"S",3])
         else:
            self.ColumnList.append(["Engine {}".format(i+1),10,menuentrymode,"S",3])

      self.ColumnList.append(["Loads",8,"ENTRY","I",1])
      self.ColumnList.append(["Empties",8,"ENTRY","I",1])
      self.ColumnList.append(["Tons",8,"ENTRY","I",1])
      self.ColumnList.append(["Length",8,"ENTRY","I",1])
      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1

      route = dbb.TrainRouteList.seek(objEdit.getParam("route"))
      if route == 0:
         return

      nval = route.getParam("route").translate(str.maketrans(",", " "))
      rstruct = nval.split()

      row = 1
      col = 3
      colh = colsub = 5
      dList = ["Total Cars", "First Car","Last Car"]
      columnspan = len(dList)

      for j in dList:
         label = Label(self.master,text=j,font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED)
         label.grid(row=row+1,column=colsub,sticky=E+W)
         colsub+=1
      row += 2
      overhead = objEdit.getCarStruct("overhead",0)
      self.setupPickupSetoff(0,overhead,0,10,row,col)
      row += 2

      colh = colsub = 5
      for i in ["Pick-up","Set-off"]:
         label = Label(self.master,text=i,font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED)
         label.grid(row=row,column=colh,sticky=E+W,columnspan=columnspan)
         colh+= columnspan
         for j in dList:
            label = Label(self.master,text=j,font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED)
            label.grid(row=row+1,column=colsub,sticky=E+W)
            colsub+=1
      row += 2

      for i in rstruct:
         ctrl = dbb.CtrlObjList.IDseek(i)
         if ctrl!=0:
            name = ctrl.name
            pickup = objEdit.getCarStruct("pickup",ctrl)
            setoff = objEdit.getCarStruct("setoff",ctrl)
         else:
            name = i
            PiDe = [-1,-1]
         self.setupPickupSetoff(name,pickup,setoff,10,row,col)
         row += 1

      self.DoCheck()

    def setupPickupSetoff(self,name,pickup,setoff,width,row,col):
      check = Label(self.master,text="-",font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED)
      check.grid(row=row,column=col,sticky=E+W)
      check.config(text="!",bg="green3")
      col += 1
      oname = name
      if name!=0:
         List = [pickup,setoff]
      else:
         oname = "OVERHEAD"
         List = [pickup]
      label = Label(self.master,text=oname,font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED)
      label.grid(row=row,column=col,sticky=E+W)
      col+= 1

      cnt = 0
      for i in List:
         EList = []
         VList = []
         for j in i:
            Var = StringVar()
            VList.append(Var)
            entry = Entry(self.master,textvariable=Var,font=self.font,bg=self.qbgc,fg=self.qfgc,relief=SUNKEN,width=width)
            entry.grid(row=row,column=col,sticky=E+W)
            EList.append(entry)
            Var.set(j)
            col+=1
         if cnt==0:
            if name==0:
               self.objList.append([EList,0,VList,check,oname,"O"])
            else:
               self.objList.append([EList,0,VList,check,oname,"P"])
         else:
            self.objList.append([EList,0,VList,check,oname,"S"])
         cnt += 1

    def DoSave(self):
       status =  self.currentobj.getParam("status").lower()
       if status not in ["building","waiting"]:
          self.currentobj.setParam("status","building")
       self.DoSaveInfo()
       self.DoSaveFinish()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainEditor(win.Editor):
    def __init__(self,objEdit,ctrl,viewer,callwindow):
      if win.Editor.__init__(self,"Train",objEdit,viewer,dbb.TrainList)==0:
         return
      self.ctrl = ctrl
      self.callwindow = callwindow

      if self.newone==1:
         title = "New Train\n at {} ({})".format(ctrl.name,ctrl.objID)
         self.currentobj.headat = ctrl
         if ctrl.yard_link!=0:
            self.currentobj.headat = ctrl.yard_link
      else:
         title = "Train {}".format(self.currentobj.getParam("name"))
         if ctrl!=0:
            title += " at {}".format(ctrl.name)

      self.master.title(title)
      self.defineOptions()
      status = self.currentobj.getParam("status").lower()

      self.ColumnList.append(["Status",10,"LABEL","S",1])
      self.ColumnList.append(["SetStatus",10,"MENU","S",1])
      self.ColumnList.append(["Name",10,"ENTRY","S",3])
      if status in ["active","dwell","called","recrew"]:
         enginemode = "LABEL"
      else:
         enginemode = "MENUENTRY"
      for i in range(int(dbb.TrainSysParms.getV("num_engines_max"))):
         if i==0:
            self.ColumnList.append(["Lead Engine",10,enginemode,"S",3])
         else:
            self.ColumnList.append(["Engine {}".format(i+1),10,enginemode,"S",3])

      if enginemode== "LABEL":
         self.ColumnList.append(["Call Time",18,"ENTRY","I",1])
      else:
         self.ColumnList.append(["Loads",8,"ENTRY","I",1])
         self.ColumnList.append(["Empties",8,"ENTRY","I",1])
         self.ColumnList.append(["Length",8,"ENTRY","I",1])
         self.ColumnList.append(["Tons",8,"ENTRY","I",1])
         self.ColumnList.append(["Direction",10,"MENU","S",2])
         self.ColumnList.append(["Priority",10,"MENU","S",2])
      self.ColumnList.append(["Conductor",15,"MENU","S",4])
      self.ColumnList.append(["Engineer",15,"MENU","S",4])
      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
      self.DoCheck()

    def defineOptions(self):
      self.b1.menu.delete(0,END)
      self.b1.menu.add_command(label="Check",command=self.DoCheck)
      defineTrainPersonelOptions(self,self.currentobj,self.b1)
##      status = "planning"
##      cnt = 0
##
##      if self.currentobj!=0:
##         status = self.currentobj.getParam("status").lower()
##         sys.stdout.write("{} status={}\n".format(self.currentobj.getParam("name"),status))
##         
##      sys.stdout.write("TRAINEDITOR status={}\n".format(status))
##      if status in ["tieddown","layeddown","stored"]:
##         self.b1.menu.add_separator()
##         submenu = Menu(self.b1.menu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=0)
##         for i in ["dwell","onduty"]:
##            submenu.add_command(label="set STATUS to {}".format(i.upper()),command=lambda v=i,t=self.currentobj:setTrainStatus(t,v))
##         self.b1.menu.add_cascade(label="Train Options",menu=submenu)
##         cnt += 1
##
##      if status in ["onduty","active"]:
##         self.b1.menu.add_separator()
##         submenu = Menu(self.b1.menu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=0)
##         for i in ["tieddown","layeddown","stored"]:
##            submenu.add_command(label="set STATUS to {}".format(i.upper()),command=lambda v=i,t=self.currentobj:setTrainStatus(t,v))
##            cnt += 1
##         self.b1.menu.add_cascade(label="Train Options",menu=submenu)
##
##         for i in ["conductor","engineer"]:
##            person = self.currentobj.getParam(i)
##            if person not in NULL_DEF:
##               submenu = Menu(self.b1.menu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=0)
##               submenu.add_command(label="Switch to DWELL",command=lambda ty=i,t=self.currentobj,s="DWELL":setTrainPersonelConditions(ty,t,s))
##               submenu.add_command(label="End Tour of Duty",command=lambda ty=i,t=self.currentobj,s="REST":setTrainPersonelConditions(ty,t,s))
##               p = TrainPersonelList.seek(p)
##               if p!= 0:
##                  name = "{} ({})".format(person.getParam("name"),person.getParam("initials"))
##               else:
##                  name = person
##               self.b1.menu.add_cascade(label="{} Options".format(name),menu=submenu)
##               cnt += 1
##      if cnt > 0:
##         self.b1.menu.add_separator()

      self.b1.menu.add_command(label="Save & Exit",command=self.DoSave)
      self.b1.menu.add_command(label="Exit",command=self.DoExit)

    def DoSave(self):
       if self.ctrl!=0:
          if self.newone==1:
             self.currentobj.setParam("status","ONDUTY")
             self.currentobj.setParam("headat",self.ctrl.name)
             self.ctrl.addTrain("LR",self.currentobj,0)
          self.ctrl.modified = 1
       else:
          if self.newone==1:
             self.currentobj.setParam("status","DWELL")
       self.DoSaveInfo()
       self.DoSaveFinish()
       if self.callwindow!=0:
          if self.ctrl!=0:
             self.callwindow.defineObjOptions()
          else:
             self.callwindow.defineObjOptions()

    def DoExit(self):
       if self in win.WindowList:
         win.WindowList.remove(self)
       self.master.destroy()
       if self.callwindow!=0:
          self.callwindow.defineObjOptions()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainCallSetup(win.Editor):
    def __init__(self,objEdit,viewer):
      if win.Editor.__init__(self,"Calltime Setup",objEdit,viewer,dbb.TrainList)==0:
         return
      status =  self.currentobj.getParam("status").lower()
      route = dbb.TrainRouteList.seek(objEdit.getParam("route"))
      if route == 0:
         menumode = "MENU"
      else:
         menumode = "LABEL"

      entrymode = "ENTRY"
      menumodeP = "MENU"
      if status in ["active","dwell"]:
         entrymodeP = menumode = menumodeP = "LABEL"

      cmode = emode = menumodeP
      if self.currentobj.getParam("conductor") in NULL_DEF:
         cmode = "MENU"
      if self.currentobj.getParam("engineer") in NULL_DEF:
         emode = "MENU"
         
      self.ColumnList.append(["Status",8,"LABEL","S",-1])
      self.ColumnList.append(["Name",10,"LABEL","S",3])
      self.ColumnList.append(["Route",15,"LABEL","S",-1])
      self.ColumnList.append(["Direction",10,menumode,"S",2])
      self.ColumnList.append(["Priority",10,menumode,"S",2])
      self.ColumnList.append(["Call Time",18,entrymode,"S",10])
      self.ColumnList.append(["Conductor",15,cmode,"S",4])
      self.ColumnList.append(["Engineer",15,emode,"S",4])

      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
      self.DoCheck()

    def defineOptions(self):
      self.b1.menu.delete(0,END)
      self.b1.menu.add_command(label="Check",command=self.DoCheck)
      self.b1.menu.add_command(label="Calculate CallTime",command=self.DoCalcCallTime)
      self.b1.menu.add_separator()
      self.b1.menu.add_command(label="Save & Exit",command=self.DoSave)
      self.b1.menu.add_command(label="Exit",command=self.DoExit)

    def DoCalcCallTime(self):
       if self.currentobj!=0:
          self.currentobj.setParam("calltime","NONE")
          self.update()
          
    def DoSave(self):
       status =  self.currentobj.getParam("status").lower()
       self.currentobj.setParam("status","CALLED")
       self.DoSaveInfo()
       self.DoSaveFinish()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainRouteViewer(win.Viewer):
    def __init__(self):
      if win.Viewer.__init__(self,"Train Route Manager (Draft)","trainrouteprofile","N") == 0:
         return

      self.MainList = dbb.TrainRouteList
      self.ColumnList.append([["Name"],10,"S"])
      self.ColumnList.append([["Direction"],10,"S"])
      self.ColumnList.append([["Priority"],10,"S"])
      self.ColumnList.append([["Route Points"],30,"S"])

      self.viewLists(16,3,0)
      self.updateLists()

    def DoAdd(self):
       TrainRouteEditor(0,self)

    def DoModify(self):
       if self.currentobj==0:
          return
       TrainRouteEditor(self.currentobj,self)

    def DoReverseAdd(self):
      route = self.currentobj.getParam("route")
      newroute = self.MainList.find("R{}".format(self.currentobj.name))
      ndir = pdir = self.currentobj.getParam("direction")
      if pdir == "South":
         ndir = "North"
      elif pdir == "North":
         ndir = "South"
      newroute.setParam("priority",self.currentobj.getParam("priority"))
      newroute.setParam("direction",ndir)
      
      if route not in NULL_DEF:
         nval = route.translate(str.maketrans(",", " "))
         rstruct = nval.split()
         mlist = ""
         i = len(rstruct)-1
         while i >= 0:
            if len(mlist) > 0:
               mlist += ","
            mlist += rstruct[i]
            i -= 1
         newroute.setParam("route",mlist)
      TrainRouteEditor(newroute,self)
       
    def defineOptions(self):
      self.b1.menu.delete(0,END)
      if self.admin_mode_only==0 or dbb.admin_mode==1:
         self.b1.menu.add_command(label="Create",command=self.DoAdd)
         if self.currentobj!=0:
            self.b1.menu.add_command(label="Create Reverse",command=self.DoReverseAdd)
            if self.currentobj.IsInUse(0)==0:
               self.b1.menu.add_command(label="Delete",command=self.DoDelete)
            self.b1.menu.add_command(label="Modify",command=self.DoModify)
         self.b1.menu.add_separator()
      self.b1.menu.add_command(label="Exit",command=self.DoExit)

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class TrainRouteEditor(win.Editor):
    def __init__(self,objEdit,viewer):
      if win.Editor.__init__(self,"Train Route",objEdit,viewer,dbb.TrainRouteList)==0:
         return

      self.ColumnList.append(["Name",10,"ENTRY","S",3])
      self.ColumnList.append(["Direction",10,"MENU","S",1])
      self.ColumnList.append(["Priority",10,"MENU","S",1])
      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1

      self.DoCheck()

      row=0
      col = 4
      rt_SP = []
      route = self.currentobj.getParam("route")
      if route not in NULL_DEF:
         nval = route.translate(str.maketrans(",", " "))
         rstruct = nval.split()
         for i in rstruct:
            ctrl = dbb.CtrlObjList.IDseek(i)
            if ctrl!=0:
               rt_SP.append(ctrl.getRouteName())
            else:
               sys.stdout.write("Cannot find CtrlObj {}\n".format(i))
      
      av_SP = []
      for i in lm.RoutablePntNames:
         if i not in rt_SP:
            av_SP.append(i)

      self.listAvailable = win.WinScrollList(self,3,"Available Route\nStop Points",30,10,row,col,av_SP)
      self.listAvailable.ListB.bind("<Double-Button-1>", self.addSP)

      self.listAssigned = win.WinScrollList(self,3,"Route\nStop Points",30,10,row,col+3,rt_SP)
      self.listAssigned.ListB.bind("<Double-Button-1>", self.deleteSP)

    def addSP(self,LB):
        self.listAvailable.setVar(LB)
        pnt = self.listAvailable.getVal()
        self.listAvailable.deleteVal(pnt)
        self.listAssigned.addVal(pnt)

    def deleteSP(self,LB):
        self.listAssigned.setVar(LB)
        pnt = self.listAssigned.getVal()
        self.listAssigned.deleteVal(pnt)
        self.listAvailable.addVal(pnt)

    def DoSave(self):
       mlist = ""
       for i in self.listAssigned.List:
          if ":" in i:
             nval = i.translate(str.maketrans(":", " "))
             rstruct = nval.split()
             ctrl = dbb.CtrlObjList.seek(rstruct[1])
          else:
             ctrl = dbb.CtrlObjList.seek(i)

          if ctrl!=0:
             if len(mlist) > 0:
               mlist += ","
             mlist += ctrl.objID
          else:
             sys.stdout.write("Cannot find {}\n".format(i))

       self.currentobj.setParam("route",mlist)
       self.DoSaveInfo()
       self.DoSaveFinish()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class QuotaContractViewer(win.Viewer):
    def __init__(self):
      if win.Viewer.__init__(self,"Quota Contract Manager (Draft)","quotacontractprofile","N") == 0:
         return

      self.MainList = dbb.QuotaContractList
      self.ColumnList.append([["Name"],10,"S"])

      self.ColumnList.append([["Deadline"],15,"S"])
      self.ColumnList.append([["Supplied"],10,"I"])
      self.ColumnList.append([["Quota"],10,"I"])
      self.ColumnList.append([["Quota Rate"],10,"I"])
      self.ColumnList.append([["Source"],10,"S"])
      self.ColumnList.append([["Destination"],10,"S"])

      self.ColumnList.append([["Penality Cost"],10,"I"])
      self.ColumnList.append([["Penalities"],10,"I"])

      self.viewLists(16,3,0)
      self.updateLists()

    def DoAdd(self):
       QuotaContractEditor(0,self)
       
    def DoModify(self):
       if self.currentobj==0:
          return
       QuotaContractEditor(self.currentobj,self)


# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class QuotaContractEditor(win.Editor):
    def __init__(self,objEdit,viewer):
      if win.Editor.__init__(self,"Quota Contract",objEdit,viewer,dbb.TrainRouteList)==0:
         return

      self.ColumnList.append(["Name",10,"ENTRY","S",3])
      self.ColumnList.append(["Supplied",10,"ENTRY","S",1])
      self.ColumnList.append(["Quota",10,"ENTRY","I",1])
      self.ColumnList.append(["Source",10,"MENU","S",1])
      self.ColumnList.append(["Destination",10,"MENU","S",1])
      self.ColumnList.append(["Deadline",10,"ENTRY","S",1])
      self.ColumnList.append(["Deadline Rate",10,"ENTRY","S",1])
      self.ColumnList.append(["Deadline Unit",10,"MENU","S",1])
      self.ColumnList.append(["Penality Cost",10,"ENTRY","I",1])

      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
##    def DoSave(self):
##       mlist = ""
##       for i in self.listAssigned.List:
##          if ":" in i:
##             nval = i.translate(str.maketrans(":", " "))
##             rstruct = nval.split()
##             ctrl = CtrlObjList.seek(rstruct[1:end])
##          else:
##             ctrl = CtrlObjList.seek(i)
##
##          if ctrl!=0:
##             if len(mlist) > 0:
##               mlist += ","
##             mlist += ctrl.objID
##          else:
##             sys.stdout.write("Cannot find {}\n".format(i))
##
##       self.currentobj.setParam("route",mlist)
##       self.DoSaveInfo()
##       self.DoSaveFinish()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class EngineViewer(win.Viewer):
    def __init__(self):
      if win.Viewer.__init__(self,"Engine Manager (Draft)","engineprofile","Y") == 0:
         return

      self.MainList = dbb.EngineList

      self.ColumnList.append([["Name"],10,"S"])
      self.ColumnList.append([["Visible"],5,"S"])
      self.ColumnList.append([["Type","Prime"],10,"S"])
      self.ColumnList.append([["Axles"],8,"S"])
      self.ColumnList.append([["Hp","Turbo"],6,"S"])
      self.ColumnList.append([["Built","Rebuilt"],10,"S"])
      self.ColumnList.append([["Train"],10,"S"])

      self.viewLists(16,3,0)
      self.updateLists()

    def defineOptions(self):
      self.b1.menu.delete(0,END)
      if dbb.admin_mode==1:
         self.b1.menu.add_command(label="Create",command=self.DoAdd)
         if self.currentobj!=0:
            if self.currentobj.IsInUse(0)==0:
               self.b1.menu.add_command(label="Delete",command=self.DoDelete)
            self.b1.menu.add_command(label="Modify",command=self.DoModify)
            self.b1.menu.add_separator()
            if self.currentobj.getParam("visible").lower() in ["n","no"]:
               self.b1.menu.add_command(label="set Visible=Y",command=lambda parm="Visible",value="Y": self.setParam(parm,value))
            else:
               self.b1.menu.add_command(label="set Visible=N",command=lambda parm="Visible",value="N": self.setParam(parm,value))
         self.b1.menu.add_separator()
      self.b1.menu.add_command(label="Exit",command=self.DoExit)

    def DoAdd(self):
       EngineEditor(0,self)

    def DoModify(self):
       if self.currentobj==0:
          return
       EngineEditor(self.currentobj,self)


# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class EngineEditor(win.Editor):
    def __init__(self,objEdit,viewer):
      if win.Editor.__init__(self,"Engine",objEdit,viewer,dbb.EngineList)==0:
         return

      if self.newone==1:
         self.ColumnList.append(["Name",10,"ENTRY","S",3])
      else:
         self.ColumnList.append(["Name",10,"LABEL","S",3])

      self.ColumnList.append(["Visible",5,"MENU","S",1])
      self.ColumnList.append(["Type",10,"ENTRY","S",3])
      self.ColumnList.append(["Prime",8,"ENTRY","S",3])
      self.ColumnList.append(["Turbo",2,"MENU","S",1])
      self.ColumnList.append(["Hp",6,"ENTRY","I",2])
      self.ColumnList.append(["Axles",6,"ENTRY","S",3])
      self.ColumnList.append(["Built",6,"ENTRY","S",3])
      self.ColumnList.append(["Rebuilt",10,"ENTRY","S",-1])

      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
      self.DoCheck()
# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class PVLOEditor(win.Base):
    def __init__(self,ctrl):
      if win.Base.__init__(self,"pvlo_editor","PVLO {}".format(ctrl.name))==0:
         return 0
      self.ctrl = ctrl
      row = 0
      self.Menu = win.WinMenu(self,"Options",-1,row,0)
      self.Menu.menub.grid(sticky=E+W)

      first = 1
      desc = ""
      for m in self.ctrl.link_list:
        name = m.objID
        if len(m.name) > 0:
           name = m.name
        if first == 0:
           desc += "\n"
        desc += "Linked to {}".format(name)
      self.b0 = win.WinLabel(self,desc ,-1,row,1)
      self.b0.label.grid(columnspan=4)
      self.Menu.menu.add_command(label="Save & Exit",command=self.DoSave)

      self.Menu.menu.add_command(label="Exit",command=self.DoExit)
      win.WinRowFeed(self,row+1)
      row+=2

      self.trainID = win.WinValueEntry(self,"Train ID",10,row,0)
      self.trainID.entry.grid(columnspan=4)
      self.trainID.setVal(self.ctrl.getParam("train"))
         
      row+=1
      self.EngineNum = win.WinValueEntry(self,"Lead Engine",10,row,0)
      self.EngineNum.entry.grid(columnspan=4)
      self.EngineNum.setVal(self.ctrl.getParam("leadengine"))
      row+=1
      
      self.Hopper = win.WinValueEntry(self,"Hopper Type",10,row,0)
      self.Hopper.entry.grid(columnspan=4)
      self.Hopper.setVal(self.ctrl.getParam("hoppertype"))

      win.WinRowFeed(self,row+1)
      row+=2

      BAList = [ "Active", "Tied Down", "Layed Down", "Stored" ]
      
      win.WinLabel(self,"Mode",-1,row,0).cconfig(FLAT,E)
      self.Mode = win.WinList(self,"radio",row,1,-1,-1,1,6,BAList,0)
      self.Mode.setVal(self.ctrl.getParam("mode"))
      for i in BAList:
         self.Mode.setcolor(i,dbb.ColorList.getParam(self.getMode(i)))

    def getMode(self,name):
       nname = "PVLO_{}".format(name.translate(str.maketrans(" ", "_")).upper())
       return nname

    def DoSave(self):
       trainID = self.trainID.getVal()
       if trainID in ["0","none",""]:
          self.ctrl.setParam("train","0")
          self.ctrl.setParam("leadengine","0")
          self.ctrl.setParam("hoppertype","0")
          self.ctrl.setParam("mode","Idle")
       else:
          self.ctrl.setParam("train",trainID)
          self.ctrl.setParam("leadengine",self.EngineNum.getVal())
          self.ctrl.setParam("hoppertype",self.Hopper.getVal())
          self.ctrl.setParam("mode",self.Mode.getVal())
       self.ctrl.modified = 1
       self.DoExit()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class IODeviceViewer(win.Viewer):
    def __init__(self):
      if win.Viewer.__init__(self,"IO Devices with Auto Assignment Constraints","portalgmprofile","N") == 0:
         return

      self.MainList = DevicePortAlgmList
      self.ColumnList.append([["Name"],10,"S"])
      self.ColumnList.append([["Port Size"],10,"I"])
      self.ColumnList.append([["Input Count","Output Count"],10,"I"])
      self.ColumnList.append([["Input Spares","Output Spares"],10,"I"])
      self.ColumnList.append([["Input Group Align","Output Group Align"],10,"S"])
      self.ColumnList.append([["Cluster Object within"],10,"S"])

      self.viewLists(16,3,0)
      self.updateLists()

    def defineOptions(self):
      self.b1.menu.delete(0,END)
      if dbb.admin_mode==1:
         if self.currentobj!=0:
            self.b1.menu.add_command(label="Modify",command=self.DoModify)
            self.b1.menu.add_separator()
      self.b1.menu.add_command(label="Exit",command=self.close)

    def DoModify(self):
       PortAlgmEditor(self.currentobj,self)
# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class PortAlgmEditor(win.Editor):
    def __init__(self,objEdit,viewer):
      if win.Editor.__init__(self,"Port Algorithm",objEdit,viewer,dbb.DevicePortAlgmList)==0:
         return

      self.ColumnList.append(["Name",10,"LABEL","S",-1])
      self.ColumnList.append(["Port Size",10,"LABEL","I",-1])

      self.ColumnList.append(["Input Count",10,"LABEL","I",-1])
      self.ColumnList.append(["Input Spares",15,"ENTRY","I",1])
      self.ColumnList.append(["Input Group Align",15,"MENU","S",1])

      self.ColumnList.append(["Output Count",10,"LABEL","I",-1])
      self.ColumnList.append(["Output Spares",15,"ENTRY","I",1])
      self.ColumnList.append(["Output Group Align",15,"MENU","S",1])
      self.ColumnList.append(["Cluster Object within ",15,"MENU","S",1])
      row = 2
      col = 0
      for c in self.ColumnList:
         self.setupEditItem(c,row,col)
         row += 1
      self.DoCheck()

class ParameterEditor(win.Base):
    def __init__(self):
         if win.Base.__init__(self,"parmeditor","Parameter Editor")==0:
            return

         self.size = 12
         self.myList = []
         myNames = []
         for t in dbb.TrainSysParms.objList:
            name = t[0]
            myNames.append(name)

         myNames.sort(key=str.lower)
         for n in myNames:
            for l in dbb.TrainSysParms.objList:
               if n == l[0]:
                  self.myList.append(l)
                  break
         row = 0
         col = 0
         self.currentParm = 0
         columnspan=4
         self.f1 = win.WinMenu(self,"Options",-1,0,1)
         self.f1.menub.grid(sticky=W) #columnspan=columnspan,
         if dbb.admin_mode==1:
            self.f1.menu.add_command(label="Save",command=self.saveDefaults)
            self.f1.menu.add_separator()
         self.f1.menu.add_command(label="Exit",command=self.DoExit)
         row += 1

         self.ObjList = []
         self.index = -1
         row = 1

         tick=32
         length = 300
         ssize=500

         col  = 0
         divide = 20
         ColumnList = []
         ColumnList.append(["Parameter",50,"V"])
         vtype = "E"
         if dbb.admin_mode == 0:
            vtype = "V"

         ColumnList.append(["Value",6,vtype])
            
         yScroll = Scrollbar(self.master)
         yScroll.grid(row=row+2,column=col,sticky=N+S,rowspan=divide)
         col+=1
         for c in ColumnList:
           mylabel = Label(self.master,text=c[0],font=self.font,bg=self.bgc,fg=self.fgc,width=c[1],relief=RAISED) #,relief=RAISED
           mylabel.grid(row=row,column=col,sticky=E+W)
           if c[2]=="V":
              myvalue = Label(self.master,text="",font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED,width=c[1])
           else:
              myvalue = Entry(self.master,font=self.font,bg=self.qbgc,fg=self.qfgc,relief=RAISED,width=c[1])
           myvalue.grid(row=row+1,column=col,sticky=E+W)

           xScroll = Scrollbar(self.master,orient=HORIZONTAL)

           mylist = Listbox(self.master,selectmode=SINGLE,font=self.font,bg=self.qbgc,fg=self.qfgc,width=c[1],
                            height=divide,relief=FLAT,yscrollcommand=yScroll.set,xscrollcommand=xScroll.set)
           xScroll.config(command=mylist.xview,bg=self.bgc)
           mylist.bind("<Double-Button-1>", self.getSelect)
           mylist.grid(row=row+2,column=col,rowspan=divide,sticky=E+W+N+S)
           xScroll.grid(row=row+divide+2,column=col,sticky=E+W)
           self.ObjList.append([myvalue,mylist,c[2]])
           col += 1
         yScroll.config(command=self.yview)
         self.updateList()
            
    def updateParm(self):

       if self.currentParm != 0 and dbb.admin_mode==1:
          name = self.currentParm[0].upper()
          value = self.ObjList[1][0].get()
          self.myList[self.index] = [name,value]
#          sys.stdout.write("updateParm {}={}\n".format(name,value))
          dbb.TrainSysParms.setV(name.lower(),value)
          self.updateList()

    def saveDefaults(self):
       self.updateParm()
       dbb.SaveBaseDefs()

    def updateList(self):
       self.ObjList[0][1].delete(0,END)
       self.ObjList[1][1].delete(0,END)
       cnt = 0
       for c in self.myList:
          self.ObjList[0][1].insert(cnt,c[0].upper())
          self.ObjList[1][1].insert(cnt,c[1])
          cnt+=1
       
    def getSelect(self,event):
        self.updateParm()
        index = event.widget.nearest(event.y)
        self.currentParm = self.myList[index]
        self.index = index
#        self.ObjList[0][0].delete(0,END)
        self.ObjList[0][0].config(text=self.currentParm[0].upper())
        if dbb.admin_mode==1:
           self.ObjList[1][0].delete(0,END)
           self.ObjList[1][0].insert(0,self.currentParm[1])
        else:
           self.ObjList[1][0].config(text=self.currentParm[1])

    def yview(self,*args):
      for l in self.ObjList:
         l[1].yview(*args)

