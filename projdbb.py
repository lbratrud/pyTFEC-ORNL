# projdbb.py
#
# Define all ObjLists and how they relate to each other.
#
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@yahoo.com)
# Project started December 2013 for Gary W. Turner
# for implementing CTC system based on C/MRI hardware platform

import math
import sys
import string
import sys
import os
import errwarn

from projbase import *
import projdb as db
import projtime as mtime
import projpath as mpath
import comdbb as cdb

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------

cdb.ProjParams.add("timeincr",20)
cdb.ProjParams.add("repeat",-1)

cdb.ColorList.addwDesc("RS485DEVICE_READY","#80ff80","RS485DEVICE")
cdb.ColorList.addwDesc("RS485DEVICE_NOTREADY","#ff8080","RS485DEVICE")

cdb.ColorList.addwDesc("RS485DEVICE_ERROR","#ff0000","RS485DEVICE")
cdb.ColorList.addwDesc("RS485DEVICE_WARN","#ffff7f","RS485DEVICE")
cdb.ColorList.addwDesc("RS485DEVICE_INFO","#8080ff","RS485DEVICE")
cdb.ColorList.addwDesc("RS485DEVICE_GOOD","#80ff80","RS485DEVICE")

cdb.ColorList.addwDesc("PLOT1","#000000","PLOT")
cdb.ColorList.addwDesc("PLOT2","#0000ff","PLOT")
cdb.ColorList.addwDesc("PLOT3","#00ff00","PLOT")
cdb.ColorList.addwDesc("PLOT4","#ff0000","PLOT")
cdb.ColorList.addwDesc("PLOT5","#ffff00","PLOT")
cdb.ColorList.addwDesc("PLOT7","#00ffff","PLOT")
cdb.ColorList.addwDesc("PLOT8","#808080","PLOT")
cdb.ColorList.addwDesc("PLOT6","#008000","PLOT")
 

def SaveSys():
    f = mpath.openFile("sys","sys","basedef",'w')
    if f==-1:
       return
    ProjParams.saveList(f,"setparm")
    ColorList.saveList(f,"colorsetup")
    f.close()

def SaveBaseDefs():
    return

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
RS485DeviceWords = ["name string","commname string main","address int 0","timeradd250us int 3", \
                   "setvoltage int 0","setvoltage1 int 100","setvoltage2 int 200",\
                   "maxpospwdelta int 5","maxnegpwdelta int -100",\
                   "frequencykhz float 39.06","dutycyclepercent float 2.0","setdutycycle1 float 10.0","setdutycycle2 float 20.0",
                   "avgsasminmax int 0","dutycycle1 int 0","dutycycle2 int 40.00",\
                   "maxvoltage int 4095", "version dynamic string none", \
                   "v0min dynamic int -1","v0max dynamic int -1","i0min dynamic int -1","i0max dynamic int -1",\
                   "numsamples int 7", "method string 0","selectmonitor1 int 0","selectmonitor2 int 1",
                   "gettemp int 0","hvfitparms string none,none",\
                   "v0 dynamic int -1","i0 dynamic  int -1","t0 dynamic int -1","pw dynamic int -1","pp dynamic int -1","fs dynamic int -1","fd dynamic int -1",\
                   "rspcode string dynamic -1","rsptype string dynamic -1","status string dynamic none",\
                   "d0 string 0.05","d1 string 0.1","d2 string 0.05","c1 string 0.3","c2 string -0.5","pw2v string 16.0",\
                  ]

HVFQCTRL_commands  = [["ERROR",0x28],["ACKNOWLEDGE",0x29], ["VERSION",0x2a],["STATUS",0x01],["WRITECONFIG",0x2c],["READCONFIG",0x2d],["GETDATA",0x2b],["VIMINMAX",0x2e]]
HVFQCTRL_commands += [["DO_START",0x2f],["DO_STOP",0x30],["WRITE_IIRPARMS",0x31]]
HVFQCTRL_commands += [["RESET",0x00],["ENABLEHV",0x04],["DISABLEHV",0x05],["SETPULSEWIDTH",0x06],["GETPULSEWIDTH",0x07],["SETPULSEPERIOD",0x08],["GETPULSEPERIOD",0x09],["SETPULSEWIDTHPERIOD",0x0a]]
HVFQCTRL_commands += [["ENABLEPWM",0x0b],["DISABLEPWM",0x0c],["ENABLEDRIVE",0x0d],["DISABLEDRIVE",0x0e],["STARTADC",0x0f],["STOPADC",0x10]]
HVFQCTRL_commands += [["SETSAMPLES",0x11],["GETV0",0x12],["GETTEMP",0x13],["TURNON",0x14],["TURNOFF",0x15],["RESETFAULT",0x16]]
HVFQCTRL_commands += [["GETI0",0x17],["SETIGAIN",0x18],["SETPGAIN",0x19],["WRITECONFIG1",0x2c],["WRITECONFIG2",0x2c]]
HVFQCTRL_commands += [["SETPW1",0x06],["SETPW2",0x06],["SWEEPPW",0x06],["SWEEPFQDC",-1]]
def FindCommand(device,command):
   global HVFQCTRL_commands

   for i in HVFQCTRL_commands:
      if i[0]==command:
         return i
   return 0

def FindCode(device,code):
   global HVFQCTRL_commands

   for i in HVFQCTRL_commands:
      if i[1]==code:
         return i
   return 0


class RS485Device(db.ParamObj):
   def __init__(self,name,MainList):
      db.ParamObj.__init__(self,name,MainList)
      self.Comm = 0
      self.line = 0
      self.writable = 0
      self.done_setup = 0
      self.JobIdList = []
      self.jobitrcnt = 0
      self.sweepPW = 0
      self.MyStatus = -1
      
   def addJobID(self,jobID):
      if jobID not in self.JobIdList:
         self.JobIdList.append(jobID)

   def deleteJobID(self,jobID):
      while jobID in self.JobIdList:
         self.JobIdList.remove(jobID)

   def Close(self):
      return
   
   def DataRead(self,filename):
      if os.path.isfile(filename)==False:
         sys.stdout.write("Cannot open file {}\n".format(filename))
         return
      else:
         sys.stdout.write("Opening file {}\n".format(filename))

      self.DataSet = []
      f = open(filename,'r')
      for line in f:
   #      sys.stdout.write("line={}".format(line))
         nline = line.translate(str.maketrans("\n,=", "   "))
         words = nline.split()
         if len(words) > 0:
             cmd = words[0].lower()
             if cmd not in ["#"] and len(words) >= 10:
                v = float(words[0])
                i = float(words[1])
                vfb = int(words[2])
                ifb = int(words[3])
                self.DataSet.append([v,i,vfb,ifb])
      f.close()

#   def Vfit(self,mode):
#      if len(self.DataSet) == 0:
#         return
#      self.VFitParameters = [0,1,0,0]
#      
#
#   def CalcError(self,datapnt,fit,paramDef):
#      if paramDef==1:
#         error = i[0]
#      else:
#         error = i[1]
#      error -= fit[0] + fit[1]*datapnt[2] + fit[2]*datapnt[3] + fit[3]*datapnt[2]*datapnt[3]
#      return error
#
#   def CalcTotalError(self,fit,paramDef):
#      terror = 0
#      for i in self.DataSet:
#         error = self.CalcError(i,fit,paramDef)
#         terror += error*error
#      return math.sqrt(terror)
#
#
#   def LevMarAlg(self,fit,paramDef,calcMode):
#       BestError = self.CalcTotalError(fit,paramDef)
#       i = 0
#       mlambda = 5
#       
#       while i < 10:
#           nfit = [0,0,0,0]
#           for i in range(4):
#               nfit[i] = fit[i]
#
#           self.CalcMtrx(nfit,paramDef,calcMode,mlambda)
#           Error = self.CalcTotalError(nfit,paramDef)
#           if Error < BestError:
#               BestError = Error
#               for i in range(4):
#                  fit[i] = nfit[i]
#               mlambda *= 0.5
#           else:
#               mlambda *= 2
#           i+=1
#
#   def CalcMtrx(self,fit,paramDef,calcMode,mlambda):
#      if calcMode==0:
#         B = [0,0]
#         A = [[0,0],[0,0]]
#         size = 2
#      else:
#         B = [0,0,0,0]
#         A = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
#         size = 4
#
#      for i in self.DataSet:
#         error = self.CalcError(i,fit,paramDef)
#         if calcMode==0:
#            if paramDef==1:
#               J = [1,i[2]]
#            else:
#               J = [1,i[3]]
#         else:
#            J = [1,i[2],i[3],i[2]*i[3]]
#
#         for j in range(size):
#            B[j] += error*J[j]
#            for k in range(size):
#               A[j][k] += J[j]*J[k]
#
#      for j in range(size):
#         A[j][j] = (1+mlambda)*A[j][j]
#         
#      delta = np.linalg.solve(A,B)
#      sys.stdout.write("delta={}\n".format(delta))
#      fit += delta   
#      sys.stdout.write("fit={}\n".format(delta))

   def Setup(self):
      if self.Comm==0:
         self.Comm = ComPortList.seek(self.getParam("commname"))

      if self.done_setup==1:
         return
      
      if self.Comm!=0:
         self.Comm.addDev(self)
         sys.stdout.write("RS485Device {} connected to COMM={}::{}\n".format(self.name,self.Comm.name,self.Comm.getParam("comport")))
         self.sendCmd("VERSION")
         self.sendCmd("WRITECONFIG")
         self.sendCmd("WRITE_IIRPARMS")
         self.sendCmd("STATUS")
         self.done_setup = 1
      else:
         sys.stdout.write("RS485Device {} cannot be connected to COMM={}\n".format(self.name,self.getParam("commname")))
         for i in ComPortList.MyList:
            sys.stdout.write("=> {}\n".format(i.name))

##  RESET_INTERFACE,	/*!<  0 Reset the MICRO */
##  REQUEST_STATUS,		/*!<  1 Request the status of the MICRO */
##  CLEAR_ERRORS,		/*!<  2 Clear the error buffer */
##  GET_LAST_ERROR,		/*!<  3 Gets the last error code */
##  ENABLE_HV,			/*!<  4 Enable the High Voltage main control */
##  DISABLE_HV,			/*!<  5 Disable the High Voltage main control */
##  SET_PULSEWIDTH,		/*!<  6 Set the DPOT which sets the pulsewidth */
##  GET_PULSEWIDTH,		/*!<  7 Get the current pulsewidth DPOT value */
##  SET_PULSEFREQ, 		/*!<  8 Set the DPOT which sets the frequency */
##  GET_PULSEFREQ, 		/*!<  9 Get the current freq DPOT value */
##  SET_ALL,			/*!< 10 Set Both the DPOTS... */
##  ENABLE_PWM_REG,		/*!< 11 Turn on the Regulator which powers the PWM */
##  DISABLE_PWM_REG, 	/*!< 12 Turn off the Regulator which powers the PWM */
##  ENABLE_DRIVE,		/*!< 13 Enable the Drive Enable for the PWM */
##  DISABLE_DRIVE,		/*!< 14 Disable the Drive Enable for the PWM */
##
##  START_ADC,			/*!< 15 Start the ADC Converting */
##  STOP_ADC,			/*!< 16 Stop the ADC from Converting */
##  CHANGE_NUM_SAMPLES,	/*!< 17 Change the number of samples that are averaged */
##  GET_V0,				/*!< 18 Get the current V0 Reading */
##  GET_TEMP,			/*!< 19 Get the current Temperature Reading */
##
##  TURN_ON,			/*!< 20 Turn the high voltage on to a particular voltage */
##  TURN_OFF,			/*!< 21 Turn the high voltage system off */
##  RESET_FAULT,		/*!< 22 Reset the system when a fault occurs */
##  GET_I0,				/*!< 23 Get the current V0 Reading */
##  SET_I_GAIN,			/*!< 24 Set the integral gain parameter */
##  SET_P_GAIN,			/*!< 25 Set the proportional gain parameter */
##  ERROR=40,			/*!< 40 Invalid Command Error Code */
##  ACKNOWLEDGE=41,		/*!< 41 Acknowledge for a command */
##  VERSION=42,			/*!< 42 Version information for the code */
##  GET_DATA=43,		/*!< 43 Get all data at once */
##  WRITE_CNFG=44,		/*!< 44 Read Config */
##  READ_CNFG=45,		/*!< 45 Read Config */
##  NULL_COMMAND		/*!< Null \b<Reserved>; MUST be the last command in the list.\n */

   def getInt(self,value):
      for i in value:
         if i=='-':
            pass
         elif i > '9' or i < '0':
            return "null"
      return int(value)

   def getFloat(self,value):
      for i in value:
         if i=='.' or i=='-':
            pass
         elif i > '9' or i < '0':
            return "null"
      return float(value)

   def newV(self,param,value):
      self.setV(param,value)
#      sys.stdout.write("Updating {}={}\n".format(param,value))
      
   def getHVParams(self):
       fit = self.getParam("hvfitparms").translate(str.maketrans("", "")).split()
       if len(fit) != 2 or fit[0]=="none" or fit[1]=="none":
          return [-75,0.08]
       return [float(fit[0]),float(fit[1])]

   def getHVEstimate(self):
       fit = self.getHVParams()
       v0 = self.getParam("v0")
       hv = int(v0)*fit[1]+fit[0]
       if hv < 0:
          hv = 0
       return hv

   def getHvColor(self):
       hv = self.getHVEstimate()      
       if hv < 160:
          hvr = hv
          if hvr < 0:
              hvr = 0
          return "#00{:02x}00".format(255-int(hvr))
       return "#ff0000"

   def getHVMinMax(self):
       fit = self.getHVParams()
       return [0,int(fit[1]*4095+fit[0])]

   def goodConfig(self):
      good = 1
      for i in ["timeradd250us","setvoltage","maxvoltage"]:
         v = self.getInt(self.getParam(i))
         if v=="null" or v < 0 or v > 4095:
            self.newV(i,"4095")
            good = 0

      v = self.getInt(self.getParam("maxpospwdelta"))
      if v=="null" or v < 0 or v > 127:
         self.newV(i,"127")
         good = 0
      v = self.getInt(self.getParam("maxnegpwdelta"))
      if v=="null" or v > 0 or v < -127:
         self.newV(i,"-127")
         good = 0

      for i in [["unscaledfactor",0.0,50.0],["pw2v",1.0,2000],["frequencykhz",24.0,100.0],["dutycyclepercent",0.0,40.0]]:
         v = self.getFloat(self.getParam(i[0]))
         if v=="null" or v < i[1]:
            self.newV(i[0],"{}".format(i[1]))
            good = 0
         elif v > i[2]:
            self.newV(i[0],"{}".format(i[2]))
            good = 0
      for i in ["c1","c2","d0","d1","d2"]:
         v = self.getFloat(self.getParam(i))
         if v=="null" or v < -10000:
            self.newV(i,"-1.0")
            good = 0
         elif v > 10000:
            self.newV(i,"1.0")
            good = 0
      
      return good

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------
   def CalcV0(self,pwm):
      return float(self.getV("pw2v"))*pwm

   def doOldCalc(self,doprint):
      v0 = self.CalcV0(self.oldoutputPWM)
      self.oldsystemStates[0] = ((-557*self.oldsystemStatesPrev[1])/ 1024)
      self.oldsystemStates[1] = self.oldsystemStatesPrev[0] + ((1571*self.oldsystemStatesPrev[1]) / 1024)
      self.oldsystemStates[0] = self.oldsystemStates[0] + self.oldoutputPWM + ((813 * v0) / 1024)
      self.oldsystemStates[1] = self.oldsystemStates[1] - ((689 * v0)/ 1024)
      self.oldsystemStatesPrev[0] = self.oldsystemStates[0]
      self.oldsystemStatesPrev[1] = self.oldsystemStates[1]

      estimatedOutput = (496 * self.oldsystemStates[0] + 304 * self.oldsystemStates[1]) / 1024
      outputError = self.voltageSetting - estimatedOutput
      self.integralError += 4*(outputError+self.outputErrorPrev)
      if self.integralError > 262144:
         self.integralError = 262144

      self.outputErrorPrev = outputError
      integralAction = (self.integralGain * self.integralError) / 1024 / 16;
      proportionalAction = (self.proportionalGain * outputError) / 16; 

      if integralAction + proportionalAction < 0:
         self.oldoutputPWM = self.minWidth;

      elif integralAction + proportionalAction > self.maxWidth:
         self.oldoutputPWM = self.maxWidth;

      else:
         self.oldoutputPWM = integralAction + proportionalAction;

      self.oldoutputPWMprev = self.oldoutputPWM;

      if self.oldoutputPWM > self.maxWidth:
         self.oldoutputPWM = self.maxWidth
      if self.oldoutputPWM < self.minWidth:
         self.oldoutputPWM = self.minWidth
      if doprint==1:
         sys.stdout.write(" OLD(vo=%4d PW=%5d)" % (int(v0),int(self.oldoutputPWM)))
      
   def doCalc(self,itr,doprint,mode):
      v0 = self.CalcV0(self.outputPWM)
      if mode=="float":
         nextState = (self.voltageSetting-v0) /16 - self.c1*self.systemStates[1] - self.c2*self.systemStates[2];
      else:
         nextState = int((self.voltageSetting-v0) * self.scale /16 - self.c1*self.systemStates[1] - self.c2*self.systemStates[2]);
         nextState = int(nextState / self.scale)

      self.systemStates[2] = self.systemStates[1];
      self.systemStates[1] = self.systemStates[0];
      self.systemStates[0] = nextState;

      deltaPWM = self.d0*self.systemStates[0]+self.d1*self.systemStates[1]+self.d2*self.systemStates[2]
      if mode=="float":
         self.outputPWM +=  deltaPWM;
      else:
         self.outputPWM +=  int(deltaPWM / self.scale)

      if self.outputPWM > self.maxWidth:
         self.outputPWM = self.maxWidth
      if self.outputPWM < self.minWidth:
         self.outputPWM = self.minWidth

      if doprint==1:
         sys.stdout.write(" IIR(vo=%4d PW=%5d)" % (int(v0),int(self.outputPWM)))

   def testIIR(self,mode):
      self.scale = 1
      self.integralGain = int(self.getV("igain"))
      self.proportionalGain = int(self.getV("pgain"))
      self.minWidth = int(self.getV("minwidth"))
      self.maxWidth = int(self.getV("maxwidth"))
      self.systemStates = [0,0,0]
      self.oldsystemStates = [0,0]
      self.oldsystemStatesPrev = [0,0]
      self.voltageSetting = 1000
      self.outputPWM = 0
      self.oldoutputPWM = 0
      self.oldoutputPWMprev = 0
      self.integralError = 0
      self.outputErrorPrev = 0

      if mode=="float":
         self.d0=float(self.getV("d0"))
         self.d1=float(self.getV("d1"))
         self.d2=float(self.getV("d2"))
         self.c1=float(self.getV("c1"))
         self.c2=float(self.getV("c2"))
      else:
         self.scale = 512*512
         self.d0=int(self.scale*float(self.getV("d0")))
         self.d1=int(self.scale*float(self.getV("d1")))
         self.d2=int(self.scale*float(self.getV("d2")))
         self.c1=int(self.scale*float(self.getV("c1")))
         self.c2=int(self.scale*float(self.getV("c2")))

      sys.stdout.write("\n======================= PARAMETERS ============================\n")
      for i in ["c1","c2","d0","d1","d2"]:
         sys.stdout.write("  {}={}".format(i,self.getV(i)))
      sys.stdout.write("\n\n  voltageSetting={} (from 0)".format(self.voltageSetting))
      sys.stdout.write(" PW-to-V-ratio={}".format(self.getV("pw2v")))
      sys.stdout.write("\n\n======================= RESPONSE ==============================\n")

      maxitr = 2
      for i in range(1,60):
         self.doCalc(i,0,mode)
         if abs(self.outputPWM * 16 - self.voltageSetting) > 0.025*self.voltageSetting:
            maxitr = i + 5

      self.systemStates = [0,0,0]
      self.outputPWM = 0
      for i in range(1,maxitr):
         sys.stdout.write(" %2d " % (i))
         self.doCalc(i,1,mode)
         self.doOldCalc(1)
         sys.stdout.write("\n")

   def getPeriod(self):
      fq = float(self.getParam("frequencykhz"))
      return int(100000/(fq+0.00001)-1)

   def writeCnfg(self,sV):
      period = self.getPeriod()
      width = int(0.01*period*float(self.getParam("dutycyclepercent")))
      maxwidth = int(0.4*period)

#      sys.stdout.write("writeCnfg period={} width={} maxwidth={}\n".format(period,width,maxwidth))

      self.Comm.TxAddInt(width)
      self.Comm.TxAddInt(period)
      self.Comm.TxAddInt(maxwidth)
      # mode0[7] is open
      mode0 = int(self.getParam("numsamples")) + 0x8*int(self.getParam("gettemp"))+ 0x10*int(self.getParam("method")) + 0x80*int(self.getParam("avgsasminmax"))
      # mode1[7:6] is open
      mode1 = int(self.getParam("selectmonitor1"))+0x8*int(self.getParam("selectmonitor2"))
      # mode2 is open,spare
      mode2 = int(self.getParam("timeradd250us"))

      self.Comm.TxAddByte(mode0)
      self.Comm.TxAddByte(mode1)
      self.Comm.TxAddByte(mode2)
      self.Comm.TxAddInt(sV & 0xfff)      
      self.Comm.TxAddInt(int(self.getParam("maxvoltage")) & 0xfff)

      self.Comm.TxAddByte(int(self.getParam("maxpospwdelta")))
      self.Comm.TxAddByte(abs(int(self.getParam("maxnegpwdelta"))))

   def sendCmd(self,command):
      if self.Comm==0:
         sys.stdout.write("No Comm for {}\n".format(self.name))
         return
      cmddef = FindCommand(self,command.upper())
      self.setParam("rsptype","-")
      self.setParam("rspcode","-")
      if cmddef==0:
         sys.stdout.write("Unknown command {}\n".format(command))
         return

      self.Comm.TxSetup(int(self.getParam("address")))
      self.Comm.TxSetCmd(cmddef[1])
      if cmddef[0] == "WRITE_IIRPARMS":
         scale = 1 << 12
         for i in ["c1","c2","d0","d1","d2"]:
            self.Comm.TxAddLong(int(scale*float(self.getV(i))))
      elif cmddef[0] == "WRITECONFIG":
         self.writeCnfg(int(self.getParam("setvoltage")))
      elif cmddef[0] == "WRITECONFIG1":
         self.writeCnfg(int(self.getParam("setvoltage1")))
      elif cmddef[0] == "WRITECONFIG2":
         self.writeCnfg(int(self.getParam("setvoltage2")))

      elif cmddef[0] == "SETPULSEWIDTH":
         period = self.getPeriod()
         width = int(0.01*period*float(self.getParam("dutycyclepercent")))
         self.Comm.TxAddInt(width)
      elif cmddef[0] == "SETPW1":
         period = self.getPeriod()
         width = int(0.01*period*float(self.getParam("setdutycycle1")))
#         sys.stdout.write("SETPW1={} DC={} PERIOD={}\n".format(width,self.getParam("setdutycycle1"),period))
         self.Comm.TxAddInt(width)
      elif cmddef[0] == "SETPW2":
         period = self.getPeriod()
         width = int(0.01*period*float(self.getParam("setdutycycle2")))
#         sys.stdout.write("SETPW1={} DC={} PERIOD={}\n".format(width,self.getParam("setdutycycle2"),period))
         self.Comm.TxAddInt(width)
      elif cmddef[0] == "SWEEPPW":
         period = self.getPeriod()
         pw1 = int(0.01*period*float(self.getParam("setdutycycle1")))
         pw2 = int(0.01*period*float(self.getParam("setdutycycle2")))
         if pw1 < pw2:
            if self.sweepPW < pw1 or self.sweepPW > pw2:
               self.sweepPW = pw1
            else:
               self.sweepPW += 1
         else:
            if self.sweepPW < pw1 or self.sweepPW > pw2:
               self.sweepPW = pw2
            else:
               self.sweepPW -= 1
         self.Comm.TxAddInt(self.sweepPW)
      elif cmddef[0] == "TURNON":
         self.Comm.TxAddInt(int(self.getParam("setvoltage")) & 0xfff)
#      sys.stdout.write("Sending {}\n".format(cmddef[0]))
      self.Comm.TxSend(cmddef[0].lower())
      
   def getMenuInfoDB(self,param):
      pname = self.ParamTrans(param)
      menuList = []
      if pname=="numsamples":
         for i in [["add","1",0],["add","2",1],["add","4",2],["add","8",3],["add","16",4],["add","32",5],["add","64",6],["add","128",7]]:
            menuList.append(i)
      if pname=="method":
         for i in [["add","IIR (float)",1],["add","IIR (long long)",2]]:
            menuList.append(i)
      if pname in ["selectmonitor1","selectmonitor2"]:
         for i in [["add","0.5ms timer",0],["add","Iteration loop",1],["add","ADC interrupt",2],["add","HV control",3],["add","AD5161 update",4],["add","HV Enanble",5],["add","Spare6",6],["add","Spare7",7]]:
            menuList.append(i)
      if pname in ["gettemp","avgsasminmax"]:
         for i in [["add","OFF",0],["add","ON",1]]:
            menuList.append(i)
      return menuList

   def postMsg(self,log,msg,code):
      if log==0:
         return
      log.insert(self.line,msg)
      bg = 0
      if code=="error":
         bg = ColorList.getParam("RS485DEVICE_ERROR")
      elif code=="warn":
         bg = ColorList.getParam("RS485DEVICE_WARN")
      elif code=="info":
         bg = ColorList.getParam("RS485DEVICE_INFO")
      elif code=="good":
         bg = ColorList.getParam("RS485DEVICE_GOOD")
      elif code!=-1:
         bg = code
      if bg!=0:
         log.itemconfig(self.line,bg=bg,fg="black")
      self.line += 1

   def defineStatus(self,log):
      self.MyStatus = -1
      logRef = 0
      if log!=0:
         log.delete()
         logRef = log.ListB 
      self.line = 0
      status = self.getV("status")
      words = status.translate(str.maketrans(","," ")).split()
      mlen = len(words)

      if status=="none":
         self.postMsg(logRef,"Not Received","warn")
         return ["not connected",ColorList.getParam("RS485DEVICE_NOTREADY")]

      smsg = "StatusRsp="
      for i in words:
         smsg += " "
         smsg += "{}".format(hex(int(i)))

      self.postMsg(logRef,smsg,"info")
      code = ["MICRO FUNCTIONAL ERROR: FIRMWARE ISSUE",ColorList.getParam("RS485DEVICE_NOTREADY")]
         
      cond = 0
      if mlen >= 2:
         byte = int(words[0])+ (int(words[1]) << 8)
         if (byte & 0x1) != 0:
            self.postMsg(logRef,"pcComm: BAD START byte #1","error")
            cond |= 0x1
         if (byte & 0x2) != 0:
            self.postMsg(logRef,"pcComm: BAD START byte #2","error")
            cond |= 0x1
         if (byte & 0x4) != 0:
            self.postMsg(logRef,"pcComm: BAD STOP byte","error")
            cond |= 0x1
         if (byte & 0x8) != 0:
            self.postMsg(logRef,"pcComm: BAD Packet Length","error")
            cond |= 0x1
         if (byte & 0x10) != 0:
            self.postMsg(logRef,"pcComm: BAD CRC","error")
            cond |= 0x1
         if (byte & 0x20) != 0:
            self.postMsg(logRef,"pcComm: Unknown Command","error")
            cond |= 0x1
         if (byte & 0x40) != 0:
            self.postMsg(logRef,"pcComm: Recieve buffer overflow","error")
            cond |= 0x1
         if (byte & 0x80) != 0:
            self.postMsg(logRef,"pcComm: Bad command data length","error")
            cond |= 0x1

         if (byte & 0x100) != 0:
            self.postMsg(logRef,"HV Fault detected! Micro disabling HV output","error")
            cond |= 0x80

      
      if mlen >= 4:
         byte = int(words[2])+ (int(words[3]) << 8)
         if (byte & 0x1) != 0:
            self.postMsg(logRef,"pcComm: Good Command",-1)

         if (byte & 0x2) == 0:
            self.postMsg(logRef,"Unconfigured","warn")
            cond |= 0x20

         if (byte & 0x4) == 0:
            self.postMsg(logRef,"ADCs not sampling","warn")
            cond |= 0x2
         if (byte & 0x8) == 0:
            self.postMsg(logRef,"Drive Disabled","warn")
            cond |= 0x2
#         if (byte & 0x10) == 0:
#            self.postMsg(logRef,"HV Output Disabled","warn")
#            cond |= 0x2
         if (byte & 0x40) == 0:
            self.postMsg(logRef,"HV Feedback Disabled","warn")
            cond |= 0x4

         if (byte & 0x20) == 0:
            self.postMsg(logRef,"Micro Timer not running","error")
            cond |= 0x40
         if (byte & 0x80) == 0:
            self.postMsg(logRef,"IIR parameter were not set","warn")
            cond |= 0x1

      if cond & 0x4:
         ModeDef = "DIRECT CONTROL"
      else:
         ModeDef = "FEEDBACK CONTROL"
      if mlen >= 6:
         byte = int(words[4])+ (int(words[5]) << 8)
         if byte > 0:
            self.postMsg(logRef,"Detected {} errors".format(byte),"error")
            cond |= 0x1

         if cond & 0x80:
            code = ["HV FAULT: WAITING FOR SERVICE",ColorList.getParam("RS485DEVICE_NOTREADY")]
         elif cond & 0x40:
            code = ["MICRO FUNCTIONAL ERROR: FIRMWARE ISSUE",ColorList.getParam("RS485DEVICE_NOTREADY")]
         elif cond & 0x20:
            code = ["Unconfigured: Please Configure",ColorList.getParam("RS485DEVICE_WARN")]
         elif cond & 0x2:
            code = ["Waiting to be turned on",ColorList.getParam("RS485DEVICE_WARN")]
            self.MyStatus = 0
         elif cond & 0x1:
            code = ["Normal ({}) with warnings".format(ModeDef),ColorList.getParam("RS485DEVICE_READY")]
            self.MyStatus = 1
         else:
            code = ["Normal ({})".format(ModeDef),ColorList.getParam("RS485DEVICE_READY")]
            self.MyStatus = 1

      self.postMsg(logRef,code[0],code[1])
      return code

   def getColor(self):
      pv = self.getV("status")
      if pv=="none":
         return ColorList.getParam("RS485DEVICE_NOTREADY")
      code = self.defineStatus(0)
      return code[1]
#      return ColorList.getParam("RS485DEVICE_NOTREADY")


      
   def TxParse(self,cmd,pList):
      cmddef = FindCode(self,cmd)
      if cmddef==0:
         self.setParam("rsptype","UNKNOWN={}".format(hex(cmd)))
         self.setParam("rspcode","?")
         sys.stdout.write("Unknown command code = {}\n".format(cmd))

      self.setParam("rsptype",cmddef[0])
      self.setParam("rspcode","OK")
      if cmddef[0] == "VERSION":
         sstr = ""
         for i in pList:
            sstr += chr(i)
         self.setParam("version",sstr)

      elif cmddef[0] == "STATUS":
         sstr = ""
         for i in pList:
            if len(sstr) > 0:
               sstr += ","
            sstr += "{}".format(int(i))
         self.setParam("status",sstr)
#         sys.stdout.write("status  {} = {}\n".format(self.name,sstr))

      elif cmddef[0] == "GETDATA":
         self.setParam("v0",pList[0] + (pList[1] << 8))
         self.setParam("i0",pList[2] + (pList[3] << 8))
         self.setParam("t0",pList[4] + (pList[5] << 8))
         pw = pList[6] + (pList[7] << 8)
         pp = pList[8] + (pList[9] << 8)
         self.setParam("pw",pw)
         self.setParam("pp",pp)
         pp += 1

         self.setParam("fd","{:.2f}".format(100*pw/pp))
         self.setParam("fs","{:.2f}KHz".format(100000/pp))
         
      elif cmddef[0] == "VIMINMAX":
         self.setParam("v0min",pList[0] + (pList[1] << 8))
         self.setParam("v0max",pList[2] + (pList[3] << 8))
         self.setParam("i0min",pList[4] + (pList[5] << 8))
         self.setParam("i0max",pList[6] + (pList[7] << 8))

      elif cmddef[0] == "GETV0":
         self.setParam("v0",pList[0] + (pList[1] << 8))
      elif cmddef[0] == "GETI0":
         self.setParam("i0",pList[0] + (pList[1] << 8))
      elif cmddef[0] == "GETTEMP":
         self.setParam("t0",pList[0] + (pList[1] << 8))

      elif cmddef[0] in ["ERROR","ACKNOWLEDGE"]:
         codedef = FindCode(self,pList[0])
         if cmddef==0:
            code = "{}".format(hex(pList[0]))
         else:
            code = codedef[0]
         self.setParam("rspcode",code)

   def getParamList(self,param):
      pname = self.ParamTrans(param)
      if pname == "method":
         return [ "None","ControlLoop","LGCM1"]
      if pname == "frequencykhz":
         mList = []
         for i in range(64):
             period = 0x610 + i * 16
 #        for i in range(64):
 #            period = 0x604 + i * 16
             freq = 100000/period
             mList.append("{:.2f}".format(freq))
         return mList
      if pname == "commname":
         mList = []
         for i in ComPortList.MyList:
            mList.append(i.name)
         return mList
      return []


   def getParam(self,param):
      pname = self.ParamTrans(param)
      if pname=="jobcount":
         return "{}".format(self.jobitrcnt)
      if pname=="device":
         return "{}:{}".format(self.getParam("commname"),self.getParam("address")).upper()
      if pname=="devicestatus":
         code = self.defineStatus(0)
         return code[0]

      return self.getV(pname)

   def setParam(self,param,value):
      pname = self.ParamTrans(param)
      if pname=="lordname":
          pname = "name"
      if pname=="siegepoints":
          pname = "bgpoints"
      self.setV(pname,value)
      return 0

class RS485DeviceListObj(db.ListObj):
   def __init__(self):
      db.ListObj.__init__(self,"rs485dev","none","S",RS485DeviceWords)
      self.writable = 0
      self.newcnt = 1
      self.IDcnt = 1

   def create(self,name):
      i = RS485Device(name,self)
      if i.objID==-1:
         i.objID = "{}{}".format(self.listtype[0].upper(),self.index)
         self.index += 1

      self.MyList.append(i)
      return i

   def findaddr(self,addr,ComPort):
      sys.stdout.write("findaddr addr={} @ ComPort={}({})\n".format(addr,ComPort.getParam("comport"),ComPort.getParam("name")))
      comname = ComPort.name
      for i in self.MyList:
         daddr = i.getParam("addr")
         dval = -1
         if daddr!= "NONE<>":
            dval = int(daddr)
         if daddr==addr:
            i.setParam("commname",comname)
            return i

      found = 0
      while found == 0:
         found = 1
         newname = "NDV{}".format(self.newcnt)
         self.newcnt += 1
         for i in self.MyList:
            if i.name == newname:
               found = 0
               break
      i = self.create(newname)
      i.setParam("address",addr)
      i.setParam("commname",ComPort.getParam("name"))
      i.Setup()
      return i

##   def saveInfo(self,f):
##      for c in self.MyList :
##        c.saveInfo(f,self.listtype,self.ParamList)

RS485DeviceList = RS485DeviceListObj()

def SaveBaseDefs():
    f = mpath.openFile("sys","sys","basedefs",'w')
    if f==-1:
        return
    ProjParams.saveList(f,"parameter")
    ColorList.saveList(f,"colorsetup")
    for t in TextConfigList.MyList:
       t.saveInfo(f)
    f.close()
