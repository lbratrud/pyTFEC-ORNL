#! /usr/bin/env python3
# pulseReConstruct.py
#
# Python main file for FEC pulse reconstruction. The pulse must at the given FREQINPUT (in hz)
#
# ORNL - 11/1/2017
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import math
import os
import glob
import projmkrpt as mkrpt
import pulseReConstruct
import FEC_IO
import FEC_DATA as FECdata
import time

fList = glob.glob("itr*")
dList = []
pwd = os.getcwd()
mytime_min = time.time()
Dose = [0,365,750,1500,3000,4500,6000,7500,9000,10500,12000]
for i in fList:
    os.chdir(i) 
    mList = FEC_IO.ExecuteCmdParse("grep RTD RadTest.log")
    RTD = [0,0,0,0,0,0,0]
    for gp in mList:
        words = gp.replace("="," ").replace("C"," ").split()
        if words[0][0:3]=="RTD":
           index = int(words[0][3:])
           RTD[index] += float(words[1])
    radF = glob.glob("*.pdf")
    d = FECdata.FEC_data()
    d.Read("runinfo.txt")
    d.ReadAll()
    rms = d.getRMS(5)
    words = i.split("_")
    mytime = time.mktime(time.strptime("{}_{}".format(words[1],words[2]),'%Y%b%d_%H%M%S'))
    dList.append([mytime,d,0,0.5*RTD[1],0.5*RTD[2],0.5*RTD[3],0.5*RTD[4],0.5*RTD[5],"{}/{}".format(i,radF[0])])
    if mytime < mytime_min:
       mytime_min = mytime
    os.chdir(pwd)

tList = []
for i in dList:
   i[0] = int(i[0]-mytime_min)
   tList.append(i[0])
#   sys.stdout.write("{}\n".format(i))

#sys.stdout.write("dList={}\n".format(dList))

DsList = []
tList.sort(key=int)

for j in tList:
   rmJ = 0
   for i in dList:
      if i[0]==j:
        DsList.append(i)
        rmJ = i
        break
   if rmJ!=0:
      dList.remove(rmJ)

for data in DsList:
    for index in range(1,6):
        rms = data[2+index]
        sys.stdout.write(" {:3.1f}".format(rms))
    for channel in range(0,160):
        rms = data[1].getRMS(channel)
        sys.stdout.write(" {:.3f}".format(rms))
    sys.stdout.write("\n")

exit()
