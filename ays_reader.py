# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import string
import sys
import os
import array
import glob
import projplot as pplot

version = "1.0 11/16/2016"

histogram = []
for i in range(0,160):
   histogram.append([])

fin = open(sys.argv[1],'r')
for line in fin:
    words = line.lower().split()
    if words[0]=="tpc0l":
       SAMPAindex = 32*int(words[1])
       for index in range(0,16):
          histogram[index].append(int(words[2+index]))

fin.close()
myplot = pplot.DataPlot(2)
for i in [0,10]:
    Xlist = []
    Y1list = []
    Y2list = []
    nextset = 1500
    maxlen = len(histogram[i])
    mval = nval = 0
    for j in range(0,nextset):
      Xlist.append(j)
      if j < maxlen:
         mval = histogram[i][j]
         
      Y1list.append(mval)
      if j+nextset < maxlen:
         nval = histogram[i][j+nextset]
      Y2list.append(nval)
    myplot.addXY(myplot.AxList[0],Xlist,Y1list)
    myplot.addXY(myplot.AxList[1],Xlist,Y2list)
    myplot.nextReference()
#    sys.stdout.write("Channel[{}]:\n".format(i))
##    for j in range(0,1024):
##       if Stats[i][j] > 0:
##          sys.stdout.write("[{}]={}\n".format(j,Stats[i][j]))
myplot.makePlot()
