#!/usr/bin/env python3

# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import threading

import string
import sys
import os
import array
import ctypes
import mmap
import glob
import FEC_DATA as FECdata
from projbase import *
import DataViewer
#import projplot as pplot

import commondb as cdb
cdb.ColorList.addwDesc("PYFEC_STATUS_INIT","#4040ff","PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_RUNNING","#ff4040","PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_IDLE","#40ff40","PYFEC")

from projbase import *
Version = "1.0 06/29/2018"
import projpath as mpath
mpath.projName = "TPC-FEC"

import commondbed as cdbed
import projwin as win
import projtime as mtime

import errwarn as errwarn
errwarn.ErrWarn.setup(0)

import FEC_IO as fec_io

server_cmd = []
done = 0
def add_cmd(cmd):
   server_cmd.append(cmd)

def FEC_server(mdelay)
  done = 0
  FEC_status = []
  for mask in range(0,8):
     FEC_status.append([0,0])
  while done==0: 
      for mask in range(0,8):
  
      
#
def RunProcess(jobID, fecData,index):
    global CmdExecutionQueue, fec

    os.chdir(fecData.Path)
    numFrames = 8 * fecData.Timebins
    for gbtxNum in range(0,2):
        CmdExecutionQueue.append(["addlog", "JOB #{}: Processing Data GBTX{}".format(jobID,gbtxNum)])
        if gbtxNum==0:
            capture = fecData.Capture0
        else:
            capture = fecData.Capture1
        cmd = "decoder_gbtx -f {} -i run{}_{}.bin".format(numFrames,gbtxNum,index)
        cmd += " -o run{}_{} -s {} -l {}".format(gbtxNum,index,gbtxNum,fecData.Threshold)
        capture.append(cmd)
        code = fec_io.ExecuteCmdParse(cmd, add_cmd=1)
        for i in code:
            capture.append(i)


def RunReconCapture(jobID,fecData):
    aList = [5,10] #,15,20,25,30,35]
    numFrames = int(8 * fecData.Timebins + 300)
    for amplitude in aList:
        self.PulseInfo = waveGen.setupReconSignal(0, [127000, amplitude * 0.001, 3e-9, "on"])
        time.sleep(0.25)
        cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir {} --mask 0x1".format(numFrames, fecData.Path)
        code = fec_io.ExecuteCmdParse(cmd, add_cmd=1)
        for j in code:
            CaptureLog.append(i)
            if j.upper().find("FATAL") != -1:
                got_fatal = 1
                break
        try:
            os.rename("{}/run000000_trorc00_link00.bin".format(fecData.Path), "{}/run0_{}.bin".format(fecData.Path, amplitude))
            os.rename("{}/run000000_trorc00_link01.bin".format(fecData.Path), "{}/run1_{}.bin".format(fecData.Path, amplitude))
        except:
            pass

    for amplitude in aList:
        for gbtxNum in [0]:
            cmd = "decoder_gbtx -f {} -i run{}_{}.bin".format(numFrames,gbtxNum,amplitude)
            cmd += " -o run{}_{} -s {} -p".format(gbtxNum,amplitude,gbtxNum)
            fec_io.ExecuteCmdParse(cmd, add_cmd=1)
            fec_io.ExecuteCmdParse("grep TPC0L run{}_{}", add_cmd=1)



def RunCapture(jobID,fecData):
      global CmdExecutionQueue, fec

      os.chdir(fecData.Path)
      CaptureLog = []
      numFrames = int(8*fecData.Timebins+300)
      cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir {} --mask 0x1".format(numFrames,fecData.Path)
      tm1 = time.time()
      got_fatal = 0
      for i in range(0,fecData.NumIters):
#          sys.stdout.write("cmd={}\n".format(cmd))
          code = fec_io.ExecuteCmdParse(cmd,add_cmd=1)
          for j in code:
              CaptureLog.append(i)
              if j.upper().find("FATAL")!=-1:
                 got_fatal = 1
                 break
          CmdExecutionQueue.append(["addlog","JOB #{}: Iteration {}/{}".format(jobID,i+1,fecData.NumIters)])

          try:
              os.rename("{}/run000000_trorc00_link00.bin".format(fecData.Path),"{}/run0_{}.bin".format(fecData.Path,i))
              os.rename("{}/run000000_trorc00_link01.bin".format(fecData.Path),"{}/run1_{}.bin".format(fecData.Path,i))
          except:
              pass

      tm2 = time.time()
      CmdExecutionQueue.append(["setcapture",0])
      CaptureLog = []
      CaptureLog.append("Output directory = {}\n".format(fecData.Path))
      CaptureLog.append("")
      if got_fatal==1:
         CmdExecutionQueue.append(["addlog","JOB #{}: Detected FATAL error during TREADOUT. LOG:".format(jobID),"#ff0000"])
         for i in code:
             CmdExecutionQueue.append(["addlog",i])
      index = 0
      MAX_THREADS = 5
      for index in range(0,fecData.NumIters):
          # myThreads = []
          # itr_stop = itr_start + MAX_THREADS
          # if itr_stop > fecData.NumIters:
          #    itr_stop = fecData.NumIters
          # for index in range(itr_start,itr_stop):
          #     CaptureLog.append("Decoding data @ {}".format(index))
          #     RunProcess(jobID,fecData,index)
          #     PT = threading.Thread(target=RunProcess, args=(jobID,fecData,index))
          #     PT.start()
          #     myThreads.append(PT)
          #
          # done  = 0
          # while done == 0:
          #     done = 1
          #     for i in myThreads:
          #         if i.isAlive():
          #            done = 0
          # itr_start += MAX_THREADS
          CaptureLog.append("Decoding data @ {}".format(index+1))
          RunProcess(jobID, fecData, index)
      tm3 = time.time()

      for i in fecData.Capture0:
          CaptureLog.append(i)
      CaptureLog.append("")
      for i in fecData.Capture1:
          CaptureLog.append(i)
      CaptureLog.append("")
      fecData.ReadAll()
      fecData.setupColorLevels()
      fecData.MakePDF("{}.pdf".format(fecData.Name))
      tm4 = time.time()
      timeinfo = "JobID#{} Time (seconds): Capture={:.3f} Processing={:.3f} Generate={:.3f} Total={:.3f} ::Frames={}".format(jobID,tm2-tm1,tm3-tm2,tm4-tm3,tm4-tm1,numFrames)
      sys.stdout.write("{}\n".format(timeinfo))
      CaptureLog.append(timeinfo)
      CmdExecutionQueue.append(["addlog","JOB #{}: Creating {}.pdf".format(jobID,fecData.Name)])
      CmdExecutionQueue.append(["addlog",timeinfo,"#ffffff"])
      CmdExecutionQueue.append(["setcapture",-1])

      LogFile = open("run.log",'w')
      for i in CaptureLog:
          LogFile.write("{}\n".format(i))
      LogFile.close()
      CmdExecutionQueue.append(["addtoviewer",fecData])


def ExecCmdProc(cmdDef):
    global windataviewer

    cmd = cmdDef[0]     
    if cmd=="clearlog":
       if winmain!=0:
          winmain.Log.delete()
    elif cmd=="addtoviewer":
       pass
#      if winmain.fecData != cmdDef[1]:
#         winmain.fecData.HistoMerge(cmdDef[1])
    elif cmd=="setcapture":
      if winmain.CaptureMultiInfo[0]=="sweep":
          if cmdDef[1] == -1:
             winmain.CaptureMultiInfo[1]+=1
             winmain.nextCaptureSweep()
             winmain.capture_active = -1
      else:
          if cmdDef[1] == -1:
             winmain.capture_active = -1
          elif cmdDef[1] == 0:
             winmain.capture_active = 0
             winmain.CaptureStatus.setVal("CAPTURE::DONE")
             winmain.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"))
             winmain.RunEvents.button.config(bg="#00cf00",fg="#ffffff",state=NORMAL)
    elif cmd=="initdone":
        winmain.Status.setVal("IDLE")
        winmain.Status.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"))
#        winmain.BoardID.setVal(hexfec.)
        winmain.Status.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"))
    elif cmd=="addlog":
      winmain.Log.addLog(cmdDef[1])
      if len(cmdDef) > 2:
         winmain.Log.setColor(cmdDef[2])


    elif cmd=="setupsync":
       fec.SetupSYNC()
       winmain.defineOptions()
#    elif cmd=="capturedata":
#       DoCapture(0)
#    elif cmd=="capturedataanalyze":
#       DoCapture(1)
    elif cmd=="setuptpc":
       fec.SetupTPC()
    elif cmd=="setuptpc100":
       for i in range(0,100):
           sys.stdout.write("Doing TPC {}/100\n".format(i))
           fec.SetupTPC()
#    elif cmd=="trgcapture":
#       fec.SetupPedestal(1)
#       FileReference = "fec_CPD_{}_{}_{}".format(fec.FreqMode,fec.setupMode,time.strftime('%Y%b%d_%H%M%S'))
#       fec.capturePedestalData(FileReference,10)
#       time.sleep(1)
#       fec.SAMPA_set_ctrl(0x1f,0,0)
#       FileReference = "fec_{}_{}_{}".format(fec.FreqMode,fec.setupMode,time.strftime('%Y%b%d_%I%M%S'))
#       time.sleep(1)
#       fec.captureData(FileReference)
#       fec.captureData(FileReference)
#       fec.SAMPA_set_ctrl(0x0,0,0)
#       fec.analyzeData(FileReference)

    elif cmd=="setuppedestalcapture":
       fec.SetupPedestal(1)
       winmain.defineOptions()
    elif cmd=="setuppedestalread":
       fec.SetupPedestal(1)
       winmain.defineOptions()
    elif cmd=="setpwr_pat1":
       fec.pedestalWriteMode = "PAT1"
    elif cmd=="sampasondirect":
       fec.TurnSampasOnDirect()
    elif cmd=="sampasonsequenced":
       fec.TurnSampasOnSequenced()
    elif cmd=="sampasoff":
       fec.TurnSampasOff()
    elif cmd=="setpwr_ramp":
       fec.pedestalWriteMode = "RAMP"
    elif cmd=="pedestalmemorywrite":
       fec.SAMPA_pedestal_memory_write()
    elif cmd=="pedestalmemoryverify":
       fec.SAMPA_pedestal_memory_verify()
# --------------------------------------------------
    elif cmd=="programgbtx1":
       fec.programGBTx1(Cnfg1.CONFIG_REGISTER_DEFINITIONS)
    elif cmd=="gengbtx0cnfg":
       gbtxcnfg.WriteRegDefConfigFile("fec-gbtx0-cnfg.txt",Cnfg0.CONFIG_REGISTER_DEFINITIONS)
    elif cmd=="gengbtx0acnfg":
       gbtxcnfg.WriteRegDefConfigFile("fec-gbtx0a-cnfg.txt",Cnfg0a.CONFIG_REGISTER_DEFINITIONS)
    elif cmd=="gengbtx1cnfg":
       gbtxcnfg.WriteRegDefConfigFile("fec-gbtx1-cnfg.txt",Cnfg1.CONFIG_REGISTER_DEFINITIONS)
# --------------------------------------------------
    elif cmd=="seriallinkread":
       fec.SAMPA_seriallink_check()
    elif cmd=="checkactivelinks":
       mList = fec.SAMPA_check_active_seriallinks()
       msg = "#activeElinks={:>2d} SyncCnt={}".format(mList[0],fec.syncCnt)
       if mList[0] > 0:
          msg += " ["
          for i in mList[1]:
             msg += " {}".format(i)
          msg += " ]"
       fec.addLog(msg)
    elif cmd=="adctest":
       fec.ADC_Test()
        
def TimerProc():
   global do_exit

   while len(CmdExecutionQueue) > 0:
      m = CmdExecutionQueue.pop(0)
      ExecCmdProc(m)
   if do_exit==1:
      sys.stdout.write("Exiting\n")
      win._root.quit()
      exit()
      return
   win._root.after(1,TimerProc)

win._root.after(20,TimerProc)
win._root.mainloop()
