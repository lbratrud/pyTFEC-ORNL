# FEC_DATA.py
#
# This file handles the interactions with data file generated by decoder_gbtx. 
#
# ORNL - 5/17/2017
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)


import math
import sys
import time
import string
import sys
import os
import smtplib
import getpass
import ctypes
import mimetypes
from email import encoders
from email.message import Message
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

receivers = ['clontslg@ornl.gov']
# receivers = ['readkf@ornl.gov','clontslg@ornl.gov']
senderID = getpass.getuser()

# ==============================================================================
# MailSend
# Send a message to receivers.
# ==============================================================================

COMMASPACE = ', '

EMAIL_SERVER='smtp.ornl.gov'
EMAIL_PORT=25

def SendEmail(receivers,hdr, body, fileList):
    global senderID
    global EMAIL_SERVER , EMAIL_PORT

    outer = MIMEMultipart()
    outer['Subject'] = hdr
    outer['To'] = COMMASPACE.join(receivers)
    outer['From'] = senderID
    outer.preamble = 'You will not see this in a MIME-aware mail reader.\n'
    if len(body) > 0:
        msg = MIMEText(body)
        outer.attach(msg)

    for file in fileList:
        if os.path.exists(file):
            ctype, encoding = mimetypes.guess_type(file)
            if ctype is None or encoding is not None:
                # No guess could be made, or the file is encoded (compressed), so
                # use a generic bag-of-bits type.
                ctype = 'application/octet-stream'
            maintype, subtype = ctype.split('/', 1)
            if maintype == 'text':
                fp = open(file)
                # Note: we should handle calculating the charset
                msg = MIMEText(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'image':
                fp = open(file, 'rb')
                msg = MIMEImage(fp.read(), _subtype=subtype)
                fp.close()
            elif maintype == 'audio':
                fp = open(file, 'rb')
                msg = MIMEAudio(fp.read(), _subtype=subtype)
                fp.close()
            else:
                fp = open(file, 'rb')
                msg = MIMEBase(maintype, subtype)
                msg.set_payload(fp.read())
                fp.close()
                # Encode the payload using Base64
                encoders.encode_base64(msg)
                # Set the filename parameter
            msg.add_header('Content-Disposition', 'attachment', filename=file)
            outer.attach(msg)
    # Now send or store the message
    try:
      s = smtplib.SMTP()
      s.connect(EMAIL_SERVER, EMAIL_PORT)
      s.sendmail(senderID, receivers,outer.as_string())
      s.quit()
      return 1
    except:
      return -1


#SendEmail(['clontslg@ornl.gov','readkf@ornl.gov'],"Testing EMAIL script", "Define body", ["FEC_email.py"])
