# mtcswin.py
#
# Define the window routines for EVERYTHING in the system
#
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)
# See projbase.py for history details

import math
import sys
import time
import string
import sys

from projbase import *
import errwarn
import projdb as db
import commondb as cdb

try:  # import as appropriate for 2.x vs. 3.x
    from tkinter import *
    from tkinter import font
except:
    from Tkinter import *
    from Tkinter import font

_root = Tk()
_root.withdraw()

default_resize_timeout = 1.5
BranchListForSys = []
WindowMainList = []
WindowList = []


def WinSeek(name):
    uname = name.lower()
    for w in WindowList:
        if w.name == uname:
            return w
    return ""


class Base:
    def __init__(self, name, title):
        nname = name.lower()
        w = WinSeek(nname)
        if w != "":
            if w.master.state() == "iconic":
                w.master.deiconify()
            w.master.lift()
            return 0
        WindowList.append(self)
        self.dbobj = WinBaseList.find(nname)
        self.name = nname
        self.mytype = "entry"
        self.modified = 0
        self.destroyOnExit = 1
        self.fgc = cdb.ColorList.getParam("WINBASE_TEXT_FG")
        self.bgc = cdb.ColorList.getParam("WINBASE_TEXT_BG")
        self.qfgc = cdb.ColorList.getParam("WINBASE_PROMPT_FG")
        self.qbgc = cdb.ColorList.getParam("WINBASE_PROMPT_BG")
        self.mfgc = cdb.ColorList.getParam("WINBASE_MENU_FG")
        self.mbgc = cdb.ColorList.getParam("WINBASE_MENU_BG")
        self.bg = cdb.ColorList.getParam("WINBASE_BG")
        self.closed = False
        self.label = cdb.TextConfigList.find("WINBASE_TEXT")
        self.font = (self.label.font, self.label.size)
        self.detailTextObj = 0
        self.internal_resize_timer = 0
        self.scale_factor = 1
        self.textList = []
        self.f2 = 0
        self.f4 = 0
        self.f4var = IntVar()
        self.mytype = "none"
        self.indicatoron = 0
        self.sticky = E + W
        self.master = Toplevel(_root)
        self.master.protocol("WM_DELETE_WINDOW", self.close)
        self.title = title
        self.master.title(title)
        self.master.config(bg=self.bg)
        self.master.resizable(FALSE, FALSE)
        self.setupXY()
        return 1

    def createSubMenu(self, mainmenu, desc):
        m = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=0)
        mainmenu.add_cascade(label=desc, menu=m)
        return m

    def setupXY(self):
        self.master.geometry("+{}+{}".format(self.dbobj.getParam("xpos"), self.dbobj.getParam("ypos")))

    def defineOptions(self):
        pass

    def saveWindowInfo(self):
        geo = self.master.winfo_geometry()
        words = geo.translate(strmaketrans("+x", "  ")).split()
        for i in [[words[2], "xpos"], [words[3], "ypos"]]:
            self.dbobj.setParam(i[1], i[0])
        dbb.WinBaseList.save()

    def setupConfigure(self):
        self.width = self.master.winfo_width()
        self.height = self.master.winfo_height()
        self.master.bind("<Configure>", self.onConfigure)

    def onConfigure(self, event):
        if event.widget.winfo_class().lower() != "toplevel":
            return
        for i in [[event.x, "xpos"], [event.y, "ypos"]]:
            self.dbobj.setParam(i[1], i[0])

    ##    def WindowCheck(self,name):
    ##       self.name = name
    ##       for w in WindowList:
    ##          if w.name == name:
    ##            wstate = w.master.state()
    ##            if wstate == "iconic":
    ##                w.master.deiconify()
    ##            w.master.lift()
    ##            sys.stdout.write("{} exists\n".format(name))
    ##            return 1
    ##       return 0

    def TextUpdate(self, typeref):
        return

    def Setup(self):
        return

    def TimerProc(self, realtimediff, traintimediff):
        return

    def popWindow(self, name):
        w = WinSeek(name)
        if w == "":
            sys.stdout.write("Window {} does not exist! How did this happen?\n".format(name))
            return

        if w.isOpen():
            wstate = w.master.state()
            if wstate == "iconic":
                w.master.deiconify()
            w.master.lift()
            return
        else:
            sys.stdout.write("Need to fix this. Window {} was destroyed\n".format(name))

    def DoExit(self):
        if self in WindowList:
            WindowList.remove(self)
        self.master.destroy()

    def close(self):
        if self in WindowList:
            if self.destroyOnExit == 0:
                self.master.iconify()
            else:
                if self.closed: return
                self.closed = True
                self.DoExit()

    def __autoflush(self):
        if self.autoflush:
            self.master.Update()

    def flush(self):
        """Update drawing to the window"""
        if self.closed:
            raise GraphicsError("window is closed")
        self.Update_idletasks()

    def isClosed(self):
        return self.closed

    def isOpen(self):
        return not self.closed


class WinObject:
    def __init__(self, window):
        self.window = window
        self.mytype = -1
        self.myval = -1
        self.entry = 0
        self.button = 0
        # List of parts for obj, correspoding graphical item, and selected IntVar!
        self.List = []
        self.ObjList = []
        self.IntVarList = []
        self.maxrow = 0
        self.maxcol = 0
        self.cnt = 0
        self.value = IntVar()
        self.value.set(0)

    def maxRC(self, row, col):
        if row > self.maxrow:
            self.maxrow = row
        if col > self.maxcol:
            self.maxcol = col

    def CreateRadio(self, msgval, val, row, col, command):
        self.List.append(msgval)
        c = Radiobutton(self.window.master, text=msgval, variable=self.value, value=val, font=self.window.font,
                        bg=self.window.qbgc,
                        fg=self.window.qfgc, indicatoron=self.window.indicatoron,
                        selectcolor=cdb.ColorList.getParam("WINBASE_SELECT"))
        if command != 0:
            c.config(command=command)

        self.ObjList.append(c)
        c.grid(row=row, column=col, sticky=self.window.sticky)
        self.maxRC(row, col)

    def CreateCheck(self, msgval, row, col, command):
        v = IntVar()
        c = Checkbutton(self.window.master, text=msgval, variable=v, font=self.window.font, bg=self.window.qbgc,
                        fg=self.window.qfgc, indicatoron=self.window.indicatoron,
                        selectcolor=cdb.ColorList.getParam("WINBASE_SELECT"))
        if command != 0:
            c.config(command=command)
        c.grid(row=row, column=col, sticky=self.window.sticky)
        self.IntVarList.append(v)
        self.List.append(msgval)
        self.ObjList.append(c)
        self.maxRC(row, col)

    def CreateLabel(self, msg, width, row, col, relief, sticky):
        label = Label(self.window.master, text=msg, font=self.window.font, bg=self.window.bgc,
                      fg=self.window.fgc, relief=relief)
        # disabledforeground=self.window.bg) #,relief=RAISED
        if width > 0:
            label.config(width=width)
        label.grid(row=row, column=col, sticky=sticky)
        self.maxRC(row, col)
        return label

    def CreateButton(self, msg, row, col, relief, sticky):
        button = Button(self.window.master, text=msg, font=self.window.font, bg=self.window.mbgc,
                        fg=self.window.mfgc, activebackground=self.window.mfgc, activeforeground=self.window.mbgc, relief=relief)  # ,relief=RAISED
        button.grid(row=row, column=col, sticky=sticky)
        self.maxRC(row, col)
        return button

    def CreateEntry(self, width, row, col, relief, sticky):
        entry = Entry(self.window.master, font=self.window.font, bg=self.window.qbgc,
                      fg=self.window.qfgc, relief=relief)
        # ,disabledforeground=self.window.bg,  disabledbackground=self.window.bg)
        if width > 0:
            entry.config(width=width)
        entry.grid(row=row, column=col, sticky=sticky)
        self.maxRC(row, col)
        return entry

    def setcolor(self, val, color):
        i = 0
        for l in self.List:
            if l == val:
                c = self.ObjList[i]
                c.config(bg=color, selectcolor=color)
                return
            i += 1

    def setcommand(self, val, command):
        i = 0
        for l in self.List:
            if l == val:
                c = self.ObjList[i]
                c.config(command=command)
                return
            i += 1

    def selectVal(self, val, on):
        i = 0
        for l in self.List:
            if l == val:
                if len(self.IntVarList) > 0:
                    v = self.IntVarList[i]
                    v.set(on)
                c = self.ObjList[i]
                if on == -1:
                    #               c.config(state=DISABLED,bg=self.window.bgc,fg=self.window.fgc)
                    c.config(state=DISABLED)
                    return
                elif on == 1:
                    c.select()
                else:
                    c.deselect()
                #             c.config(state=NORMAL,bg=self.window.qbgc,fg=self.window.qfgc)
                c.config(state=NORMAL)
                return
            i += 1

    def getSelectList(self):
        myList = []
        i = 0
        for v in self.IntVarList:
            val = v.get()
            if val == 1:
                myList.append(self.List[i])
            i += 1
        return myList

    def getVal(self):
        return "HOTTY"

    def setVal(self, val):
        if self.mytype == 0:
            self.entry.delete(0, END)
            self.entry.insert(0, val)
        elif self.mytype == 1:
            self.entry.config(text=val)
            self.myval = val


class WinValueEntry(WinObject):
    def __init__(self, window, msg, width, row, col):
        WinObject.__init__(self, window)
        self.mytype = 0
        self.label = self.CreateLabel(msg, -1, row, col, FLAT, E)
        self.entry = self.CreateEntry(width, row, col + 1, SUNKEN, E + W)

    def setVal(self, val):
        self.entry.delete(0, END)
        self.entry.insert(0, val)

    def getVal(self):
        return self.entry.get()


class WinValueLabel(WinObject):
    def __init__(self, window, msg, width, row, col):
        WinObject.__init__(self, window)
        self.mytype = 1
        self.label = self.CreateLabel(msg, -1, row, col, FLAT, E)
        self.entry = self.CreateLabel("", width, row, col + 1, SUNKEN, E + W)


    def setVal(self, val):
        self.entry.config(text=val)
        self.myval = val


    def getVal(self):
        return self.myval


    def setSize(self, size):
        font = (self.window.label.font, size)
        self.label.config(font=font)
        self.entry.config(font=font)

    def setColumnSpan(self, span1,span2):
        self.label.grid(columnspan=span1)
        self.entry.grid(columnspan=span2)


class WinButton(WinObject):
    def __init__(self, window, msg, row, col):
        WinObject.__init__(self, window)
        self.mytype = 2
        self.button = self.CreateButton(msg, row, col, RIDGE, E + W)

    def cconfig(self, relief, sticky):
        self.button.config(relief=relief)
        self.button.grid(sticky=sticky)

    def setVal(self,msg,mode=NORMAL):
        self.button.config(text=msg,state=mode)

    def setSize(self, size):
        font = (self.window.label.font, size)
        self.button.config(font=font)

class WinEntry(WinObject):
    def __init__(self, window, width, row, col):
        WinObject.__init__(self, window)
        self.mytype = 0
        self.entry = self.CreateEntry(width, row, col, SUNKEN, E + W)

    def setVal(self, val):
        self.entry.delete(0, END)
        self.entry.insert(0, val)

    def getVal(self):
        return self.entry.get()


class WinLabel(WinObject):
    def __init__(self, window, msg, width, row, col):
        WinObject.__init__(self, window)
        self.mytype = 3
        self.label = self.CreateLabel(msg, width, row, col, FLAT, E + W)

    def cconfig(self, relief, sticky):
        self.label.config(relief=relief)
        self.label.grid(sticky=sticky)

    def setVal(self, val):
        self.label.config(text=val)
        self.myval = val

    def getVal(self):
        return self.myval

    def setSize(self, size):
        font = (self.window.label.font, size)
        self.label.config(font=font)


class WinRowFeed(WinObject):
    def __init__(self, window, row):
        WinObject.__init__(self, window)
        self.label = self.CreateLabel(" ", -1, row, 0, FLAT, E + W + N + S)
        self.label.config(bg=self.window.bg)


class WinSetDummyLabel(WinObject):
    def __init__(self, window, row, col):
        WinObject.__init__(self, window)
        self.label = self.CreateLabel(" ", -1, row, col, FLAT, E + W + N + S)
        self.label.config(bg=self.window.bg)


class WinColumnFeed(WinObject):
    def __init__(self, window, column):
        WinObject.__init__(self, window)
        self.label = self.CreateLabel(" ", -1, 0, column, FLAT, E + W + N + S)
        self.label.config(bg=self.window.bg)


class WinList(WinObject):
    def __init__(self, window, mytype, row, col, minrow, maxrow, mincol, maxcol, listin, command):
        WinObject.__init__(self, window)
        self.mytype = mytype
        self.value.set(0)
        row = row
        col = col
        cnt = 0
        for b in listin:
            if mytype == "radio":
                self.CreateRadio(b, cnt, row, col, command)
            elif mytype == "check":
                self.CreateCheck(b, row, col, command)

            cnt += 1
            if maxrow < 0:
                col += 1
                if col > maxcol:
                    col = mincol
                    row += 1
            elif maxcol < 0:
                row += 1
                if row > maxrow:
                    row = minrow
                    col += 1

    def getVal(self):
        if self.mytype == "radio":
            index = self.value.get()
            if index >= len(self.List):
                return 0
            return self.List[index]
        else:
            return self.getSelectList()

    def setVal(self, val):
        i = 0
        for l in self.List:
            if l == val:
                c = self.ObjList[i]
                c.select()
                return
            i += 1

        if self.mytype == "radio":
            self.value.set(len(self.List) + 1)


class WinScrollList(WinObject):
    def __init__(self, window, doEntry, msg, width, height, row, col, listin, mycolumnspan=1):
        WinObject.__init__(self, window)
        self.doEntry = doEntry
        self.entry = 0
        Lwidth = width
        listspan = columnspan = 2
        yScrolloffset = 2
        if doEntry == 2:
            self.label = self.CreateLabel(msg, -1, row, col, FLAT, E + W)
            self.entry = self.CreateEntry(-1, row + 1, col, SUNKEN, W + E)
            self.mytype = 0
            row += 1
            listspan = 1
            columnspan = 1
            yScrolloffset = 1
            Lwidth = 0
        elif doEntry == 1:
            self.label = self.CreateLabel(msg, -1, row, col, FLAT, E)
            self.entry = self.CreateEntry(width, row, col + 1, SUNKEN, W + E)
            self.entry.grid(columnspan=2)
            self.mytype = 0
            Lwidth = 0
        elif doEntry == 0:
            self.label = self.CreateLabel(msg, -1, row, col, FLAT, E)
            self.entry = self.CreateLabel("", width, row, col + 1, SUNKEN, W + E)
            #          self.entry.grid(columnspan=1)
            self.mytype = 1
            Lwidth = 0
        else:
            self.label = self.CreateLabel(msg, -1, row, col + 1, FLAT, E + W)
            columnspan = listspan = 1
        if columnspan < mycolumnspan:
            columnspan = mycolumnspan
            listspan = mycolumnspan - 1
            self.label.grid(columnspan=columnspan - 1)

        #         self.label.grid(columnspan=2)

        yScroll = Scrollbar(self.window.master)
        xScroll = Scrollbar(self.window.master, orient=HORIZONTAL)
        self.yScroll = yScroll
        self.ListB = Listbox(self.window.master, selectmode=SINGLE, font=self.window.font, bg=self.window.qbgc,
                             fg=self.window.qfgc, relief=FLAT, height=height, yscrollcommand=yScroll.set,
                             xscrollcommand=xScroll.set)

        if Lwidth > 0:
            self.ListB.config(width=Lwidth)
        yScroll.config(command=self.ListB.yview, bg=window.bgc, activebackground=window.bgc)
        xScroll.config(command=self.ListB.xview, bg=window.bgc)

        self.ListB.grid(row=row + 1, column=col + 1, columnspan=listspan, rowspan=height, sticky=E + W + S + N)
        yScroll.grid(row=row + 1, column=col, sticky=N + S + E, rowspan=height)
        xScroll.grid(row=row + height + 1, column=col + 1, sticky=E + W, columnspan=columnspan)
        self.maxRC(row + height + 2, col + 1)
        self.xScroll = xScroll
        self.yScroll = yScroll
        #      self.ListB.bind("<Double-Button-1>", self.setVar)
        self.cnt = 0
        self.List = []
        for L in listin:
            self.addVal(L)

    def delete(self):
        self.ListB.delete(0, END)
        self.cnt = 0

    def remakeList(self, listin):
        self.List = []
        self.ListB.delete(0, END)
        self.cnt = 0
        for L in listin:
            self.addVal(L)

    def addVal(self, val):
        self.List.append(val)
        self.ListB.insert(self.cnt, val)
        self.cnt += 1

    def setColor(self, color):
        self.ListB.itemconfig(self.cnt - 1, bg=color, fg=seekRevColor(color))

    def addLog(self, val):
        self.ListB.insert(self.cnt, val)
        self.cnt += 1

    def getVal(self):
        if self.doEntry == 1:
            return self.entry.get()
        else:
            return self.myval

    def deleteVal(self, val):
        index = 0
        pos = self.yScroll.get()
        for i in self.List:
            if i == val:
                self.ListB.delete(index)
                self.List.pop(index)
                index -= 1
            index += 1
        self.ListB.yview_moveto(pos[0])
        self.myval = -1
        if self.doEntry == 1:
            self.entry.delete(0, END)

    def setVar(self, LB):
        selected = self.ListB.curselection()
        if self.doEntry == 1:
            for i in selected:
                index = int(i)
                self.entry.delete(0, END)
                self.entry.insert(0, self.List[index])
        else:
            text = ""
            for i in selected:
                index = int(i)
                text += "{}".format(self.List[index])
            if self.entry != 0:
                self.entry.config(text=text)
            self.myval = text


class WinMenu(WinObject):
    def __init__(self, window, msg, width, row, col):
        WinObject.__init__(self, window)
        self.mytype = 7
        self.menub = Menubutton(self.window.master, bg=self.window.mbgc, fg=self.window.mfgc,
                                font=self.window.font, text=msg, relief=RIDGE)
        if width > 0:
            self.menub.config(width=width)

        self.menu = Menu(self.menub, bg=self.window.mbgc, fg=self.window.mfgc, font=self.window.font, relief=RIDGE,
                         tearoff=1)
        self.menub.config(menu=self.menu)
        self.menub.grid(row=row, column=col)


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------      
class WinMultipleView:
    def __init__(self, window, command):
        self.currentobj = 0
        self.window = window
        self.showInvisible = StringVar()
        self.showInvisible.set("All")
        self.sortby = StringVar()
        self.command = command

        self.SortOrder = []
        self.MainList = []
        self.objList = []
        self.maxrow = 1

        self.MenuVar = 0
        self.Menu = 0
        self.ColumnList = []
        self.enableSelect = 1

    def addColumn(self, entry):
        self.ColumnList.append(entry)

    def addObj(self, entry):
        if entry not in self.MainList:
            self.MainList.append(entry)

    def createLists(self, height, row, col, viewheader):
        yScroll = Scrollbar(self.window.master)
        self.maxrows = 1
        cnt = 0
        for c in self.ColumnList:
            l = len(c[0])
            if l > self.maxrows:
                self.maxrows = l
            cnt += 1

        if len(viewheader) > 0:
            label = Label(self.window.master, text=viewheader, font=self.window.font, bg=self.window.bgc,
                          fg=self.window.fgc, relief=RAISED)
            label.grid(row=row, column=col + 1, sticky=E + W, columnspan=cnt)
            row += 1

        self.b2 = WinMenu(self.window, "Sort by", -1, row, col + 1)
        self.b2.menub.grid(sticky=E + W)
        row += 1
        if self.enableSelect == 1:
            self.currentValue = WinValueLabel(self.window, "Selected", -1, row, col + 1)
            row += 1

        self.sortdir = StringVar()
        AList = ["Ascending", "Descending"]
        self.sortdir.set(AList[0])
        for l in AList:
            self.b2.menu.add_radiobutton(label=l, variable=self.sortdir, value=l, command=self.updateLists)

        self.b2.menu.add_separator()

        yScroll.grid(row=row + self.maxrows, column=col, sticky=N + S, rowspan=height)
        col += 1
        AList = ["None"]
        for c in self.ColumnList:
            width = c[1]
            rowd = row
            for name in c[0]:
                if c[2] != "X":
                    AList.append(name)
                label = Label(self.window.master, text=name, font=self.window.font, bg=self.window.bgc,
                              fg=self.window.fgc, relief=RAISED)
                label.grid(row=rowd, column=col, sticky=E + W)
                rowd += 1

            xScroll = Scrollbar(self.window.master, orient=HORIZONTAL)
            LB = Listbox(self.window.master, selectmode=SINGLE, font=self.window.font, bg=self.window.qbgc, width=width,
                         fg=self.window.qfgc, height=height, relief=FLAT, yscrollcommand=yScroll.set,
                         xscrollcommand=xScroll.set)
            LB.bind("<Double-Button-1>", self.getSelect)
            self.objList.append([LB])

            LB.grid(row=row + self.maxrows, column=col, rowspan=height, sticky=E + W + N + S)
            xScroll.config(command=LB.xview)
            xScroll.grid(row=row + height + 1, column=col, sticky=E + W)
            col += 1
        yScroll.config(command=self.yview)
        self.yScroll = yScroll

        self.sortby.set(AList[0])
        for l in AList:
            self.b2.menu.add_radiobutton(label=l, variable=self.sortby, value=l, command=self.updateLists)

    def getSelect(self, event):
        index = event.widget.nearest(event.y)
        if self.maxrows != 1:
            index -= index % self.maxrows
            index /= self.maxrows
        self.currentobj = self.SortOrder[int(index)]
        if self.enableSelect == 1:
            self.currentValue.setVal(self.currentobj.name)

        if self.command != 0:
            self.command()

    def sortViewer(self):
        sortby = self.sortby.get()
        if sortby == "None":
            self.SortOrder = []
            for i in self.MainList:
                self.SortOrder.append(i)
            return

        mdir = self.sortdir.get()
        reverse = False
        if mdir == "Descending":
            reverse = True
        index = self.findIndex(sortby)
        sortt = self.ColumnList[index][2]
        if sortt == "S":
            sortkey = str.lower
        else:
            sortkey = int
        Names = []
        ValList = []
        SValList = []
        vFilter = self.showInvisible.get()
        for i in self.MainList:
            enable = 1
            if vFilter == "Invisible":
                visable = i.getParam("visible")
                if visable == "Y":
                    enable = 0
            elif vFilter == "Visible":
                visable = i.getParam("visible")
                if visable == "N":
                    enable = 0
            if enable == 1:
                if i.name not in Names:
                    Names.append(i.name)
                pval = i.getParam(sortby)
                if pval not in SValList:
                    SValList.append(pval)
                if sortt == "I":
                    try:
                        val = int(pval)
                        if val not in ValList:
                            ValList.append(val)
                    except:
                        sortt = "S"

        Names.sort(key=str.lower)
        List = []
        for i in Names:
            for j in self.MainList:
                if j.name == i:
                    List.append(j)

        self.SortOrder = []
        #      sys.stdout.write("Len={},{} List={}\n".format(len(SValList),len(ValList),SValList))
        if sortt == "S" and len(SValList) > 0:
            SValList.sort(key=str.lower, reverse=reverse)
            for i in SValList:
                for j in List:
                    val = j.getParam(sortby)
                    if i == val:
                        self.SortOrder.append(j)
        elif len(ValList) > 0:
            ValList.sort(key=int, reverse=reverse)
            for i in ValList:
                for j in List:
                    val = int(j.getParam(sortby))
                    if i == val:
                        self.SortOrder.append(j)

    def updateList(self, params, index):
        self.objList[index][0].delete(0, END)
        cnt = 0
        for a in self.SortOrder:
            color = a.getColor()
            off = 0
            #         sys.stdout.write("updateList {} cnt={}\n".format(a.name,cnt))
            for i in range(self.maxrows):
                if i < len(params[0]):
                    p = params[0][i]
                    mlist = a.getParam(p)
                else:
                    p = mlist = ""
                #             sys.stdout.write("{} {}={}\n".format(a.objID,p,mlist))
                self.objList[index][0].insert(cnt + off, mlist)
                self.objList[index][0].itemconfig(cnt + off, bg=color)
                off += 1
            cnt += self.maxrows

    def updateLists(self):
        if self.enableSelect == 1:
            if self.currentobj != 0:
                self.currentValue.setVal(self.currentobj.name)
            else:
                self.currentValue.setVal("")

        self.sortViewer()
        pos = self.yScroll.get()
        i = 0
        for b in self.ColumnList:
            self.updateList(b, i)
            i += 1
        for l in self.objList:
            l[0].yview_moveto(pos[0])

    def findIndex(self, name):
        i = 0
        for b in self.ColumnList:
            if name in b[0]:
                return i
            i += 1
        return -1

    def yview(self, *args):
        for l in self.objList:
            l[0].yview(*args)


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------      
class Viewer(Base):
    def __init__(self, title, viewerdef, do_visible, enable_selected):
        if Base.__init__(self, viewerdef, title) == 0:
            return 0
        self.admin_mode_only = 1
        self.dbobj.setParam("type", "viewer")
        self.enableSelect = enable_selected

        self.currentobj = 0
        height = 15
        row = 0
        self.b1 = WinMenu(self, "Options", -1, row, 1)
        self.b1.menub.grid(sticky=E + W)
        self.MainList = 0
        self.defineOptions()
        self.showInvisible = StringVar()
        self.showInvisible.set("All")
        row += 1
        col = 1
        WinRowFeed(self, row)
        row += 1

        self.b2 = WinMenu(self, "Sort by", -1, row, col)
        col += 1
        self.b2.menub.grid(sticky=E + W)
        self.sortdir = StringVar()
        AList = ["Ascending", "Descending"]
        self.sortdir.set(AList[0])
        for l in AList:
            self.b2.menu.add_radiobutton(label=l, variable=self.sortdir, value=l, command=self.updateLists)
        self.b2.menu.add_separator()

        if do_visible in ["Y", "y"]:
            self.b3 = WinMenu(self, "Display", -1, row, 2)
            col += 1
            self.b3.menub.grid(sticky=E + W)
            AList = ["Visible", "Invisible", "All"]
            for l in AList:
                self.b3.menu.add_radiobutton(label=l, variable=self.showInvisible, value=l, command=self.updateLists)

        if self.enableSelect == 1:
            self.currentValue = WinValueLabel(self, "Selected", -1, row, col)
            col += 2

        self.SortOrder = []
        self.sortby = StringVar()
        self.objList = []
        self.ActiveButtons = []
        self.maxrow = 1
        self.currentobj = 0

        self.MenuVar = 0
        self.Menu = 0
        self.ColumnList = []
        return 1

        ## ColumnList has the following format: Name, Width, Value Display, VarType
        ## Name/Width is name/width of column
        ## Value Display is how to treat the value for the name
        ##    TRUE = This process can edit
        ##    FALSE = This process cannot edit
        ##    MENU = Put in a menu list
        ##    Otherwise, nothing is done based on ColumnList
        ## VarType is S=String or I=Int
        ##    If I is set, but S values are detected, then the process changes to S.

    ##    def setParam(self,parm,value):
    ##       if self.currentobj!=0:
    ##          self.currentobj.setParam(parm,value)
    ##          i = 0
    ##          for c in self.ColumnList:
    ##            if parm in c[0]:
    ##                pos = self.yScroll.get()
    ##                self.updateList(c,i)
    ##                self.objList[i][0].yview_moveto(pos[0])
    ##            i+=1
    ##          self.MainList.save()

    def viewLists(self, height, row, col):
        yScroll = Scrollbar(self.master)
        self.maxrows = 1
        for c in self.ColumnList:
            l = len(c[0])
            if l > self.maxrows:
                self.maxrows = l
        #      sys.stdout.write("Maxrows={}\n".format(self.maxrows))
        yScroll.grid(row=row + self.maxrows, column=col, sticky=N + S, rowspan=height)
        col += 1
        AList = []
        for c in self.ColumnList:
            width = c[1]
            rowd = row
            for name in c[0]:
                if c[2] != "X":
                    AList.append(name)
                label = Label(self.master, text=name, font=self.font, bg=self.bgc, fg=self.fgc, relief=RAISED)
                label.grid(row=rowd, column=col, sticky=E + W + N + S)
                rowd += 1

            xScroll = Scrollbar(self.master, orient=HORIZONTAL)
            LB = Listbox(self.master, selectmode=SINGLE, font=self.font, bg=self.qbgc, width=width,
                         fg=self.qfgc, height=height, relief=FLAT, yscrollcommand=yScroll.set,
                         xscrollcommand=xScroll.set)
            LB.bind("<Double-Button-1>", self.getSelect)
            self.objList.append([LB])

            LB.grid(row=row + self.maxrows, column=col, rowspan=height, sticky=E + W)
            xScroll.config(command=LB.xview)
            xScroll.grid(row=row + height + 2, column=col, sticky=E + W)
            col += 1
        yScroll.config(command=self.yview)
        self.yScroll = yScroll

        self.sortby.set(AList[0])
        for l in AList:
            self.b2.menu.add_radiobutton(label=l, variable=self.sortby, value=l, command=self.updateLists)

    def getSelect(self, event):
        index = event.widget.nearest(event.y)
        if self.maxrows != 1:
            index -= index % self.maxrows
            index /= self.maxrows
        self.currentobj = self.SortOrder[int(index)]
        if self.enableSelect == 1:
            self.currentValue.setVal(self.currentobj.name)
        self.defineOptions()

    def defineOptions(self):
        self.b1.menu.delete(0, END)
        if self.admin_mode_only == 0 or dbb.admin_mode == 1:
            self.b1.menu.add_command(label="Create", command=self.DoAdd)
            if self.currentobj != 0:
                if self.currentobj.IsInUse(0) == 0:
                    self.b1.menu.add_command(label="Delete", command=self.DoDelete)
                self.b1.menu.add_command(label="Modify", command=self.DoModify)
            self.b1.menu.add_separator()
        self.b1.menu.add_command(label="Exit", command=self.DoExit)

    def sortViewer(self):
        sortby = self.sortby.get()
        mdir = self.sortdir.get()
        reverse = False
        if mdir == "Descending":
            reverse = True
        index = self.findIndex(sortby)
        sortt = self.ColumnList[index][2]
        if sortt == "S":
            sortkey = str.lower
        else:
            sortkey = int
        Names = []
        ValList = []
        SValList = []
        vFilter = self.showInvisible.get()
        for i in self.MainList.MyList:
            enable = 1
            visable = i.getParam("visible").upper()
            if vFilter == "Invisible":
                if visable == "Y":
                    enable = 0
            elif vFilter == "Visible":
                if visable == "N":
                    enable = 0
            if enable == 1:
                if i.name not in Names:
                    Names.append(i.name)
                pval = i.getParam(sortby)
                if pval not in SValList:
                    SValList.append(pval)
                if sortt == "I":
                    try:
                        val = int(pval)
                        if val not in ValList:
                            ValList.append(val)
                    except:
                        sortt = "S"
                elif sortt == "F":
                    try:
                        val = float(pval)
                        if val not in ValList:
                            ValList.append(val)
                    except:
                        sortt = "S"

        if self.MainList.valtype == "I":
            Names.sort(key=int)
        elif self.MainList.case == "lower":
            Names.sort(key=str.lower)
        elif self.MainList.case == "upper":
            Names.sort(key=str.upper)
        List = []
        for i in Names:
            for j in self.MainList.MyList:
                if j.name == i:
                    List.append(j)

        self.SortOrder = []
        #      sys.stdout.write("Len={},{} List={}\n".format(len(SValList),len(ValList),SValList))
        if sortt == "S" and len(SValList) > 0:
            SValList.sort(key=str.lower, reverse=reverse)
            for i in SValList:
                for j in List:
                    val = j.getParam(sortby)
                    if i == val:
                        self.SortOrder.append(j)
        elif len(ValList) > 0:
            if sortt == "F":
                ValList.sort(key=float, reverse=reverse)
                for i in ValList:
                    for j in List:
                        val = float(j.getParam(sortby))
                        if i == val:
                            self.SortOrder.append(j)
            else:
                ValList.sort(key=int, reverse=reverse)
                for i in ValList:
                    for j in List:
                        val = int(j.getParam(sortby))
                        if i == val:
                            self.SortOrder.append(j)

    def updateList(self, params, index):
        self.objList[index][0].delete(0, END)
        cnt = 0
        for a in self.SortOrder:
            color = a.getColor()
            off = 0
            #         sys.stdout.write("updateList {} cnt={}\n".format(a.name,cnt))
            for i in range(self.maxrows):
                if i < len(params[0]):
                    p = params[0][i]
                    mlist = a.getParam(p)
                else:
                    p = mlist = ""
                #             sys.stdout.write("{} {}={}\n".format(a.objID,p,mlist))
                self.objList[index][0].insert(cnt + off, mlist)
                self.objList[index][0].itemconfig(cnt + off, bg=color, fg=seekRevColor(color))
                off += 1
            cnt += self.maxrows

    def updateLists(self):
        if self.enableSelect == 1:
            if self.currentobj != 0:
                self.currentValue.setVal(self.currentobj.name)
            else:
                self.currentValue.setVal("")
        self.sortViewer()
        pos = self.yScroll.get()
        i = 0
        for b in self.ColumnList:
            self.updateList(b, i)
            i += 1
        for l in self.objList:
            l[0].yview_moveto(pos[0])

    def findIndex(self, name):
        i = 0
        for b in self.ColumnList:
            if name in b[0]:
                return i
            i += 1
        return -1

    def yview(self, *args):
        for l in self.objList:
            l[0].yview(*args)

    def DoAdd(self):
        return

    def DoModify(self):
        if self.currentobj == 0:
            return
        return

    def DoDelete(self):
        if self.currentobj == 0:
            return
        if self.currentobj.IsInUse(0) == 0:
            self.MainList.delete(self.currentobj.name)
            self.currentobj = 0
        else:
            sys.stdout.write("{} is in use\n".format(self.currentobj.name))

        self.updateLists()
        self.defineOptions()


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------      
class Editor(Base):
    def __init__(self, desc, objEdit, viewer, MainList, wname):
        self.enableChecking = 1
        info = ""
        #      self.dbobj.setParam("type","editor")
        if objEdit == 0:
            #         desc = "Creating new {}".format(desc)
            info = "New"
            title = "{} Editor (draft) for NEW".format(desc)
            self.newone = 1
        else:
            #         desc = "{} Editor for {}".format(desc,objEdit.name)
            info = "Modify"
            title = "{} for {}".format(desc, objEdit.name)
            self.newone = 0

        if Base.__init__(self, wname, title) == 0:
            return 0

        self.MainList = MainList
        self.viewer = viewer
        if self.newone == 1:
            self.currentobj = MainList.find("newone")
        else:
            self.currentobj = objEdit

        height = 15
        self.b0 = WinLabel(self, info, -1, 0, 2)

        self.b1 = WinMenu(self, "Options", -1, 0, 1)
        self.b1.menub.grid(sticky=E + W)
        self.defineOptions()

        self.objList = []
        self.ColumnList = []
        return 1

    ## ColumnList has the following format: Name, Width, Value Display, VarType
    ## Name/Width is name/width of column
    ## Value Display is how to treat the value for the name
    ##    ENTRY = This process can edit
    ##    LABEL = This process cannot edit
    ##    MENU = Put in a menu list
    ##    ENTRYMENU = Put in a menu list and/or edit directly
    ##    Otherwise, nothing is done based on ColumnList
    ## VarType is S=String or I=Int
    ##    If I is set, but S values are detected, then the process changes to S.
    def defineOptions(self):
        self.b1.menu.delete(0, END)
        if self.enableChecking == 1:
            self.b1.menu.add_command(label="Check", command=self.DoCheck)
        self.b1.menu.add_command(label="Save & Exit", command=self.DoSave)
        self.b1.menu.add_command(label="Exit", command=self.DoExit)

    def setupEditItem(self, item, row, col):
        name = item[0]
        width = item[1]
        mtype = item[2]
        check = 0
        if self.enableChecking == 1:
            check = Label(self.master, text="-", font=self.font, bg=self.bgc, fg=self.fgc, relief=RAISED)
            check.grid(row=row, column=col, sticky=E + W)
            col += 1
        if mtype != "CALCVAR":
            if mtype != "MENUENTRY":
                label = Label(self.master, text=name, font=self.font, bg=self.bgc, fg=self.fgc, relief=RAISED)
                label.grid(row=row, column=col, sticky=E + W)
            else:
                menub = Menubutton(self.master, bg=self.mbgc, fg=self.mfgc,
                                   font=self.font, text=name, relief=RIDGE)

                menu = Menu(menub, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=0)
                menub.config(menu=menu)
                menub.grid(row=row, column=col, sticky=E + W)

            col += 1
        f1 = entry = Var = 0
        self.Var = 0
        self.Menu = 0
        value = ""
        if self.currentobj != 0:
            value = self.currentobj.getParam(name)
        mytype = "N"
        if mtype == "CALCVAR":
            entry = Label(self.master, text=value, font=self.font, bg=self.bgc, fg=self.fgc, relief=RAISED)
            entry.grid(row=row, column=col, sticky=E + W)
            mytype = "CV"
        elif mtype == "LABEL":  ## This editor cannot change the value!
            entry = Label(self.master, text=value, font=self.font, bg=self.bgc, fg=self.fgc, relief=RAISED)
            entry.grid(row=row, column=col, sticky=E + W)
            if self.enableChecking == 1:
                check.config(text="*", bg="green3")
            mytype = "L"
        elif mtype == "ENTRY":  ## This editor can change the value!
            self.Var = StringVar()
            entry = Entry(self.master, textvariable=self.Var, font=self.font, bg=self.qbgc, fg=self.qfgc, relief=SUNKEN,
                          width=width)
            entry.grid(row=row, column=col, sticky=E + W)
            self.Var.set(value)
            mytype = "E"
        elif mtype == "MENUENTRY":  ## This editor can change the value!
            self.Var = StringVar()
            entry = Entry(self.master, textvariable=self.Var, font=self.font, bg=self.qbgc, fg=self.qfgc, relief=SUNKEN,
                          width=width)
            entry.grid(row=row, column=col, sticky=E + W)
            self.Var.set(value)
            self.setupMenuEntryBase(menu, entry, self.currentobj.getParamList(name))
            mytype = "ME"
        elif mtype == "MENU":  ## This editor can change the value!
            self.setupMenuBase(row, col, self.currentobj.getParamList(name), lambda vparm=name: self.updateMenu(vparm))
            self.Menu.menub.config(text=value)
            self.Var.set(value)
            mytype = "M"
        elif mtype == "MENUDB":  ## This editor can change the value!
            self.setupMenuBaseDB(name, row, col, 0, lambda vparm=name: self.updateMenu(vparm))
            self.Var.set(value)
            mytype = "MDB"

        self.objList.append([entry, self.Menu, self.Var, check, name, mytype])

    def update(self):
        self.updatePlain()
        self.defineOptions()

    def updatePlain(self):
        if self.currentobj == 0:
            return
        i = 0
        for c in self.ColumnList:
            value = self.currentobj.getParam(c[0])
            if c[2] == "LABEL":
                self.objList[i][0].config(text=value)
            elif c[2] in ["MENU", "MENUDB"]:
                self.objList[i][1].menub.config(text=value)
                self.objList[i][2].set(value)
            elif c[2] in ["ENTRY", "MENUENTRY"]:
                self.objList[i][2].set(value)
            i += 1

    def setupMenuEntryBase(self, menu, entry, ListIn):
        for l in ListIn:
            menu.add_command(label=l, command=lambda name=l, e=entry: self.setMenuEntry(e, name))

    def setMenuEntry(self, entry, value):
        entry.delete(0, END)
        entry.insert(0, value)

    def setupMenuBase(self, row, col, ListIn, Command):
        self.Menu = WinMenu(self, "", -1, row, col)
        self.Menu.menub.grid(sticky=E + W)
        self.Var = StringVar()
        for l in ListIn:
            self.Menu.menu.add_radiobutton(label=l, variable=self.Var, value=l, command=Command)

    def setupMenuBaseDB(self, name, row, col, entry, Command):
        self.Menu = WinMenu(self, "", -1, row, col)
        self.Menu.menub.grid(sticky=E + W)
        self.Var = StringVar()
        menuStack = []
        pmenu = self.Menu.menu
        ListIn = self.currentobj.getMenuInfoDB(name)
        for i in ListIn:
            if i[0] == "push":
                menuStack.append(pmenu)
                nmenu = Menu(pmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=0)
                pmenu.add_cascade(label=i[1], menu=nmenu)
                pmenu = nmenu
            elif i[0] == "pop":
                pmenu = menuStack.pop()
            elif i[0] == "add":
                if entry != 0:
                    pmenu.add_command(label=i[1],
                                      command=lambda name=i[1], value=i[2], e=entry: self.setMenuEntry(e, name))
                else:
                    pmenu.add_radiobutton(label=i[1], indicatoron=1, variable=self.Var, value=i[2],
                                          command=lambda vparm=name: self.updateMenu(vparm))

    def findIndex(self, name):
        i = 0
        for b in self.ColumnList:
            if b[0] == name:
                return i
            i += 1
        return -1

    def updateMenu(self, name):
        update = 0
        obj_name = self.objList[0][4]
        index = self.findIndex(name)
        if index != -1:
            if self.objList[index][2] != 0:
                value = self.objList[index][2].get()
                if self.currentobj != 0 and self.currentobj.name == obj_name:
                    self.currentobj.setParam(name, value)
                    self.updateList(name, index)
                    update = 1
                if self.objList[index][1] != 0:
                    self.objList[index][1].menub.config(text=value)
        return update

    ##    def updateMenuDB(self,name,valueName):
    ##      update=0
    ##      obj_name = self.objList[0][4]
    ##      index = self.findIndex(name)
    ##      if index!=-1:
    ##          if self.objList[index][2]!=0:
    ##             value = self.objList[index][2].get()
    ##             if self.currentobj!=0 and self.currentobj.name == obj_name:
    ##                self.currentobj.setParam(name,value)
    ##                self.updateList(name,index)
    ##                update = 1
    ##             if self.objList[index][1]!=0:
    ##                self.objList[index][1].menub.config(text=valueName)
    ##      return update

    # DoCheck
    # Check the editor and make certain everything meets the criteria
    #
    # returns # of errors
    def DoCheck(self):
        if self.enableChecking == 0:
            return
        i = 0
        errors = 0
        for c in self.ColumnList:
            if c[2] in ["ENTRY", "MENU", "MENUENTRY"]:
                val = self.objList[i][2].get()
                code = "!"
                bg = "green3"
                if len(val) < c[4]:
                    code = "X"
                    bg = "red"
                    errors += 1
                if c[3] == "I":
                    try:
                        ival = int(val)
                    except ValueError:
                        code = "X"
                        bg = "red"
                        errors += 1
                #             sys.stdout.write("{} = {} len={}({}) code={}\n".format(c[0],val,len(val),c[4],code))
                self.objList[i][3].config(text=code, bg=bg)
            i += 1
        while i < len(self.objList):
            if self.objList[i][5] in ["O", "S", "P"]:
                cnt = 0
                got_errs = 0
                code = "!"
                bg = "green3"
                ival = -1
                car1 = car2 = ""
                for j in self.objList[i][2]:
                    val = j.get()
                    if cnt == 0:  # Cars (Integer >= 0)
                        try:
                            ival = int(val)
                            if ival < 0:
                                got_errs += 1
                        except ValueError:
                            got_errs += 1
                    else:  # Car identifiers (string where length > 1)
                        if len(val) == 0:
                            got_errs += 1
                        if cnt == 1:
                            car1 = val
                        else:
                            car2 = val
                    #                sys.stdout.write("{}[{}] val={} errors={}\n".format(self.objList[i][4],cnt,val,got_errs))
                    cnt += 1
                if ival > 0 and car1 == car2:
                    got_errs += 1

                if got_errs > 0:
                    code = "X"
                    bg = "red"
                    errors += got_errs
                self.objList[i][3].config(text=code, bg=bg)
            #             sys.stdout.write("Checking {}:{}\n".format(self.objList[i][4],self.objList[i][5]))
            i += 1
        return errors

    def CheckModified(self):
        i = 0
        for c in self.ColumnList:
            if c[2] in ["ENTRY", "MENU", "MENUENTRY"]:
                val = self.objList[i][2].get()
                ref = self.currentobj.getParam(c[0])
                if val != ref:
                    return 1
            i += 1
        return 0

    def findName(self, name):
        code = 1
        while code == 1:
            code = self.MainList.seek(name)
            if code != 0:
                name += "!"
        return name

    def updateObj(self, obj, nameupdate):
        i = 0
        for c in self.ColumnList:
            if c[2] in ["ENTRY", "MENU", "MENUENTRY", "MENUDB"]:
                val = self.objList[i][2].get()
                if c[0].upper() == "NAME":
                    if nameupdate == 1:
                        obj.setParam(c[0], val)
                    elif nameupdate == 0:
                        pname = obj.getParam(c[0])
                        if pname != val:
                            val = self.findName(val)
                            obj.setParam(c[0], val)
                            self.objList[i][2].set(val)
                    else:
                        obj.setParam(c[0], nameupdate)
                else:
                    obj.setParam(c[0], val)
            i += 1

        for i in self.objList:
            if i[5] in ["O"]:
                sys.stdout.write("PS: Got {} {}\n".format(i[5], i[4]))
                obj.setCarStruct("overhead", 0, i[2][1].get(), i[2][2].get(), i[2][0].get())
            elif i[5] in ["P", "S"]:
                sys.stdout.write("PS: Got {} {}\n".format(i[5], i[4]))
                param = "pickup"
                if i[5] == "S":
                    param = "setoff"
                ctrl = dbb.CtrlObjList.seek(i[4])
                if ctrl != 0:
                    sys.stdout.write(
                        "{}: {} => {},{},{}\n".format(param, ctrl.name, i[2][1].get(), i[2][2].get(), i[2][0].get()))
                    obj.setCarStruct(param, ctrl, i[2][1].get(), i[2][2].get(), i[2][0].get())

        obj.Setup()

    def DoSaveInfo(self):
        if self.newone == 0:
            if self.currentobj != 0:
                self.updateObj(self.currentobj, 0)
            return

        i = 0
        name = -1
        for c in self.ColumnList:
            if c[0] == "Name":
                name = self.objList[i][2].get().upper()
                break
            i += 1
        if name == -1:
            return

        nname = ""
        mode = 0
        for i in name:
            if i in [",", "-", "[", "]"]:
                nname += " "
                if i == ",":
                    mode |= 0x1
                if i == "-":
                    mode |= 0x2
                if i == "[":
                    mode |= 0x4
            else:
                nname += i

        wname = nname.split()
        nlen = len(wname)

        first = 1
        if mode < 4:
            self.updateObj(self.currentobj, name)

        elif mode == 5:
            i = 1
            while i < nlen:
                name = self.findName("{}{}".format(wname[0], wname[i]))
                if first == 1:
                    first = 0
                    self.updateObj(self.currentobj, name)
                else:
                    obj = EngineList.find(name)
                    self.updateObj(obj, 0)
                i += 1

        elif mode == 6:
            start = stop = -1
            incr = 1
            start = int(wname[1])
            stop = int(wname[2])
            if start > stop:
                i = stop
                stop = start
                start = i
            if nlen == 4:
                incr = int(wname[3])

            if nlen in [3, 4]:
                while start <= stop:
                    name = self.findName("{}{}".format(wname[0], start))
                    if first == 1:
                        first = 0
                        self.updateObj(self.currentobj, name)
                    else:
                        obj = EngineList.find(name)
                        self.updateObj(obj, 0)
                    start += incr

    def DoSaveFinish(self):
        if self.viewer != 0:
            self.viewer.updateLists()
        for j in dbb.db.AllLists:
            if j.Modified == 1:
                j.Modified = 0
                j.save()
        dbb.SaveSys()
        self.DoExit()

    def DoSave(self):
        self.DoSaveInfo()
        self.DoSaveFinish()


def setSelectedObj(nw, obj):
    if obj == 0:
        return
    for d in nw.DrawList:
        if d.ctrl == obj and obj != 0:
            nw.setSelectedObj(obj)
            return


def updateBranchWindow(branchID, selected_obj):
    global default_resize_timeout
    db = dbb.ProtoObjList.seek(branchID)
    nw = WinSeek("branches")
    nw.branchDB = db
    if nw.DB == db:
        return
    nw.disable_resize = 1
    nw.DB = db
    nw.setObjSizes()
    nw.deleteDrawList()
    nw.Build(db, 0, 0, [])

    for i in db.objList:
        if i.mytype == "TERMINAL":
            xsign = ysign = 1
            if i.BranchPnt.x > 0:
                xsign = -1
            if i.BranchPnt.y > 0:
                ysign = -1

            t = dbb.TextConfigList.seek("panel")
            MPnts = []
            Label = obj.Label(name="To Main^N-S", ID=i.ID)
            Label.transcale.Copy(i.transcale)
            Label.label.CopyN(t)
            Label.label.name = "<label"
            Label.label.size = 20
            Label.label.color = cdb.ColorList.getParam("BRANCH_TEXT")
            if abs(i.BranchPnt.x) > abs(i.BranchPnt.y):
                xsize = xsign * 1.0
                P1 = Point(i.A.x + xsize, i.A.y)
                MPnts.append([i.A, P1])
                Label.label.anchor = "n"
                P2 = Point(i.A.x + 0.5 * xsize, i.A.y)
                PLabel = Point(i.A.x + 0.5 * xsize, i.A.y + 0.1)
            else:
                xsize = xsign * 0.25
                ysize = ysign * 1.5
                P1 = Point(i.A.x, i.A.y + ysize)
                P2 = Point(i.A.x + xsize, i.A.y + ysize)
                P3 = Point(i.A.x - xsize, i.A.y + ysize)
                MPnts.append([i.A, P1])
                #             MPnts.append([P2,P3])
                if ysign == -1:
                    Label.label.anchor = "s"
                    PLabel = Point(P1.x, P1.y - 0.1)
                else:
                    Label.label.anchor = "n"
                    PLabel = Point(P1.x, P1.y + 0.1)

            Label.label.setX(PLabel.x)
            Label.label.setY(PLabel.y)
            Label.label.textpos = "n"
            Label.build(nw, 0, 0, [], db)

            color = cdb.ColorList.getParam("BRANCH")
            for j in MPnts:
                line = obj.Line(j[0], j[1], color=color, width=5, ID=i.ID)
                line.transcale.Copy(i.transcale)
                line.build(nw, 0, 0, [], db)

    for d in nw.DrawList:
        d.ObjCreate()
        if d.protoElem.ctrl != 0:
            d.protoElem.ctrl.modified = 1
    title = db.title.translate(strmaketrans("^", " "))
    nw.master.title("{} (Branch View)".format(title))
    nw.defineOptions()
    nw.AutoSetScroll()
    setSelectedObj(nw, selected_obj)
    nw.internal_resize_timer = default_resize_timeout
    nw.disable_resize = 0


# --------------------------------------------------------------------------------
# --------------------------------------------------------------------------------
WinBaseKeyWords = ["name string", "type string", "width int -1", "height int -1", "xpos int 0", "ypos int 0",
                   "scale float 1.0", "zoom float 1.0"]


class WinBase(db.ParamObj):
    def __init__(self, name, MainList):
        db.ParamObj.__init__(self, name.upper(), MainList)
        self.name = name
        self.subtype = []
        self.DrawList = []
        self.title = ""
        self.xsize = 0
        self.ysize = 0
        self.feature_size = 0

    def saveInfo(self, f, ref, keywords):
        if self.getV("type") == "none":
            return
        for s in keywords:
            v = self.getBase(s)
            f.write("{} {} {}={}\n".format(ref, self.objID, s, v))


class WinBaseListObj(db.ListObj):
    def __init__(self):
        db.ListObj.__init__(self, "winbase", "upper", "S", WinBaseKeyWords)
        self.user_option = 1
        self.nameasID = 1

    def create(self, name):
        i = WinBase(name, self)
        self.MyList.append(i)
        return i


WinBaseList = WinBaseListObj()
