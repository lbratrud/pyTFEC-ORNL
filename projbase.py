# mtcsbase.py
#
# This file is a list of constants and simple routines that I use everywhere!
#
# --------------------------------------------------------------------------------
# Created 1/1/2015 by Lloyd Clonts clontslg@yahoo.com
# Project started December 2013 for Gary W. Turner
# for implementing CTC system

import sys
import os
import time
import sys

Version = "f.0 (05/03/2019)"
SCALE_FONTSIZE_RATIO = 1/90

NULL_DEF = ["none"]
if sys.version_info[0]>2:
   def strmaketrans(s1,s2):
      return str.maketrans(s1,s2)
   def byte2chr(byte):
      if isinstance(byte,int):
         return chr(byte)
      else:
         return byte
   def IntCheck(value):
       if isinstance(value, int):
           return 1
       else:
           return 0
else:
   import types
   import string
   def strmaketrans(s1,s2):
      return string.maketrans(s1,s2)
   def byte2chr(byte):
      if type(byte) is types.IntType:
         return chr(byte)
      else:
         return byte
   def IntCheck(value):
       if type(value) is types.IntType:
           return 1
       else:
           return 0

# --------------------------------------------------------------------------------
# Basic routines!
def cconcat(words,start,stop):
   s = ""
   for i in range(start,stop):
       s += words[i]
       if i!=stop-1:
           s += " "
   return s

# --------------------------------------------------------------------------------
def valueToTime(value):
   millisecond = int(1000*value) - 1000*int(value)
   cnt = int(value)
   msec = cnt % 60
   cnt -= msec
   cnt /= 60
   mmin = cnt % 60
   cnt -= mmin
   cnt /= 60
   mhr = cnt % 24
   cnt -= mhr
   cnt /= 24
   msg = ""
   if millisecond > 0:
      msec += 0.001*millisecond
   first = 1
   for i in [["d",int(cnt)],["h",int(mhr)],["m",int(mmin)],["s",msec]]:
      if i[1] > 0:
         if first==0:
            msg += ":"
         first = 0
         msg += "{}{}".format(i[1],i[0])
   return msg


# --------------------------------------------------------------------------------
# List routines!
def ListAdd(mylist,obj):
   if obj not in mylist:
      mylist.append(obj)
def ListRemove(mylist,obj):
   if obj in mylist:
      mylist.remove(obj)

def ListCheck(list1,list2):
   for l in list2:
      if l in list1:
         return 1
   return 0

def ListVerify(list1,list2):
  errors = 0
  for i in list1:
     if i not in list2:
        errors += 1
  for i in list2:
     if i not in list1:
        errors += 1
  return errors

# --------------------------------------------------------------------------------
# Reverse Color Routines
# --------------------------------------------------------------------------------
def getRevColor(ri,gi,bi):
    avg = int((ri+gi+bi)/3)
    bs = 255
    if avg > 128:
       bs = 0
       
    return "#%02x%02x%02x" % (bs,bs,bs)

def color2IntRGB(color):
    r = int(color[1:3],base=16)
    g = int(color[3:5],base=16)
    b = int(color[5:7],base=16)
    return [r,g,b]

def color2RGB(color):
    r,g,b = color2IntRGB(color)
    return [1.0*r/256,1.0*g/256,1.0*b/256]

def seekRevColor(color):
    r,g,b = color2IntRGB(color)
    return getRevColor(r,g,b)

# --------------------------------------------------------------------------------
# Basic X,Y point in space
# --------------------------------------------------------------------------------      
class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    def doc(self):
        return  "{},{}".format(self.x,self.y)

# --------------------------------------------------------------------------------      
# Transformat class for moving and putting things at angles
# --------------------------------------------------------------------------------      
class TranScale:
    def __init__(self):
       self.xscale = 1
       self.xoff = 0
       self.yscale = 1
       self.yoff = 0

    def Copy(self,other):
       self.xscale = other.xscale
       self.xoff = other.xoff
       self.yscale = other.yscale
       self.yoff = other.yoff

    def doc(self):
        return  "scale={},{} offset={},{}".format(self.xscale,self.yscale,self.xoff,self.yoff)

