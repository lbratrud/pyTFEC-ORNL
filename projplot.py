# projdbb.py
#
# Define all ObjLists and how they relate to each other.
#
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@yahoo.com)
# Project started December 2013 for Gary W. Turner
# for implementing CTC system based on C/MRI hardware platform

import time
import math
import string
import sys
import os
import commondb as cdb
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as axes3d
from matplotlib import cm
from projbase import *

cdb.ColorList.addwDesc("PLOT1","#000000","PLOT")
cdb.ColorList.addwDesc("PLOT2","#0000ff","PLOT")
cdb.ColorList.addwDesc("PLOT3","#00ff00","PLOT")
cdb.ColorList.addwDesc("PLOT4","#ff0000","PLOT")
cdb.ColorList.addwDesc("PLOT5","#00ffff","PLOT")
cdb.ColorList.addwDesc("PLOT6","#808080","PLOT")
cdb.ColorList.addwDesc("PLOT7","#008000","PLOT")
cdb.ColorList.addwDesc("PLOT8","#ffff00","PLOT")

markerList = ['s','o','v','+','d','D','_','|']
VERSION="1.0-11/17/2016"

class DataPlot:
   def __init__(self,ncols):
      self.fig, self.AxList = plt.subplots(ncols=ncols)              
      plt.subplots_adjust(wspace=0.4,hspace=0.2)
      self.resetReference()
  
   def resetReference(self):
      self.color_index = 1
      self.marker_index = 0

   def nextReference(self):
       self.color_index += 1
       if self.color_index > 8:
          self.color_index = 1
          self.marker_index += 1
          if self.marker_index > len(markerList):
             self.marker_index = 0 
   def addXY(self,myplot,xList,yList):
       global markerList
       color = cdb.ColorList.getParam("PLOT{}".format(self.color_index))
       marker = markerList[self.marker_index]
       myplot.plot(xList,yList,color=color,marker=marker)

   def makePlot(self):
      plt.show()
