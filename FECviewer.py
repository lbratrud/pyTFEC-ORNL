#!/usr/bin/env python3

# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import threading
import string
import sys
import os
import _thread
import glob
from projbase import *

import commondb as cdb
import FEC_DATA

cdb.ColorList.addwDesc("PYFEC_STATUS_INIT", "#4040ff", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_RUNNING", "#ffffff", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_IDLE", "#40ff40", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_ATTN", "#ffff40", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_ERROR", "#ff4040", "PYFEC")

cdb.ColorList.addwDesc("PYFEC_BUTTON", "#4080ff", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_BUTTON_PUSH", "#00ff00", "PYFEC")

cdb.ColorList.addwDesc("PYFEC_VIEW_BUTTON_PUSH", "#4080ff", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_VIEW_BUTTON", "#000000", "PYFEC")


cdb.ColorList.addwDesc("TEST_STATUS_PASSED", "#40ff40", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_WARN", "#ffff40", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_FAILED", "#ff4040", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_IDLE", "#4040ff", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_DONE", "#40ff40", "TEST")

cdb.ColorList.addwDesc("BARCODE_OPR", "#4040ff", "BARCODE")
cdb.ColorList.addwDesc("BARCODE_NONE", "#ff4040", "BARCODE")
cdb.ColorList.addwDesc("BARCODE_SET", "#40ff40", "BARCODE")

cdb.ColorList.addwDesc("STEP_TODO", "#4040ff", "STEP")
cdb.ColorList.addwDesc("STEP_WORKING", "#ff4040", "STEP")
cdb.ColorList.addwDesc("STEP_DONE", "#40ff40", "STEP")

Version = "1.2-05_06_2019"

import projwin as win
try:  # import as appropriate for 2.x vs. 3.x
    from tkinter import *
except:
    from Tkinter import *

import FEC_IO
# or
# try:  # import as appropriate for 2.x vs. 3.x
#   from reportlab.pdfgen import canvas
#   pdf_generator_enabled = 1
# except:
#   sys.stdout.write("Cannot find reportlab. PDF Generator turned off\n")
#   pdf_generator_enabled = 0

do_exit = 0


def ThreadLoadPDF(Params):
#    sys.stdout.write("ThreadLoadPDF={}\n".format(Params))
    FEC_IO.ExecuteCmdParse("evince {}".format(Params))


UpdateWindowsList = []
# =----------------------------------------------------------------------------------------------
# =----------------------------------------------------------------------------------------------
def seekID(dList,mybID):
    for i in dList:
        if i[0] == mybID:
            return i
    return 0


def seekSID(dList,mySID):
    for i in dList:
        if i[1] == mySID:
            return i
    return 0


class WindowMain(win.Base):
    def __init__(self,):
        self.Log = 0
        if win.Base.__init__(self, "mmain", "FEC Viewer Station #{} Version={}".format(FEC_IO.myCompID,Version)) == 0:
            return 0
        row = 0
        self.b1 = win.WinMenu(self, "Options", -1, row, 1)
        self.b1.menub.config(fg=cdb.ColorList.getParam("PYFEC_BUTTON_PUSH"))
        self.b1.menub.grid(sticky=E + W + S + N)
        win.WinRowFeed(self, 1)
        win.WinColumnFeed(self, 8)

        mainmenu = self.b1.menu
        self.path = "/data/TPC-FEC"
        os.chdir(self.path)
        mList = glob.glob("fid*")
        self.fData = []
        self.bID = []
        self.hID = []
        self.fID = []
        self.boardCnt = 0
        self.passedCnt = 0
        for m in mList:
            binfo = self.GetFidInfo(m)
            bid = int(binfo[0])
            if bid > 0 and int(binfo[1],16) > 0:
                self.fData.append(binfo)
                self.boardCnt += 1 
                if binfo[2][0:6]=="PASSED":
                   self.passedCnt += 1 
#                sys.stdout.write("Adding {} {}\n".format(bid, binfo[1]))
                if bid in self.bID:
                    sys.stdout.write("Got duplicate board ID: {} @ ScaID={}\n".format(bid,binfo[1]))
                ListAdd(self.bID,bid)
                ListAdd(self.fID,binfo[1])

        self.bID.sort(key=int,reverse=0)
        self.fID.sort(reverse=0)
        # for i in self.bID:
        #     ListAdd(self.hID, hex(i)[2:])

        # sys.stdout.write("bID={}\n".format(bID))
        # sys.stdout.write("fID={}\n".format(fID))

        #for j in range(0,4000,10):
        #    for i in self.fData:
        #        if i[0] >= j and i[0] < j+10:
        #            self.MakeFidInfoEntity(i,mainmenu,path)
        #            break

        mainmenu.add_command(label="Load PDF report", command=self.loadPDF)
        mainmenu.add_separator()
        mainmenu.add_command(label="Exit", command=self.doSysExit)

        self.jobID = 1
        col = 1
        row = 2
        row_init = row
        col = 1
        row += 1
        # self.bIDList = win.WinScrollList(self, 3, "Decimal bID", 15, 15, row, 0, self.bID, mycolumnspan=2)
        # self.bIDList.ListB.bind("<Double-Button-1>", self.getbID)
        # self.bIDList = win.WinScrollList(self, 3, "Hex bID", 15, 15, row, 3, self.hID, mycolumnspan=2)
        # self.bIDList.ListB.bind("<Double-Button-1>", self.gethID)
        # self.fIDList = win.WinScrollList(self, 3, "SCA ID", 15, 15, row, 6, self.fID, mycolumnspan=2)
        # self.fIDList.ListB.bind("<Double-Button-1>", self.getfID)
        self.bIDList = win.WinScrollList(self, 3, "Decimal bID", 15, 15, row, 0, [], mycolumnspan=2)
        self.bIDList.ListB.bind("<Double-Button-1>", self.getbID)

        self.hIDList = win.WinScrollList(self, 3, "Hex bID", 15, 15, row, 3, [], mycolumnspan=2)
        self.hIDList.ListB.bind("<Double-Button-1>", self.gethID)

        self.fIDList = win.WinScrollList(self, 3, "SCA ID", 15, 15, row, 6, [], mycolumnspan=2)
        self.fIDList.ListB.bind("<Double-Button-1>", self.getfID)
        index = 0
        for i in self.bID:
                myfData = seekID(self.fData,i)
#                sys.stdout.write("{} myfData={}\n".format(i,myfData))
                if myfData[2][0:6]=="PASSED":
                    color = "#00ff00"
                else:
                    color = "#ff0000"
                self.bIDList.ListB.insert(index, i)
                self.bIDList.ListB.itemconfig(index, bg=color, fg=seekRevColor(color))
                ListAdd(self.hID, hex(i)[2:])
                self.hIDList.ListB.insert(index, hex(i)[2:].upper())
                self.hIDList.ListB.itemconfig(index, bg=color, fg=seekRevColor(color))
                index += 1
        index = 0
        for i in self.fID:
                myfData = seekSID(self.fData,i)
#                sys.stdout.write("{} myfData={}\n".format(i,myfData))
                if myfData[2][0:6]=="PASSED":
                    color = "#00ff00"
                else:
                    color = "#ff0000"
                self.fIDList.ListB.insert(index, i)
                self.fIDList.ListB.itemconfig(index, bg=color, fg=seekRevColor(color))
                index += 1


        col += 7
        row = 1
        coln = col

#        self.BoardCnt = win.WinValueLabel(self, "Boards Total", -1, row, coln)
#        self.BoardCnt.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"),fg="#000000")
#        self.BoardCnt.setVal("{}".format(self.boardCnt))
#        row += 1

#        self.PassedCnt = win.WinValueLabel(self, "Boards Passed", -1, row, coln)
#        self.PassedCnt.entry.config(bg="#40ff40",fg="#ffffff")
#        self.PassedCnt.setVal("{}".format(self.passedCnt))
#        row += 1

        row = row_init+1
        self.ResultsList = []
        self.BoardID = win.WinValueLabel(self, "BoardID", -1, row, col)
        self.BoardID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))
        self.BoardID.setVal("Unknown")
        row += 1
        for i in ["Pwr", "Noise", "Pedestal", "SCA Info", "Gain 20x", "Gain 30x", "CrossTalk","CrossTalkVictim"]:
            VL = win.WinValueLabel(self, "{} Test".format(i), 15, row, col)
            VL.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"),fg="#000000")
            row += 1
            self.ResultsList.append([i,VL])
        #row += 1
        self.ScaID = win.WinValueLabel(self, "ScaID", -1, row, col)
        row += 1
        self.Status = win.WinValueLabel(self, "Status", -1, row, col)
        row += 1
        win.WinSetDummyLabel(self,row,col)
        row += 1
        self.PassPerc = win.WinValueLabel(self, "Passed / Total", -1, row, col)
        self.PassPerc.entry.config(bg="#40ff40",fg="#ffffff")
        self.PassPerc.setVal("{:4d}/ {:4d} = {:.2f}".format(self.passedCnt,self.boardCnt,self.passedCnt/self.boardCnt))
        col += 3
        win.WinSetDummyLabel(self,row+15,col+3)
#        self.TestLog = win.WinScrollList(self, 3, "Test Log", 15, 15, row_init, col, [])
        self.Clear()
        UpdateWindowsList.append(self)

    #
    # def defineOptions(self):
    #    if self.Log==0:
    #       return
    #
    #    mode = ""
    #    mainmenu = self.b1.menu
    #    mainmenu.delete(0,END)
    #    mainmenu.add_command(label="Clear Log",command=lambda cmd="clearlog":self.DoCmd(cmd))
    #    mainmenu.add_command(label="Run Basic Tests",command=lambda cmd="basictests":self.DoCmd(cmd))
    #    mainmenu.add_command(label="Run Pulser Tests",command=lambda cmd="pulsertests":self.DoCmd(cmd))
    #    mainmenu.add_separator()
    #    mainmenu.add_command(label="Exit (FEC remains powered)",command=self.doSysExit)
    #    mainmenu.add_command(label="Shutdown (FEC is powered down)",command=self.doSysShutdown)


    def getbID(self,event):
        index = event.widget.nearest(event.y)
        mybID = int(self.bID[index])
        for i in self.fData:
            if i[0]==mybID:
                #sys.stdout.write("BID={}\n".format(i))
                self.fecData = self.getData("fid{}".format(i[1]))
                self.UpdateData()
                return
        self.UpdateData()
        return

    def gethID(self,event):
        index = event.widget.nearest(event.y)
        mybID = int(self.hID[index],16)
        for i in self.fData:
            if i[0]==mybID:
                self.fecData = self.getData("fid{}".format(i[1]))
                self.UpdateData()
                return
        fecData = FEC_DATA.FEC_data()
        self.UpdateData()

    def getfID(self,event):
        index = event.widget.nearest(event.y)
        myfID = self.fID[index]
        self.fecData = self.getData("fid{}".format(myfID))
        self.UpdateData()

    def getData(self, fidinfo):
        fecData = FEC_DATA.FEC_data()
        p1 = os.path.join(self.path, fidinfo.lower())
        p2 = os.path.join(p1, "Summary")
        FEC_DATA.readSummary(fecData, p2)
        return fecData

    def GetFidInfo(self,fidinfo):
        fecData = self.getData(fidinfo)
        return [int(fecData.getBID(),16),fecData.getFID(),fecData.getBoardStatus()]


    def loadPDF(self,fname):
        _thread.start_new_thread(ThreadLoadPDF, (fname,))

    def UpdateData(self):
        #sys.stdout.write("UpdateData {},{}\n".format(int(self.fecData.getBID(),16),self.fecData.getFID()))

        self.CheckData()
        self.ScaID.setVal(self.fecData.getFID())
        self.BoardID.setVal("0x{},  {:0>4}".format(self.fecData.getBID(),self.fecData.BoardID))

        mainmenu = self.b1.menu
        mainmenu.delete(0,END)
        p1 = os.path.join(self.path, "fid{}".format(self.fecData.getFID().lower()))
        p2 = os.path.join(p1, "Summary")
        try:
            os.chdir(p2)
            mList = glob.glob("auto*.pdf")
            if len(mList) > 0:
                msub = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
                for i in mList:
                    bname = "autosum_fec{}_{}_".format(self.fecData.getBID(),self.fecData.getFID()).upper()
                    pdfname = i.upper().replace(bname,"")
                    mname = os.path.join(p2, i)

                    msub.add_command(label="{}".format(pdfname), command=lambda fname=mname: self.loadPDF(fname))
                mainmenu.add_cascade(label="Load PDF Report", menu=msub)
                mainmenu.add_separator()
        except:
            pass

        mainmenu.add_command(label="Exit", command=self.doSysExit)


    def Clear(self):
        self.setLogView("opr")
        self.TestLog = []
        self.lastseconds = 0
        self.ScaID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"),fg="#000000")
        self.ScaID.setVal("Unknown")
        self.BoardID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"),fg="#000000")
        self.BoardID.setVal("Unknown")
        self.Status.setVal("Unknown")
        self.Status.entry.config(bg="#40ff40",fg="#000000")
        for i in self.ResultsList:
            i[1].setVal("Unknown")
            i[1].entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))

    def setStatus(self,msg,colorcode="PYFEC_STATUS_IDLE",log=0):
        self.Status.setVal(msg)
        self.Status.entry.config(bg=cdb.ColorList.getParam(colorcode))

    def setStep(self,TestStep):
        self.fecIntf.TestStep = TestStep
        self.fecIntf.NewMode = 1
    def setLogView(self,viewMode):
        self.ViewMode = viewMode

    def addToLog(self,msg,color=0):
        self.Log.addLog(msg)
        if color!=0:
            self.Log.setColor(color)

    def TestMsg(self,msg,color="#40ff40"):
        self.TestLog.append([msg,color])

    def FindTestBT(self,testname):
        for i in self.ResultsList:
            if i[0] == testname:
                return i[1]
        return 0

    def TextReportObj(self,got_data,testname,mList):
        #sys.stdout.write("TextReportObj {}\n".format(testname))
        max_yellow = FEC_DATA.get_max_yellows(testname)
        VL = self.FindTestBT(testname)

        if got_data==0:
            VL.setVal("Unknown")
            VL.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"),fg="#000000")
            return -1

        if VL==0:
            sys.stdout.write("Cannot find Button {}\n".format(testname))
            return 1

        if len(mList)==0:
            self.TestMsg("{} PASSED".format(testname),color="#40ff40")
            VL.setVal("PASSED")
            VL.entry.config(bg=cdb.ColorList.getParam("TEST_STATUS_PASSED"),fg="#000000")
            return 0

        cond = [0,0]
        for i in [[0,"RED","#c04040","FAILED"],[1,"YELLOW","#c0c040","WARNED"]]:
            for channel in mList:
                if channel[1]==i[1]:
                    cond[i[0]] += 1

            if cond[i[0]] > 0:
                index = 1
                self.TestMsg("",color="#4040ff")
                self.TestMsg("{} {}={}".format(testname,i[3],cond[i[0]]),color=i[2])
                self.TestMsg("",color="#000000")
                for channel in mList:
                    if channel[1]==i[1]:
                        try:
                            pname = "Ch{}".format(int(channel[0]))
                        except:
                            pname = channel[0]
                        self.TestMsg("[{}] {}={:.3f} {}={:.3f}".format(index,pname, channel[3], channel[2],channel[4]),i[2])
                        index += 1

        color = cdb.ColorList.getParam("TEST_STATUS_FAILED")
        if cond[0] > 0:
            VL.setVal("FAILED-{}".format(cond[0]))
            code = 1
        elif cond[1] > max_yellow:
            VL.setVal("FAILED-Y{}".format(cond[1]))
            self.TestMsg("{} YELLOW conditions {} beyond limit={}".format(testname, cond[1], max_yellow), color=color)
            code = 1
        else:
            VL.setVal("PASSED-Y{}".format(cond[1]))
            color = cdb.ColorList.getParam("TEST_STATUS_PASSED")
            self.TestMsg("{} Passed with {} YELLOW conditions < {}".format(testname, cond[1], max_yellow), color=color)
            code = 0
        VL.entry.config(bg=color,fg="#000000")
        return code

#     def ReadFecSummary(self):
#         if self.fecIntf.fid==-1:
#             return
#         pathList = []
#
#         pathList.append("{}/fid{}/Summary".format(FEC_DATA.DATAPATH,self.fecIntf.fid.lower()))
#         pathList.append("{}/fid{}".format(FEC_DATA.AUTOSUMPATH,self.fecIntf.fid.lower()))
#         for path in pathList:
# #            sys.stdout.write("ReadFecSummary={}\n".format(path))
#             barfile = "{}/barcode_fid{}".format(path,self.fecIntf.fid.lower())
#             self.fecIntf.ReadBarCodes(filename=barfile)
#             code = FEC_DATA.readSummary(self.fecIntf.fecData, path)
#             sys.stdout.write("ReadFecSummary {} = {}\n".format(path,code))
#             if code == "GOT":
#                 break
#         self.fecIntf.loadBarCodes()
#         self.CheckData()

    def CheckData(self):
        code = 0
        found = 0
        self.Clear()
        if self.fecData.got_basic_data==1:
            found += 1
            code += self.TextReportObj(self.fecData.got_basic_data,"Pwr",self.fecData.checkPwr())
            code += self.TextReportObj(self.fecData.got_basic_data,"Noise",self.fecData.checkData(2,self.fecData.RmsLevels))
            code += self.TextReportObj(self.fecData.got_basic_data,"Pedestal",self.fecData.checkData(1,self.fecData.AvgLevels))
            code += self.TextReportObj(self.fecData.got_basic_data, "SCA Info", self.fecData.checkMeasurements())
        if self.fecData.got_pulse_data==1:
            found += 1
            code += self.TextReportObj(self.fecData.got_pulse_data,"Gain 20x",self.fecData.checkGain("20"))
            code += self.TextReportObj(self.fecData.got_pulse_data,"Gain 30x",self.fecData.checkGain("30"))
            code += self.TextReportObj(self.fecData.got_pulse_data,"CrossTalk",self.fecData.checkCrosstalk())
            code += self.TextReportObj(self.fecData.got_pulse_data,"CrossTalkVictim",self.fecData.checkCrosstalkVictum())

        if found!=2:
            status = "UNKNOWN"
            color = cdb.ColorList.getParam("PYFEC_STATUS_INIT")
        elif code > 0:
            status = "FAILED"
            color = cdb.ColorList.getParam("TEST_STATUS_FAILED")
        else:
            status = "PASSED"
            color = cdb.ColorList.getParam("TEST_STATUS_PASSED")

        self.ScaID.entry.config(bg=color,fg="#000000")
        self.BoardID.entry.config(bg=color,fg="#000000")
        self.Status.setVal(status)
        self.Status.entry.config(bg=color,fg="#000000")

    def doSysShutdown(self):
        if self.fecIntf.Supply != 0:
            self.fecIntf.Supply.Off()
        self.doSysExit()

    def doSysExit(self):
#        self.close()
        exit(0)


winmain = WindowMain()

win._root.mainloop()
