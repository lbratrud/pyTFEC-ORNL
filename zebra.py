#!/usr/bin/env python3

import os
# os.environ['PYUSB_DEBUG'] = 'debug'

import usb.core
import usb.util
import time
import pprint

#--------------------------------------------------------------------------------
class ZebraSsi:
  ''' Virtual base class for SSI communication with Zebra devices '''
  SRC_DECODER = 0x00
  SRC_HOST = 0x04

  OPCODE_AIM_OFF = 0xc4
  OPCODE_AIM_ON = 0xc5
  OPCODE_TRIGGER_MODE = 0x8a
  OPCODE_CONTINUOUS_DECODE = 0x289
  OPCODE_BEEP_GOOD_DECODE = 0x38
  OPCODE_BEEP = 0xe6
  OPCODE_PARAM_REQUEST = 0xc7
  OPCODE_BEEPER_VOLUME = 0x8c # Not supported by all scanners (but by DS457 and DS2278)
  OPCODE_BEEPER_TONE = 0x91 # Not supported by all scanners (but by DS457 and DS2278)

  OPCODE_ACK = 0xd0   # Positive acknowledge
  LENGTH_ACK = 0x04
  OPCODE_NACK = 0xd1  # Negative acknowledge

  #--
  def __init__(self):
    self.debug = 0x0

  #--
  class ScanError(Exception):
    def __init__(self, msg):
      self.msg = 'SSI Input Error: ' + msg
  #--
  class ValueError(Exception):
    def __init__(self, msg):
      self.msg = 'SSI Value Error: ' + msg
  #--
  class TransmissionError(Exception):
    def __init__(self, msg):
      self.msg = 'SSI Transmission Error: ' + msg

  #--
  def setDebug(self, debug):
    self.debug = debug

  #--
  def chksum(self, data):
    cs = [-i for i in data]
    data.append((sum(cs) & 0xff00) >> 8)
    data.append(sum(cs) & 0xff)
    return data

  #--
  def isAck(self, data, throw = False):
    if (len(data) < 2) or (data[0] != self.LENGTH_ACK) or (data[1] != self.OPCODE_ACK):
      if (throw):
        raise self.TransmissionError('Non-ack packet received: {:s}'.format(str(data)))
      return False
    return True

  #--
  def dumpAllParameters(self):
    ''' Dumps byte array of scanner parameters on console '''
    PARAMS_ALL = 0xfe
    self.write([0x05, self.OPCODE_PARAM_REQUEST, 0x04, 0x00, PARAMS_ALL], False)
    r = self.read(4096)
    print(len(r), r)

  #--
  def sendParameter(self, param, value, valueType = 'BYTE', permanent = False, beep = False, debug = False):
    # CAREFUL: Supports only 'BYTE' and 'WORD' values right now
    data = [0x0, 0xc6, 0x04, 0x8 if permanent else 0x0, 0x01 if beep else 0xff]

    # Parameter value type
    if (valueType == 'WORD'):
      data.append(0xf4)
    elif (valueType == 'BYTE'):
      pass

    # Parameter number
    if (param >= 1024):
      data.append(0xf8)
      data.append((param & 0xff00) >> 8)
      data.append(param & 0xff)
    elif (param >= 768):
      data.append(0xf2)
      data.append(param - 768)
    elif (param >= 512):
      data.append(0xf1)
      data.append(param - 512)
    elif (param >= 256):
      data.append(0xf0)
      data.append(param - 256)
    else:
      data.append(param)

    # Parameter data
    if (valueType == 'WORD'):
      data.append((value & 0xff00) >> 8)
      data.append(value & 0xff)
    else: # default: 'BYTE'
      data.append(value & 0xff)

    # Recalculate field length
    data[0] = len(data)
    if (self.debug > 0x0) or debug:
      print('sendParameter', 'len = ', len(data), 'data = ', data)

    # Send parameter and check that scanner replies with a ACK packet here
    self.write(data, True)

  #--
  def beep(self, beepCode = 0x01):
    self.write([0x05, self.OPCODE_BEEP, self.SRC_HOST, 0x00, beepCode], True)

  #--
  def beeper(self, volume = 'LOW', tone = None, permanent = True):
    ''' Set beeper volume and tone. Values are given as  HIGH, MEDIUM, LOW '''
    self.sendParameter(self.OPCODE_BEEPER_VOLUME,
                       0x0 if volume == 'HIGH' else
                       0x1 if volume == 'MEDIUM' else
                       0x2,
                       valueType = 'BYTE', permanent = permanent)
    if (tone):
      self.sendParameter(self.OPCODE_BEEPER_TONE,
                         0x0 if tone == 'HIGH' else
                         0x1 if tone == 'MEDIUM' else
                         0x2,
                         valueType = 'BYTE', permanent = permanent)

  #--
  def aim(self, enable = True):
    self.write([0x04, self.OPCODE_AIM_ON if enable else self.OPCODE_AIM_OFF, 0x04, 0x00], True)

  #--
  def triggerMode(self, triggerMode, permanent = True):
    ''' Set trigger mode. See the triggerModes specializations for accepted values '''
    if (triggerMode not in self.triggerModes):
      raise self.ValueError('Trigger mode {:s} is not supported by this scanner'.format(triggerMode))
    if (self.debug):
      print('Set trigger mode \'{:s}\''.format(triggerMode))
    self.sendParameter(self.OPCODE_TRIGGER_MODE, self.triggerModes[triggerMode],
                       valueType = 'BYTE', permanent = permanent)

  #--
  def continuousBarCodeRead(self, enable = True, permanent = True):
    ''' Enable continuous decoding of bar codes '''
    self.sendParameter(self.OPCODE_CONTINUOUS_DECODE, 0x1 if enable else 0x0,
                       valueType = 'BYTE', permanent = permanent)

  #--
  def beepGoodDecode(self, enable = True, permanent = False):
    self.sendParameter(self.OPCODE_BEEP_GOOD_DECODE, 0x1 if enable else 0x0,
                       valueType = 'BYTE', permanent = permanent)



#--------------------------------------------------------------------------------
class ZebraUsb(ZebraSsi):
  '''
    Virtual base class for USB-attached scanners
  '''

  #--
  def __init__(self, device, ep_in, ep_out):
    ''' Device and endpoint instances acquired externally (CDC bulk endpoint instances are required) '''
    self.device = device
    self.ep_in = ep_in
    self.ep_out = ep_out

  #--
  @classmethod
  def from_connect(cls, idVendor = 0x05e0, idProduct = 0x1701):
    ''' Try to connect to scanner specified by vendor & product id '''
    # Product ID for Zebra DS2278
    #  0x1200 USB HID
    #  0x1701 USB CDC (SSI over CDC) -> we want this
    dev = usb.core.find(idVendor = idVendor, idProduct = idProduct)

    reattach = False
    if dev.is_kernel_driver_active(0):
      reattach = True
      dev.detach_kernel_driver(0)

    if dev is None:
      raise ValueError('Unable to connect to device 0x{:04x} 0x{:04x}'.format(idVendor, idProduct))

    dev.set_configuration()

    # Get endpoint instances
    cfg = dev.get_active_configuration()

    interface_number = cfg[(0, 0)].bInterfaceNumber
    intf = usb.util.find_descriptor(cfg, bInterfaceClass = 0xa) # 0xa -> CDC data interface class

    # Match the first IN endpoint
    ep_in = usb.util.find_descriptor(intf,
                   custom_match = \
                   lambda e: \
                   usb.util.endpoint_direction(e.bEndpointAddress) == \
                   usb.util.ENDPOINT_IN)

    # Match the first OUT endpoint
    ep_out = usb.util.find_descriptor(intf,
                   custom_match = \
                   lambda e: \
                   usb.util.endpoint_direction(e.bEndpointAddress) == \
                   usb.util.ENDPOINT_OUT)

    if ep_in is None or ep_out is None:
      raise ValueError('Unable to acquire CDC bulk data endpoints for device 0x{:04x} 0x{:04x}'.format(idVendor, idProduct))

    return cls(dev, ep_in, ep_out)

  #--
  def read(self, length = -1, timeout = 1000):
    return self.device.read(self.ep_in.bEndpointAddress,
                            self.ep_in.wMaxPacketSize*2 if length == -1 else length,
                            timeout = timeout)

  #--
  def write(self, data, throwIfNoAck = False):
    '''
      Calculate checksum and write to device
      Check is received packet is a ACK if throwIfNoAck is enabled
    '''
    d = self.chksum(data);
    if (self.debug):
      print('Writing to {:s}, endpoint 0x{:02x}, data = {:s}'.format(\
               str(self.device)[:19], self.ep_out.bEndpointAddress, str(d)))
    self.device.write(self.ep_out.bEndpointAddress, d)

    r = True
    if (throwIfNoAck):
      r = self.isAck(self.read(), throwIfNoAck)
    return r



#--------------------------------------------------------------------------------
class ZebraSerial(ZebraSsi):
  ''' Virtual base class for scanners attached via serial port '''
  def __init__(self, device = '/dev/ttyACM0'):
    self.sp = serial.Serial(device, 9600, timeout = 1)

  def __del__(self):
    self.sp.close()

  def read(self, length = 4096, timeout = 1000):
    '''
      Keep timeout for compatibility with USB class, check implementation at some point
    '''
    return self.sp.read(length)

  def write(self, data, throwIfNoAck = False):
    '''
      Calculate checksum and write to serial port.
      Checking of response is not implemented here (yet)
    '''
    self.sp.write(self.chksum(data))



#--------------------------------------------------------------------------------
class ZebraDs2278(ZebraUsb):
  '''
    Support for hand-held DS2278 (with CR2278 cradle)
  '''
  OPCODE_HANDHELD_AIM = 0x132
  OPCODE_PICKLIST_MODE = 0x192
  OPCODE_HANDSFREE_MODE = 0x276

  triggerModes = {\
      'LEVEL': 0,           # Trigger activates decoding, continues until decoded or trigger released
      'PRESENTATION': 7,    # Scanner activates when it detects code in field of view
      'AUTOAIM': 9          # Scanner projects aiming pattern when lifted, trigger activates decoding
    }

  def __init__(self, device, ep_in, ep_out):
    super().__init__(device, ep_in, ep_out)
    pass

  def aimHandheld(self, enable = True, permanent = True):
    self.sendParameter(self.OPCODE_HANDHELD_AIM, 0x2 if enable else 0x0,
                       valueType = 'BYTE', permanent = permanent)

  def picklistMode(self, mode, permanent = True):
    picklistModes = {'ON': 2, 'OFF': 0, 'HANDHELD': 1, 'HANDSFREE': 3}
    if (mode not in picklistModes):
      raise self.ValueError('Illegal picklist mode selected: {:s}. Supported are {:s}'.format(
                            mode, str(list(picklistModes.keys()))))
    self.sendParameter(self.OPCODE_PICKLIST_MODE, picklistModes[mode],
                       valueType = 'BYTE', permanent = permanent)

  def handsFreeMode(self, enable = True, permanent = True):
    ''' If enabled, scanner automatically triggers when it is in the cradle and detects a bar code.
        If disabled, it behaves according to the trigger mode '''
    self.sendParameter(self.OPCODE_HANDSFREE_MODE, 0x1 if enable else 0x0,
                       valueType = 'BYTE', permanent = permanent)


  def getCode(self, timeout = 1000):
    return ''.join(chr(i) for i in self.read(-1, timeout))



#--------------------------------------------------------------------------------
class ZebraDs457(ZebraSerial):
  '''
    Support for fixed-mount DS457 (as used in Lund).
    Currently untested.
  '''
  OPCODE_SCAN_ENABLE = 0xe9
  OPCODE_SCAN_DISABLE = 0xea

  triggerModes = {\
      'LEVEL': 0,
      'PRESENTATION': 7,
      'HOST': 8,
      'SW_ONLY': 15
    }

  def __init__(self, device = '/dev/ttyACM0'):
    super().__init__(device)
    pass

  def scan(self, enable = True):
    ''' Send scan_enable or disable command '''
    self.write([0x04, self.OPCODE_SCAN_ENABLE if enable else OPCODE_SCAN_DISABLE, 0x04, 0x00])

  def flushQueue(self):
    self.write([0x04, 0xd2, 0x04, 0x00])

  def acquire(self, turnOff = True):
    ''' Enable scanner and attempt to acquire value
          Return acquired data in case of success, otherwise throws ScanError

        FIXME: this is specific for the SAMPA bar code scanning in Lund... maybe move
               to own class (at some point)
    '''
    self.scan(True)  # Enable
    self.flushQueue()
    self.write([0x04, 0xe4, 0x04, 0x00]) # start_session (i.e. attempt scan)
    r = self.read(1024)
    if (turnOff):
      self.scan(False)  # Disable
      self.flushQueue()

    # Is there a cleaner way to do the error checking?
    #   Observed the following:
    #     Scanner gets code successfully -> len(r) > 50
    #     Scanner cannot get code -> len(r) < 35
    #print(len(r), r)
    if (len(r) < 35):
      raise self.ScanError('Unable to acquire bar code')
    return r


#--------------------------------------------------------------------------------
# Testing
def main():
  s = Ds457()
  s.setDebug(0x1)
  s.beeper(volume = 'LOW', tone = 'MEDIUM')
  s.beepGoodDecode(False)

  s.sendParameter(137, 50, valueType = 'BYTE', debug = True)
  s.sendParameter(138, 1, valueType = 'BYTE', debug = True)
  #s.imageCropWindow(0, 500, 0, 0)
  s.dumpAllParameters()
  #s.imageCrop(True)
  #s.aim(True)
  #time.sleep(1)
  #s.beep()
  #r = s.acquire()
  #print(len(r), ''.join(map(chr, r)))
  print("Done")


#--------------------------------------------------------------------------------
if __name__ == "__main__":
  main()
