# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import string
import sys
import os
import array
import ctypes
import mmap
import gbtxconfigIO as gbtxcnfg
import GBTx0cnfg as Cnfg0
import GBTx0acnfg as Cnfg0a
import GBTx1cnfg as Cnfg1

windex = 0
configDef = 0
cmpFile = ""
while windex < len(sys.argv):
    if sys.argv[windex]=="-c":
       windex += 1
       configFile = sys.argv[windex]
    elif sys.argv[windex]=="-f":
       windex += 1
       cmpFile = sys.argv[windex]
    else:
       sys.stdout.write("Unknow option {}\n".format(sys.argv[windex]))
    windex += 1
        
if len(configFile) > 0:
    mList = gbtxcnfg.ReadConfigFile(configFile)
if len(cmpFile) > 0:
    cList = gbtxcnfg.ReadConfigFile(cmpFile)
diffs = gbtxcnfg.CompareRefDefConfigFile(mList,cList,sys.stdout)
sys.stdout.write("Found {} differences\n".format(diffs))
