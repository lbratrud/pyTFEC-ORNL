# pulseReConstruct.py
#
# Python main file for FEC pulse reconstruction. The pulse must at the given FREQINPUT (in hz)
#
# ORNL - 11/1/2017
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import math
import os
import glob
from scipy import stats

import FEC_DATA
import FEC_IO
version = "1.0 6/27/2018"

#FREQSAMPLE=4999995
FREQSAMPLE=5000000
#FREQSAMPLE=5010000
#FREQSAMPLE=4990000

SAMPLEPERIOD=1.0/FREQSAMPLE
FREQINPUT=33300
#FREQINPUT=509000
INPUTPERIOD=1.0/FREQINPUT
BINSPERSAMPLEFREQ = 1
TIMERESOLUTION = SAMPLEPERIOD / BINSPERSAMPLEFREQ
TIMESCALE = 1.0
num_bins = int(INPUTPERIOD / TIMERESOLUTION + 1)
USTOPNTS = 1
numFrames = (175000+250)*8
pulseInfoList = []
resolution = 16
reconInfo = "Unknown"
def DoSetup(numSubBins,setFREQINPUT=33300):
       global BINSPERSAMPLEFREQ,TIMERESOLUTION,TIMESCALE,num_bins
       global USTOPNTS,INPUTPERIOD,FREQINPUT
       global reconInfo
       resolution = numSubBins
       FREQINPUT = setFREQINPUT
       INPUTPERIOD = 1.0 / FREQINPUT
       BINSPERSAMPLEFREQ=numSubBins
       TIMERESOLUTION=SAMPLEPERIOD/BINSPERSAMPLEFREQ
       USTOPNTS = 1.0e-6/TIMERESOLUTION
       TIMESCALE = 1.0/ TIMERESOLUTION
       num_bins = int(INPUTPERIOD/TIMERESOLUTION+1)
       reconInfo = "InputFrequency={} Oversample={}".format(FREQINPUT,numSubBins)

DoSetup(32)

def ConstructPulse(histogram, index,maxpnts=1e10):
   global TIMESCALE, INPUTPERIOD, num_bins
   # timepnt = 16*SAMPLEPERIOD/BINSPERSAMPLEFREQ
   timepnt = 0
   waveform = []
   waveformSum = []
   waveformCnt = []
   for i in range(0, num_bins):
      waveform.append(0)
      waveformSum.append(0)
      waveformCnt.append(0)

   mlen = len(histogram[index])
   itr = 0
   for pnt in range(0, mlen):
      wvPnt = int(timepnt * TIMESCALE)
      if wvPnt >= num_bins:
         wvPnt = num_bins - 1
      waveformSum[wvPnt] += histogram[index][pnt]
      waveformCnt[wvPnt] += 1
      timepnt += SAMPLEPERIOD
      if timepnt >= INPUTPERIOD:
         timepnt -= INPUTPERIOD
      itr += 1
      if itr > maxpnts:
         break

   waveOut = []
   for i in range(0, num_bins):
      if waveformCnt[i] > 0:
         waveOut.append([i,1.0*waveformSum[i] / waveformCnt[i]])
   return waveOut


class LoadPulseData:
   def __init__(self,fileDef):
      self.FileDef = fileDef
      self.ReconList = []
      self.size = 0
      self.histogram = []
      for i in range(0,160):
         self.histogram.append([])
         self.ReconList.append([])
      self.info = []

   def getChannelPeakInfo(self,wave,info):
      offset = 0
      ymax = 0
      mlen = len(wave)
      for index in range(2,mlen-2):
          peak = wave[index-2][1]+wave[index-1][1]+wave[index][1]+wave[index+1][1]+wave[index+2][1]
          if peak > ymax:
             offset = index
             ymax = peak
      if ymax > info[2]:
         info[0] = offset
         info[1] = mlen
         info[2] = ymax

   def getPeak(self,index):
      peak = 0
      for i in self.ReconList[index]:
         if i[1] > peak:
            peak = i[1]
      return peak

   def channelAlign(self,wave,adjust,size):
      for i in wave:
         i[0] += adjust
         if i[0] >= size:
            i[0] -= size
         elif i[0] < 0:
            i[0] += size

   def pulseAlign(self):
      offset = 0
      size = 0
      infoOdd = [0,0,0]
      infoEven = [0,0,0]
      for index in range(0,160,2):
         self.getChannelPeakInfo(self.ReconList[index],infoEven)
         self.getChannelPeakInfo(self.ReconList[index+1],infoOdd)

      adjustEven = int(infoEven[1] / 4) - infoEven[0]
      adjustOdd  = int(infoOdd[1] / 4) - infoOdd[0]
      TIMERESOLUTIONE6 = 1e6*TIMERESOLUTION

      for index in range(0,160,2):
         self.channelAlign(self.ReconList[index],adjustEven,infoEven[1])
         self.channelAlign(self.ReconList[index+1],adjustOdd,infoOdd[1])

      oldRecon = self.ReconList
      self.size = size = infoEven[1]
      self.ReconList = []
      for i in oldRecon:
          ndata = []
          for j in range(0,size):
             ndata.append([0,0])
          for j in i:
             index = j[0]
             ndata[index][0] = index * TIMERESOLUTIONE6
             ndata[index][1] = j[1]
          self.ReconList.append(ndata)

   def TPCID2index(self,chanID):
     if chanID[0:3]=="TPC":
        return self.ID2index(chanID[3:])
     return -1

   def ID2index(self,chanID):
     if chanID == "0L":
        return 16*0
     elif chanID == "0H":
        return 16*1
     elif chanID == "1L":
        return 16*2
     elif chanID == "1H":
        return 16*3
     elif chanID == "2L":
        return 16*4
     elif chanID == "2H":
        return 16*5
     elif chanID == "3L":
        return 16*6
     elif chanID == "3H":
        return 16*7
     elif chanID == "4L":
        return 16*8
     elif chanID == "4H":
        return 16*9
     return -1

   def loadConstructedData(self, fileName):
      if os.path.isfile(fileName) == False:
         sys.stdout.write("Invalid file name {}\n".format(fileName))
         return -1
      self.ReconList = []
      for i in range(0,160):
         self.ReconList.append([])
      fin = open(fileName,'r')
      for line in fin:
          words = line.upper().split(",")
          try:
             timeindex = int(words[0])
#             timebin = float(words[1])
             for i in range(2,len(words)):
                self.ReconList[i-2].append([timeindex,float(words[i])])
          except:
             pass
      fin.close()
      # index = 0
      # for i in self.ReconList:
      #    sys.stdout.write("Recon[{}]={}\n".format(index,i))
      #    index += 1

   def loadReconFile(self, fileName):
      if os.path.isfile(fileName) == False:
         return -1

      fin = open(fileName,'r')
      itr = 0
      for line in fin:
        words = line.upper().split()
        if words[0] == "RECON":
             dataIndex = self.ID2index(words[1])
             if dataIndex!=-1:
                chanID = int(words[2])
                tIndex = 0
                mlen = len(words)
                mIndex =3
                cData = []
                while mIndex < mlen:
                     cData.append([tIndex,float(words[mIndex])])
                     mIndex += 1
                     tIndex += 1
                self.ReconList[dataIndex+chanID] = cData
      fin.close()

   def loadFile(self, fileName, maxitr=125000):
      if os.path.isfile(fileName) == False:
         return -1

      fin = open(fileName,'r')
      itr = 0
      for line in fin:
          words = line.upper().split()
          dataIndex = self.TPCID2index(words[0])
          if dataIndex!=-1:
             if int(words[1]) > maxitr:
                break
             for index in range(0,16):
                self.histogram[dataIndex+index].append(int(words[index+2]))
      fin.close()

   def construct(self,maxpnts=125000):
      for index in range(0,160):
         w = ConstructPulse(self.histogram,index,maxpnts=maxpnts)
         self.ReconList.append(w)
      self.pulseAlign()

   def channelInfoFind(self,index):
      peak = 0
      peakcnt = 0
      baseline = 0
      baselinecnt = 0
      minval = 1024
      maxval = 0

      pulserange = int(0.35e-6/TIMERESOLUTION)+1
      zerorange = int(8.1e-6 / TIMERESOLUTION) + 1
#      pulserange = int(0.15*self.size)
      peakindex = int(0.25*self.size)
      zeroindex = int(0.75*self.size)
      peakmin = peakindex - pulserange
      peakmax = peakindex + pulserange
      zeromin = zeroindex - zerorange
      zeromax = zeroindex + zerorange
      peakexpand = 1

      for pnt in range(0,self.size):
         val = self.ReconList[index][pnt][1]
         if pnt >= peakmin and pnt <= peakmax:
            if val < minval:
               minval = val
            if val > maxval:
               maxval = val
            if pnt >= peakindex -peakexpand and pnt <= peakindex+peakexpand:
               peak += val
               peakcnt += 1
         if pnt < peakmin or pnt > zeromax:
            baseline += val
            baselinecnt += 1
      if baselinecnt < 1:
         baselinecnt = 1
      if peakcnt < 1:
         peakcnt = 1
      myinfo = [baseline/baselinecnt,peak/peakcnt,minval,maxval]
      self.info.append(myinfo)
#      sys.stdout.write("channelInfoFind {}\n".format(myinfo))

   def removeBaseLine(self,offset=0):
      for index in range(0,160):
         pedestal = self.info[index][0]
         for i in self.ReconList[index]:
            i[1] -= pedestal - offset

   def calcPeakRatioDiff(self,val,avg):
      if avg < 0.1:
         val = -1
      else:
         val = 100*(1.0*val/avg-1)
      return val

   def calcPeakRatio(self,val,avg):
      if avg > 0.1:
         return 100.0*val/avg
      else:
         return -1

   def infofind(self):
      self.info = []
      for index in range(0,160):
         self.channelInfoFind(index)

      peakavg = 0
      peakavgcnt = 0.0001
      for index in range(0,160):
         peak = self.info[index][1]-self.info[index][0]
         if peak > 100:
            peakavg += peak
            peakavgcnt += 1
      peakavg /= peakavgcnt
      self.peakavg = peakavg

   def infoPrint(self,io=sys.stdout):
      io.write("AvgPeak,{}\n".format(peakavg))
      io.write("Channel,Pedestal,gain percentage,gain ratio,peakmin-normalized,peakmax-normalized\n".format(peakavg))
      for index in range(0,160):
         myinfo = self.info[index]
         if myinfo[0] > 1:
            val = self.calcPeakRatioDiff(myinfo[1]-myinfo[0],peakavg)
            if abs(val) < 50:
               io.write("{},{},{},{},{},{}\n".format(index,myinfo[0],val,self.calcPeakRatio(myinfo[1]-myinfo[0],peakavg),
                                                          self.calcPeakRatio(myinfo[2]-myinfo[0],peakavg),self.calcPeakRatio(myinfo[3]-myinfo[0],peakavg)))
      for index in range(0,160):
         myinfo = self.info[index]
         if myinfo[0] > 1:
            val = self.calcPeakRatioDiff(myinfo[1]-myinfo[0],peakavg)
            if abs(val) > 50:
               io.write("{},{},{},{},{},{}\n".format(index,myinfo[0],val,self.calcPeakRatio(myinfo[1]-myinfo[0],peakavg),
                                                          self.calcPeakRatio(myinfo[2]-myinfo[0],peakavg),self.calcPeakRatio(myinfo[3]-myinfo[0],peakavg)))
      io.write("Raw Processed Data\n")
      io.write("Channel,pedestak,peak,peakmin,peakmax\n".format(peakavg))
      for index in range(0,160):
         myinfo = self.info[index]
         if myinfo[0] > 1:
            io.write("{},{},{},{},{}\n".format(index,myinfo[0],myinfo[1],myinfo[2],myinfo[3]))

   def Print(self,io=sys.stdout):
      io.write("timebin,us")
      indexList = []
#      mlen = len(self.ReconList)
      for index in range(0,160):
         got_data = 0
         if len(self.ReconList[index]) > 0:
            for tp in range(0,num_bins):
               if self.ReconList[index][tp][1] > 1:
                  got_data = 1
                  break
            if got_data==1:
               io.write(",{}".format(index))
               indexList.append(index)
      io.write("\n")
      for tp in range(0,num_bins):
          io.write("{},{:3.3f}".format(tp,tp*TIMERESOLUTION*1e6))
          for index in indexList:
              io.write(",{:3.1f}".format(self.ReconList[index][tp][1]))
          io.write("\n")

def getFiles():
   global resolution
   return glob.glob("run0_*.recon{}".format(resolution))

def savePulseInfo(of,mtype):
   global pulseInfoList
   cnt = 0
   for i in pulseInfoList:
      of.write("string {}Pulse{} {}\n".format(mtype,cnt, i))
      cnt += 1

def makeCrossTalkFile(fecname,fec, DataList):
   global pulseInfoList, reconInfo, version

   pwd = os.getcwd()
   chanList = []
   for i in range(0, 160):
      chanList.append([0, 0])
   index = 0
   for i in DataList:
      for j in range(0, 160):
         val = i.getPeak(j)
         chanList[j][index] = val
      index += 1

   # sys.stdout.write("Path={}\n".format(sumPath))
   filename = "crosstalk_{}.txt".format(fecname)
   pdfname = "crosstalk_{}.pdf".format(fecname)
   gainFile = open(filename, 'w')
   gainFile.write("# pulseReConstruct Version {}\n".format(version))
   gainFile.write("# Crosstalk Data Directory {}\n".format(pwd))
   gainFile.write("# Format: crosstalk Eon_Ooff Eoff_Oon\n")
   gainFile.write("BoardID {}\n".format(fec.BoardID))
   gainFile.write("string crosstalkPath {}\n".format(pwd))
   gainFile.write("string crosstalkRCI {}\n".format(reconInfo))
   fec.addParameter("crosstalkPath",pwd)
   savePulseInfo(gainFile,"crosstalk")
   for i in range(0, 160):
      gainFile.write("crosstalk {} {} {}\n".format(i, chanList[i][0], chanList[i][1]))
      fec.addCrossTalk(1, i, chanList[i][0])
      fec.addCrossTalk(0, i, chanList[i][1])
   gainFile.close()
   fec.MakeCrossTalkPDF(pdfname)
   return filename

def makeGainFile(fecname,fec, DataList,gainDef="20",do_channel_plot=1):
   global pulseInfoList, reconInfo, version

   pwd = os.getcwd()
   peakList = []
   sizeList = []
   chanList = []
   ampmin = 5000
   ampmax = 0
   for i in range(0, 160):
      chanList.append([])

   for i in DataList:
      myData = []
      words = i.FileDef.upper().split("_")
      amp = int(words[1].upper().replace("MVPP", ""))
      sizeList.append(amp)
      myData.append(amp)
      if amp < ampmin:
         ampmin = amp
      if amp > ampmax:
         ampmax = amp
      mChanList = []
      for j in range(0, 160):
         val = i.getPeak(j)
         myData.append(val / amp)
         chanList[j].append([amp * 2, val * 2.2 / 1.024])
      peakList.append(myData)
      chanList.append(mChanList)

   fec.PulseMagnitudeData = chanList
   npeakList = []
   for i in sizeList:
      for j in peakList:
         if j[0] == i:
            npeakList.append(j)


            #    sys.stdout.write("Path={}\n".format(sumPath))
   filename = "gain{}_{}.txt".format(gainDef,fecname)
   pdfname = "gain{}_{}.pdf".format(gainDef,fecname)
   gainFile = open(filename, 'w')
   gainFile.write("# pulseReConstruct Version {}\n".format(version))
   gainFile.write("# Gain Data Directory {}\n".format(pwd))
   gainFile.write("# Format: changain <chan#> <slope> <intercept> <r_value> <p_value> <std_err>\n")
   gainFile.write("BoardID {}\n".format(fec.BoardID))
   gainFile.write("string gainPath {}\n".format(pwd))
   savePulseInfo(gainFile,"gain")
   fec.addParameter("gainPath",pwd)

   for i in range(0, 160):
      x = []
      y = []
      for j in chanList[i]:
         x.append(j[0])
         y.append(j[1])
      slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
      if gainDef=="20":
         fec.addChannelGain20x(i, slope, intercept, r_value, p_value, std_err)
      else:
         fec.addChannelGain30x(i, slope, intercept, r_value, p_value, std_err)

      gainFile.write("changain{} {} {} {} {} {} {}\n".format(gainDef,i, slope, intercept, r_value, p_value, std_err))
   gainFile.close()
   fec.MakeGainPDF(pdfname, gainDef=gainDef)
   if do_channel_plot==1:
      fec.MakeChannelGainPDF("chan_"+pdfname,gainDef=gainDef)
   return filename

def ProcessFiles(fecIntf,fec,fecname,load_runinfo=1,force_update=0,do_channel_gain_plot=0,do_recon_plot=0,xminDef=0,xmaxDef=0):
   global pulseInfoList

   path = os.getcwd()
   fec.Path = path
   files = getFiles()
   if len(files) == 0:
       files = glob.glob("run*.bin")
       mlen = len(files)
       index = 1
       for i in files:
           fecIntf.setModeCond("RP-{}/{}".format(index,mlen))
           cmd = "decoder_gbtx -f {} -i {}".format(numFrames, i)
           nData = i.replace(".bin", "")
           gbtxNum = int(nData[3])
           cmd += " -o {} -s {} -r".format(nData, gbtxNum)
           fecIntf.addToLog("Decoding GBTx file {}\n".format(nData))
           FEC_IO.ExecuteCmdParse(cmd)
           index += 1
       files = getFiles()

   crosstalk_opt = 0
   DataList = []
   gainDef = "20"
   mlen = len(files)
   index = 1
   for f in files:
       fecIntf.setModeCond("PD-{}/{}".format(index, mlen))
       fileDef = f.replace("run0_", "")
       fileDef = fileDef.replace(".recon{}".format(resolution), "")
       fecIntf.addToLog("Reading {}\n".format(fileDef))
       pulseInfoList.append(fileDef)
       f1 = "run1_{}.recon{}".format(fileDef, resolution)
       data = LoadPulseData(fileDef)
       if fileDef[-7:] in ["_on_off", "_off_on"]:
           crosstalk_opt = 1
       if fileDef[-3:] in ["g30"]:
          gainDef = "30"
       data.loadReconFile(f)
       data.loadReconFile(f1)
       data.size = len(data.ReconList[0])
       data.pulseAlign()
       data.infofind()
       data.removeBaseLine()
       DataList.append(data)
       index += 1

   if load_runinfo==1:
      fec.Read("runinfo.txt")
      fec.ScaID = int(fec.ScaID)
      fec.BoardID = int(fec.BoardID)
   if crosstalk_opt == 1:
       filename = makeCrossTalkFile(fecname,fec,DataList)
       code = "Crosstalk"
       mList = fec.checkCrosstalk()
   else:
       filename = makeGainFile(fecname,fec,DataList,gainDef=gainDef,do_channel_plot=do_channel_gain_plot)
       code = "Gain{}".format(gainDef)
       mList = fec.checkGain(gainDef)

   if do_recon_plot==1:
      fec.ReconPlot("recon_"+code.lower()+"_"+fecname+".pdf",DataList,code,xminDef,xmaxDef)

   fecIntf.addToLog("{} Directory {}\n".format(code,path))
   if len(mList)==0:
      fecIntf.addToLog("Test {} had no errors\n".format(code))
   else:
      fecIntf.addToLog("Test {} had errors:\n".format(code))
      for i in mList:
         fecIntf.addToLog("=> {} {}\n".format(code,i))
   FEC_IO.checkSummaryFile(fecname,filename,force_update=force_update)
