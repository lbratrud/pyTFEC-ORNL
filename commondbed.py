# commondbed.py
#
# This file is the editor for colors, parameters, and text information
# in commondb.py (cde)
#
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.com)
# 5/27/2016

import math
import sys
import string
import sys
import os
import errwarn

from projbase import *
import commondb as cdb
import projwin as win
import projpath as mpath

try:  # import as appropriate for 2.x vs. 3.x
   from tkinter import *
except:
   from Tkinter import *

try:
   import tkfont as tkfont
except:
   import tkinter.font as tkfont

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------
class ColorEditor(win.Base):
    def __init__(self):
         sys.stdout.write("ColorEditor\n")
         if win.Base.__init__(self,"coloreditor","Color Display Editor")==0:
            return
#         self.master.config(bg=cdb.ColorList.getParam("background"))
         self.myList = []

         row = 0
         col = 0
         self.currentParm = 0
         columnspan=10
         self.ObjShow = StringVar()
         self.TextShow = StringVar()

         self.f1 = win.WinMenu(self,"Options",-1,0,0)
         self.f1.menub.grid(columnspan=3,sticky=E+W)

         for i in [["Object",self.ObjShow,"C"],["Text",self.TextShow,"T"]]:
            i[1].set("ON")
            rcolor,revcolor = self.getCategoryColor(i[2])
            onmenu = Menu(self.f1.menu,bg=rcolor, fg=revcolor,font=self.font,relief=RIDGE,tearoff=0)
            for d in ["On","Off"]:
               onmenu.add_radiobutton(label=d,variable=i[1],value=d.upper(),command=self.updateList)
            self.f1.menu.add_cascade(label="{} Category".format(i[0]),menu=onmenu,foreground=revcolor,background=rcolor)

         self.f1.menu.add_command(label="Use Defaults",command=self.restoreDefaults)
         self.f1.menu.add_command(label="Save",command=self.saveDefaults)
         row += 1
         self.ObjList = []
         row = 1

         win.WinRowFeed(self,row)
         row += 1

         self.colorname = win.WinValueLabel(self,"Selected Color",-1,row,0)
         self.colorname.setVal("\n")
         self.colorname.label.grid(columnspan=4,sticky=E+W+N+S)
         self.colorname.entry.grid(columnspan=6,sticky=E+W+N+S,column=4)
         row += 1
         win.WinRowFeed(self,row)
         row += 1

         name = win.WinLabel(self,"Reference Colors",-1,row,col+1)
         name.label.grid(columnspan=columnspan-2,sticky=E+W)
         row += 1
         bColor = ["#000000","#C0C0C0","#808080","#FFFFFF","#800000","#FF0000","#800080","#FF00FF",\
                   "#008000","#00FF00","#808000","#FFFF00","#000080","#0000FF","#008080","#00FFFF"]
         x = 1
         for color in bColor:
                 e = Button(self.master, text="", width=2,bg=color,command=lambda cr=color: self.setColor(cr))
                 e.grid(row=row, column=col+x, sticky=E+W)
                 x += 1
                 if (x >= columnspan-1):
                   x = 1
                   row += 1

         win.WinRowFeed(self,row)
         row += 1
         self.colordef = win.WinButton(self,"Set Color",row,1)
         self.colordef.button.config(command=self.saveColor,state="disabled")
         self.colordef.button.grid(columnspan=columnspan-2,sticky=E+W)
         row += 1
         win.WinRowFeed(self,row)
         row += 1

         self.rscaleVal = IntVar()
         self.gscaleVal = IntVar()
         self.bscaleVal = IntVar()
         tick=32
         length = 300
         ssize=500
         self.rscale = Scale(self.master,label="Red Mix",bg="red",troughcolor="red4",font=self.font,\
                             length=length,tickinterval=tick,orient=HORIZONTAL,variable = self.rscaleVal, from_=0,to_ = 255,command = self.updateCr)
         self.rscale.grid(row=row,column=col,columnspan=columnspan,sticky=E+W)
         row += 1
         self.gscale = Scale(self.master,label="Green Mix",bg="green3",troughcolor="green",font=self.font,\
                             length=length,tickinterval=tick,orient=HORIZONTAL,variable = self.gscaleVal, from_=0,to_ = 255,command = self.updateCr)
         self.gscale.grid(row=row,column=col,columnspan=columnspan,sticky=E+W)
         row += 1
         self.bscale = Scale(self.master,label="Blue Mix",bg="blue",troughcolor="blue4",font=self.font,\
                             length=length,tickinterval=tick,orient=HORIZONTAL,variable = self.bscaleVal, from_=0,to_ = 255,command = self.updateCr)
         self.bscale.grid(row=row,column=col,columnspan=columnspan,sticky=E+W)
         row += 1

         row = 0
         col  = columnspan+1

         divide = 20
         ColumnList = [["Category",15],["Condition",20]]
         yScroll = Scrollbar(self.master)
         yScroll.grid(row=row+1,column=col,sticky=N+S,rowspan=divide)
         col += 1
         for c in ColumnList:
           mylabel = Label(self.master,text=c[0],font=self.font,bg=self.bgc,fg=self.fgc,width=c[1],relief=RAISED) #,relief=RAISED
           mylabel.grid(row=row,column=col,sticky=E+W)
           mylist = Listbox(self.master,selectmode=SINGLE,font=self.font,bg=self.qbgc,fg=self.qfgc,width=c[1],
                            height=divide,relief=FLAT,yscrollcommand=yScroll.set)
           mylist.bind("<Double-Button-1>", self.getSelect)
           mylist.grid(row=row+1,column=col,rowspan=divide,sticky=E+W+N+S)
           self.ObjList.append(mylist)
           col += 1
         yScroll.config(command=self.yview)
         self.updateList()
            
    def getColor(self):
       return "#%02x%02x%02x" % (self.rscaleVal.get(),self.gscaleVal.get(),self.bscaleVal.get())

    def updateCr(self,val):
       self.colordef.button.config(bg=self.getColor(),fg=getRevColor(self.rscaleVal.get(),self.gscaleVal.get(),self.bscaleVal.get()))
       
    def saveDefaults(self):
       cdb.SaveBaseDefs()

    def restoreDefaults(self):
       cdb.ColorList.defaults()

    def getObjColor(self,obj):
      if obj[2]=="C":
         return cdb.ColorList.getParam(obj[1])
      elif obj[2]=="T":
         t = cdb.TextConfigList.find(obj[1])
         return t.color
      return "#000000"

    def setObjColor(self,obj,color):
       if obj==0:
          return
       if obj[2]=="C":
          cdb.ColorList.setParam(obj[1],color)
       elif obj[2]=="T":
          t = cdb.TextConfigList.find(obj[1])
          t.color = color
          for x in win.WindowMainList:
             x.TextUpdate(t.name)
       self.setupSelected()
          
    def sortList(self,myList):
         myNames = []
         for i in myList:
              myNames.append(i[0])

         myNames.sort(key=str.lower)
         for n in myNames:
            for i in myList:
               if n == i[0]:
                  self.myList.append(i)
                  break

    def getList(self):
         if self.ObjShow.get()=="OFF" and self.TextShow.get()=="OFF":
            self.ObjShow.set("OFF")
            
         self.myList = []            
         if self.ObjShow.get()=="ON":
            myList = []
            for c in cdb.ColorList.objList:
               name = cname = cdb.StripUnderScore(c[0].upper())
               group = cdb.ColorList.getDesc(c[0])
               words = cname.split()
               mlen = len(words)
               if group != words[0].upper():
                  name = "{} {}".format(group,cname)
                  subdesc = cdb.getSubDesc(words,group)
               else:
                  if mlen == 1:
                     subdesc = "object"
                  else:
                     subdesc = cconcat(words,1,mlen)
               myList.append([name,c[0],"C",group,subdesc])
            self.sortList(myList)
               
         if self.TextShow.get()=="ON":
            myList = []
            for t in cdb.TextConfigList.MyList:
               tname = cdb.StripUnderScore(t.name).upper()
               name = "TEXT {}".format(tname)
               words = name.split()
               mlen = len(words)
               myList.append([name,t.name,"T","TEXT",tname])
            self.sortList(myList)

     
    def getCategoryColor(self,ctype):
          if ctype=="P":
             rcolor = "#808080"
          elif ctype=="T":
             rcolor = "#c0c0c0"
          else:
             rcolor = "#ffffff"
          rrevcolor = seekRevColor(rcolor)
          return [rcolor,rrevcolor]
            
    def updateList(self):
       self.getList()
       self.ObjList[0].delete(0,END)
       self.ObjList[1].delete(0,END)
       i = 0
       last_rev = 0
       for c in self.myList:
          color = self.getObjColor(c)
          revcolor=seekRevColor(color)
          rcolor,rrevcolor = self.getCategoryColor(c[2])
          ref = c[3]
          if ref==last_rev:
             ref = ""
          else:
             last_rev = ref
          self.ObjList[0].insert(i,ref)
          self.ObjList[1].insert(i,c[4])
          self.ObjList[0].itemconfig(i, bg=rcolor, fg=rrevcolor)
          self.ObjList[1].itemconfig(i, bg=color, fg=revcolor)
          i+=1
       
    def setupSelected(self):
        if self.currentParm!=0:
           color = self.getObjColor(self.currentParm)
           revcolor = seekRevColor(color)
        else:
           color = self.bg
           revcolor = self.bg
        self.colorname.entry.config(bg=color,fg=revcolor)
        self.setColor(color)

    def getSelect(self,event):
        self.colordef.button.config(command=self.saveColor,state="normal")
        index = event.widget.nearest(event.y)
        self.currentParm = self.myList[index]
        txt = "{}\n{}".format(self.currentParm[3],self.currentParm[4])
        self.colorname.setVal(txt)
        self.setupSelected()

    def yview(self,*args):
      for l in self.ObjList:
         l.yview(*args)

    def saveColor(self):
       color = self.getColor()
       self.setObjColor(self.currentParm,color)
          
       self.ObjList[1].config(bg=color)
       i = 0
       for c in self.myList:
          if c==self.currentParm:
             self.ObjList[1].itemconfig(i, bg=color)
             break
          i+=1
          
    def setColor(self,color):
       r = int(color[1:3],base=16)
       g = int(color[3:5],base=16)
       b = int(color[5:7],base=16)
       self.rscale.set(r)
       self.gscale.set(g)
       self.bscale.set(b)
               
# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class FontEditor(win.Base):
    def __init__(self):
         if win.Base.__init__(self,"fonteditor","Text Display Editor")==0:
            return

         self.size = 12
         self.myList = []
         myList = []
         myNames = []

         categoryList = []
         catNameList = []
         for t in cdb.TextConfigList.MyList:
            category = cdb.TextConfigList.getDesc(t.name)
            if category == 0:
               category = "CUSTOM"
            ListAdd(categoryList,[category,0])
            ListAdd(catNameList,category)

         catNameList.sort(key=str.lower)
         stepsize = (256-80)/(len(catNameList)+1)
         index = 0
         for n in catNameList:
            val = stepsize*index+80
            index += 1
            for l in categoryList:
               if n == l[0]:
                  l[1] = "#%02x%02x%02x" % (val,val,val)
                  break

         for t in cdb.TextConfigList.MyList:
            uname = t.name.upper()
            category = cdb.TextConfigList.getDesc(t.name)
            add=1
            if category == 0:
               category = "CUSTOM"
               if uname == "LABEL":
                  add=0
            color = 0
            for c in categoryList:
               if c[0]==category:
                  color = c[1]
                  break
            if color == 0:
                  color = "#404040"

            name = cname = cdb.StripUnderScore(uname)
            words = cname.split()
            mlen = len(words)
            if category != words[0]:
               name = "{} {}".format(category,cname)
               subdesc = cdb.getSubDesc(words,category)
            else:
               if mlen == 1:
                  subdesc = "title"
                  uname = ""
               else:
                  subdesc = cconcat(words,1,mlen)
            name = "{} {}".format(category,uname)

            if add==1:
               myList.append([name,t.name,category,subdesc,color])
               myNames.append(name)

         myNames.sort(key=str.lower)
         for n in myNames:
            for l in myList:
               if n == l[0]:
                  self.myList.append(l)
                  break
            
         FONTS = list( tkfont.families() )
         FONTS.sort(key=str.lower)
         row = 0
         col = 0
         self.currentParm = 0
         columnspan=4
         self.f1 = win.WinMenu(self,"Options",-1,0,0)
         self.f1.menub.grid(sticky=E+W) #columnspan=columnspan,
         self.f1.menu.add_command(label="Restore Defaults",command=self.restoreDefaults)
         self.f1.menu.add_command(label="Save",command=self.saveDefaults)

         self.ObjList = []
         row = 1

         msg = "ABCDEFGHIJKLMNO\nPQRSTUVWXYZ\n0123456789!@#$\n%^&*()-_+=<>?/.,\nabcdefghijklmno\npqrstuvzxyz"
         win.WinRowFeed(self,row)
         row += 1
         self.font1 = win.WinValueLabel(self,"Example\nSize={}".format(self.size),-1,row,0)
         self.font1.setVal(msg)
         self.font1.entry.config(bg=cdb.ColorList.getParam("background"))
         row += 1
         win.WinRowFeed(self,row)
         row += 1
         self.colordef = win.WinButton(self,"Save Selected",row,1)
         self.colordef.button.config(command=self.saveFont)
         self.colordef.button.grid(sticky=E+W)
         self.colordef.button.config(state="disabled")

         row += 1
         win.WinRowFeed(self,row)
         row += 1

         self.SelectedObj = []
         for c in [["Selected Text","V"],["Selected Size","E"]]:
           mylabel = Label(self.master,text=c[0],font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED) #,relief=RAISED
           mylabel.grid(row=row,column=col,sticky=E+W)
           if c[1]=="V":
              myvalue = Label(self.master,text="",font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED)
           else:
              myvalue = Entry(self.master,font=self.font,bg=self.qbgc,fg=self.qfgc,relief=RAISED)
           myvalue.grid(row=row,column=col+1,sticky=E+W)
           self.SelectedObj.append(myvalue)

           row += 1

         self.fontlist1 = win.WinScrollList(self,0,"Selected Font",20,10,row,col,FONTS)
         self.fontlist1.ListB.bind("<Double-Button-1>", self.setVar)
         self.fontlist1.setVal("Arial")
         tick=32
         length = 300
         ssize=500

         label = Label(self.master,text=" ",bg=self.bg)
         label.grid(row=0,column=columnspan)
         row = 0
         col  = columnspan+1
         divide = 20
         yScroll = Scrollbar(self.master)
         yScroll.grid(row=row+2,column=col,sticky=N+S,rowspan=divide)
         col+=1
         ColumnList = [["Category",15],["Text Definition",15],["Font",15],["Size",4]]
         for c in ColumnList:
           mylabel = Label(self.master,text=c[0],font=self.font,bg=self.bgc,fg=self.fgc,width=c[1],relief=RAISED) #,relief=RAISED
           mylabel.grid(row=row,column=col,sticky=E+W)
           mylist = Listbox(self.master,selectmode=SINGLE,font=self.font,bg=self.qbgc,fg=self.qfgc,width=c[1],
                            height=divide,relief=FLAT,yscrollcommand=yScroll.set)
           mylist.bind("<Double-Button-1>", self.getSelect)
           mylist.grid(row=row+1,column=col,rowspan=divide,sticky=E+W+N+S)
           self.ObjList.append(mylist)
           col += 1
         yScroll.config(command=self.yview)
         self.updateList()
            
    def getCategoryColor(self,catList,name):
       return "#%02x%02x%02x" % (self.rscaleVal.get(),self.gscaleVal.get(),self.bscaleVal.get())
    def showFont(self):
        font = self.fontlist1.getVal()
        self.font1.entry.config(font=(font,self.size))

    def setVar(self,LB):
        self.fontlist1.setVar(LB)
        self.showFont()

    def saveDefaults(self):
       cdb.SaveBaseDefs()

    def restoreDefaults(self):
       cdb.ColorList.defaults()

    def updateList(self):
       for o in self.ObjList:
          o.delete(0,END)

       index = 0
       last_cat = 0
       for c in self.myList:
         t = cdb.TextConfigList.find(c[1])
         if last_cat == c[2]:
            category = ""
         else:
            last_cat = category = c[2]

         self.ObjList[0].insert(index,category)
         self.ObjList[1].insert(index,c[3])
         self.ObjList[2].insert(index,t.font)
         self.ObjList[3].insert(index,t.size)
         for o in self.ObjList:
            o.itemconfig(index, bg=t.color)
         self.ObjList[0].itemconfig(index, bg=c[4])
         index+=1
       
    def getSelect(self,event):
        index = event.widget.nearest(event.y)
        self.currentParm = self.myList[index]
        t = cdb.TextConfigList.find(self.currentParm[1])
        revcolor = seekRevColor(t.color)
        name = "{}\n{}".format(self.currentParm[2],self.currentParm[3])
        self.SelectedObj[0].config(text=name,bg=t.color,fg=revcolor)
        self.SelectedObj[1].delete(0,END)
        self.SelectedObj[1].insert(0,"{}".format(t.size))

        self.font1.entry.config(fg=t.color)
#        self.fontlist1.entry.config(bg=t.color,fg=revcolor)
        self.fontlist1.setVal(t.font)
        self.showFont()
        self.colordef.button.config(state="normal")

    def yview(self,*args):
      for l in self.ObjList:
         l[1].yview(*args)

    def saveFont(self):
       if self.currentParm==0:
          return
       t = cdb.TextConfigList.find(self.currentParm[1])
       font = self.fontlist1.getVal()
       if font != -1:
          t.font = font
       t.size = int(self.SelectedObj[1].get())
       for x in win.WindowMainList:
          x.TextUpdate(t.name)
       self.updateList()

# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class ParameterEditor(win.Base):
    def __init__(self):
         if win.Base.__init__(self,"parmeditor","Parameter Editor")==0:
            return

         self.size = 12
         self.myList = []
         myNames = []
         for t in cdb.ProjParams.objList:
            name = t[0]
            myNames.append(name)

         myNames.sort(key=str.lower)
         for n in myNames:
            for l in cdb.ProjParams.objList:
               if n == l[0]:
                  self.myList.append(l)
                  break
         row = 0
         col = 0
         self.currentParm = 0
         columnspan=4
         self.f1 = win.WinMenu(self,"Options",-1,0,1)
         self.f1.menub.grid(sticky=W) #columnspan=columnspan,
         self.f1.menu.add_command(label="Save",command=cdb.SaveBaseDefs)
         row += 1

         self.ObjList = []
         self.index = -1
         row = 1

         tick=32
         length = 300
         ssize=500

         col  = 0
         divide = 20
         ColumnList = []
         ColumnList.append(["Parameter",50,"V"])
         vtype = "E"

         ColumnList.append(["Value",6,vtype])
            
         yScroll = Scrollbar(self.master)
         yScroll.grid(row=row+2,column=col,sticky=N+S,rowspan=divide)
         col+=1
         for c in ColumnList:
           mylabel = Label(self.master,text=c[0],font=self.font,bg=self.bgc,fg=self.fgc,width=c[1],relief=RAISED) #,relief=RAISED
           mylabel.grid(row=row,column=col,sticky=E+W)
           if c[2]=="V":
              myvalue = Label(self.master,text="",font=self.font,bg=self.bgc,fg=self.fgc,relief=RAISED,width=c[1])
           else:
              myvalue = Entry(self.master,font=self.font,bg=self.qbgc,fg=self.qfgc,relief=RAISED,width=c[1])
           myvalue.grid(row=row+1,column=col,sticky=E+W)

           xScroll = Scrollbar(self.master,orient=HORIZONTAL)

           mylist = Listbox(self.master,selectmode=SINGLE,font=self.font,bg=self.qbgc,fg=self.qfgc,width=c[1],
                            height=divide,relief=FLAT,yscrollcommand=yScroll.set,xscrollcommand=xScroll.set)
           xScroll.config(command=mylist.xview,bg=self.bgc)
           mylist.bind("<Double-Button-1>", self.getSelect)
           mylist.grid(row=row+2,column=col,rowspan=divide,sticky=E+W+N+S)
           xScroll.grid(row=row+divide+2,column=col,sticky=E+W)
           self.ObjList.append([myvalue,mylist,c[2]])
           col += 1
         yScroll.config(command=self.yview)
         self.updateList()
            
    def updateParm(self):
       if self.currentParm != 0:
          name = self.currentParm[0].upper()
          value = self.ObjList[1][0].get()
          self.myList[self.index] = [name,value]
          cdb.ProjParams.setV(name.lower(),value)
          self.updateList()

    def saveDefaults(self):
       self.updateParm()
       cdb.SaveBaseDefs()

    def updateList(self):
       self.ObjList[0][1].delete(0,END)
       self.ObjList[1][1].delete(0,END)
       cnt = 0
       for c in self.myList:
          self.ObjList[0][1].insert(cnt,c[0].upper())
          self.ObjList[1][1].insert(cnt,c[1])
          cnt+=1
       
    def getSelect(self,event):
        self.updateParm()
        index = event.widget.nearest(event.y)
        self.currentParm = self.myList[index]
        self.index = index
#        self.ObjList[0][0].delete(0,END)
        self.ObjList[0][0].config(text=self.currentParm[0].upper())
        self.ObjList[1][0].delete(0,END)
        self.ObjList[1][0].insert(0,self.currentParm[1])

    def yview(self,*args):
      for l in self.ObjList:
         l[1].yview(*args)
