# pwrtest.py
#
# Define the algorithms for the power measurement and handling
import math
import sys
import time
import string
import sys
import os
import array
import ctypes

from projbase import *

SAMPA_2V2_CR = 0.19
SAMPA_2V2_CR_TOL = [0.15,0.15]
SAMPA_3V2_CR = 0.065
SAMPA_3V2_CR_SAMPA2 = 0.045
SAMPA_3V2_CR_TOL = [0.20,0.30]

FEC_2V2_CR = 1.0
FEC_2V2_CR_TOL = [0.10,0.05]
FEC_3V2_CR = 2.0
FEC_3V2_CR_TOL = [0.05,0.075]

def findPowerMeasure(mList, name):
    for i in mList:
        if i[0] == name:
            return i
    return 0

def testPowerToList(SupplyInfo):
    pwrLog = []
    OFF = findPowerMeasure(SupplyInfo, "OFF")
    if OFF == 0:
        pwrLog.append(["FEC", "RED", "NO-SAMPA-OFF", 0, 0])
    else:
        PList = []
        for i in range(0, 5):
            ON = findPowerMeasure(SupplyInfo, "ON {}".format(i))
            if ON != 0:
                PList.append([i, ON[1], ON[2] - OFF[2], ON[3], ON[4] - OFF[4]])

        min_2v2_cr = SAMPA_2V2_CR * (1 - SAMPA_2V2_CR_TOL[0])
        max_2v2_cr = SAMPA_2V2_CR * (1 + SAMPA_2V2_CR_TOL[1])
        min_3v2_cr = SAMPA_3V2_CR * (1 - SAMPA_3V2_CR_TOL[0])
        max_3v2_cr = SAMPA_3V2_CR * (1 + SAMPA_3V2_CR_TOL[1])
        min_3v2_cr_SAMPA2 = SAMPA_3V2_CR_SAMPA2 * (1 - SAMPA_3V2_CR_TOL[0])
        max_3v2_cr_SAMPA2 = SAMPA_3V2_CR * (1 + SAMPA_3V2_CR_TOL[1])
        fmin_2v2_cr = FEC_2V2_CR * (1 - FEC_2V2_CR_TOL[0])
        fmax_2v2_cr = FEC_2V2_CR * (1 + FEC_2V2_CR_TOL[1])
        fmin_3v2_cr = FEC_3V2_CR * (1 - FEC_3V2_CR_TOL[0])
        fmax_3v2_cr = FEC_3V2_CR * (1 + FEC_3V2_CR_TOL[1])
        for j in PList:
            if j[0]==2:
                if j[2] < min_2v2_cr:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "BELOW-MINIMUM-2.2V", j[2],min_2v2_cr])
                if j[2] > max_2v2_cr:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "ABOVE-MAXIMUM-2.2V", j[2],max_2v2_cr])

                if j[4] < min_3v2_cr_SAMPA2:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "BELOW-MINIMUM-3.2V", j[4],min_3v2_cr_SAMPA2])
                if j[4] > max_3v2_cr_SAMPA2:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "ABOVE-MAXIMUM-3.2V", j[4],max_3v2_cr_SAMPA2])
            else:
                if j[2] < min_2v2_cr:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "BELOW-MINIMUM-2.2V", j[2],min_2v2_cr])
                if j[2] > max_2v2_cr:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "ABOVE-MAXIMUM-2.2V", j[2],max_2v2_cr])

                if j[4] < min_3v2_cr:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "BELOW-MINIMUM-3.2V", j[4],min_3v2_cr])
                if j[4] > max_3v2_cr:
                    pwrLog.append(["SAMPA{}".format(j[0]), "RED", "ABOVE-MAXIMUM-3.2V", j[4],max_3v2_cr])

        Prs = findPowerMeasure(SupplyInfo, "ON NORMAL")
        if Prs[2] < fmin_2v2_cr:
            pwrLog.append(["FEC", "RED", "BELOW-MINIMUM-2.2V", Prs[2],fmin_2v2_cr])
        if Prs[2] > fmax_2v2_cr:
            pwrLog.append(["FEC", "RED", "ABOVE-MAXIMUM-2.2V", Prs[2],fmax_2v2_cr])

        if Prs[4] < fmin_3v2_cr:
            pwrLog.append(["FEC", "RED", "BELOW-MINIMUM-3.2V", Prs[4],fmin_3v2_cr])
        if Prs[4] > fmax_3v2_cr:
            pwrLog.append(["FEC", "RED", "ABOVE-MAXIMUM-3.2V", Prs[4],fmax_3v2_cr])

    return pwrLog

def checkPwrError(mList,name):
    for i in mList:
        if i[0]==name:
            return 1
    return 0

def testPower(SupplyInfo):
    pwrLog = []
    testLog = testPowerToList(SupplyInfo)
    OFF = findPowerMeasure(SupplyInfo, "OFF")
    if OFF != 0:
        PList = []
        for i in range(0, 5):
            ON = findPowerMeasure(SupplyInfo, "ON {}".format(i))
            if ON != 0:
                PList.append([i, ON[1], ON[2] - OFF[2], ON[3], ON[4] - OFF[4]])

        for j in PList:
            if checkPwrError(testLog,"SAMPA{}".format(j[0]))==1:
                color = "#ff4040"
            else:
                color = "#40ff40"
            pwrLog.append([color,"SAMPA{}    {:.4f} @ {:.4f}    {:.4f} @ {:.4f}".format(j[0], j[1], j[2], j[3], j[4])])

        Prs = findPowerMeasure(SupplyInfo, "ON NORMAL")
        if checkPwrError(testLog, "FEC") == 1:
            color = "#ff4040"
        else:
            color = "#40ff40"
        pwrLog.append([color,"FEC            {:.4f} @ {:.4f}    {:.4f} @ {:.4f}".format(Prs[1], Prs[2], Prs[3], Prs[4])])

    pwrLog.append(["#000000", ""])
    min_2v2_cr = SAMPA_2V2_CR * (1 - SAMPA_2V2_CR_TOL[0])
    max_2v2_cr = SAMPA_2V2_CR * (1 + SAMPA_2V2_CR_TOL[1])
    min_3v2_cr = SAMPA_3V2_CR * (1 - SAMPA_3V2_CR_TOL[0])
    max_3v2_cr = SAMPA_3V2_CR * (1 + SAMPA_3V2_CR_TOL[1])
    min_3v2_cr_SAMPA2 = SAMPA_3V2_CR_SAMPA2 * (1 - SAMPA_3V2_CR_TOL[0])
    max_3v2_cr_SAMPA2 = SAMPA_3V2_CR * (1 + SAMPA_3V2_CR_TOL[1])
    fmin_2v2_cr = FEC_2V2_CR * (1 - FEC_2V2_CR_TOL[0])
    fmax_2v2_cr = FEC_2V2_CR * (1 + FEC_2V2_CR_TOL[1])
    fmin_3v2_cr = FEC_3V2_CR * (1 - FEC_3V2_CR_TOL[0])
    fmax_3v2_cr = FEC_3V2_CR * (1 + FEC_3V2_CR_TOL[1])
    pwrLog.append(["#000000", "Current Limit Notes:"])
    pwrLog.append(["#000000", "2V2 SAMPA Center Current = {:.3f} Min = {:.3f} ({:.1f}%) Max = {:.3f} ({:.1f}%)".format(SAMPA_2V2_CR, min_2v2_cr,100*SAMPA_2V2_CR_TOL[0],max_2v2_cr,100*SAMPA_2V2_CR_TOL[1])])
    pwrLog.append(["#000000", "3V2 SAMPA Center Current = {:.3f} Min = {:.3f} ({:.1f}%) Max = {:.3f} ({:.1f}%)".format(SAMPA_3V2_CR, min_3v2_cr,100*SAMPA_3V2_CR_TOL[0],max_3v2_cr,100*SAMPA_3V2_CR_TOL[1])])
    pwrLog.append(["#000000", "3V2 SAMPA (SAMPA2) Min = {:.3f} Max = {:.3f} ".format(min_3v2_cr_SAMPA2,max_3v2_cr_SAMPA2)])
    pwrLog.append(["#000000", ""])
    pwrLog.append(["#000000", "2V2 FEC Center Current = {:.3f} Min = {:.3f} ({:.1f}%) Max = {:.3f} ({:.1f}%)".format(FEC_2V2_CR, fmin_2v2_cr,100*FEC_2V2_CR_TOL[0],fmax_2v2_cr,100*FEC_2V2_CR_TOL[1])])
    pwrLog.append(["#000000", "3V2 FEC Center Current = {:.3f} Min = {:.3f} ({:.1f}%) Max = {:.3f} ({:.1f}%)".format(FEC_3V2_CR, fmin_3v2_cr,100*FEC_3V2_CR_TOL[0],fmax_3v2_cr,100*FEC_3V2_CR_TOL[1])])

    if len(testLog) > 0:
        pwrLog.append(["#000000", ""])
        pwrLog.append(["#000000", "Errors:"])
    for i in testLog:
        pwrLog.append(["#ff4040","{} {} CURRENT={:.3f} Limit={:.3f}".format(i[0],i[2],i[3],i[4])])

    return pwrLog


def getPowerSummary(SupplyInfo):
    pwrInfo = []
    OFF = findPowerMeasure(SupplyInfo, "OFF")
    if OFF != 0:
        for i in range(0, 5):
           ON = findPowerMeasure(SupplyInfo, "ON {}".format(i))
           if ON!=0:
               pwrInfo.append(["SAMPA{} ".format(i), ON[1], ON[2] - OFF[2], ON[3], ON[4] - OFF[4]])

    ON = findPowerMeasure(SupplyInfo, "ON NORMAL")
    if ON != 0:
        pwrInfo.append(["NORMAL", ON[1], ON[2], ON[3], ON[4]])
    return pwrInfo

def docPower(SupplyInfo,doc_all=1):
    pwrLog = []
    OFF = findPowerMeasure(SupplyInfo, "OFF")
    if OFF != 0:
        PList = []
        if doc_all==1:
            for i in range(0, 5):
                ON = findPowerMeasure(SupplyInfo, "ON {}".format(i))
                if ON != 0:
                    PList.append(["SAMPA{} ".format(i), ON[1], ON[2] - OFF[2], ON[3], ON[4] - OFF[4]])
        ON = findPowerMeasure(SupplyInfo, "ON NORMAL")
        if ON != 0:
            PList.append(["NORMAL", ON[1], ON[2], ON[3], ON[4]])
        for j in PList:
            color = "#50ff50"
            failed = 0
            pwrLog.append([color,"{}      {:.4f}@{:.4f}      {:.4f}@{:.4f}".format(j[0], j[1], j[2], j[3], j[4])])
    return pwrLog

def readPowerFile(Path,ScaIDNum):
    try:
      scaID = "{:0>6}".format(hex(ScaIDNum)[2:])
    except:
      scaID = ScaIDNum
    pwrfilename = "{}/pwrInfo_fid{}.txt".format(Path,scaID)
    if os.path.isfile(pwrfilename) == False:
    	pwrfilename = "{}/pwrInfo.log".format(Path)
    	if os.path.isfile(pwrfilename) == False:
           sys.stdout.write("Cannot find PowerFile {}\n".format(pwrfilename))
           return []
    pwrFile = open(pwrfilename, 'r')
    SupplyInfo = []
    for line in pwrFile:
        words = line.split()
        mlen = len(words)
        if mlen > 0:
            cmd = words[0].lower()
            if cmd == "test":
                s1 = 1
                while words[s1] != "SRC1":
                    s1 += 1
                tname = cconcat(words, 1, s1)
                v1 = float(words[s1 + 1])
                i1 = float(words[s1 + 2])
                v2 = float(words[s1 + 4])
                i2 = float(words[s1 + 5])
                SupplyInfo.append([tname, v1, i1, v2, i2])
    pwrFile.close()
    return SupplyInfo

def TestPresentPwr(Prs):
    pwrLog = []
    color = color1 = color2 = "#5050ff"
    min_2v2_cr = FEC_2V2_CR * (1 - FEC_2V2_CR_TOL[0])
    max_2v2_cr = FEC_2V2_CR * (1 + FEC_2V2_CR_TOL[1])
    min_3v2_cr = FEC_3V2_CR * (1 - FEC_3V2_CR_TOL[0])
    max_3v2_cr = FEC_3V2_CR * (1 + FEC_3V2_CR_TOL[1])
    msg1 = "  V1={:.3f} I1={:.3f} ".format(Prs[1], Prs[2])
    msg2 = "  V2={:.3f} I2={:.3f} ".format(Prs[3], Prs[4])
    if Prs[2] < min_2v2_cr or Prs[2] > max_2v2_cr:
        msg1 += "FAILED"
        color = color1 = "#ff0050"
    else:
        msg1 += "PASSED"
    if Prs[4] < min_3v2_cr or Prs[4] > max_3v2_cr:
        msg2 += "FAILED"
        color = color2 = "#ff0050"
    else:
        msg2 += "PASSED"
    pwrLog.append([color,"Present Supply"])
    pwrLog.append([color1,msg1])
    pwrLog.append([color2,msg2])
    return pwrLog

