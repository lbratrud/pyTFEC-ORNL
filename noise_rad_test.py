#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import time
import glob
import FEC_DATA as FECdata

version = "1.1 10/17/2018"

sys.stdout.write("\nRadiation Test Noise Capture TPC version {}\n\n".format(version))
from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_IO as fec_io
import waveGen
import pulseReConstruct

i = 0
itr = -1
while  i < len(sys.argv):
   cmd = sys.argv[i].lower()
   if cmd == "-itr":
      itr = sys.argv[i + 1]
      i += 1
   elif cmd == "-test":
      resolution = int(sys.argv[i + 1])
      i += 1
   i+=1

if itr==-1:
   mDir = glob.glob("itr*")
#   sys.stdout.write("mDir={}\n".format(mDir))
   if len(mDir)==0:
      itr = 0 
   else:
       max_itr = 0
       for i in mDir:
          words = i.split("_")
          if words[0][0:3]=="itr":
              itr = int(words[0][3:])
              if itr > max_itr:
                 max_itr = itr
       itr = max_itr+1		
       sys.stdout.write("First unused Itr={}\n".format(itr))
CmdExecutionQueue = []
make_raw_capture=0
versionList = [ ["rad_noise_test",version],["FEC_IO",fec_io.version]]

fec = fec_io.FEC(CmdExecutionQueue,versionList)

myPath = os.getcwd()

def writeLog(log,code):
  for i in code:
     log.write(i+"\n")

def GetBrdInfo(log):
  cmd = "tsampa -v --mask 0x1 --sampa-mask 0x1f --pwr-on"
  code = fec_io.ExecuteCmdParse(cmd)
  writeLog(log,code)

def DoTest(itr):
  fname = time.strftime('%Y%b%d_%H%M%S')
  fpath = "{}/itr{}_{}".format(myPath, itr, fname)
  os.makedirs(fpath)
  os.chdir(fpath)
  log = open("RadTest.log",'w')
  numFrames = 4*5000000

  GetBrdInfo(log)
  cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir . --mask 0x1".format(numFrames)
  code = fec_io.ExecuteCmdParse(cmd)
  writeLog(log,code)

  GetBrdInfo(log)

  sys.stdout.write("\nDone with data capture\n\n")
  try:
    os.rename("run000000_trorc00_link00.bin", "run0_0.bin")
    os.rename("run000000_trorc00_link01.bin", "run1_0.bin")
    for i in [0,1]:
       cmd = "decoder_gbtx -f {} -i run{}_0.bin".format(numFrames,i)
       cmd += " -o run{}_0 -s {} -p".format(i,i)
       code = fec_io.ExecuteCmdParse(cmd)
       writeLog(log,code)
    
  except:
    sys.stdout.write("Failed to move files\n")
    pass
  log.close()

  fecData = FECdata.FEC_data()
  fecData.setAttr("Name","RadTest-{}".format(itr))
  fecData.setAttr("BoardID",0x804)
  fecData.setAttr("Mode","NoiseTest")
  fecData.setAttr("Path",fpath)
  fecData.setAttr("Description","RadTest Snapshot {}".format(itr))
  fecData.setAttr("Threshold",500)
  fecData.setAttr("Sequence","NONE")
  fecData.setAttr("PulseInfo","NONE")
  fecData.setAttr("NumIters",1)
  fecData.Save()
  fecData.ReadAll()
  PDFname = "radtest_{}_{}.pdf".format(itr,fname)
  fecData.MakePDF(PDFname)

DoTest(itr) 
exit()
