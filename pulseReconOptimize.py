#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
version = "2.0 9/28/2017"

sys.stdout.write("\nCapture TPC version {}\n\n".format(version))
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import pulseReConstruct
import projmkrpt as mkrpt
import FEC_IO as fec_io

#files = glob.glob("*GBTX0.dat")
files = ["C:\\Users\\lgc\\Desktop\\PYTFEC\\2017Nov02_180711_GBTX0.dat"]
#sys.stdout.write("INPUTFREQ={}\n".format(pulseReConstruct.FREQINPUT))
#if len(files)==0:
#	for gbtxNum in [0]:
#		files = glob.glob("run{}_*.bin".format(gbtxNum))
#		if len(files) > 0:
#			for i in files:
#				cmd = "decoder_gbtx -f {} -i {}".format(pulseReConstruct.numFrames,i)
#				nData = i.replace(".bin","")
#				cmd += " -o {} -s {} -p".format(nData,gbtxNum)
#				sys.stdout.write("Decoding GBTx file {}\n".format(nData))
#				fec_io.ExecuteCmdParse(cmd)
#	files = glob.glob("*GBTX0.dat")

dataList = []
for i in files:
	sys.stdout.write("MDA {}\n".format(i))
	data = pulseReConstruct.LoadPulseData(i)
	data.loadFile(i,maxitr=200000)
	data.construct()
	data.infofind()
	dataList.append(data)

pdfgen = mkrpt.makeReport("optimize25k.pdf", "Optimization")
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]

myData = []
myZeroData = []
lineList = []
cindex = 0
mList = range(25000,150000,25000)
mlen = len(mList)
for num_bins in [16,32]:
	pulseReConstruct.DoSetup(num_bins)
	cindex = 0
	for maxpnts in mList:
		lineList.append(["Samples={} Ts={}ns".format(maxpnts,pulseReConstruct.TIMERESOLUTION),ColorList[cindex],None])
		cindex += 1
		if cindex >= len(ColorList):
			cindex = 0
		H = pulseReConstruct.ConstructPulse(dataList[0].histogram,0,maxpnts=maxpnts)
		dataList[0].ReconList = [H]
		H = pulseReConstruct.ConstructPulse(dataList[0].histogram, 1, maxpnts=maxpnts)
		dataList[0].ReconList.append(H)
		dataList[0].pulseAlign()
		myData.append(dataList[0].ReconList[0])
		myZeroData.append(dataList[0].ReconList[1])
xmax = 0
ymax = 0
for i in myData:
	for k in i:
		if k[0] > xmax:
		   xmax = k[0]
		if k[1] > ymax:
			ymax = k[1]
xmax = 0.1*int(10.5*xmax)
ymax = int(1.05*ymax)

pdfgen.addBasicMultiPlot(xmax,ymax,myData[0:mlen],"Different Samples and Bins (Channel 0)",lineList[0:mlen],xpnts=10,plotsize=600)
pdfgen.addBasicMultiPlot(xmax,ymax,myData[mlen:],"Different Samples and Bins (Channel 0)",lineList[mlen:],xpnts=10,plotsize=600)
pdfgen.addBasicMultiPlot(xmax,ymax,myData,"Different Samples and Bins (Channel 0)",lineList,xpnts=10,plotsize=600)

xmax = 0
ymax = 0
for i in myZeroData:
	for k in i:
		if k[0] > xmax:
		   xmax = k[0]
		if k[1] > ymax:
			ymax = k[1]
xmax = 0.1*int(10.5*xmax)
ymax = int(1.05*ymax)

pdfgen.addBasicMultiPlot(xmax,ymax,myZeroData[0:mlen],"Different Samples and Bins (Channel 1)",lineList[0:mlen],xpnts=10,plotsize=600)

pdfgen.DoExit()

exit()
