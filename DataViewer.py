# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time

import string
import sys
import os
import array
import ctypes
import glob
import FEC_DATA as FECdata
from projbase import *
import projwin as win
import projtime as mtime
import commondb as cdb

import errwarn as errwarn
import projplot as pplot

import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d as axes3d
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
try:  # import as appropriate for 2.x vs. 3.x
   from tkinter import *
except:
   from Tkinter import *

viewModes = ["NoiseTest","WithPulser","PulsedInputs"]
do_exit = 0
MarkerList = ['s','o','v','+','d','D','_','|']
ColorList = ["#ff0000","#00ff00","#0000ff",
             "#800000","#008000","#000080",
             "#ff4040","#40ff40","#4040ff",
             "#804000","#008040","#400080",
             "#8080ff","#ff8080","#80ff80","#ffffff"]

class Plotter():
    def __init__(self,fecData):
        self.fecData = fecData
        self.markerIndex = 0
        self.colorIndex = 0

        fig, AxList = plt.subplots(ncols=2)              
        for i in range(0,5):
            for j in range(0,32):
                marker,color = self.getMarkerColor()
                self.getTimeDeltas(32*i+j,AxList[0],color,marker)
                self.getEnergy(32*i+j,AxList[1],color,marker)
#            self.MatLabPlot.nextReference()
        plt.show()

    def getMarkerColor(self):
        color = ColorList[self.colorIndex]
        self.colorIndex += 1
        if self.colorIndex >= len(ColorList):
            self.colorIndex = 0
            self.markerIndex += 1
        if self.markerIndex >= len(MarkerList):
            self.markerIndex = 0
        marker = MarkerList[self.markerIndex]
        return [marker,color]
    
    def ListToPlot(self,Axes,color,marker,Data):
        for i in Data:
           Axes.plot([i[0]],[i[1]],color=color,marker=marker)
           
        for i in Energy:
           Axes.plot([i[0]],[i[1]],color=color,marker=marker)

class DataViewer(win.Base):
    def __init__(self,fecData):
      if win.Base.__init__(self,"DataViewer","FEC Data Viewer")==0:
         return 0
      win.WinColumnFeed(self,1)
      if fecData != 0:
          self.fecData = fecData
      else:
          self.fecData = FECdata.FEC_data("UNKNOWN",-1,"UNKNOWN","UNKNOWN")

      self.display_mode = "index"
#      self.ColorIndex = ["#000000","#000080","#008000","#ffff00","#ff0000"]

      self.b1 = win.WinMenu(self,"Options",-1,0,1)
      self.b1.menub.grid(sticky=E+W+S+N)
      self.data_low_to_high=1
      self.data_view="rms"
      self.OprMode = "board"
      mode = self.fecData.Mode
         
#      win.WinRowFeed(self,1)
      LabDef = win.WinLabel(self,"Board ID",-1,1,3)
      self.OprDef = win.WinLabel(self,"{}".format(self.fecData.Mode),-1,1,15)
      self.OprDef.label.grid(columnspan=3)
      self.boardID = win.WinLabel(self,"{}".format(hex(int(self.fecData.BoardID))),-1,1,5)
      Desc = win.WinLabel(self,"Description",-1,2,3)
      self.Desc = win.WinLabel(self,"{}".format(self.fecData.Description),-1,2,5)
      win.WinLabel(self,"DataDirectory",-1,3,3)
      self.DataFileLabel = win.WinLabel(self,"{}".format(self.fecData.Path),-1,3,5)
      self.defineOptions()
      self.maxval = -5.0
      self.minval = 50000000
      self.avgval = 0
      
      row=4
      col = 1
      bcolor = "#4000ff"
      nrc = win.WinLabel(self,"Color Definitions",-1,row,col)
      nrc.label.config(bg=bcolor)
      row += 1
      self.DATA_ranges = []
      self.DATA_ranges_title = []
      self.DATA_ranges_equal = []
      self.DATA_color_level = []
      for i in fecData.RmsLevels:
          self.DATA_color_level.append(i)
      for i in range(0,5):
          color = cdb.ColorList.getParam("DATA_LEVEL{}".format(i))
          LabDef = win.WinLabel(self,"Value",-1,row,col)
          LabDef.label.config(bg=color,fg=seekRevColor(color))
          self.DATA_ranges_title.append(LabDef)
          if i==4:
             Leq = win.WinLabel(self,">",-1,row,col+1)
             Leq.label.config(bg=color,fg=seekRevColor(color))
          else:
             LabEntry = win.WinEntry(self,5,row,col+2)
             LabEntry.entry.config(bg=color,fg=seekRevColor(color))
             LabEntry.setVal(fecData.RmsLevels[i])
             self.DATA_ranges.append(LabEntry)
             Leq = win.WinLabel(self,"<",-1,row,col+1)
             Leq.label.config(bg=color,fg=seekRevColor(color))
          self.DATA_ranges_equal.append(Leq)
          col += 4
      nrc.label.grid(columnspan=col-2)
      self.Desc.label.grid(columnspan=col-6)
      self.DataFileLabel.label.grid(columnspan=col-6)
      row += 1
      win.WinRowFeed(self,row)
      row += 1

      self.DATA_labels = []
      self.DATA_headers = []
      self.DATA_values = []
      self.DATA_Cnt_labels = []
          
      col = 1
      for i in range(0,5):
          newSAMPA = []
          rowr = row + 1
          ml = win.WinLabel(self,"SAMPA{}".format(i),-1,rowr,col)
          ml.label.config(bg=bcolor)
          ml.label.grid(columnspan=3)

          ml = win.WinLabel(self,"Average RMS".format(i),-1,rowr+1,col)
          self.DATA_headers.append(ml)
          win.WinLabel(self,"=".format(i),-1,rowr+1,col+1)
          ml = win.WinLabel(self,"0.0".format(i),-1,rowr+1,col+2)
          self.DATA_values.append(ml)
          win.WinLabel(self,"# Samples".format(i),-1,rowr+2,col)
          win.WinLabel(self,"=".format(i),-1,rowr+2,col+1)
          ml = win.WinLabel(self,"0".format(i),-1,rowr+2,col+2)
          self.DATA_Cnt_labels.append(ml)

          win.WinRowFeed(self,row+4)
          rowr = row+5
          for i in range(0,16):
              ldef = win.WinLabel(self,"",-1,rowr,col)
              rowr += 1
              newSAMPA.append(ldef)
          col += 1
          win.WinColumnFeed(self,col)
          col += 1
          rowr = row+5
          for i in range(0,16):
              ldef = win.WinLabel(self,"",-1,rowr,col)
              rowr += 1
              newSAMPA.append(ldef)
          self.DATA_labels.append(newSAMPA)
          col += 1
          win.WinColumnFeed(self,col)
          col += 1
      win.WinRowFeed(self,rowr)
      self.DATAupdate()
      
    def setColorLevels(self):
        index = 0
        for i in self.DATA_ranges:
          myval = i.setVal("{}".format(self.DATA_color_level[index]))
          index += 1
        mode = self.getDataView()
        for i in self.DATA_ranges_title:
            i.label.config(text="{}".format(mode))
        cnt = 0
        for i in self.DATA_ranges_equal:
            code = self.data_low_to_high
            if cnt==4:
               code ^= 0x1
            if code==1:
               i.label.config(text="<")
            else:
               i.label.config(text=">")
            cnt += 1

    def getDataView(self):
        mode = self.data_view.upper()
        if mode=="MIN":
           mode="MIN-BL"
        elif mode=="MAX":
           mode="MAX-BL"
        elif mode=="EVCNT":
           mode="# Events"
        elif mode=="EVAVG":
           mode="Average Period"
        elif mode=="EVRMS":
           mode="Period RMS"
        elif mode=="EVTOL":
           mode="Threshold"
        return mode
        
    def DATAcolor(self,val):
        if self.data_low_to_high==1:
            for i in range(0,4):
                if val < self.DATA_color_level[i]:
                   return cdb.ColorList.getParam("DATA_LEVEL{}".format(i))
        else:
            for i in range(0,4):
                if val > self.DATA_color_level[i]:
                   return cdb.ColorList.getParam("DATA_LEVEL{}".format(i))
        return cdb.ColorList.getParam("DATA_LEVEL4")

    def DATAupdateGetLevels(self):
       index = 0
       for i in self.DATA_ranges:
          myval = i.getVal()
          try:
              fval = float(myval)
              self.DATA_color_level[index] = fval
          except:
              sys.stdout.write("{} is an invalid floating point number\n".format(myval))
          index += 1
       self.DATAupdate(update_color=0)
       
    def DATAupdate(self,update_color=1):
        self.data_low_to_high, self.DATA_color_level = self.fecData.defineDisplay(self.data_view)
        self.DataFileLabel.label.config(text=self.fecData.Path)
        for i in self.DATA_headers:
          i.label.config(text="Average {}".format(self.data_view.upper()))
        for i in range(0,5):
            avg = 0
            for j in range(0,32):
                val = self.fecData.Display[32*i+j]
                if self.data_view in ["rms","evrms"]:
                   valDef="{:.3f}".format(val)
                elif self.data_view in ["avg","evavg"]:
                   valDef="{:.1f}".format(val)
                elif self.data_view=="min":
                   valDef="{:3d}".format(val)
                elif self.data_view in ["max","evcnt","evtol"]:
                   valDef="{:4d}".format(val)
                avg += val
                ldef = self.DATA_labels[i][j]
                color = self.DATAcolor(val)
                if self.display_mode =="index":
                   ldef.label.config(text="[{:3d}]={}".format(i*32+j,valDef),bg=color,fg=seekRevColor(color))
                else:
                   ldef.label.config(text="[{:2d}]={}".format(j,valDef),bg=color,fg=seekRevColor(color))
            avg = avg / 32
            color = self.DATAcolor(avg)
            if self.data_view in ["min","max","avg","evcnt","evtol"]:
               self.DATA_values[i].label.config(text="{:.1f}".format(avg),bg=color,fg=seekRevColor(color))
            else:
               self.DATA_values[i].label.config(text="{:.3f}".format(avg),bg=color,fg=seekRevColor(color))
            self.DATA_Cnt_labels[i].label.config(text="{:7d}".format(self.fecData.Data[32*i][0]),bg=color,fg=seekRevColor(color))
                
    def setChannelDisplayMode(self,mode):
       self.display_mode = mode
       self.DATAupdateGetLevels()
       self.defineOptions() 

    def setDataView(self,mode):
        self.data_view=mode
        self.DATAupdateGetLevels()
        self.setColorLevels()
        self.defineOptions()
        self.DATAupdate()
        
    def makePDFReport(self):
       self.fecData.MakePDF("{}/{}-{}.pdf".format(self.fecData.Path,self.fecData.Name,time.strftime('%Y%b%d_%H%M%S')))
       sys.stdout.write("Done making PDF file\n")


    def updateLabels(self):
        self.boardID.label.config(text=self.fecData.BoardID)
        self.DataFileLabel.label.config(text=self.fecData.Path)
        self.Desc.label.config(text=self.fecData.desc)

    def readTPCFile(self,use_all):
        filename = tkFileDialog.askopenfilename(initialdir=fec.getCapturePath(),filetypes=[('analysis files','.ays')])
        fec.AYS_FileReader(filename,use_all=1)
        fec.outputFileName = filename
        self.updateLabels()
        fec_io.SParser.setupColorLevels(self.OprMode)
        self.DATAupdate(update_color=0)

    def defineOptions(self):
       global Running_Data_Capture,pdf_generator_enabled
       mainmenu = self.b1.menu
       mainmenu.delete(0,END)
       mainmenu.add_command(label="Update",command=self.DATAupdateGetLevels)

       m = Menu(mainmenu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=1)
       m.add_command(label="Only Valid Events",command=lambda use_all=0:self.readTPCFile(use_all))
       m.add_command(label="All Events",command=lambda use_all=1:self.readTPCFile(use_all))
       mainmenu.add_cascade(label="Read TPC file",menu=m)

       m = Menu(mainmenu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=1)
       mList = []
       mList.append(["rms","set Data View = RMS"])
       mList.append(["avg","set Data View = AVG"])
       mList.append(["min","set Data View = MIN-baseline(BL)"])
       mList.append(["max","set Data View = MAX-baseline(BL)"])
       if self.fecData.events_active==1:
          mList.append(["evcnt","set Data View = Event Counts"])
          mList.append(["evavg","set Data View = Event Average Period"])
          mList.append(["evrms","set Data View = Event Period RMS"])
          mList.append(["evtol","set Data View = Event Threshold"])
       for i in mList:
           if self.data_view!=i[0]:
              m.add_command(label=i[1],command=lambda mode=i[0]: self.setDataView(mode))
          
       mainmenu.add_cascade(label="Data View = {}".format(self.getDataView()),menu=m)

       mainmenu.add_separator()
       mainmenu.add_command(label="Make PDF report",command=self.makePDFReport)
       mainmenu.add_separator()

       if self.display_mode=="index":
           mainmenu.add_command(label="Channel Mode",command=lambda mode="channel":self.setChannelDisplayMode(mode))
       else:
           mainmenu.add_command(label="Index Mode",command=lambda mode="index":self.setChannelDisplayMode(mode))
       mainmenu.add_command(label="Exit",command=self.DoExit)
       
    def DoExit(self):
        global windataviewer,do_exit
        windataviewer=0
        if do_exit == 1:
           exit(0)
        else:
           win.Base.DoExit(self)
