#!/usr/bin/env python3

# captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob

import projpath as mpath
mpath.projName = "TPC-FEC"

import projtime as mtime

version = "1.2 1/05/2018"

from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_DATA as fecDATA
import FEC_IO as fec_io

d1 = fecDATA.FEC_data()
d1.Read("runinfo.txt")

do_update = 0
display = 0
do_help = 0
globDef = []
Threshold = 125

i = 1
while i < len(sys.argv):
    cmd = sys.argv[i].lower()
    if cmd in ["-update","--update","--u","-u"]:
       do_update = 1
    elif cmd in ["-t","--tol","--thr"]:
       i+=1
       Threshold = int(sys.argv[i])
       d1.setAttr("Threshold",Threshold)
    elif cmd in ["-d","--display"]:
       display = 1
    elif cmd in ["-g","--glob"]:
       i+=1
       globDef = int(sys.argv[i])
    else:
       do_help = 1
    i+=1

if display==1:
   import commondbed as cdbed
   import DataViewer
   DataViewer.do_exit=1

sys.stdout.write("viewFecData Version={}\n".format(version))
sys.stdout.write("A tool to view the FEC data and generate PDF output file\n")
if do_help==1:
   sys.stdout.write("Options:\n")
   sys.stdout.write("-u,-update\tProcess bin files and generate statistics files\n")    
   sys.stdout.write("-t,-tol,-thr <tolerance>\tSpecific new tolerance/threshold for event detection. Requires -u.\n")    
   sys.stdout.write("-g,-glob <dirname>\tMerge multiple directoreis together and give a combined result\n")    
   sys.stdout.write("-h,-help\tThis help information\n")    
   exit(0)
   
if do_update==1:
   for j in [0,1]:
      dList = glob.glob("run{}_*.bin".format(j))
      for i in dList:
         oname = i.replace(".bin","")
         cmd = "decoder_gbtx -f 100000000 -i {} -s {} -o {} -l {}".format(i,j,oname,Threshold)
         code = fec_io.ExecuteCmdParse(cmd,add_cmd=1)
         for k in code:
            sys.stdout.write("{}\n".format(k))

d1.ReadAll()
dList = glob.glob("*/runinfo.txt")
for i in dList:
   d1.Read(i)
   d1.ReadAll()

d1.setAttr("Path",os.getcwd())
d1.setupColorLevels()
if display==1:
   import projwin as win
   DataViewer.DataViewer(d1)
   win._root.mainloop()
else:
   filename = "{}/{}-{}.pdf".format(d1.Path,d1.Name,time.strftime('%Y%b%d_%H%M%S'))
   d1.MakePDF(filename)
   exit()
