#!/usr/bin/env python3
#
# Variation of captureTPC.py for DOING ALL PULSE TESTING!
#
#
# ORNL - 6/25/2018
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os

version = "1.0 6/25/2018"
import sys
import string
import sys
import os
import math

from projbase import *
import projpath as mpath

mpath.projName = "TPC-FEC"
import errwarn as errwarn

errwarn.ErrWarn.setup(0)
import FEC_IO
import waveGen
import pulseReConstruct
import FEC_DATA as fecDATA
from projbase import *
import projpath as mpath

mpath.projName = "TPC-FEC"
import errwarn as errwarn

errwarn.ErrWarn.setup(0)
import FEC_IO
import waveGen
import pulseReConstruct

version = "1.0 6/25/2018"

sys.stdout.write("\nbasicTests {}\n\n".format(version))
sys.stdout.write("Implement the basic FEC board tests\n")
CmdExecutionQueue = []
make_raw_capture = 0
fec = FEC_IO.FEC(CmdExecutionQueue, [version])
sys.stdout.write("Doing FEC INIT\n")
if fec.checkFastInit()==-1:
    if fec.DoInitProc() == -1:
        sys.stdout.write("Cannot establish a link with the FEC. Stopping test!")
        exit(0)

sys.stdout.write("WAVEFORM generator setup\n")
waveGen.setupFullSignal(0, [1000, 0.001, 10e-9, 10e-9, "off", 10e-6, "off"])
waveGen.setupFullSignal(1, [1000, 0.001, 10e-9, 10e-9, "off", 10e-6, "off"])

Data = fecDATA.FEC_data()
FEC_IO.DoBasicTests(fec, Data)

exit()
