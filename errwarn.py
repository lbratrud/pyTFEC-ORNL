# errwarn.py
#
# Define the log mechanism for the project!
#
# --------------------------------------------------------------------------------
# Created 1/1/2015 by Lloyd Clonts clontslg@yahoo.com
# Project started December 2013 for Gary W. Turner
# for implementing CTC system



import sys
import os
import time
import projpath as mpath
import string
from projbase import *

ErrWarn = 0
# --------------------------------------------------------------------------------      
# --------------------------------------------------------------------------------      
class myErrorWarn:
   def __init__(self):
       self.warn_cnt = 0
       self.error_cnt = 0
       self.fileName = ""
       self.lineCnt = -1
       self.LogList = []
       self.logcnt = 0
       self.errorcnt = 0
       self.warncnt = 0
       self.LogObj = 0
       self.log = 0
       self.disable_LogObj = 1

   def setup(self,listobj):
      datef = time.strftime('%Y_%b_%d_%I_%M')
      path = mpath.getPath("log")
      self.log = mpath.openFile("log","log",datef,'w')
      sys.stdout.write("Log Directory {}\n".format(path))
      sys.stdout.write("Creating log_{}\n".format(datef))
      sys.stdout.write("Messages will be saved to the Main Window \n")
      sys.stdout.write("Debugging information will be saved in this window \n")
      self.LogObj = listobj
       
   def close(self):
      self.log.close()

   def SetFileCnt(self,filename,lineCnt):
      self.fileName = filename
      self.lineCnt = lineCnt
      
   def FinalInfo(self):
      self.MakeLog("Errors={} Warnings={}\n".format(self.error_cnt,self.warn_cnt))
      
   def reset(self):
      self.errorcnt = 1
      self.warncnt = 1
      self.LogObj.insert(0,"Errors: (RED)")
      self.LogObj.insert(1,"Warning: (YELLOW)")
      
   def postWarning(self,msg):
      cnt = self.errorcnt+self.warncnt
      self.LogObj.insert(cnt,msg)
      self.LogObj.itemconfig(cnt,bg="yellow",fg="black")
      self.warncnt += 1

   def postError(self,msg):
      self.LogObj.insert(self.errorcnt,msg)
      self.LogObj.itemconfig(self.errorcnt,bg="red",fg="black")
      self.errorcnt += 1

   def addWindowLog(self,msg):
      self.LogObj.insert(self.logcnt,msg)
      if msg[0:5]=="WARN=":
         self.LogObj.itemconfig(self.logcnt,bg="yellow",fg="black")
      elif msg[0:6]=="ERROR=":
         self.LogObj.itemconfig(self.logcnt,bg="red",fg="black")
      self.logcnt += 1

   def MakeLog(self,msg):
      if self.disable_LogObj==0:
         if self.LogObj==0:
             self.LogList.append(msg)
         else:
             if len(self.LogList) > 0:
                for ll in self.LogList:
                   self.addWindowLog(ll)
                self.LogList = []
             self.addWindowLog(msg)

      if self.log!=0:
         self.log.write(msg+"\n")

   def RecWarn(self,cnt_incr,line):
         self.warn_cnt+=cnt_incr
         if cnt_incr==0:
            self.MakeLog("             {}\n".format(line))
         elif self.lineCnt >= 0:
            self.MakeLog("WARN={} at {} line={}".format(self.warn_cnt,self.fileName,self.lineCnt))
            self.MakeLog("  WHY: {}".format(line))
            self.MakeLog("")
         else:
            self.MakeLog("WARN={}: {}".format(self.warn_cnt,line))
            
   def RecErr(self,cnt_incr,line):
         self.error_cnt+=cnt_incr
         if cnt_incr==0:
            self.MakeLog("             {}".format(line))
         elif self.lineCnt >= 0:
            self.MakeLog("ERROR={} at {} line={}".format(self.error_cnt,self.fileName,self.lineCnt))
            self.MakeLog("  WHY: {}".format(line))
            self.MakeLog("")
         else:
            self.MakeLog("ERROR={}: {}".format(self.error_cnt,line))

ErrWarn = myErrorWarn()

# Time based logging
def MsgCommon(msg):
   datef = time.strftime('%Y/%b/%d %I:%M:%S ')
   nmsg = datef+msg.translate(strmaketrans("\n"," "))
##   if ErrWarn.LogObj==0:
##      sys.stdout.write("{}\n".format(nmsg))
   return nmsg

def Close():
   if ErrWarn.log!=0:
      ErrWarn.close()
      ErrWarn.log = 0

def LogError(msg):
   ErrWarn.RecErr(1,MsgCommon(msg))

def LogWarn(msg):
   ErrWarn.RecWarn(1,MsgCommon(msg))

def LogMsg(msg):
   ErrWarn.MakeLog(MsgCommon(msg))

