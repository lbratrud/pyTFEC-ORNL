#!/usr/bin/env python3

# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import threading
import _thread

import string
import sys
import os
import array
import ctypes
import mmap
import glob
import FEC_DATA as FECdata
from projbase import *
import DataViewer
#import projplot as pplot

import commondb as cdb
cdb.ColorList.addwDesc("PYFEC_STATUS_INIT","#4040ff","PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_RUNNING","#ff4040","PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_IDLE","#40ff40","PYFEC")

from projbase import *
Version = "3.0 06/12/2017"
import projpath as mpath
mpath.projName = "TPC-FEC"

import commondbed as cdbed
import projwin as win
import projtime as mtime

import errwarn as errwarn
errwarn.ErrWarn.setup(0)

import FEC_IO as fec_io
import gbtxconfigIO as gbtxcnfg
import GBTx0cnfg as Cnfg0
import GBTx0acnfg as Cnfg0a
import GBTx1cnfg as Cnfg1
import waveGen
import supplyCtrl

try:  # import as appropriate for 2.x vs. 3.x
   from tkinter import *
except:
   from Tkinter import *

if sys.version_info[0]>2:
   import threading
else:
   import thread
# or
#try:  # import as appropriate for 2.x vs. 3.x
#   from reportlab.pdfgen import canvas
#   pdf_generator_enabled = 1
#except:
#   sys.stdout.write("Cannot find reportlab. PDF Generator turned off\n")
#   pdf_generator_enabled = 0

cdb.ReadDB()
#---

#MatLabPlot = 0
#---
windataviewer = 0
do_exit = 0

CmdExecutionQueue = []

#cdbed.ColorEditor()
#=----------------------------------------------------------------------------------------------
#=----------------------------------------------------------------------------------------------
class WindowMain(win.Base):
    def __init__(self):
      self.Log = 0
      if win.Base.__init__(self,"mmain","pyTFEC version={}".format(Version))==0:
         return 0
      row = 1
      self.jobID = 1

      self.b1 = win.WinMenu(self,"Options",-1,row,0)
      self.b1.menub.grid(sticky=E+W+S+N)
      self.PulseInfo = "None"
      row += 1
      win.WinRowFeed(self,row+1)
      row += 2

      self.CaptureStatus = win.WinValueLabel(self,"Status",-1,row,0)
      self.CaptureStatus.setVal("Initializing")
      self.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))
      row += 1

      hdr = win.WinLabel(self,"Data Capture Parameters",-1,row,0)
      hdr.label.grid(sticky=E+W,columnspan=2)
      row += 1
      self.fecDATA = 0
      self.CaptureType = "None"
      self.CaptureTime = "None"
      # -1 == COMPLETELY OFF
      #  0 == Processing data (capture is available)
      #  1 == Capturing data.
      self.capture_active = -1 # COMPELTELY OFF!
      self.CaptureMultiInfo = [0,0] # When > 0, we are capturing data!
      self.gain = "20mV/fC"
      self.shaping = "160ns"
      self.polarity = "Neg"

      win.WinLabel(self,"Capture Type",-1,row,0)
      self.CaptureTypeDef = win.WinMenu(self,"NONE",-1,row,1)
      self.CaptureTypeDef.menub.grid(sticky=E+W)
      for i in DataViewer.viewModes:
          self.CaptureTypeDef.menu.add_command(label=i,command=lambda val=i:self.setCaptureType(val))
      row += 1
      win.WinLabel(self,"# Timebins",-1,row,0)
      self.CaptureTimeDef = win.WinMenu(self,"NONE",-1,row,1)
      self.CaptureTimeDef.menub.grid(sticky=E+W)
      for i in ["100k","500k","1M","5M","10M","50M","100M","200M"]:
          self.CaptureTimeDef.menu.add_command(label=i,command=lambda val=i: self.setCaptureTime(val))
      self.RunEvents = win.WinButton(self,"Run",row,2)
      self.RunEvents.button.config(command=self.DoSingleCapture)
      self.setCaptureType("None")
      self.setCaptureTime("None")

      row += 1
      self.Desc = win.WinValueEntry(self,"Description",20,row,0)
      self.Desc.entry.grid(columnspan=2)
      self.Desc.setVal("Unknown")
      self.destroyOnExit = 1
      self.closed = False
     
      self.Log = win.WinScrollList(self,3,"Log",80,35,1,4,[])
      self.defineOptions()

    def setCaptureType(self,val):
        self.CaptureTypeDef.menub.config(text=val)
        self.CaptureType = val
        if self.CaptureType == "None" or self.CaptureTime == "None":
           self.RunEvents.button.config(bg="#00007f",fg="#000000",state=DISABLED)
        else:
           self.RunEvents.button.config(bg="#00cf00",fg="#ffffff",state=NORMAL)
           self.defineOptions()
        
    def setCaptureTime(self,val):
        self.CaptureTimeDef.menub.config(text=val)
        if val!="None":
           mtl = val[-1].upper()
           scale = 1
           if mtl=="K":
              scale = 1000
           elif mtl=="M":
              scale = 1000000
           self.CaptureTime = int(val[0:-1])*scale
#        sys.stdout.write("{}==>{}\n".format(val,self.CaptureTime))
        if self.CaptureType == "None" or self.CaptureTime == "None":
           self.RunEvents.button.config(bg="#00007f",fg="#000000",state=DISABLED)
        else:
           self.RunEvents.button.config(bg="#00cf00",fg="#ffffff",state=NORMAL)
           self.defineOptions()
        
    def setupFecCapture(self,fecData):
        if self.CaptureTime > 5e6:
           ctime = 5e6
           numIters = int(self.CaptureTime / ctime+0.5)
        else:
           ctime = int(self.CaptureTime)
           numIters = 1
        if ctime * numIters < int(self.CaptureTime):
           numIters += 1

        fecData.setAttr("Timebins", int(ctime))
        fecData.setAttr("NumIters", numIters)

    def DoSingleCapture(self):
        global fec
        self.CaptureStatus.setVal("CAPTURE::BUSY")
        self.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_RUNNING"))
        self.RunEvents.button.config(bg="#00007f",fg="#000000",state=DISABLED)
        fname = time.strftime('%Y%b%d_%H%M%S')
        fpath = "{}/{}/{}".format(fec.capturePath,self.CaptureType,fname)
        fecData = FECdata.FEC_data()
        fecData.setAttr("Name",fname)
        fecData.setAttr("BoardID",fec.boardID)
        fecData.setAttr("Mode",self.CaptureType.upper())
        fecData.setAttr("Path",fpath)
        fecData.setAttr("Description",self.Desc.getVal())
        fecData.setAttr("Threshold",fec.Threshold)
        fecData.setAttr("Sequence","NONE")
        fecData.setAttr("PulseInfo",self.PulseInfo)
        fecData.setAttr("Gain",self.gain)
        fecData.setAttr("Shaping",self.shaping)
        fecData.setAttr("Polarity",self.polarity)
        self.setupFecCapture(fecData)
        self.fecData = fecData
        self.doCaptureSequence(fecData)


    def setupCaptureSweep(self,sweepMode):
        global fec
        if self.CaptureMultiInfo[0] != 0:
            return
        self.setCaptureType("PulsedInputs")
        self.setCaptureTime("10M")
        self.SweepList = []
        baseVpp = 0.02
        baseFreq = 6000
        basePulseFR = 3e-9
        if sweepMode == "FREQ":
            # freq, Vpp, rise/fall time for pulse (faster is better)
            for i in range(1000, 6000, 1000):
                self.SweepList.append([i, baseVpp, basePulseFR, "on"])
        elif sweepMode == "INPUT":
            for i in range(2, 21, 2):
                self.SweepList.append([baseFreq, 0.001 * i, basePulseFR, "on"])
        elif sweepMode == "EDGE":
            for i in range(1, 20):
                self.SweepList.append([baseFreq, baseVpp, i * 3e-9, "on"])
        else:
            return

        num_captures = len(self.SweepList)
        self.CaptureStatus.setVal("CAPTURE-SWEEP::BUSY 0/{}".format(num_captures))
        self.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_RUNNING"))
        self.RunEvents.button.config(bg="#00007f", fg="#000000", state=DISABLED)
        self.CaptureMultiInfo = ["sweep", 0, num_captures]
        self.defineOptions()
        fname = time.strftime('%Y%b%d_%H%M%S')
        mfpath = "{}/Sweep_{}/{}".format(fec.capturePath, sweepMode,fname)
        os.makedirs(mfpath)
        self.FilePath = mfpath
        self.fecData = 0
        self.nextCaptureSweep()
        # fname = "SW0"
        # fpath = "{}/{}".format(mfpath, fname)
        # fecData = FECdata.FEC_data()
        # fecData.setAttr("Name", "SW0")
        # fecData.setAttr("BoardID", fec.boardID)
        # fecData.setAttr("Mode", self.CaptureType.upper())
        # fecData.setAttr("PathMulti", mfpath)
        # fecData.setAttr("Path", fpath)
        # fecData.setAttr("Description", self.Desc.getVal())
        # self.setupFecCapture(fecData)
        # fecData.setAttr("Threshold", fec.Threshold)
        # fecData.setAttr("Sequence", "{}/{}".format(self.CaptureMultiInfo[1] + 1, self.CaptureMultiInfo[2]))
        # PulseInfo = waveGen.setupSignal(0, self.SweepList[self.CaptureMultiInfo[1]])
        # fecData.setAttr("PulseInfo",PulseInfo)
        # self.fecData = fecData
        # self.doCaptureSequence(fecData)

    def nextCaptureSweep(self):
        global fec
        if self.CaptureMultiInfo[1] >= self.CaptureMultiInfo[2]:
            self.CaptureMultiInfo = [0, 0, 0]
            self.CaptureStatus.setVal("CAPTURE-SWEEP::DONE")
            self.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"))
            self.RunEvents.button.config(bg="#00cf00", fg="#ffffff", state=NORMAL)
            self.fecData.MakePDF("{}.pdf".format(self.fecData.Name))
            return
        else:
            self.CaptureStatus.setVal(
                "CAPTURE-SWEEP:: BUSY {}/{}".format(self.CaptureMultiInfo[1], self.CaptureMultiInfo[2]))

        fname = "SW{}".format(self.CaptureMultiInfo[1])
        fpath = "{}/{}".format(self.FilePath, fname)
        fecData = FECdata.FEC_data()
        if self.fecData == 0:
           self.fecData = fecData
           fecData.setAttr("Name", fname)
           fecData.setAttr("BoardID", fec.boardID)
           fecData.setAttr("Mode", self.CaptureType.upper())
           fecData.setAttr("PathMulti", self.FilePath)
           fecData.setAttr("Path", fpath)
           fecData.setAttr("Description", self.Desc.getVal())
           self.setupFecCapture(fecData)
           fecData.setAttr("Threshold", fec.Threshold)
        else:
           fecData.CopyInfo(self.fecData)
        fecData.setAttr("Name", fname)
        fecData.setAttr("Path", fpath)
        fecData.setAttr("Sequence", "{}/{}".format(self.CaptureMultiInfo[1] + 1, self.CaptureMultiInfo[2]))
        self.PulseInfo = waveGen.setupSignal(0, self.SweepList[self.CaptureMultiInfo[1]])
        fecData.setAttr("PulseInfo",self.PulseInfo)
        self.doCaptureSequence(fecData)

    def nextCaptureMulti(self):
        global fec
        if self.CaptureMultiInfo[1] >= self.CaptureMultiInfo[2]:
           self.CaptureMultiInfo = [0,0,0]
           self.CaptureStatus.setVal("CAPTURE-MULTI::DONE")
           self.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"))
           self.RunEvents.button.config(bg="#00cf00",fg="#ffffff",state=NORMAL)
           self.fecData.MakePDF("{}.pdf".format(self.fecData.Name))
           return
        else:
           self.CaptureStatus.setVal("CAPTURE-MULTI:: BUSY {}/{}".format(self.CaptureMultiInfo[1],self.CaptureMultiInfo[2]))
           
        fname = time.strftime('%Y%b%d_%H%M%S')
        fpath = "{}/{}".format(self.fecData.PathMulti,fname)
        fecData = FECdata.FEC_data()
        fecData.CopyInfo(self.fecData)
        fecData.setAttr("Name","{}_{}".format(self.CaptureType,fname))
        fecData.setAttr("Path",fpath)
        fecData.setAttr("Sequence","{}/{}".format(self.CaptureMultiInfo[1]+1,self.CaptureMultiInfo[2]))
        self.doCaptureSequence(fecData)
        
    def doCaptureRecon(self,fecData):
        os.makedirs(fecData.Path)
        self.Log.addLog("Running Data Capture RECON JobID={}".format(self.jobID))
        self.Log.addLog("==> Path = {}".format(fecData.Path))
        self.Log.addLog("==> Name = {}".format(fecData.Name))
        self.capture_active = 1
        PT = threading.Thread(target=RunReconCapture, args=(self.jobID, fecData))
        PT.start()
        self.jobID += 1
        fecData.Save()

    def doCaptureSequence(self,fecData):
        os.makedirs(fecData.Path)
        self.Log.addLog("Running Data Capture JobID={}".format(self.jobID))
        self.Log.addLog("==> Path = {}".format(fecData.Path))
        self.Log.addLog("==> Name = {}".format(fecData.Name))
        self.capture_active = 1
        PT = threading.Thread(target=RunCapture, args=(self.jobID, fecData))
        PT.start()
        self.jobID += 1
        fecData.Save()

    def FindLastRunInfo(self):
      path = fec.capturePath
      fecname = fec.getFecName()
      fList = glob.glob("{}/cp{}_*.ays".format(path,fecname))
      maxref = 1
      for i in fList:
          lines = i.translate(string.maketrans("_", " ")).split()
          try:
              val = int(lines[1])
          except:
              val = 0
          if val >= maxref:
             maxref = val + 1
      self.fileRef.setVal("cp{}_{}".format(fecname,maxref))
      
    def PowerTest(self):
        global fec
        fec.POWER_Scan()

    def PowerMeasure(self):
        global fec
        sys.stdout.write("PowerMeasure\n")
        fec.getSupplyInfo("MEASURE")

    def DoCmd(self,code,msg=0):
        CmdExecutionQueue.append([code,msg])

    def DoEditor(self,editor):
      if editor=="color":
          cdbed.ColorEditor()
      elif editor=="font":
          cdbed.FontEditor()
      elif editor=="parameter":
          cdbed.ParameterEditor()

    def DataViewer(self):
        global windataviewer
        if windataviewer==0:
           windataviewer = DataViewer.DataViewer(self.fecData)
        else:
           windataviewer.updateLabels()


    def defineWaveform(self,amplitude):
        self.PulseInfo = waveGen.setupSignal(0, [6000,amplitude*0.001,3e-9,"on"])
    def defineReconWaveform(self,amplitude):
        self.PulseInfo = waveGen.setupFullSignal(0,[33300,amplitude*0.001,4e-9,8e-6,"on",15e-6])

    def setGain(self,gain):
        if gain==0:
           self.gain = "3mV/fC"
        elif gain==1:
           self.gain = "20mV/fC"
        else:
           self.gain = "30mV/fC"
        fec.SAMPA_setGain(gain)

    def setShaping(self,shaping):
        if shaping==0:
           self.shaping = "160ns"
        else:
           self.shaping = "320ns"
        fec.SAMPA_setShaping(shaping)
    def setPolarity(self,polarity):
        if polarity==0:
           self.polarity = "Pos"
        else:
           self.polarity = "Neg"
        fec.SAMPA_setPolarity(polarity)

    def defineOptions(self):
       if self.Log==0:
          return
       
       mode = ""
       mainmenu = self.b1.menu
       mainmenu.delete(0,END)
       mainmenu.add_command(label="Clear Log",command=lambda cmd="clearlog":self.DoCmd(cmd))
       mainmenu.add_command(label="Data Analyzer",command=self.DataViewer)
       mainmenu.add_command(label="POWER Measure",command=self.PowerMeasure)
       m = Menu(mainmenu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=1)
       for i in ["Color","Font","Parameter"]:
           m.add_command(label="{} Editor".format(i),command=lambda editor=i.lower():self.DoEditor(editor))
       mainmenu.add_cascade(label="GUI Editors",menu=m)
      
       m = Menu(mainmenu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=1)
       m.add_command(label="POWER Test",command=self.PowerTest)
       m.add_command(label="ADC Test",command=lambda cmd="adctest":self.DoCmd(cmd))
       m.add_separator()
       m.add_command(label="SAMPAs ON Direct",command=lambda cmd="sampasondirect":self.DoCmd(cmd))
       m.add_command(label="SAMPAs ON Sequenced",command=lambda cmd="sampasonsequenced":self.DoCmd(cmd))
       m.add_separator()
       m.add_command(label="SAMPAs OFF",command=lambda cmd="sampasoff":self.DoCmd(cmd))
       if mode==0:
          m.add_separator()
          m.add_command(label="SAMPA Serial Link Test",command=lambda cmd="seriallinkread":self.DoCmd(cmd))
          m.add_command(label="Test Serial Link",command=lambda cmd="checkactivelinks":self.DoCmd(cmd))
       mainmenu.add_cascade(label="Board Tests",menu=m)

       m = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
       for i in [ ["Frequency","FREQ"],["Input Voltage","INPUT"],["Edge Rate","EDGE"]]:
           m.add_command(label="{} Sweep".format(i[0]), command=lambda mode=i[1]:self.setupCaptureSweep(mode))
       mainmenu.add_cascade(label="Multi-Sequence Capture",menu=m)

       m = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
       for i in [0,10, 15, 20, 25, 30,35,40,45,50,55,60,67,70]:
           m.add_command(label="{}mV".format(i), command=lambda amplitude=i:self.defineWaveform(amplitude))
       mainmenu.add_cascade(label="Pulse Amplitude @ 6Hz & 3ns edge",menu=m)
       m = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
       for i in [0,5,10, 15, 20, 25, 30,35,40,45,50,55,60,67,70]:
           m.add_command(label="{}mV".format(i), command=lambda amplitude=i:self.defineReconWaveform(amplitude))
       mainmenu.add_cascade(label="Pulse Amplitude @ 509kHz & 3ns edge",menu=m)

       m = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
       for i in [["4mV/fC",0],["20mV/fC",1],["30mV/fC",2]]:
           m.add_command(label="{}".format(i[0]), command=lambda gain=i[1]:self.setGain(gain))
       mainmenu.add_cascade(label="Set Gain",menu=m)

       m = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
       for i in [["160ns",0],["320ns",1]]:
           m.add_command(label="{}".format(i[0]), command=lambda gain=i[1]:self.setShaping(gain))
       mainmenu.add_cascade(label="Set Shaping",menu=m)
       m = Menu(mainmenu, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
       for i in [["Neg",1],["Pos",0]]:
           m.add_command(label="{}".format(i[0]), command=lambda gain=i[1]:self.setPolarity(gain))
       mainmenu.add_cascade(label="Set Polarity",menu=m)

       m = Menu(mainmenu,bg=self.mbgc, fg=self.mfgc,font=self.font,relief=RIDGE,tearoff=1)
       m.add_command(label="Program GBTx1",command=lambda cmd="programgbtx1":self.DoCmd(cmd))
       m.add_separator()
       m.add_command(label="Create GBTx0 Configuration file",command=lambda cmd="gengbtx0cnfg":self.DoCmd(cmd))
       m.add_command(label="Create GBTx0a Configuration file",command=lambda cmd="gengbtx0acnfg":self.DoCmd(cmd))
       m.add_command(label="Create GBTx1 Configuration file",command=lambda cmd="gengbtx1cnfg":self.DoCmd(cmd))
       mainmenu.add_cascade(label="Programming",menu=m)
#=----------------------
 
       mainmenu.add_command(label="Test Power Supply Variation",command=self.POWER_Scan_test_variation)
       mainmenu.add_separator()
       mainmenu.add_command(label="Exit (FEC remains powered)",command=self.doSysExit)
       mainmenu.add_command(label="Shutdown (FEC is powered down)",command=self.doSysShutdown)

    def POWER_Scan_test_variation(self):
        fec.POWER_Scan_test_variation()

    def DoProjReport(self):
#       report.report_SpyProjSeq()
#       CastleEditor(self.currentobj,self)
      return

    def doSysShutdown(self):
       if fec.Supply!=0:
          fec.Supply.Off()
       self.doSysExit()

    def doSysExit(self):
       global do_exit
       # Does not hurt to turn off waveform generator
       if waveGen.wavegen!=0:
          waveGen.wavegen.Channel[0].Off()
       do_exit=1
       self.close()

winmain = WindowMain()
versionList = [ ["pyTFEC",Version],["FEC_IO",fec_io.version], ["gbtxconfigIO",gbtxcnfg.version],["GBTxOConfigure",Cnfg0.version],["GBTx1Configure",Cnfg1.version]]
fec = fec_io.FEC(CmdExecutionQueue,versionList)

# The Setup is a thread so the GUI does not wait until it finishes.
def SetupFEC(delay):
      global fec
      time.sleep(delay)
      fec.DoInitProc()
_thread.start_new_thread(SetupFEC,(2,))
      
      
#
def RunProcess(jobID, fecData,index):
    global CmdExecutionQueue, fec

    os.chdir(fecData.Path)
    numFrames = 8 * fecData.Timebins
    for gbtxNum in range(0,2):
        CmdExecutionQueue.append(["addlog", "JOB #{}: Processing Data GBTX{}".format(jobID,gbtxNum)])
        if gbtxNum==0:
            capture = fecData.Capture0
        else:
            capture = fecData.Capture1
        cmd = "decoder_gbtx -f {} -i run{}_{}.bin".format(numFrames,gbtxNum,index)
        cmd += " -o run{}_{} -s {} -l {}".format(gbtxNum,index,gbtxNum,fecData.Threshold)
        capture.append(cmd)
        code = fec_io.ExecuteCmdParse(cmd, add_cmd=1)
        for i in code:
            capture.append(i)


def RunReconCapture(jobID,fecData):
    aList = [5,10] #,15,20,25,30,35]
    numFrames = int(8 * fecData.Timebins + 300)
    for amplitude in aList:
        self.PulseInfo = waveGen.setupReconSignal(0, [127000, amplitude * 0.001, 3e-9, "on"])
        time.sleep(0.25)
        cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir {} --mask 0x1".format(numFrames, fecData.Path)
        code = fec_io.ExecuteCmdParse(cmd, add_cmd=1)
        for j in code:
            CaptureLog.append(i)
            if j.upper().find("FATAL") != -1:
                got_fatal = 1
                break
        try:
            os.rename("{}/run000000_trorc00_link00.bin".format(fecData.Path), "{}/run0_{}.bin".format(fecData.Path, amplitude))
            os.rename("{}/run000000_trorc00_link01.bin".format(fecData.Path), "{}/run1_{}.bin".format(fecData.Path, amplitude))
        except:
            pass

    for amplitude in aList:
        for gbtxNum in [0]:
            cmd = "decoder_gbtx -f {} -i run{}_{}.bin".format(numFrames,gbtxNum,amplitude)
            cmd += " -o run{}_{} -s {} -p".format(gbtxNum,amplitude,gbtxNum)
            fec_io.ExecuteCmdParse(cmd, add_cmd=1)
            fec_io.ExecuteCmdParse("grep TPC0L run{}_{}", add_cmd=1)



def RunCapture(jobID,fecData):
      global CmdExecutionQueue, fec

      os.chdir(fecData.Path)
      CaptureLog = []
      numFrames = int(8*fecData.Timebins+300)
      cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir {} --mask 0x1".format(numFrames,fecData.Path)
      tm1 = time.time()
      got_fatal = 0
      for i in range(0,fecData.NumIters):
#          sys.stdout.write("cmd={}\n".format(cmd))
          code = fec_io.ExecuteCmdParse(cmd,add_cmd=1)
          for j in code:
              CaptureLog.append(i)
              if j.upper().find("FATAL")!=-1:
                 got_fatal = 1
                 break
          CmdExecutionQueue.append(["addlog","JOB #{}: Iteration {}/{}".format(jobID,i+1,fecData.NumIters)])

          try:
              os.rename("{}/run000000_trorc00_link00.bin".format(fecData.Path),"{}/run0_{}.bin".format(fecData.Path,i))
              os.rename("{}/run000000_trorc00_link01.bin".format(fecData.Path),"{}/run1_{}.bin".format(fecData.Path,i))
          except:
              pass

      tm2 = time.time()
      CmdExecutionQueue.append(["setcapture",0])
      CaptureLog = []
      CaptureLog.append("Output directory = {}\n".format(fecData.Path))
      CaptureLog.append("")
      if got_fatal==1:
         CmdExecutionQueue.append(["addlog","JOB #{}: Detected FATAL error during TREADOUT. LOG:".format(jobID),"#ff0000"])
         for i in code:
             CmdExecutionQueue.append(["addlog",i])
      index = 0
      MAX_THREADS = 5
      for index in range(0,fecData.NumIters):
          # myThreads = []
          # itr_stop = itr_start + MAX_THREADS
          # if itr_stop > fecData.NumIters:
          #    itr_stop = fecData.NumIters
          # for index in range(itr_start,itr_stop):
          #     CaptureLog.append("Decoding data @ {}".format(index))
          #     RunProcess(jobID,fecData,index)
          #     PT = threading.Thread(target=RunProcess, args=(jobID,fecData,index))
          #     PT.start()
          #     myThreads.append(PT)
          #
          # done  = 0
          # while done == 0:
          #     done = 1
          #     for i in myThreads:
          #         if i.isAlive():
          #            done = 0
          # itr_start += MAX_THREADS
          CaptureLog.append("Decoding data @ {}".format(index+1))
          RunProcess(jobID, fecData, index)
      tm3 = time.time()

      for i in fecData.Capture0:
          CaptureLog.append(i)
      CaptureLog.append("")
      for i in fecData.Capture1:
          CaptureLog.append(i)
      CaptureLog.append("")
      fecData.ReadAll()
      fecData.setupColorLevels()
      fecData.MakePDF("{}.pdf".format(fecData.Name))
      tm4 = time.time()
      timeinfo = "JobID#{} Time (seconds): Capture={:.3f} Processing={:.3f} Generate={:.3f} Total={:.3f} ::Frames={}".format(jobID,tm2-tm1,tm3-tm2,tm4-tm3,tm4-tm1,numFrames)
      sys.stdout.write("{}\n".format(timeinfo))
      CaptureLog.append(timeinfo)
      CmdExecutionQueue.append(["addlog","JOB #{}: Creating {}.pdf".format(jobID,fecData.Name)])
      CmdExecutionQueue.append(["addlog",timeinfo,"#ffffff"])
      CmdExecutionQueue.append(["setcapture",-1])

      LogFile = open("run.log",'w')
      for i in CaptureLog:
          LogFile.write("{}\n".format(i))
      LogFile.close()
      CmdExecutionQueue.append(["addtoviewer",fecData])


def ExecCmdProc(cmdDef):
    global windataviewer

    cmd = cmdDef[0]     
    if cmd=="clearlog":
       if winmain!=0:
          winmain.Log.delete()
    elif cmd=="addtoviewer":
       pass
#      if winmain.fecData != cmdDef[1]:
#         winmain.fecData.HistoMerge(cmdDef[1])
    elif cmd=="setcapture":
      if winmain.CaptureMultiInfo[0]=="sweep":
          if cmdDef[1] == -1:
             winmain.CaptureMultiInfo[1]+=1
             winmain.nextCaptureSweep()
             winmain.capture_active = -1
      else:
          if cmdDef[1] == -1:
             winmain.capture_active = -1
          elif cmdDef[1] == 0:
             winmain.capture_active = 0
             winmain.CaptureStatus.setVal("CAPTURE::DONE")
             winmain.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"))
             winmain.RunEvents.button.config(bg="#00cf00",fg="#ffffff",state=NORMAL)
    elif cmd=="initdone":
        winmain.CaptureStatus.setVal("IDLE")
        winmain.CaptureStatus.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"))
    elif cmd=="addlog":
      winmain.Log.addLog(cmdDef[1])
      if len(cmdDef) > 2:
         winmain.Log.setColor(cmdDef[2])


    elif cmd=="setupsync":
       fec.SetupSYNC()
       winmain.defineOptions()
#    elif cmd=="capturedata":
#       DoCapture(0)
#    elif cmd=="capturedataanalyze":
#       DoCapture(1)
    elif cmd=="setuptpc":
       fec.SetupTPC()
    elif cmd=="setuptpc100":
       for i in range(0,100):
           sys.stdout.write("Doing TPC {}/100\n".format(i))
           fec.SetupTPC()
#    elif cmd=="trgcapture":
#       fec.SetupPedestal(1)
#       FileReference = "fec_CPD_{}_{}_{}".format(fec.FreqMode,fec.setupMode,time.strftime('%Y%b%d_%H%M%S'))
#       fec.capturePedestalData(FileReference,10)
#       time.sleep(1)
#       fec.SAMPA_set_ctrl(0x1f,0,0)
#       FileReference = "fec_{}_{}_{}".format(fec.FreqMode,fec.setupMode,time.strftime('%Y%b%d_%I%M%S'))
#       time.sleep(1)
#       fec.captureData(FileReference)
#       fec.captureData(FileReference)
#       fec.SAMPA_set_ctrl(0x0,0,0)
#       fec.analyzeData(FileReference)

    elif cmd=="setuppedestalcapture":
       fec.SetupPedestal(1)
       winmain.defineOptions()
    elif cmd=="setuppedestalread":
       fec.SetupPedestal(1)
       winmain.defineOptions()
    elif cmd=="setpwr_pat1":
       fec.pedestalWriteMode = "PAT1"
    elif cmd=="sampasondirect":
       fec.TurnSampasOnDirect()
    elif cmd=="sampasonsequenced":
       fec.TurnSampasOnSequenced()
    elif cmd=="sampasoff":
       fec.TurnSampasOff()
    elif cmd=="setpwr_ramp":
       fec.pedestalWriteMode = "RAMP"
    elif cmd=="pedestalmemorywrite":
       fec.SAMPA_pedestal_memory_write()
    elif cmd=="pedestalmemoryverify":
       fec.SAMPA_pedestal_memory_verify()
# --------------------------------------------------
    elif cmd=="programgbtx1":
       fec.programGBTx1(Cnfg1.CONFIG_REGISTER_DEFINITIONS)
    elif cmd=="gengbtx0cnfg":
       gbtxcnfg.WriteRegDefConfigFile("fec-gbtx0-cnfg.txt",Cnfg0.CONFIG_REGISTER_DEFINITIONS)
    elif cmd=="gengbtx0acnfg":
       gbtxcnfg.WriteRegDefConfigFile("fec-gbtx0a-cnfg.txt",Cnfg0a.CONFIG_REGISTER_DEFINITIONS)
    elif cmd=="gengbtx1cnfg":
       gbtxcnfg.WriteRegDefConfigFile("fec-gbtx1-cnfg.txt",Cnfg1.CONFIG_REGISTER_DEFINITIONS)
# --------------------------------------------------
    elif cmd=="seriallinkread":
       fec.SAMPA_seriallink_check()
    elif cmd=="checkactivelinks":
       mList = fec.SAMPA_check_active_seriallinks()
       msg = "#activeElinks={:>2d} SyncCnt={}".format(mList[0],fec.syncCnt)
       if mList[0] > 0:
          msg += " ["
          for i in mList[1]:
             msg += " {}".format(i)
          msg += " ]"
       fec.addLog(msg)
    elif cmd=="adctest":
       fec.ADC_Test()
        
def TimerProc():
   global do_exit

   while len(CmdExecutionQueue) > 0:
      m = CmdExecutionQueue.pop(0)
      ExecCmdProc(m)
   if do_exit==1:
      sys.stdout.write("Exiting\n")
      win._root.quit()
      exit()
      return
   win._root.after(1,TimerProc)

win._root.after(20,TimerProc)
win._root.mainloop()
