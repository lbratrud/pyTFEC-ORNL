#!/usr/bin/env python3

# captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
import pwrtest

import projpath as mpath
mpath.projName = "TPC-FEC"

import projtime as mtime

version = "1.2 1/05/2018"

from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_DATA as fecDATA

homepath = mpath.getPath("data")
tDef = time.strftime('%Y%b%d_%H%M%S')
fName = "summary_{}.log".format(tDef)
pfName = "summary_power_{}.log".format(tDef)
of = open(fName,"w")
pof = open(pfName,"w")
sys.stdout.write("Creating {}\n".format(fName))
sys.stdout.write("Creating {}\n".format(pfName))

sName = os.path.join(homepath, "fid")
fList = glob.glob("{}*".format(sName))
fDList = []
for i in fList:
   d1 = fecDATA.FEC_data()
   fDList.append(d1)
   fecname = fecDATA.readSummary(d1,i)
   pwdRef = i.replace("/home/daquser/TPC-FEC/data/fid", "")
   words = pwdRef.split("/")
   try:
      d1.ScaID = int(words[0],16)
   except:
      pass
   bID = "{:0>3}".format(hex(d1.BoardID)[2:])
   sID = "{:0>6}".format(hex(d1.ScaID)[2:])
   filename = "sum_fec{}_{}_{}.pdf".format(bID,sID,tDef)
   d1.MakePDF(filename)
   of.write(d1.makeSummaryMinMax())
   pof.write(d1.makePowerSummary())

of.close()
pof.close()

fecDATA.MultiFetHistogram(fDList)
exit()
