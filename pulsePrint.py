#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
version = "2.0 9/28/2017"

sys.stdout.write("\npulsePrint version {}\n\n".format(version))
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import pulseReConstruct
import projmkrpt as mkrpt
import FEC_IO as fec_io

FREQINPUT = 33300
resolution=16
i = 0
while  i < len(sys.argv):
   cmd = sys.argv[i].lower()
   if cmd == "-freq":
	   FREQINPUT = int(sys.argv[i + 1])
	   i += 1
   elif cmd == "-freqk":
	   FREQINPUT = int(1000 * float(sys.argv[i + 1]))
	   i += 1
   elif cmd == "-res":
	   resolution = int(sys.argv[i + 1])
	   i += 1
   i+=1

pulseReConstruct.DoSetup(resolution,setFREQINPUT=FREQINPUT)

def getFiles():
	return glob.glob("run0_*GBTX0.dat")


path = os.getcwd()
sys.stdout.write("INPUTFREQ={} path={}\n".format(pulseReConstruct.FREQINPUT,path))
files = getFiles()
#
if len(files)==0:
	for gbtxNum in [0,1]:
		files = glob.glob("run{}_*.bin".format(gbtxNum))
		if len(files) > 0:
			for i in files:
				cmd = "decoder_gbtx -f {} -i {}".format(pulseReConstruct.numFrames,i)
				nData = i.replace(".bin","")
				cmd += " -o {} -s {} -p".format(nData,gbtxNum)
				sys.stdout.write("Decoding GBTx file {}\n".format(nData))
				fec_io.ExecuteCmdParse(cmd)
	files = getFiles()

#pdfgen = mkrpt.makeReport("analyze2.pdf", "Optimization")
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]

DataList = []
for f in files:
	fileDef0 = f.replace("_GBTX0.dat", "")
	fileDef = fileDef0.replace("run0_", "")
#	fileDef = fileDef.replace("_GBTX1.dat", "")#
	#fileDef = fileDef.replace("run0_", "")
	#fileDef = fileDef.replace("run1_", "")
	data = pulseReConstruct.LoadPulseData(fileDef)
	sys.stdout.write("Reading {}\n".format(fileDef))
	data.loadFile(f,maxitr=200000)
	f1 = "run1_{}_GBTX1.dat".format(fileDef)
	data.loadFile(f1,maxitr=200000)
	data.construct()
	of = open("{}_PD{}.dat".format(fileDef,pulseReConstruct.BINSPERSAMPLEFREQ),'w')
	of.write("# Directory {}\n".format(path))
	data.Print(of)
	of.close()
	data.infofind()
	data.removeBaseLine()
	DataList.append(data)
	# of = open("{}_PDBLC{}.dat".format(fileDef,pulseReConstruct.BINSPERSAMPLEFREQ),'w')
	# of.write("# Directory {}\n".format(path))
	# data.Print(of)
	# of.close()
MaxList = []
for index in range(0, 160):
	myMax = []
	for dl in DataList:
		got_data = 0
		ymax = 0
		for dt in dl.ReconList[index]:
			if dt[1] > ymax:
			   ymax = dt[1]
		if ymax > 1:
		   myMax.append(ymax)
	if len(myMax) > 0:
		MaxList.append(myMax)

index = 0
of = open("channelPeak{}.txt".format(pulseReConstruct.BINSPERSAMPLEFREQ),'w')
for dl in DataList:
    of.write("{} ".format(dl.FileDef))
    for i in MaxList:
        of.write("{:.1f} ".format(i[index]))
    of.write("\n")
    index += 1
of.close()
exit()
