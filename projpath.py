# projpath.py
#
# Define all paths for the system and operations.
#
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@yahoo.com)
# Project started December 2013 for Gary W. Turner
# for implementing CTC system based on C/MRI hardware platform

import projbase as base
import sys
import time
import sys
import os
import errwarn

cwd = os.getcwd()
if os.name == "posix":
   savepath = os.path.expanduser("~")
#   savepath = os.path.join(savepath,"TPC-FEC")
#   savepath = "/home/ubuntu/pyApps"
else:
   savepath = "C:\TPC-FEC"

projName = "myProj"

def makePath(path,name):
  configpath = os.path.join(path,name)
  if not os.path.exists(configpath): os.makedirs(configpath)
  return configpath

def getPath(mode):
  global savepath,cwd,projName

  if mode=="backup":
    config = makePath(savepath,projName)
    return makePath(config,"Backup")

  elif mode in ["report","data","tests"]:
    config = makePath(savepath,projName)
    return makePath(config,mode)
  else:
    config = makePath(savepath,projName)
    return makePath(config,mode)

def getFileName(pathmode,fileRef,mytype):
  return getFileNameExt(pathmode,fileRef,mytype,"txt")

def getFileNameExt(pathmode,fileRef,mytype,ext):
  path = getPath(pathmode)
  fileType = "{}_{}.{}".format(fileRef,mytype,ext)
  return os.path.join(path,fileType)
  
def openFile(pathmode,fileRef,mytype,mode):
  path = getPath(pathmode)
  fileType = "{}_{}.txt".format(fileRef,mytype)
  filename = os.path.join(path,fileType)
  filedef =  "{}::{}".format(fileRef,mytype).upper()
  if mode=='w':
     f = open(filename,mode)
     errwarn.ErrWarn.MakeLog("Saving {}::{}\n".format(pathmode.upper(),filedef))
     f.write("# Date = {}\n".format(time.strftime('%Y,%b,%d %a %I:%M:%S %p')))
     f.write("# Path = {}\n".format(path))
     f.write("# File = {}\n".format(filename))
     f.write("# FileType = {}\n".format(mytype))
     f.write("# Version = {}\n\n".format(base.Version))
  else:
     if os.path.isfile(filename)==False:
        return -1
     f = open(filename,mode)
     errwarn.ErrWarn.MakeLog("Reading {}\n".format(filedef))
  return f
