#!/usr/bin/env python3

# captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
import subprocess
def ExecuteCmd(cmd):
    p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
    p.communicate()

def CopyFiles(fList,outpath):
   mlen = len(fList)
   if mlen != 7:
      sys.stdout.write("#files={}\n".format(len(fList)))
   for j in fList:
      #sys.stdout.write("cp {} {}\n".format(j,outpath))	
      ExecuteCmd("cp {} {}".format(j,outpath))	

fList = glob.glob("/data/TPC-FEC/fid*")
for i in fList:
   os.chdir(i+"/Summary")
   fecname = i.replace("/data/TPC-FEC/","")
   sys.stdout.write("Working on {} {}\n".format(i,fecname))
   outpath = "/home/daquser/TPC-FEC/data/{}".format(fecname)
   if not os.path.exists(outpath): os.makedirs(outpath)
   CopyFiles(glob.glob("*.txt"),outpath)
   os.chdir(outpath)
   ExecuteCmd("makeReport")
exit()
