# pulseReConstruct.py
#
# Python main file for FEC pulse reconstruction. The pulse must at the given FREQINPUT (in hz)
#
# ORNL - 11/1/2017
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import math
import projmkrpt as mkrpt
import os
from scipy import stats

def getFiles():
	global resolution
	return glob.glob("amp_*PD{}.dat".format(resolution))

resolution = 16
ctbase = 0
i = 0
FREQINPUT = pulseReConstruct.FREQINPUT
outputName = "pulsePlot"
xminDef = xmaxDef = "None"
pChannels = range(0, 160)
while  i < len(sys.argv):
   cmd = sys.argv[i].lower()
   if cmd == "-freq":
	   FREQINPUT = int(sys.argv[i + 1])
	   i += 1
   elif cmd == "-freqk":
	   FREQINPUT = int(1000 * float(sys.argv[i + 1]))
	   i += 1
   elif cmd == "-res":
	   resolution = int(sys.argv[i + 1])
	   i += 1
   elif cmd == "-channel":
	   pChannels = [int(sys.argv[i + 1])]
	   i += 1
   elif cmd == "-ctbase":
	   ctbase = 1
   elif cmd == "-xmin":
       xminDef = float(sys.argv[i + 1])
       i += 1
   elif cmd == "-xmax":
	   xmaxDef = float(sys.argv[i + 1])
	   i += 1
   elif cmd == "-o":
	   outputName = sys.argv[i + 1].replace(".pdf","")
	   i += 1
   i+=1

pulseReConstruct.DoSetup(resolution,setFREQINPUT=FREQINPUT)
if xminDef=="None":
   xminDef = 0
else:
   xminDef = int(xminDef*pulseReConstruct.USTOPNTS)

if xmaxDef == "None":
   xmaxDef = pulseReConstruct.num_bins
else:
   xmaxDef = int(xmaxDef*pulseReConstruct.USTOPNTS)

#sys.stdout.write("xmin={} xmax={}\n".format(xminDef, xmaxDef))

files = getFiles()
DataList = []
for f in files:
	data = pulseReConstruct.LoadPulseData(f)
	sys.stdout.write("Reading {}\n".format(f))
	data.loadConstructedData(f)
	data.pulseAlign()
	data.infofind()
	if ctbase==1:
	   data.removeBaseLine()
	DataList.append(data)

fin = open("channelPeak.txt",'r')
DataList = []
sizeList = []
chanList = []
for i in range(0,160):
   chanList.append([])
ampmin = 50000
ampmax = 0
for line in fin:
    words = line.upper().split()
    name = words[0].split("_")
    myData = []
    amp = int(name[1].replace("MVPP",""))
    sizeList.append(amp)
    myData.append(amp)
    if amp < ampmin:
       ampmin = amp
    if amp > ampmax:
       ampmax = amp
    mChanList = []
    for i in range(1,len(words)):
       val = float(words[i])
       myData.append(val/amp)
       if amp < 55:
          chanList[i-1].append([amp*2,val*2.2/1.024])
    DataList.append(myData)
    chanList.append(mChanList)
fin.close()

nDataList = []
for i in sizeList:
   for j in DataList:
      if j[0]==i:
         nDataList.append(j)

statList = []
AvgData = []
RmsData = []
mlen = len(nDataList[0])-1
sys.stdout.write("mlen={}\n".format(mlen))
for i in range(0,mlen):
   sys.stdout.write("Channel {}".format(i))
   avg = 0
   sumsq = 0
   cnt = 0
   for j in nDataList:
      avg += j[i+1]
      sumsq += j[i + 1]*j[i+1]
      cnt += 1
   avg /= cnt
   rms = math.sqrt(sumsq / cnt -avg*avg)
   sys.stdout.write(" Avg={:.2f} Rms={:.2f}".format(avg,rms))
   for j in nDataList:
      sys.stdout.write(" {:.2f}".format(j[i+1]))
   sys.stdout.write("\n")
   if avg > 5:
      statList.append([avg,rms])
      AvgData.append([i,avg])
      RmsData.append([i,rms])

pwd = os.getcwd()
sys.stdout.write("pwd={}\n".format(pwd))
pdfgen = mkrpt.makeReport("channelGain.pdf", "Channel Gain (ADC value / mVpp) Channels")
pdfgen.title2 = "{}/channelGain.pdf".format(pwd)
pdfgen.addHeader()
pdfgen.add_header = 1
pdfgen.joinedLines = 1
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]

xpnts = 10
lineList = []
cindex = 0
for i in DataList:
   lineList.append(["{}mVpp".format(i[0]), ColorList[cindex], mkrpt.makeMarker("Circle")])
   cindex += 1
   if cindex >= len(ColorList):
      cindex = 0
#sys.stdout.write("chanList={}\n".format(chanList))

lineList = []
lineList.append([os.getcwd(), ColorList[0], mkrpt.makeMarker("Circle")])
for i in range(0,mlen):
   lineList = []
   lineList.append(["Raw", mkrpt.colors.blue, mkrpt.makeMarker("Circle"),3,(0,60)])
   lineList.append(["Fitted", mkrpt.colors.red, None])
   x = []
   y = []
   for j in chanList[i]:
      x.append(j[0])
      y.append(j[1])
   slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
   f = []
   for j in x:
      f.append([j,slope*j+intercept])

#   f = numpy.polyval(pF,x)
   pData = [chanList[i],f]
#   sys.stdout.write("pf{}={}\n".format(i,pF))
   color = mkrpt.colors.black
   if intercept < 0:
      fdesc = "y={:.3f} x{:.3f}".format(slope,intercept)
   else:
      fdesc = "y={:.3f} x+{:.3f}".format(slope, intercept)
   plotsize = 400

   pdfgen.addBasicMultiPlot(120, 2100,pData, "ADC Converted Voltage (mV) vs Input Charge (fC)\nChannel {}".format(i), lineList, xpnts = 12, plotsize = plotsize,ymin=100,xmin=0)

   y = pdfgen.y + 0.75*plotsize
   fsize = 10
   pdfgen.addText(3*mkrpt.margin, y + 30+3,fdesc, fsize+2, [color.red, color.green, color.blue])
   pdfgen.addText(3*mkrpt.margin, y + 20,"Rv={:.6f}".format(r_value), fsize, [color.red, color.green, color.blue])
   pdfgen.addText(3*mkrpt.margin, y + 10,"Pv={:.5e}".format(p_value), fsize, [color.red, color.green, color.blue])
   pdfgen.addText(3*mkrpt.margin, y,"StdErr={:.5f}".format(std_err), fsize, [color.red, color.green, color.blue])

   color = mkrpt.colors.blue
   pdfgen.addText(3*mkrpt.margin, y - 10,"data = blue", fsize, [color.red, color.green, color.blue])
   color = mkrpt.colors.red
   pdfgen.addText(3*mkrpt.margin, y - 20,"linear fit = red", fsize, [color.red, color.green, color.blue])


def getMinMax(mList):
   mmin = 50000
   mmax = 0
   for i in mList:
      if i[1] < mmin:
         mmin = i[1]
      if i[1] > mmax:
         mmax = i[1]
   return [0.01*int(mmin*95),0.01*int(mmax*105)]


AvgMM = getMinMax(AvgData)
RmsMM = getMinMax(RmsData)
sys.stdout.write("AvgMM={}\n".format(AvgMM))
sys.stdout.write("RmsMM={}\n".format(RmsMM))
pdfgen.addBasicMultiPlot(160, AvgMM[1], [AvgData], "Average ADC value / amplitude mVpp", lineList, xpnts = xpnts, plotsize = 300,ymin=AvgMM[0])
pdfgen.addBasicMultiPlot(160, RmsMM[1], [RmsData], "RMS ADC value / amplitude mVpp", lineList, xpnts = xpnts, plotsize = 300,ymin=RmsMM[0])
pdfgen.DoExit()

exit()
