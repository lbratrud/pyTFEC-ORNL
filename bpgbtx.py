#!/usr/bin/env python3

#-------------------------------------------------------------------------------
#
# Bus Pirate for programming of GBTx
# Copyright 2017 Stefan Kirsch <kirsch@fias.uni-frankfurt.de>
#
# For Bus Pirate binary mode documentation:
#   http://dangerousprototypes.com/docs/I2C_(binary)
#
#
#
# The low-level Bus Pirate access draws heavy inspiration from the work
#  related to pyBusPirate by Sean Nelson <audiohacked@gmail.com>
#  (see https://github.com/mikebdp2/Bus_Pirate/tree/master/scripts/pyBusPirateLite/pyBusPirateLite)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
#-------------------------------------------------------------------------------

import sys
import select
import serial
import sys
import time


#-------------------------------------------------------------------------------
class BpBitbang:

  #--
  def __init__(self, device = '/dev/ttyUSB0', baud = 115200, timeout = 1, debug = 0):
    self.port = serial.Serial(device, baud, timeout = timeout)
    self.debug = debug

  #--
  class Pin:
    # Configure direction
    #   010xxxxx - Configure pins as input(1) or output(0):
    #     010|AUX|MOSI|CLK|MISO|CS
    #
    # Enable/disable:
    #   1xxxxxxx - Set on (1) or off (0):
    #     1|POWER|PULLUP|AUX|MOSI|CLK|MISO|CS
    CS     = 0
    MISO   = 1
    CLK    = 2
    MOSI   = 3
    AUX    = 4
    PULLUP = 5
    POWER  = 6

  #--
  def modeBitbang(self):
    # Sometimes sending 20 zeroes in a row to a BP already in binary mode
    # leads to BP getting stuck in BitBang mode
    # (see forum: http://dangerousprototypes.com/forum/viewtopic.php?t=1440#msg13179 )
    # so if we detect a response (indicating we're in BitBang mode before the 20 zeroes)
    # stop sending zeroes and go on.
    self.port.flushInput()
    for i in range(20):
      self.port.write(b'\x00')
      r, w, e = select.select([self.port], [], [], 0.01)
      if (r):
        break
    return 1 if self.response(5)[0:4] == b'BBIO' else 0

  #--
  def modeBpI2c(self):
    self.port.write(b'\x02')
    return 1 if self.response(4)[0:3] == b'I2C' else 0

  #--
  def reset(self):
    self.port.write(b'\x00')
    time.sleep(0.1)

  #--
  def resetBusPirate(self):
    self.reset()
    self.port.write(b'\x0f')
    self.port.flushInput()

  #--
  def response(self, byte_count = 1, timeout = 0.1, errOnTimout = True):
    startTime = time.time()

    # Wait until we have the desired number of bytes in the receive buffer
    while (self.port.inWaiting() < byte_count) and (time.time() < (startTime + timeout)):
      time.sleep(0.001)

    if time.time() > (startTime + timeout) and errOnTimout:
      raise IOError("Response timed out")

    r = self.port.read(byte_count)
    
    if (self.debug >= 5):
      print(" ***** response : ", r)

    return r



#-------------------------------------------------------------------------------
class BpI2c(BpBitbang):

  #--
  def __init__(self, device = '/dev/ttyUSB0', baud = 115200, timeout = 1, debug = 0):
    super().__init__(device, baud, timeout, debug)

  #--
  class Frequency:
    f400kHz = 0x03
    f100kHz = 0x02
    f50kHz  = 0x01
    f5kHz   = 0x00

  #--
  def sendStartBit(self):
    self.port.write(b'\x02')
    self.response()

  #--
  def sendStopBit(self):
    self.port.write(b'\x03')
    self.response()

  #--
  def readByte(self):
    self.port.write(b'\x04')
    return int.from_bytes(self.response(1), byteorder = sys.byteorder)

  #--
  def sendAck(self):
    self.port.write(b'\x06')
    self.response()

  #--
  def sendNack(self):
    self.port.write(b'\x07')
    self.response()

  #--
  def setSpeed(self, speed = 0):
    self.port.write(int(0x60 | speed).to_bytes(1, byteorder = sys.byteorder))
    self.response()

  #--
  def getSpeed(self):
    self.port.write(b'\x70')
    return int.from_bytes(self.response(1), byteorder = sys.byteorder)

  #--
  def bulkWrite(self, data):
    """
      Expects data to write as integer (byte) or list of integers (bytes)
      Returned are the BP reply (0x1), followed by the ACKs/NACKs for the written bytes (as bytearray)

      From Bus Pirate documentation:
        0001xxxx – Bulk I2C write, send 1-16 bytes (0=1byte!)

        BP replies 0×01 to the bulk I2C command. After each data byte
        the Bus Pirate returns the ACK (0x00) or NACK (0x01) bit from the slave device.

    """
    if type(data) == int:
      byteList = [0] * data
      byteCount = data
    elif type(data) == list:
      byteList = data
      byteCount = len(data)
    else:
      raise ValueError("Invalid data type")

    self.port.write(int(0x10 | (byteCount - 1)).to_bytes(1, byteorder = sys.byteorder))
    
    for i in range(byteCount):
      self.port.write(int(byteList[i]).to_bytes(1, byteorder = sys.byteorder))
    
    rx_data = list(map(lambda x: x, self.response(byteCount + 1)))

    if (self.debug >= 4):
      print(' **** bulkWrite : [{}] -> [{}]'.format(', '.join(hex(x) for x in byteList),
                   ', '.join(hex(x) for x in rx_data)))
    return rx_data

  #--
  class XAuxState:
    low     = 0x0
    high    = 0x1
    hiz     = 0x2
  class XAuxCmd:
    read    = 0x3
    use_aux = 0x10
    use_cs  = 0x20

  #--
  def setAux(self, aux_state):
    """
      Set AUX port to state (expects XAuxState as argument)
    """
    self.port.write(b'\x09')
    self.port.write(int(self.XAuxCmd.use_aux).to_bytes(1, byteorder = sys.byteorder))
    self.response()
    self.port.write(b'\x09')
    self.port.write(int(aux_state).to_bytes(1, byteorder = sys.byteorder))
    self.response()

  #--
  def getAux(self):
    """
      Get current AUX port state (as integer)
    """
    self.port.write(b'\x09')
    self.port.write(int(self.XAuxCmd.use_aux).to_bytes(1, byteorder = sys.byteorder))
    self.response()
    self.port.write(b'\x09')
    self.port.write(int(self.XAuxCmd.read).to_bytes(1, byteorder = sys.byteorder))
    return int.from_bytes(self.response(1), byteorder = sys.byteorder)

  #--
  # Found here
  #   https://wherelabs.wordpress.com/2009/10/14/bus-pirate-binary-i2c-mode/
  # but not here
  #   http://dangerousprototypes.com/docs/I2C_(binary)
  # Not sure if this is properly implemented
  def readPins(self):
    self.port.write(b'\x50')
    return int.from_bytes(self.response(1), byteorder = sys.byteorder)


#-------------------------------------------------------------------------------
class BpI2cGbtx(BpI2c):
  """
    GBTx-related I2c communication with Bus Pirate
  """

  nGbtxRegisters = 366

  #--
  def __init__(self, i2c_addr, device = '/dev/ttyUSB0', baud = 115200, timeout = 1, debug = 0):
    super().__init__(device, baud, timeout, debug)
    self.i2c_addr = i2c_addr

  #--
  def readRegister(self, reg_addr):
    """
      Read single GBTx register

      From GBTx manual:
        Read from Register
          1. Master transmits START command
          2. Master transmits 7-bit GBTX address followed by the 8th bit (R/W_) set to zero
          3. Master transmits bits[7:0] of the register address.
          4. Master transmits bits[15:8] of the register address.
          5. Master transmits repeated START command
          6. Master transmits 7-bit GBTX address followed by the 8th bit (R/W_) set to one
          7. Slave transmits 8-bit register dataword (can be repeated).
          8. Master transmits STOP command.
    """
    self.sendStartBit() # Step 1
    stat = self.bulkWrite([self.i2c_addr << 1, reg_addr & 0xff, (reg_addr >> 8) & 0xff])[1:] # 2, 3, 4
    self.sendStartBit() # 5
    stat += self.bulkWrite([self.i2c_addr << 1 | 1])[1:] # 6
    r = self.readByte()  # 7
    self.sendNack()
    self.sendStopBit()  # 8

    # stat should only contain the ACK from the slave and be all zero
    for s in stat:
      if s == 0x1:
        raise IOError("I2C command on address 0x%02x not acknowledged" % (self.i2c_addr))
    return r

  #--
  def writeRegister(self, reg_addr, value):
    """
      From GBTX manual:
        Write to Register
        1. Master transmits START command
        2. Master transmits the 7-bit GBTX address followed by the 8th bit (R/W_) set to zero.
        3. Master transmits bits[7:0] of the register address.
        4. Master transmits bits[15:8] of the register address.
        5. Master transmits 8-bit register data word (can be repeated).
        6. Master transmits STOP command.
    """
    self.sendStartBit() # Step 1
    stat = self.bulkWrite([self.i2c_addr << 1, reg_addr & 0xff, (reg_addr >> 8) & 0xff, value & 0xff])[1:] # Steps 2-5
    self.sendStopBit()  # Step 6

    for s in stat:
      if s == 0x1:
        raise IOError("I2C command on address 0x%02x not acknowledged" % (self.i2c_addr))

  #--
  def dumpConfig(self, filename = None):
    """
      Dump GBTx config to file, or stdout if filename = None
    """
    gs = str()
    gs += '### GBTx at 0x{:x} [{:s}]\n'.format(self.i2c_addr, time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime()))
    gs += '###  Format: reg_addr (dec)   reg_value (hex)\n'

    try:
      for i in range(0, self.nGbtxRegisters):
        v = i2c.readRegister(i)
        gs += '{:3d} 0x{:02x}\n'.format(i, v)

      if filename:
          with open(filename, 'w') as f:
            f.write(gs)
      else:
        print(gs)
    except EnvironmentError as e:
      print('ERROR: While dumping GBTx config: {:s}'.format(str(e)))

  #--
  def writeConfig(self, filename, simulate = False):
    """
      Write configuration from file to GBTx
    """
    FF_GBT_PROGRAMMER = 0x1
    FF_OUR            = 0x2

    file_format = FF_GBT_PROGRAMMER
    reg_addr = 0x0
    reg_value = 0x0

    try:
      line_no = int(-1)
      with open(filename, 'r') as f:
        for line in f:
          line_no += 1
          if line_no == 0 and line[0:11] == '### GBTx at':  # Our file format marker
            file_format = FF_OUR

          if (line_no == 0 and self.debug >= 2):
            print(' ** writeConfig : Detected file format {:s}'.format(
                'FF_GBT_PROGRAMMER' if file_format == FF_GBT_PROGRAMMER else 'FF_OUR'))

          if line[0] == '#':    # Our file format supports comments
            continue

          if file_format == FF_GBT_PROGRAMMER:
            reg_addr = line_no
            l = line[:-1].strip().split(' ');
            reg_value = int(line[:-1].split()[0], 16)

          if file_format == FF_OUR:
            l = line[:-1].strip().split(' ');
            reg_addr = int(l[0], 0)
            reg_value = int(l[1], 0)

          if (self.debug >= 3):
            print(' *** writeConfig : {:s}addr {:3d}, data 0x{:02x}'.format(
              "SIM : " if simulate else "", reg_addr, reg_value))

          if (not simulate):
            self.writeRegister(reg_addr, reg_value)

    except EnvironmentError as e:
      print('ERROR: While writing config : {:s}'.format(str(e)))

  #--
  def addressScan(self):
    """
      Do GBTx address scan
      Returns list of addresses with positive response
    """
    addr_bak = self.i2c_addr
    addr_found = []

    for i in range(1, 0x10):
      self.i2c_addr = i
      try:
        self.readRegister(62) # GBTx phase aligner track mode register, but everything should be fine
        addr_found.append(i)
      except IOError as e:
        pass

    # Restore 'our' i2c address
    self.i2c_addr = addr_bak
    return addr_found



#-------------------------------------------------------------------------------
if __name__ == "__main__":
  import argparse

  parser = argparse.ArgumentParser(description='GBTx I2c access with Bus Pirate',
                                   formatter_class = argparse.ArgumentDefaultsHelpFormatter)

  parser.add_argument('i2c_addr', type = lambda x: int(x, 0), nargs = '?',
                      help = 'GBTx I2c address', default = 0xf)

  parser.add_argument('--write-cfg', metavar = 'FILENAME', type = str, nargs = 1,
                      help = 'Write configuration to GBTx',
                      default = argparse.SUPPRESS)
  parser.add_argument('--dump-cfg', metavar = 'FILENAME', nargs = '?',
                      help = 'Dump GBTx configuration to file, if FILENAME given, or stdout',
                      default = argparse.SUPPRESS)

  parser.add_argument('--read', metavar = ('ADDR'),
                      nargs = 1, type = lambda x: int(x, 0),
                      help = 'Read single GBTx address',
                      default = argparse.SUPPRESS)
  parser.add_argument('--write', metavar = ('ADDR', 'VALUE'),
                      nargs = 2, type = lambda x: int(x, 0),
                      help = 'Write single GBTx address with value',
                      default = argparse.SUPPRESS)

  parser.add_argument('--powercycle', action = 'store_true',
                      help = 'Powercycle FEC (switches 1.5V regulator off/on via AUX pin)')
  parser.add_argument('--i2c-scan', action = 'store_true',
                      help = 'Do I2C address scan and exit')

  parser.add_argument('--no-bp-reset', action = 'store_true',
                      help = 'Omit Bus Pirate reset on exit')

  parser.add_argument('--dev', metavar = 'DEVICE', type = str, nargs = 1,
                      help = 'Bus Pirate device file', default = ['/dev/ttyUSB0'])
  parser.add_argument('--baud', metavar = 'BAUDRATE', type = int, nargs = 1,
                      help = 'Bus Pirate baud rate', default = [115200])
  parser.add_argument('--debug', '-d', action='count',
                      help = 'Increase debug level', default = 0)

  # Hidden options
  #  test (for debugging)
  parser.add_argument('--test', action = 'store_true', help = argparse.SUPPRESS)
  #  hard reset of Bus Pirate, which should then enter terminal mode, and exit
  parser.add_argument('--hard-reset', action = 'store_true', help = argparse.SUPPRESS)

  args = parser.parse_args()

  if (args.debug > 0):
    print(' *', args)

  # Recover from error condition
  #   -> hard reset bus pirate
  if args.hard_reset:
    try:
      bb = BpBitbang(args.dev[0], args.baud[0], debug = args.debug)
      bb.resetBusPirate()
      sys.exit(0)
    except serial.serialutil.SerialException as e:
      print("ERROR : ", e)
      sys.exit(-1)


  # Regular operation
  #   -> Bus pirate setup phase
  i2c = None
  try:
    i2c = BpI2cGbtx(args.i2c_addr, args.dev[0], args.baud[0], debug = args.debug)
  except serial.serialutil.SerialException as e:
    print("ERROR : ", e)
    sys.exit(-1)

  if (not i2c.modeBitbang()):
    print("ERROR : Unable to enter binary bitbang mode, check Bus Pirate status on terminal")
    sys.exit(-1)

  if (not i2c.modeBpI2c()):
    print("ERROR : Unable to enter binary I2c mode, check Bus Pirate status on terminal")
    sys.exit(-1)

  i2c.setSpeed(BpI2c.Frequency.f400kHz)

  # Test option
  if args.test:
    print("Pins = ", hex(i2c.readPins()))
    pass

  # Powercycle 1v5 on FEC
  if args.powercycle:
    print('Powering down 1v5')
    i2c.setAux(BpI2c.XAuxState.low)
    time.sleep(.5)
    print('Powering up 1v5')
    i2c.setAux(BpI2c.XAuxState.hiz)

  # Push config to GBTx
  if hasattr(args, 'write_cfg'):
    print('Writing configuration \'{:s}\' to GBTx at address 0x{:x}'.format(args.write_cfg[0], args.i2c_addr))
    i2c.writeConfig(args.write_cfg[0])

  # Dump GBTx config to file
  if hasattr(args, 'dump_cfg'):
    print('Dumping configuration of GBTx at address 0x{:x} to \'{:s}\''.format(args.i2c_addr,
              'STDOUT' if args.dump_cfg == None else args.dump_cfg))
    i2c.dumpConfig(args.dump_cfg)

  # Read register
  if hasattr(args, 'read'):
    print('0x{:02x}'.format(i2c.readRegister(args.read[0])))

  # Write register
  if hasattr(args, 'write'):
    i2c.writeRegister(args.write[0], args.write[1])

  # Address scan
  if args.i2c_scan:
    print('Scanning I2C addresses [0x1 to 0xf]')
    addr = i2c.addressScan()
    for i in addr:
      print(' Found device at I2C address 0x{:x}'.format(i))

  # Finalize
  if (not args.no_bp_reset):
    if args.debug > 0:
      print(' * Resetting Bus Pirate')
    i2c.resetBusPirate()
