#
import usbtmc
import sys
import math
import sys
import time
import string
import sys
import os
import array
import ctypes
import subprocess
import projpath as mpath
import errwarn as errwarn
import FEC_IO as fec_io
from projbase import *

def TimeDef(val):
    if val < 1e-6:
       return "{}ns".format(int(val*1e9))
    if val < 1e-3:
       return "{:.1f}us".format(val*1e6)
    return "{:.1f}ms".format(val*1e3)

class WaveFormChannel:
    def __init__(self, instr, ID):
       self.ID = ID
       self.Instr = instr
       self.Info = []
       self.mytype = "UNKNOWN"
       self.attr = []
       self.status = "UNKNOWN"

    def doc(self,ofile):
        for i in self.attr:
            ofile.write("{} = {}\n".format(i,getattr(self,i)))

    def On(self):
        self.Instr.write("OUTPut{} ON".format(self.ID))
        self.Instr.write("OUTPut{}:POLarity INVerted".format(self.ID))
#        sys.stdout.write("ON {}\n".format(self.ID))

    def Off(self):
        self.Instr.write("OUTPut{} OFF".format(self.ID))
#        sys.stdout.write("OFF {}\n".format(self.ID))

    def setPulse(self, freq, vin, trailing, width=-1,leading=10e-6,phase=0):
        period = 1 / freq
        self.freq = freq
        self.vdef = vin
        self.voff = 0
        self.leading = leading
        self.trailing = trailing
        if width==-1:
           self.width = (period - leading - trailing) / 2
        else:
           self.width = width
        self.phase = phase

        mList = []
        mList.append(["width","SOUR{}:FUNction:PULSe:WIDTh".format(self.ID)])
        mList.append(["trailing","SOUR{}:FUNction:PULSe:TRANsition:TRAiling".format(self.ID)])
        mList.append(["leading","SOUR{}:FUNction:PULSe:TRANsition:LEADing".format(self.ID)])
#        mList.append(["phase","SOUR{}:FUNction:PULSe:PHAse".format(self.ID)])
        self.Instr.write("SOUR{}:APPly:PULse {:.3e},{:.3e},{:.3e}".format(self.ID, self.freq, self.vdef, self.voff))
        for i in mList:
            cmd = "{} {:.6e}".format(i[1], getattr(self,i[0]))
            self.Instr.write(cmd)
#            sys.stdout.write("{}\n".format(cmd))

    def getPulse(self):
        code = "PULSE {} ".format(self.ID)
        if self.status!=0:
           code += "ON"
        else:
            code += "OFF"
        code += " FREQ={} VPP={}mV VOFF={}mV PHASE={}".format(self.freq, 1000*self.vdef, 1000*self.voff,self.phase)
#        code = "PULSE {} {} FREQ={} VPP={}mV VOFF={}mV".format(self.ID,SW[3].upper(),self.freq, 1000*self.vdef, 1000*self.voff)
        code += " LEAD={} TRAIL={} WIDTH={}".format(TimeDef(self.leading),TimeDef(self.trailing),TimeDef(self.width))
        return code

    def InqueryPulse(self,freq,vdef,voff):
#        sys.stdout.write("InqueryPulse {}\n".format(self.ID))
        self.mytype = "PULSE"
        self.attr = ["freq","vdef","voff","trailing","leading","width"]
        self.freq = freq
        self.vdef = vdef
        self.voff = voff
        mList = []
        mList.append(["width","SOUR{}:FUNction:PULSe:WIDTh".format(self.ID)])
        mList.append(["trailing","SOUR{}:FUNction:PULSe:TRANsition:TRAiling".format(self.ID)])
        mList.append(["leading","SOUR{}:FUNction:PULSe:TRANsition:LEADing".format(self.ID)])
        for i in mList:
            val = float(self.Instr.ask("{}?".format(i[1])))
            setattr(self,i[0],val)

    def Inquery(self):
#        sys.stdout.write("Inquery {}\n".format(self.ID))
        self.status = self.Instr.ask("OUTPut{}?".format(self.ID))
#        sys.stdout.write("Status {} = {}\n".format(self.ID,self.status))
        code = self.Instr.ask("SOUR{}:APPly?".format(self.ID))
#        sys.stdout.write("Def {} = {}\n".format(self.ID,code))
        words = code.translate(strmaketrans(",\"", "  ")).split()
        if words[0]=="PULS":
           self.InqueryPulse(words[1],words[2],words[3])
#        else:
#           sys.stdout.write("Unknown channel waveform {}\n".format(code))
#0957:5707
class Keysight33622A:
    def __init__(self, idA=0x957, idB=0x5707):
        self.valid = 0
        self.Error = 0
        try:
            self.Instr = usbtmc.Instrument(idA, idB)
            self.Description = self.Instr.ask("*IDN?")
        except Exception as e:
            self.Error = e
            return
        self.valid = 1

        #sys.stdout.write("Desciption={}\n".format(self.Description))
        self.Channel = [WaveFormChannel(self.Instr,1),WaveFormChannel(self.Instr,2)]
        for src in [0,1]:
            self.Channel[src].Inquery()
            self.Channel[src].Off()

    def doc(self,ofile):
        for src in [0,1]:
            self.Channel[src].doc(ofile)

    def setupSignal(self,channelNum, SW):
        try:
            channel = self.Channel[channelNum]
            if SW[3]=="off":
               channel.Off()
            else:
               channel.On()
            channel.setPulse(SW[0],SW[1],SW[2])
            return channel.getPulse()
        except Exception as e:
            self.valid = 0
            self.Error = e
            return "ERROR"

    def setupFullSignal(self,channelNum, SW,phase=0):
        try:
            channel = self.Channel[channelNum]
            width = -1
            if len(SW) > 5:
               width = SW[5]
               width = SW[5]
        #    self.PulseInfo = waveGen.setupFullSignal(0, [33300, amplitude * 0.001, 3e-9, 10e-6, "on", 20e-6])
            channel.setPulse(SW[0],SW[1],SW[3],leading=SW[2],width=width,phase=phase)
            if SW[4]=="off":
               channel.Off()
            else:
               channel.On()
            return channel.getPulse()
        except Exception as e:
            self.valid = 0
            self.Error = e
            return "ERROR"

    def setupPhasedSignal(self,channelNum,SW):
        try:
            channel = self.Channel[channelNum]
            if SW[3]=="off":
               channel.Off()
            else:
               channel.On()
            channel.setPulse(SW[0],SW[2],SW[1],leading=100e-9,phase=0)
            return channel.getPulse()
        except Exception as e:
            self.valid = 0
            self.Error = e
            return "ERROR"


def ListDevices():
    code = fec_io.ExecuteCmdParse("lsusb", add_cmd=0)
    for i in code:
        sys.stdout.write("{}\n".format(i))


#setupPhasedSignal([509000,0.01,3e-9,"on"])
#wavegen.doc(sys.stdout)
#wavegen.Channel[0].On()
# for i in range(2000,6000,1000):
#     wavegen.Channel[0].setPulse(i,0.040,40e-9)
#     time.sleep(2)
# wavegen.Channel[0].Off()
