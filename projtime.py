# mtcsdbb.py
#
# Define all ObjLists and how they relate to each other.
#
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@yahoo.com)
# Project started December 2013 for Gary W. Turner
# for implementing CTC system based on C/MRI hardware platform

from projbase import * 
import sys
import time

prototime = 0

# --------------------------------------------------------------------------------
# Time define routines
# --------------------------------------------------------------------------------
def String2Time(value,ctype):
    global prototime
    nval = value.translate(strmaketrans(":/", "  "))
    tstruct = nval.split()

    if len(tstruct)==6:
       if ctype=="datetime":
         m = time.strptime(value,'%m/%d/%Y %H:%M')
       else:
         m = time.strptime(value,'%m/%d/%Y %H:%M:%S%p')
    elif len(tstruct)==5:
         m = time.strptime(value,'%m/%d/%Y %H:%M')
    elif len(tstruct)==3 and ctype=="date":
      m = time.strptime(value,'%m/%d/%Y')
    else:
      return prototime
    return time.mktime(m)

def Time2String(val,ctype):
    global prototime

#    sys.stdout.write("Time2String val={} ctype={}\n".format(val,ctype))
    if val in [-2,"-2"]:
       value = time.localtime(prototime+2*3600)
    elif val in [-1,"-1"]:
       value = time.localtime()
    else:
       value = time.localtime(float(val))

    if ctype == "regdatetime":
       return time.strftime('%m/%d/%Y %H:%M:%S%p',value)
    elif ctype == "datetime":
       return time.strftime('%m/%d/%Y %H:%M',value)
    elif ctype == "time":
       return time.strftime('%H:%M',value)
    else:
       return time.strftime('%m/%d/%Y',value)

