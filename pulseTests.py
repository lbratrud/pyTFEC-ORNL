#!/usr/bin/env python3
#
# Variation of captureTPC.py for DOING ALL PULSE TESTING!
#
#
# ORNL - 6/25/2018
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import time

from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_IO
import waveGen
import pulseReConstruct
import FEC_DATA

version = "1.0 6/25/2018"

sys.stdout.write("\npulseTests {}\n\n".format(version))
sys.stdout.write("Capture FEC Gain and Crosstalk data\n\n")
CmdExecutionQueue = []
make_raw_capture=0
fec = FEC_IO.FEC(CmdExecutionQueue,[version])
force_update = 1
i = 0
while  i < len(sys.argv):
   cmd = sys.argv[i].lower()
   if cmd == "-u":
	   force_update = 0
   i+=1

if FEC_IO.TestingInit(fec)==-1:
    sys.stdout.write("Cannot establish a link with the FEC. Stopping test!")
    exit(0)

fecData = FEC_DATA.FEC_data()
FEC_IO.DoPulseTests(fec,fecData,force_update=force_update)

exit()
