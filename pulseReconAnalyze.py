#!/usr/bin/env python3
#
# Process GBT0 and GBT1 data fiels.
#
# ORNL - 11/9/2017
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
version = "1.0 11/09/2017"

sys.stdout.write("\npulsePrint version {}\n\n".format(version))
import pulseReConstruct
import projmkrpt as mkrpt
import FEC_IO as fec_io

files = glob.glob("*GBTX0.dat")
sys.stdout.write("INPUTFREQ={}\n".format(pulseReConstruct.FREQINPUT))
if len(files)==0:
	for gbtxNum in [0]:
		files = glob.glob("run{}_*.bin".format(gbtxNum))
		if len(files) > 0:
			for i in files:
				cmd = "decoder_gbtx -f {} -i {}".format(pulseReConstruct.numFrames,i)
				nData = i.replace(".bin","")
				cmd += " -o {} -s {} -p".format(nData,gbtxNum)
				sys.stdout.write("Decoding GBTx file {}\n".format(nData))
				fec_io.ExecuteCmdParse(cmd)
	files = glob.glob("*GBTX0.dat")

maxItrList=[0.1e6]
for maxitr in maxItrList:
	dataList = []
	for i in files:
		sys.stdout.write("MDA {}\n".format(i))
		data = pulseReConstruct.LoadPulseData(i)
		data.loadFile(i,"TPC0L",maxitr=maxitr)
		dataList.append(data)

	pdfgen = mkrpt.makeReport("mypdf_{}ks.pdf".format(int(maxitr/1000)), "testing")
	ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
			 mkrpt.colors.darkolivegreen]
	xmax = 0
	ymax = 0
	for i in dataList:
		for j in range(0,8):
			for k in i.ReconList[j]:
				if k[0] > xmax:
				   xmax = k[0]
				if k[1] > ymax:
					ymax = k[1]
	xmax = 0.1*int(10.5*xmax)
	ymax = int(1.05*ymax)

	lineList = []
	for i in range(0,8):
		lineList.append(["S0 Chan{}".format(i),ColorList[i],None])

	for i in dataList:
		myData = []
		for index in range(0,8):
			myData.append(i.ReconList[index])
		if pdfgen.addBasicMultiPlot(xmax,ymax,myData,i.Desc,lineList,xpnts=10,plotsize=600)!=0:
		   pdfgen.addComponent([-2])

	for i in dataList:
		myData = []
		if pulseReConstruct.FREQINPUT > 300e6:
			spnt = 0
			epnt = int(i.size * 0.5)
		else:
			spnt = int(i.size*0.15)
			epnt = int(i.size*0.35)
		for index in range(0,8):
			myData.append(i.ReconList[index][spnt:epnt])
		if pdfgen.addBasicMultiPlot(epnt*1e6*pulseReConstruct.TIMERESOLUTION,ymax,myData,i.Desc,lineList,xpnts=10,plotsize=600,xmin=spnt*1e6*pulseReConstruct.TIMERESOLUTION)!=0:
		   pdfgen.addComponent([-2])

	for index in range(0,8):
		lineList = []
		myData = []
		cindex = 0
		for i in dataList:
			lineList.append([i.Desc,ColorList[cindex],None])
			cindex += 1
			if cindex >= 8:
			   cindex = 0
			if pulseReConstruct.FREQINPUT > 300e3:
				spnt = 0
				epnt = int(i.size * 0.5)
			else:
				spnt = int(i.size*0.15)
				epnt = int(i.size*0.35)
			myData.append(i.ReconList[index][spnt:epnt])
		if pdfgen.addBasicMultiPlot(epnt*1e6*pulseReConstruct.TIMERESOLUTION,ymax,myData,"S0 Chan{}".format(index),lineList,xpnts=10,plotsize=600,xmin=spnt*1e6*pulseReConstruct.TIMERESOLUTION)!=0:
		   pdfgen.addComponent([-2])

	pdfgen.DoExit()

exit()
