# -*- coding: utf-8 -*-
"""
Created on Fri Apr 21 11:20:04 2017

@author: daquser
Create a GBTx Configuration file
"""

# Variable Name, Mask, Default value, Mapping/Redundancy to register bit
GBTxVarMapping = [
    ["clockMng",        "cmEpllRxReferenceSelect",     0x3,    0,  [[0,0],[0,2],[0,4]]],
    ["clockMng",        "SLVoutTestSel",               0x3,    0,  [[0,6]],
    ["clockMng",        "cmEpllTxReferenceSelect",     0x3,    0,  [[1,0],[1,2],[1,4]]],
    ["clockMng",        "cmRxReferenceTestMuxSelect",  0x3,    0,  [[2,0],[2,2],[2,4]]],
    ["clockMng",        "cmTxReferenceTestMuxSelect",  0x3,    0,  [[3,0],[3,2],[3,4]]],

    ["phaseShifter",    "fineDelay0",                  0xf,    0,  [[4,0]]],
    ["phaseShifter",    "fineDelay1",                  0xf,    0,  [[4,4]]],
    ["phaseShifter",    "fineDelay2",                  0xf,    0,  [[5,0]]],
    ["phaseShifter",    "fineDelay3",                  0xf,    0,  [[5,4]]],
    ["phaseShifter",    "fineDelay4",                  0xf,    0,  [[6,0]]],
    ["phaseShifter",    "fineDelay5",                  0xf,    0,  [[6,4]]],
    ["phaseShifter",    "fineDelay6",                  0xf,    0,  [[7,0]]],
    ["phaseShifter",    "fineDelay7",                  0xf,    0,  [[7,4]]],

    ["phaseShifter",    "coarseDelay0",                0x1f    0,  [[8,0]]],
    ["phaseShifter",    "extS0",                       0x3,    0,  [[8,5]]],
    ["phaseShifter",    "extEarly0"                    0x1,    0,  [[8,7]]],
    ["phaseShifter",    "coarseDelay1",                0x1f    0,  [[9,0]]],
    ["phaseShifter",    "extS1",                       0x3,    0,  [[9,5]]],
    ["phaseShifter",    "extEarly1"                    0x1,    0,  [[9,7]]],
    ["phaseShifter",    "coarseDelay2",                0x1f    0,  [[10,0]]],
    ["phaseShifter",    "extS2",                       0x3,    0,  [[10,5]]],
    ["phaseShifter",    "extEarly2"                    0x1,    0,  [[10,7]]],
    ["phaseShifter",    "coarseDelay3",                0x1f    0,  [[11,0]]],
    ["phaseShifter",    "extS3",                       0x3,    0,  [[11,5]]],
    ["phaseShifter",    "extEarly3"                    0x1,    0,  [[11,7]]],
    ["phaseShifter",    "coarseDelay4",                0x1f    0,  [[12,0]]],
    ["phaseShifter",    "extS4",                       0x3,    0,  [[12,5]]],
    ["phaseShifter",    "extEarly4"                    0x1,    0,  [[12,7]]],
    ["phaseShifter",    "coarseDelay5",                0x1f    0,  [[13,0]]],
    ["phaseShifter",    "extS5",                       0x3,    0,  [[13,5]]],
    ["phaseShifter",    "extEarly5"                    0x1,    0,  [[13,7]]],
    ["phaseShifter",    "coarseDelay6",                0x1f    0,  [[14,0]]],
    ["phaseShifter",    "extS6",                       0x3,    0,  [[14,5]]],
    ["phaseShifter",    "extEarly6"                    0x1,    0,  [[14,7]]],
    ["phaseShifter",    "coarseDelay7",                0x1f    0,  [[15,0]]],
    ["phaseShifter",    "extS7",                       0x3,    0,  [[15,5]]],
    ["phaseShifter",    "extEarly7"                    0x1,    0,  [[15,7]]],

    ["clockMng",        "scCset",                      0xf,    0,  [[273,0]]],
    ["clockMng",        "cmRxPhase40MHz",              0x3,    0,  [[274,0],[274,4],[275,0]]],
    ["clockMng",        "cmRxPhase80MHz",              0x3,    0,  [[275,4],[276,0],[276,4]]],
    ["clockMng",        "cmRxTestMuxSelect40",         0x3,    0,  [[277,0],[278,0],[279,0]]],
    ["clockMng",        "cmRxTestMuxSelect80",         0x3,    0,  [[277,2],[278,2],[279,2]]],
    ["clockMng",        "cmRxTestMuxSelect160",        0x3,    0,  [[277,4],[278,4],[279,4]]],
    ["clockMng",        "cmRxTestMuxSelect320",        0x3,    0,  [[277,6],[278,6],[279,6]]],
    ["clockMng",        "testOutputSelect",            0xff,   0,  [[280,0]]],
    ["clockMng",        "cmReferenceClockSelect",      0xff,   0,  [[281,0],[281,2],[281,4]]],
    ["clockMng",        "cmPsReferencSelect",          0x3,    0,  [[282,0],[282,2],[282,4]]],
    ["clockMng",        "testOutSelect",               0x7f,   0,  [[283,0]]],
    ["clockMng",        "cmTxPhase40MHz",              0xf,    0,  [[284,0],[284,4],[285,0]]],
    ["clockMng",        "cmTxPhase80MHz",              0xf,    0,  [[285,4],[286,0],[286,4]]],
    ["clockMng",        "cmTxTestMuxSelect40",         0x3,    0,  [[287,0],[288,0],[289,0]]],
    ["clockMng",        "cmTxTestMuxSelect80",         0x3,    0,  [[287,2],[288,2],[289,2]]],
    ["clockMng",        "cmTxTestMuxSelect160",        0x3,    0,  [[287,4],[288,4],[289,4]]],
    ["clockMng",        "cmTxTestMuxSelect320",        0x3,    0,  [[287,6],[288,6],[289,6]]],
    ["clockMng",        "cmXpllReferenceSelect",       0x3,    0,  [[290,0],[290,2],[290,4]]],
    ["clockMng",        "ePllTxPhase320MHz",           0xf,    0,  [[291,0],[291,4],[292,0]]],
    ["clockMng",        "ePllRxPhase320MHz",           0xf,    0,  [[292,4],[302,0],[302,4]]],
    ["clockMng",        "ePllTxEnablePhase",           0xff,   0,  [[293,0],[294,0],[295,0]]],
    ["clockMng",        "ePllTxCap",                   0x3,    0,  [[296,5],[297,5],[298,5]]],
    ["clockMng",        "ePllTxPhase160MHz",           0x1f,   0,  [[296,0],[297,0],[298,0]]],
    ["clockMng",        "ePllTxIcp",                   0xf,    0,  [[299,0],[300,0],[301,0]]],
    ["clockMng",        "ePllTxRes",                   0xf,    0,  [[299,4],[300,4],[301,4]]],
    ["clockMng",        "ePllTxReset",                 0x1,    0,  [[303,0],[303,1],[303,2]]],
    ["clockMng",        "ePllRxReset",                 0x1,    0,  [[303,4],[303,5],[303,6]]],
    ["clockMng",        "ePllRxEnablePhase",           0xff,   0,  [[304,0],[305,0],[306,0]]],
    ["clockMng",        "ePllRxPhase160MHz",           0x1f,   0,  [[307,0],[308,0],[309,0]]],
    ["clockMng",        "ePllRxCap",                   0x3,    0,  [[307,5],[308,5],[309,5]]],
    ["clockMng",        "ePllRxIcp",                   0xf,    0,  [[310,0],[311,0],[312,0]]],
    ["clockMng",        "ePllRxRes",                   0xf,    0,  [[310,4],[311,4],[312,4]]],
    ["clockMng",        "xPllFrequencyTrim",           0x7f,   0,  [[313,0],[314,0],[315,0]]],
    ["clockMng",        "xPllEnable",                  0x1,    0,  [[313,7],[314,7],[315,7]]],
    ["clockMng",        "xPllGmSelect"                 0xf,    0,  [[316,0],[316,4],[317,0]]],
    ["clockMng",        "xPllControlOverride",         0x1,    0,  [[318,0],[318,1],[318,2]]],
    ["clockMng",        "xPllEnableAutoRestart",       0x1,    0,  [[318,3],[318,4],[318,5]]],
    ["clockMng",        "cmTestMuxSelect",             0x1f,   0,  [[319,0]]],

    ["clockMng",        "ConfigDone",                  0xff,   0xff,  [[365,0]]]
]    
class GBTxClock:
   def __init__(self):
       self.cmEpllRxReferenceSelect = [0,0x3]

       self.cmEpllRxReferenceTestMuxSelect = [0,0x3]
       self.scCset = [0,0xf]
       
       self.cmEpllTxReferenceSelect = [0,0x3]
       self.SLVoutTestSel = [0,0x3]
       
       self.scEnableTermination = [0x1,0x1]
       
       self.cmRxPhase40MHz = [0,0xf]
       self.cmRxPhase80MHz = [0,0xf]
       
       self.cmRxTestMuxSelect40 = [0,0x3]
       self.cmRxTestMuxSelect80 = [0,0x3]
       self.cmRxTestMuxSelect160 = [0,0x3]
       self.cmRxTestMuxSelect320 = [0,0x3]

       self.testOutSelect = [0,0xff]
       self.cmReferenceClockSelect = [0,0x3]
       self.cmPsReferenceClockSelect = [0,0x3]
    
class GBTxGroup:
   def __init__(self):
       self.ChannelTerm = [0xff,0xff]
       self.ClockDrive = [0xf,0xf]

class GBTxCnfgMake:
   def __init__(self):

