#!/usr/bin/env python3

# Cross-platform python interface for the CERN GBT USBI2C dongle
# Based on: https://svnweb.cern.ch/cern/wsvn/ph-ese/be/gbtx_testers/usbI2C_dongle/sw/usb_i2c_python/usb_dongle.py
# Requires Python 3

import hidapi as hid
import time
import ctypes
import sys

ID_VENDOR_NUM = 0x16C0
ID_PRODUCT_NUM = 0x05DF

# USB Specific stuff
# The report sent to the HID device starts with the report ID followed by the report.
# Maximum size of the report is 1+16=17, i.e. 1 byte report ID and max 16 bytes for the report.
REPORT_ID = 0x1
START_BYTE = 0xDA # Header for dongle

#
# Commands
#
CMD_NONE = 0 # No command, why is this even here?

# LinkM I2C core commands
CMD_I2CTRANS = 1 # i2c read & write (N args: addr + other??) makes no sense
CMD_I2CWRITE = 2 # i2c write to slave (N args: addr + other??)
CMD_I2CREAD  = 3 # i2c read (1 arg: addr)
CMD_I2CSCAN  = 4 # i2c bus scan (2 args: start,end)
CMD_I2CCONN  = 5 # i2c connect/disconnect (1 args: 1/0)
CMD_I2CINIT  = 6 # i2c init (0 args)

# LinkM board commands (Not used?)
CMD_VERSION_GET = 100 # Return LinkM version
CMD_STATLED_SET = 101 # Set status LED (1 arg: 1/0)
CMD_STATLED_GET = 102 # Get status LED (0 args)
CMD_PLAY_SET    = 103 # Set parameters of player FSM 
CMD_PLAY_SET    = 104 # Get parameters of player FSM 
CMD_EESAVE      = 105 # Save LinkM state to EEPROM
CMD_EELOAD      = 106 # Load LinkM state to EEPROM
CMD_BOOTLOADER  = 107 # Trigger USB bootload

# GBT dongle specific commands
CMD_GBT_VTARGET_SET = 200 # Switch target LDO on/off (1 arg: 1/0)
CMD_GBT_VTARGET_GET = 201 # Get status of target LDO (0 args)
CMD_GBT_BURN_FUSE   = 202 # Apply e-fuse voltage and pulse (0 args)
CMD_GBT_IO1_SET     = 203 # Switch IO1 output on/off (1 arg: 1/0)
CMD_GBT_IO1_GET     = 204 # Status of IO1
CMD_GBT_IO2_SET     = 205 # Switch IO2 output on/off (1 arg: 1/0)
CMD_GBT_IO2_GET     = 206 # Status of IO2
CMD_GBT_VFUSE_SET   = 207 # Switch 3v3 efuse LDO on/off (1 arg: 1/0)
CMD_GBT_VFUSE_GET   = 208 # Status of 3v3 efuse LDO
CMD_GBT_FUSEPULSE   = 209 # 1 ms fuse pulse

#
# Return codes
#
ERR_NONE = 0
ERR_BADSTART = 101
ERR_BADARGS  = 102 # Unable to decode command
ERR_I2C      = 103
ERR_I2C_READ = 104
ERR_NOTOPEN  = 199
ERR_USBOVERFLOW = 200 # Dongle USB return buffer overflow

class InterfaceError(Exception):
    """Error class to manage returned codes from the interface"""
    def __init__(self,error_code):
        self.error_code=error_code
        self.error_string=''
        if (self.error_code==0):
            self.error_string='ERROR_NONE'
        elif (self.error_code==101):
            self.error_string='ERR_BADSTART'
        elif (self.error_code==102):
            self.error_string='ERR_BADARGS'
        elif (self.error_code==103):
            self.error_string='ERR_I2C'
        elif (self.error_code==104):
            self.error_string='ERR_I2CREAD'
        elif (self.error_code==199):
            self.error_string='ERR_NOTOPEN'
        elif (self.error_code==200):
            self.error_string='ERR_USBOVERFLOW'
        else :
            self.error_string='ERR_UNKNOWN'

    def __str__(self):
        return ('USB dongle error: '+self.error_string+'('+str(self.error_code)+')')

class GBT_USB_dongle():
    """ HID interface """
    def __init__(self):
        """

        :rtype : object
        """
        try:
            hid.hid_init()
            self.hid_devices = hid.hid_enumerate(ID_VENDOR_NUM, ID_PRODUCT_NUM)
            if len(self.hid_devices) == 1:
                return
                #print("1 GBT USBI2C interface found")
                #self.open()
                #self.gbtx_init()
            if len(self.hid_devices) > 1:
                #self.device=hid.hid_open_path(hid_devices[0].path)
                print("{} GBT USBI2C interfaces found".format(len(hid_devices)))
                #i = 0
                #for dev in self.hid_devices:
                #    print ("----------------------\n{}:\n----------------------".format(i))
                #    print(dev.description())
                #    i+=1
            else:
                 raise IOError("No interface attached to USB")
        except IOError as e:
            print(e)
            sys.exit(-1)
    
    def open(self, dev_num = 0):
        """Open device"""
        self.device = hid.hid_open_path(self.hid_devices[dev_num].path)

    
    def close(self):
        """Close the connection"""
        self.setvtargetldo(0)
        hid.hid_close(self.device)
        hid.hid_exit()
        del self

    def __usb_command(self, cmd=0, num_send=0,num_recv=0,payload=[]):
            # Send the feature report to the device
            buf = []
            header = [REPORT_ID, START_BYTE, cmd, num_send, num_recv]
            buf = header + payload
            for i in range(132-len(buf)):
                    buf.append(0x0)
            hid.hid_send_feature_report(self.device, buf)
            # Get the reply report
            # [ID, ERROR_CODE](+[DATA,...,...])
            _ret = [0x1, 0x0]
            for i in range(num_recv):
                _ret.append(0x0)
            ret = hid.hid_get_feature_report(self.device, _ret)
            try:
                if (ret[1]==int(ERR_NONE) and len(ret) == 2+num_recv):
                    return ret[1:num_recv+2]
                else:
                    raise InterfaceError(ret[1])
            except InterfaceError as e:
                print (e)


    # user functions
    # ---------------
    def setvtargetldo(self,value):
        """ drive target LDO (0/1)"""
        self.__usb_command(CMD_GBT_VTARGET_SET,1,0,[int(value)])

    def getvtargetldo(self):
        """ get target LDO status (0/1)"""
        ldo=self.__usb_command(CMD_GBT_VTARGET_GET,1,1)
        return ldo[1]

    def setod1(self,value):
        """ drive open drain 1 output (0/1)"""
        self.__usb_command(CMD_GBT_IO1_SET,1,0,[int(value)])

    def getod1(self):
        """ get open drain 1 output status (0/1)"""
        od1=self.__usb_command(CMD_GBT_IO1_GET,1,1)
        return od1[1]

    def setod2(self,value):
        """ drive open drain 2 output (0/1)"""
        self.__usb_command(CMD_GBT_IO2_SET,1,0,[int(value)])

    def getod2(self):
        """ get open drain 2 output status (0/1)"""
        od2=self.__usb_command(CMD_GBT_IO2_GET,1,1)
        return od2[1]

    def burnefuse(self):
        """ apply 3.3V on target and pulse to burn E-Fuse"""
        self.__usb_command(CMD_GBT_BURNFUSE)

    def get_firmware_version(self):
        """Return dongle firmware version as string - major version.minor version"""
        answer=self.__usb_command(CMD_VERSION_GET,1,3)
        return str(answer[1])+"."+str(answer[2])

    def go_bootload(self):
        """put the dongle in bootloader mode"""
        self.__usb_command(CMD_BOOTLOADER)

    def setvfuseldo(self,value):
        """ drive Efuse 3.3V LDO (0/1)"""
        self.__usb_command(CMD_GBT_VFUSE_SET,1,0,[int(value)])

    def getvfuseldo(self):
        """ get Efuse 3.3V LDO status (0/1)"""
        vfuse=self.__usb_command(CMD_GBT_VFUSE_GET,1,1)
        return vfuse[1]

    def fusepulse(self):
        """ trigger a 1.5V pulse"""
        self.__usb_command(CMD_GBT_FUSEPULSE)

    # user functions - I2C
    # -----------------------
    def __i2c_reset(self):
        """Reset I2C bus - Stop and Init"""
        self.__usb_command(CMD_I2CINIT)

    def __i2c_connect(self,value):
        """enable or disable I2C voltage translator (0/1)"""
        self.__usb_command(CMD_I2CCONN,1,0,[int(value)])

    def __i2c_scan(self,start_addr=1,end_addr=15):
        """scan I2C bus for devices in a given address range (up to 14 slaves) - return list of slaves address"""
        ret= self.__usb_command(CMD_I2CSCAN,2,15,[start_addr,end_addr])
        # ret[0] - status
        # ret[1] - number of slaves
        # ret[2..] - slaves addresses
        #print ("found {} I2C slave(s)".format(ret[1]))
        devices= (ret[2+i] for i in range(ret[1]))
        return list (devices)

    def __i2c_write(self,addr=1,data=[]):
        """write data to I2C slave at adress - data as bytes list"""
        payload=[addr]+data
        return self.__usb_command(CMD_I2CWRITE,len(payload),0,payload)

    def __i2c_read(self,addr=1,bytestoread=1):
        """read N bytes from I2C slave at adress - return error code + data as list"""
        return self.__usb_command(CMD_I2CREAD,1,bytestoread,[addr])

    def __i2c_writeread(self,addr=1,bytestoread=2,data=[]):
        """write data to slave and then read N bytes - return error code + data as list"""
        payload=[addr]+data
        #print(payload)
        return self.__usb_command(CMD_I2CTRANS,len(payload),bytestoread,payload)

    # GBTx functions
    # -----------------------
    def gbtx_init(self, verbose = False):
        try:
            self.setvtargetldo(1)
            self.__i2c_reset()
            self.__i2c_connect(1)
            devices=self.__i2c_scan() # use default
            if len(devices) == 1:
                if verbose:
                    print('Found GBTx -- setting address to: {}'.format(hex(devices[0])))
                self.gbtx = devices[0]
            elif len(devices)>1:
                for dev in devices:
                    print('Found GBTxs on addresses: {}'.format(hex(dev)))
                print("Use gbtx_address(addr) to choose device")
            else:
                raise IOError("No GBTx detected")
            return devices
        except IOError as e:
            print(e)
            return []

    def gbtx_address(self, addr):
        self.gbtx = addr

    def gbtx_write_register(self, register, value):
        """ Write to GBTx register """
        reg_addr_l = register & 0xff
        reg_addr_h = (register>>8) & 0xff
        self.__i2c_write(self.gbtx, [reg_addr_l,reg_addr_h,value])

    def gbtx_read_register(self, register):
        """ Read from GBTx register """
        reg_addr_l = register & 0xff
        reg_addr_h = (register>>8) & 0xff
        return self.__i2c_writeread(self.gbtx, 1, [reg_addr_l,reg_addr_h])[1]
    
        
    def gbtx_read_register_block(self, idx=0):
        """ Read back a block of registers (idx+14), returns register values as a list """
        reg_addr_l = idx & 0xff
        reg_addr_h = (idx>>8) & 0xff
        return list(self.__i2c_writeread(self.gbtx, 15, [reg_addr_l,reg_addr_h])[1:])

    def gbtx_write_cfg(self, config_values = []):
        for reg in range(len(config_values)):
            self.gbtx_write_register(reg, config_values[reg])

    def gbtx_dump_cfg(self):
        """ Read back R/W registers, returns register values as a list """
        buf = []
        for reg in range(0,360,15):
            buf += self.gbtx_read_register_block(reg)
        for reg in range(360,366):
            buf.append(self.gbtx_read_register(reg))
        return buf

    def gbtx_fuse(self, fuse_values = []):
        if (len(fuse_values) < 367) or (len(fuse_values) > 367):
            raise Exception('You need to define all the 366 writeable registers before fusing.\nLength of provided array: {}'.format(len(fuse_values)))
        else:
            for reg in range(len(fuse_values)):
                reg_addr_lsb = reg & 0xff
                reg_addr_msb = (reg>>8) & 0xff
                ## fuseBlowAddressLSB
                self.gbtx_write_register(238, reg_addr_lsb)
                ## fuseBlowAddressMSB
                self.gbtx_write_register(239, reg_addr_msb)
                ## fuseBlowData
                self.gbtx_write_register(240, fuse_values[reg])
                ## Enable VFUSEPOWER at start of process
                if reg == 0:
                    print ("Enabling VFUSEPOWER")
                    print("Fusing...")
                    self.setvfuseldo(1)
                self.fusepulse() # Pulse VFUSEPULSE
            ## Disable VFUSEPOWER at the end
            print ("Disabling VFUSEPOWER")
            self.setvfuseldo(0)
            # Reset GBTx

    def gbtx_reset(self):
        print('Resetting 1v5...')
        self.setod1(1)
        time.sleep(0.2)
        self.setod1(0)
        time.sleep(0.2)
        print('Resetting 2v5...')
        self.setod2(1)
        time.sleep(0.2)
        self.setod2(0)


#-----------------------------------------------------------------
# Standalone
if __name__ == "__main__":
    import argparse

    T1 = time.time()
    parser = argparse.ArgumentParser(description='GBTx I2C Access with GBT dongle',
                                     formatter_class = argparse.ArgumentDefaultsHelpFormatter)
    # Arguments
    parser.add_argument('dongle_idx', type = int, nargs = '?',
                         help = 'Dongle index if more than one connected', default = 0)
    parser.add_argument('i2c_addr', type = lambda x: int(x,0), nargs = '?',
                         help = 'I2C address', default = '0xf')
    # Dongle fw specific commands
    parser.add_argument('--list-dongles', action='store_true',
                         help = 'List connected devices')
    # I2C commands
    parser.add_argument('--i2c-scan', action = 'store_true',
                        help = 'Scan for I2C devices attached to dongle')
    parser.add_argument('--read', metavar = ('ADDR'),
                        nargs = 1, type = lambda x: int(x, 0),
                        help = 'Read single GBTx register address',
                        default = argparse.SUPPRESS)
    parser.add_argument('--write', metavar = ('ADDR', 'VALUE'),
                        nargs = 2, type = lambda x: int(x, 0),
                        help = 'Write single GBTx register address with value',
                        default = argparse.SUPPRESS)
    # Files
    parser.add_argument('--write-cfg', metavar = 'FILENAME', nargs = '?',
                         help = 'Write config file to GBTx', default = argparse.SUPPRESS)
    
    parser.add_argument('--dump-cfg', metavar = 'FILENAME', nargs = '?',
                         help = 'Dump GBTx config to file, or stdout', default = argparse.SUPPRESS)
    
    parser.add_argument('--fuse-cfg', metavar = 'FILENAME', nargs = '?',
                         help = 'Write config file to GBTx', default = argparse.SUPPRESS)
    parser.add_argument('--no-verify', action = 'store_true',
                         help = 'Do not verify fusing')

    args = parser.parse_args()

    # Regular operation
    dongle = None
    try:
        dongle = GBT_USB_dongle() 
        if (len(dongle.hid_devices) == 1):
            args.dongle_idx = 0
        if(not args.list_dongles and not args.i2c_scan):
            dongle.open(args.dongle_idx)
            dongle.gbtx_init()
    except IOError as e:
        #print(e)
        sys.exit(-1)

    # List connected dongles
    if (args.list_dongles):
        print("----------------------\nGBT USBI2C interface(s) description:")
        i = 0
        for dev in dongle.hid_devices:
            print ("----------------------\nIndex {}:\n----------------------".format(i))
            print(dev.description())
            i+=1
        #dongle.close()
        sys.exit(0)

    # Check for devices
    if(args.i2c_scan):
        #if (not args.dongle_idx == 666):
        dongle.open(args.dongle_idx)
        devs = dongle.gbtx_init()
        for i in devs:
            print('GBTx found at address: 0x{:02x}'.format(i))
        dongle.close()
        sys.exit(0)

    if hasattr(args, 'read'):
        print('0x{:02x}'.format(dongle.gbtx_read_register(args.read[0])))

    if hasattr(args, 'write'):
        dongle.gbtx_write_register(args.write[0], args.write[1])

    # Write config
    if hasattr(args, 'write_cfg'):
        print('Writing configuration \'{:s}\' to GBTx at address 0x{:x}'.format(args.write_cfg, args.i2c_addr))
        cfg_file = open(args.write_cfg, "r")
        conf = [line.rstrip('\n') for line in cfg_file]
        conf = [int(x,16) for x in conf]
        dongle.gbtx_write_cfg(conf)
        cfg_file.close()

    # Dump config
    if hasattr(args, 'dump_cfg'):
        print('Dumping configuration of GBTx at address 0x{:x} to \'{:s}\''.format(args.i2c_addr, 'STDOUT' if args.dump_cfg == None else args.dump_cfg))
        conf = dongle.gbtx_dump_cfg()
        if args.dump_cfg == None:
            print(conf)
        else:
            cfg_dump_file = open(args.dump_cfg, "w")
            for reg_val in conf:
                cfg_dump_file.write('{:02x}\n'.format(reg_val))
            cfg_dump_file.close()

    # Fuse config
    if hasattr(args, 'fuse_cfg'):
        cfg_file = open(args.fuse_cfg, "r")
        conf = [line.rstrip('\n') for line in cfg_file]
        conf = [int(x,16) for x in conf]
        # Do some crude checking of the file
        if len(conf) == 366:
            conf.append(0x7) # reg addr 366 needs to be fused to 0x7 for the values to be transferred on startup
        elif len(conf) == 367 and not conf[366] == 0x7:
            print("configFuse[2:0] (reg 366) needs to be burned to 0x7 for the startup state machine to transfer the fused values... Exiting.")
            sys.exit(-1)
        elif len(conf) < 366:
            print("All 366 registers (0-365) needs to be defined in the configuration file... Exiting.")
            sys.exit(-1)
        elif len(conf) > 367:
            print("Only 366 registers (0-365) needs to be defined in the configuration file, found {}... Exiting.".format(len(conf)))
            sys.exit(-1)
        else:
            print('Fusing configuration \'{:s}\' to GBTx at address 0x{:x}'.format(args.fuse_cfg, args.i2c_addr))
        # Start fusing process
        dongle.gbtx_fuse(conf)
        dongle.gbtx_reset()
        if not args.no_verify:
            time.sleep(1) # Not sure how long it takes for the GBTx to get a lock TODO: Check manual for lock register
            loaded_regs_from_fused = dongle.gbtx_dump_cfg()
            if (loaded_regs_from_fused != conf[:-1]):
                print('\x1b[0;30;41m'+"ERROR: Fusing failed!"+'\x1b[0m')
                print("Reg : Fuse val != File val")
                for reg in range(len(conf)-1):
                    if (loaded_regs_from_fused[reg] != conf[reg]):
                        print('{} : {:02x} != {:02x}'.format(reg, loaded_regs_from_fused[reg],conf[reg]))
            else:
                print('\x1b[1;37;42m'+"GBTx fused OK!"+'\x1b[0m')
        cfg_file.close()
T2 = time.time()
#iTime = 0.1*int(10*(T2-T1))
sys.stdout.write("Total Time = {:.1f}\n".format(T2-T1))
#sys.stdout.write("Total Time = {}\n".format(0.1*int(10*(T2-T1))
