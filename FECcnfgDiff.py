# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import string
import sys
import os
import array
import ctypes
import mmap
import gbtxconfigIO as gbtxcnfg
import GBTx0cnfg as Cnfg0
import GBTx0acnfg as Cnfg0a
import GBTx1cnfg as Cnfg1

windex = 1
configDef = 0
cmpFile = ""
while windex < len(sys.argv):
    if sys.argv[windex]=="-c":
       windex += 1
       configDef = sys.argv[windex]
    elif sys.argv[windex]=="-f":
       windex += 1
       cmpFile = sys.argv[windex]
    else:
       sys.stdout.write("Unknow option {}\n".format(sys.argv[windex]))
    windex += 1
        
if len(cmpFile) > 0:
    cList = gbtxcnfg.ReadConfigFile(cmpFile)
    if configDef=="0":
        sys.stdout.write("Comparing to CNFG0\n")
        myList = Cnfg0.CONFIG_REGISTER_DEFINITIONS
    elif configDef=="0a":
        sys.stdout.write("Comparing to CNFG0A\n")
        myList = Cnfg0a.CONFIG_REGISTER_DEFINITIONS
    elif configDef=="1":
        sys.stdout.write("Comparing to CNFG1\n")
        myList = Cnfg1.CONFIG_REGISTER_DEFINITIONS
    else:
        myList = gbtxcnfg.ReadConfigFile(configDef)
    diffs = gbtxcnfg.CompareRefDefConfigFile(myList,cList,sys.stdout)
    sys.stdout.write("Found {} differences\n".format(diffs))