#!/usr/bin/env python3
#
# Variation of captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import time
import FEC_DATA as FECdata

version = "1.0 4/23/2018"

sys.stdout.write("\nRadiation Test Noise Capture TPC version {}\n\n".format(version))
from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_IO as fec_io
import waveGen
import pulseReConstruct

CmdExecutionQueue = []
make_raw_capture=0
versionList = [ ["pyTFEC",Version],["FEC_IO",fec_io.version]]

fec = fec_io.FEC(CmdExecutionQueue,versionList)
fec.DoInitProc()

fname = time.strftime('%Y%b%d_%H%M%S')
myPath = "{}/RadNoiseTest/{}".format(fec.capturePath, fname)
sys.stdout.write("Path = {}\n".format(myPath))

def writeLog(log,code):
  for i in code:
     log.write(i+"\n")

def GetBrdInfo(log):
  cmd = "tsampa -v --mask 0x1 --sampa-mask 0x1f --pwr-on"
  code = fec_io.ExecuteCmdParse(cmd)
  writeLog(log,code)

def DoTest(ntime,itr):
  while time.time() < ntime: 
     time.sleep(1)
  fname = time.strftime('%Y%b%d_%H%M%S')
  fpath = "{}/{}".format(myPath, fname)
  sys.stdout.write("Taking snapshot {}\n".format(itr))
  sys.stdout.write("Path {}\n".format(fpath))
  os.makedirs(fpath)
  os.chdir(fpath)
  log = open("RadTest.log",'w')
  numFrames = 4*5000000

  GetBrdInfo(log)
  cmd = "treadout --no-run-dir --events 1 --frames {} --output-dir . --mask 0x1".format(numFrames)
  code = fec_io.ExecuteCmdParse(cmd)
  writeLog(log,code)

  GetBrdInfo(log)


  try:
    os.rename("run000000_trorc00_link00.bin", "run0_0.bin")
    os.rename("run000000_trorc00_link01.bin", "run1_0.bin")
    for i in [0,1]:
       cmd = "decoder_gbtx -f {} -i run{}_0.bin".format(numFrames,i)
       cmd += " -o run{}_0 -s {} -l 500".format(i,i)
       code = fec_io.ExecuteCmdParse(cmd)
       writeLog(log,code)
    
  except:
    sys.stdout.write("Failed to move files\n")
    pass
  sys.stdout.write("Done {}\n".format(itr))
  log.close()

  fecData = FECdata.FEC_data()
  fecData.setAttr("Name","RadTest-{}".format(itr))
  fecData.setAttr("BoardID",fec.boardID)
  fecData.setAttr("Mode","NoiseTest")
  fecData.setAttr("Path",fpath)
  fecData.setAttr("Description","RadTest Snapshot Itr={}".format(itr))
  fecData.setAttr("Threshold",500)
  fecData.setAttr("Sequence","NONE")
  fecData.setAttr("PulseInfo","NONE")
  fecData.setAttr("NumIters",1)
  fecData.Save()
  fecData.ReadAll()
  PDFname = "radtest_{}_{}.pdf".format(itr,fname)
  fecData.MakePDF(PDFname)

time_base = 60
ntime = time.time()
itr=1
for i in range(0,5):
   DoTest(ntime,itr) 
   ntime += time_base
   itr += 1

while 1:
   DoTest(ntime,itr) 
   ntime += 10*time_base
   itr += 1

exit()
