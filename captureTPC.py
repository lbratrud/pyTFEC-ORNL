# captureTPC.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os


version = "2.0 9/28/2017"

sys.stdout.write("\nCapture TPC version {}\n\n".format(version))
from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import FEC_IO as fec_io

CmdExecutionQueue = []
make_raw_capture=0
fec = fec_io.FEC(CmdExecutionQueue,[version])
fec.DoInitProc()
exit(0)
