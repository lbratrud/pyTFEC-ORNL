#! /usr/bin/env python3
# pulseReConstruct.py
#
# Python main file for FEC pulse reconstruction. The pulse must at the given FREQINPUT (in hz)
#
# ORNL - 11/1/2017
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import math
import os
import glob
import projmkrpt as mkrpt
import pulseReConstruct
import FEC_IO
import FEC_DATA as FECdata
import time

fList = glob.glob("itr*")
dList = []
pwd = os.getcwd()
mytime_min = time.time()
Dose = [0,365,750,1500,3000,4500,6000,7500,9000,10500,12000]
Dose = [0,365,703,1407,2813,5626,8440,11253,14067,16880,19693]
Dose = [0,0.1,628,2513,5026,7539,10052,12565,15078,17591]
Dose = [0,3134,6268,9402]
for i in fList:
    os.chdir(i) 
    mList = FEC_IO.ExecuteCmdParse("grep RTD RadTest.log")
    RTD = [0,0,0,0,0,0,0]
    for gp in mList:
        words = gp.replace("="," ").replace("C"," ").split()
        if words[0][0:3]=="RTD":
           index = int(words[0][3:])
           RTD[index] += float(words[1])
    radF = glob.glob("*.pdf")
    d = FECdata.FEC_data()
    d.Read("runinfo.txt")
    d.ReadAll()
    rms = d.getRMS(5)
    words = i.split("_")
    mytime = time.mktime(time.strptime("{}_{}".format(words[1],words[2]),'%Y%b%d_%H%M%S'))
    dList.append([mytime,d,0,0.5*RTD[1],0.5*RTD[2],0.5*RTD[3],0.5*RTD[4],0.5*RTD[5],"{}/{}".format(i,radF[0])])
    if mytime < mytime_min:
       mytime_min = mytime
    os.chdir(pwd)

tList = []
for i in dList:
   i[0] = int(i[0]-mytime_min)
   tList.append(i[0])
#   sys.stdout.write("{}\n".format(i))

#sys.stdout.write("dList={}\n".format(dList))

DsList = []
tList.sort(key=int)

for j in tList:
   rmJ = 0
   for i in dList:
      if i[0]==j:
        DsList.append(i)
        rmJ = i
        break
   if rmJ!=0:
      dList.remove(rmJ)

tMade = time.strftime('%Y%b%d_%H%M%S')
index = 0
DoseLen = len(Dose)-1
for data in DsList:
    data[2] = Dose[index]
    if index < DoseLen:
       index += 1
#    cmd = "cp {} dose{}.pdf".format(data[8],data[2],tMade)
#    mList = FEC_IO.ExecuteCmdParse(cmd)
    
   
pdfgen = mkrpt.makeReport("RadTest_{}.pdf".format(tMade), "Radiation Test per Channels")
pdfgen.add_header = 0
pdfgen.joinedLines = 1
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]



def getMinMax(minv,maxv,DELTA_MIN = 0.5):
    delta = maxv-minv
    if delta < DELTA_MIN:
       delta = DELTA_MIN
    avg = 0.5*(minv+maxv)
    nval = avg+0.5*delta
    if nval > maxv:
       maxv = nval
    else:
       maxv = nval + delta

    nval = avg-0.5*delta
    if nval < minv:
       minv = nval
    else:
       minv = nval - delta

    return [minv,maxv]

plotsize = 250
lineList = []
lineList.append(["Time", mkrpt.colors.blue, mkrpt.makeMarker("Circle"),4,(1,1)])
pData = []
minv = -1
maxv = -1
for data in DsList:
        rms = data[0]/60
#        sys.stdout.write(" {}".format(rms))
        if minv==-1 and maxv==-1:
           minv=rms
           maxv=rms
        if rms > maxv:
           maxv = rms
        if rms < minv:	
           minv = rms
        pData.append([data[2],rms])
        xmax = data[2]

minv = 0
maxv = 0.1*int(11*maxv)
xmax = 0.1*int(11*xmax)
pdfgen.addBasicMultiPlot(xmax,maxv,[pData], "Test Duration", lineList, \
    xpnts = 10, ymin=minv,plotsize = plotsize,xmin=0,ydesc="Computer Time (minutes)",xdesc="Dose (Rad)",add_linedef=0)


minv = -1
maxv = -1
pData = []
lineList = []
for index in range(1,6):
    rData = []
    lineList.append(["RTD{}".format(index), ColorList[index-1], mkrpt.makeMarker("Circle"),3,(1,1)])
    for data in DsList:
        rms = data[2+index]
        if minv==-1 and maxv==-1:
           minv=rms
           maxv=rms
        if rms > maxv:
           maxv = rms
        if rms < minv:	
           minv = rms
        rData.append([data[2],rms])
        xmax = data[2]
    pData.append(rData)

minv,maxv = getMinMax(minv,maxv)
minv = 0.1*int(10*minv)
maxv = 0.1*int(10*maxv)
xmax = 0.1*int(11*xmax)
pdfgen.addBasicMultiPlot(xmax,maxv,pData, "RTD Info", lineList, \
    xpnts = 10, ymin=minv,plotsize = plotsize,xmin=0,ydesc="Temperature 'C",xdesc="Dose (Rad)",add_linedef=1)

STEPSIZE = 2
for index in range(0,160,STEPSIZE):
    pData = []
    minv = -1
    maxv = -1
    lineList = []
    for span in range(0,STEPSIZE):
      channel = index + span
      lineList.append(["Channel {}".format(channel), ColorList[span], mkrpt.makeMarker("Circle"),3,(1,1)])
      rData = []
      pData.append(rData)
      for data in DsList:
        rms = data[1].getRMS(channel)
        if minv==-1 and maxv==-1:
           minv=rms
           maxv=rms
        if rms > maxv:
           maxv = rms
        if rms < minv:	
           minv = rms
        rData.append([data[2],rms])
        xmax = data[2]

    minv,maxv = getMinMax(minv,maxv)
    minv = 0.01*int(100*minv)
    if minv < 0:
       minv  = 0
    maxv = 0.01*int(100*maxv)
    xmax = 0.1*int(11*xmax)
    pdfgen.addBasicMultiPlot(xmax,maxv,pData, "Channel Noise", lineList, \
            xpnts = 10, ymin=minv,plotsize = plotsize,xmin=0,ydesc="RMS",xdesc="Dose (Rad)",add_linedef=1)

pdfgen.DoExit()

exit()
