#!/usr/bin/env python3

# pyFEC.py
#
# Python main file for FEC testing
#
# ORNL - 10/24/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import math
import sys
import time
import threading
import string
import sys
import os
import array
import ctypes
import mmap
import glob
import FEC_DATA
import _thread
from projbase import *
import DataViewer
# import projplot as pplot

import commondb as cdb

cdb.ColorList.addwDesc("PYFEC_STATUS_INIT", "#66B2FF", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_INIT_FG", "#000000", "PYFEC")

cdb.ColorList.addwDesc("PYFEC_STATUS_RUNNING", "#ffffff", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_IDLE", "#40ff40", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_ATTN", "#ffff40", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_STATUS_ERROR", "#ff4040", "PYFEC")

cdb.ColorList.addwDesc("PYFEC_BUTTON", "#4080ff", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_BUTTON_PUSH", "#00ff00", "PYFEC")

cdb.ColorList.addwDesc("PYFEC_VIEW_BUTTON_PUSH", "#4080ff", "PYFEC")
cdb.ColorList.addwDesc("PYFEC_VIEW_BUTTON", "#000000", "PYFEC")


cdb.ColorList.addwDesc("TEST_STATUS_PASSED", "#40ff40", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_WARN", "#ffff40", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_FAILED", "#ff4040", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_IDLE", "#4040ff", "TEST")
cdb.ColorList.addwDesc("TEST_STATUS_DONE", "#40ff40", "TEST")

cdb.ColorList.addwDesc("BARCODE_OPR", "#4040ff", "BARCODE")
cdb.ColorList.addwDesc("BARCODE_NONE", "#ff4040", "BARCODE")
cdb.ColorList.addwDesc("BARCODE_SET", "#40ff40", "BARCODE")

cdb.ColorList.addwDesc("STEP_TODO", "#4040ff", "STEP")
cdb.ColorList.addwDesc("STEP_WORKING", "#ff4040", "STEP")
cdb.ColorList.addwDesc("STEP_DONE", "#40ff40", "STEP")

from projbase import *

Version = "2.3-05_23_2019"
import projpath as mpath

mpath.projName = "TPC-FEC"

import commondbed as cdbed
import projwin as win
import projtime as mtime

import errwarn as errwarn
import FEC_IO
errwarn.ErrWarn.setup(0)
import FEC_DATA
import FEC_IO

try:  # import as appropriate for 2.x vs. 3.x
    from tkinter import *
except:
    from Tkinter import *

if sys.version_info[0] > 2:
    import threading
else:
    import thread
# or
# try:  # import as appropriate for 2.x vs. 3.x
#   from reportlab.pdfgen import canvas
#   pdf_generator_enabled = 1
# except:
#   sys.stdout.write("Cannot find reportlab. PDF Generator turned off\n")
#   pdf_generator_enabled = 0

do_exit = 0

UpdateWindowsList = []
# =----------------------------------------------------------------------------------------------
# =----------------------------------------------------------------------------------------------

class FecBarScanWindow(win.Base):
    def __init__(self, winmain,view_mode=0):
        if win.Base.__init__(self, "bscan", "Bar Code Scan for #{}".format(winmain.station)) == 0:
            return 0
        row = 0
        self.winmain = winmain
        self.view_mode = view_mode
        self.indexlast = 0

        if self.view_mode==0:
            self.DN = win.WinButton(self, "Done", row, 1)
            self.DN.button.config(bg=cdb.ColorList.getParam("BARCODE_OPR"), command=self.Done)
            self.setDone(0)

        self.b1 = win.WinMenu(self, "Options", -1, row, 2)
        self.b1.menub.grid(sticky=E + W + S + N)
        if self.view_mode==0:
            self.b1.menu.add_command(label="Backup by one", command=self.Back)
            self.b1.menu.add_separator()
            self.b1.menu.add_command(label="Clear All", command=self.ClearScan)
        else:
            self.b1.menu.add_command(label="Exit", command=self.Done)

        self.num_BCs = len(FEC_IO.BarCodeScanList)
        row += 1
        win.WinRowFeed(self, row)
        row += 1

        row_init = row
        col = 1

        self.Scan = 0
        self.Status = 0
        if self.view_mode==0:
            row = row_init
            self.Scan = win.WinValueLabel(self, "Scanning", -1, row, col)
            self.Scan.entry.config(bg=cdb.ColorList.getParam("BARCODE_OPR"))
            row += 1
            self.StatusCode = ""
            self.Status = win.WinValueLabel(self, "Status", -1, row, col)
            self.Status.entry.config(bg=cdb.ColorList.getParam("BARCODE_OPR"))
            row += 1
            win.WinRowFeed(self, row)
            row += 1

        index = 0
        self.BCList = []
        all_done = 1
        for i in FEC_IO.BarCodeScanList:
            fname = "BC{}".format(index)
            VL = win.WinValueLabel(self, "{}".format(i), 30, row, col)
            if self.winmain.fecIntf.scannedCodes[index]==0:
                VL.setVal("Unknown")
                color = cdb.ColorList.getParam("BARCODE_NONE")
                all_done = 0
            else:
                VL.setVal(self.winmain.fecIntf.scannedCodes[index])
                color = cdb.ColorList.getParam("BARCODE_SET")
                if all_done==1:
                    self.winmain.fecIntf.ScanIndex = index+1
            VL.entry.config(bg=color)
            self.BCList.append(VL)
            row += 1
            index += 1
        if self.view_mode==0:
            self.setDone(all_done)
        win.WinRowFeed(self, row)
        win.WinColumnFeed(self, col + 2)

        if self.view_mode  == 0:
            self.ClearScan()
        UpdateWindowsList.append(self)

    def ClearScan(self):
        self.winmain.fecIntf.ClearBarCodeScan()
        if self.Scan!=0:
            self.Scan.setVal("Scan {}".format(FEC_IO.BarCodeScanList[self.winmain.fecIntf.ScanIndex]))
        if self.Status!=0:
            self.Status.setVal("Ready to start")
        self.DN.button.config(state=DISABLED)
        self.setDone(0)
        self.Update()

    def Done(self):
        if self.view_mode==0:
            self.winmain.fecIntf.SaveBarCodes(do_tmp=1)
            self.winmain.fecIntf.TestStep = FEC_IO.TEST_STEP_POWERUP
            self.winmain.fecIntf.setNewMode("waiting",setNewMode=1)

        if self in UpdateWindowsList:
            UpdateWindowsList.remove(self)
        self.DoExit()

    def setDone(self, code):
        if code == 1:
            self.DN.button.config(state=NORMAL, bg=cdb.ColorList.getParam("BARCODE_SET"))
        else:
            self.DN.button.config(state=DISABLED, bg=cdb.ColorList.getParam("BARCODE_NONE"))

    def Back(self):
        oindex = self.winmain.fecIntf.ScanIndex
        self.self.winmain.fecIntf.scannedCodes[oindex] = 0
        if self.index > 0:
            self.winmain.fecIntf.ScanIndex -= 1
            self.winmain.Log.add("Index changed from {} to {}".format(FEC_IO.BarCodeScanList[oindex], FEC_IO.BarCodeScanList[self.winmain.fecIntf.ScanIndex]))
        else:
            self.StatusCode = "Index Cannot got beyond {}".format(FEC_IO.BarCodeScanList[self.winmain.fecIntf.ScanIndex])
        self.Update()

    # Scanner instance and setup
    def Update(self,force_update=0):
        if self.view_mode==1 and force_update==0:
            return
        if force_update!=0:
            self.indexlast = -1

        if self.indexlast != self.winmain.fecIntf.ScanIndex:
            if self.winmain.fecIntf.ScanIndex < self.winmain.fecIntf.num_BCs:
                self.Scan.setVal("Scan {}".format(FEC_IO.BarCodeScanList[self.winmain.fecIntf.ScanIndex]))
                self.setDone(0)
            else:
                self.Scan.setVal("DONE")
                self.setDone(1)
            self.indexlast = self.winmain.fecIntf.ScanIndex
            for i in range(0, self.winmain.fecIntf.num_BCs):
                VL = self.BCList[i]
                data = self.winmain.fecIntf.scannedCodes[i]
                if data != 0:
                    color = cdb.ColorList.getParam("BARCODE_SET")
                else:
                    color = cdb.ColorList.getParam("BARCODE_NONE")
                    data = "Unknown"
                VL.entry.config(bg=color)
                VL.setVal(data)



# cdbed.ColorEditor()
# =----------------------------------------------------------------------------------------------
# =----------------------------------------------------------------------------------------------
ViewModeList = [["opr","Operator Details"],["tests","Test Results"], ["log","Internal Log"]]
class WindowMain(win.Base):
    def __init__(self, station):
        self.Log = 0
        if win.Base.__init__(self, "mmain{}".format(station), "Test Station #{} Version={}".format(FEC_IO.myCompID,Version)) == 0:
            return 0
        self.station = station
        self.fecIntf = 0
        row = 0
        self.b1 = win.WinMenu(self, "Options", -1, row, 1)
        self.b1.menub.config(fg=cdb.ColorList.getParam("PYFEC_BUTTON_PUSH"))
        self.b1.menub.grid(sticky=E + W + S + N)
        win.WinRowFeed(self, 1)
        win.WinColumnFeed(self, 8)
        self.ViewMode =ViewModeList[0][0]

        mainmenu = self.b1.menu
        m = mainmenu
        msub = Menu(m, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
        for i in FEC_IO.TestStepInfo:
            if i[FEC_IO.ATTR_INDEX] > 0 and i[FEC_IO.ATTR_STEPTO] == 1:
                msub.add_command(label=i[FEC_IO.ATTR_NAME], command=lambda jumpto=i[FEC_IO.ATTR_INDEX]: self.setStep(jumpto))
        m.add_cascade(label="Step Control", menu=msub)
        mainmenu.add_separator()

        msub = Menu(m, bg=self.mbgc, fg=self.mfgc, font=self.font, relief=RIDGE, tearoff=1)
        msub.add_command(label="Open Viewer Window", command=self.runFecBarScanWindow)
        msub.add_command(label="Clear BarCodes", command=self.ClearBarCodes)
        m.add_cascade(label="BarCode Options", menu=msub)

        mainmenu.add_separator()
        mainmenu.add_command(label="Exit", command=self.doSysExit)

        self.jobID = 1
        col = 1
        row = 2
        OprDef = win.WinLabel(self, "Operator", -1, row, 1)
        OprDef.setSize(40)

        self.OprDo  = win.WinButton(self, "", row, 2)
        self.OprDo.button.config(command=self.StepExecute,width=15)
        self.OprDo.button.config(bg=cdb.ColorList.getParam("PYFEC_BUTTON"),fg=cdb.ColorList.getParam("PYFEC_BUTTON_PUSH"))
        self.OprDo.button.grid(columnspan=2)
        self.OprDo.setSize(40)
        row += 1
        row_init = row
        col = 1
        for i in ViewModeList:
            DN = win.WinButton(self, i[1], row, col)
            DN.button.config(bg=cdb.ColorList.getParam("PYFEC_VIEW_BUTTON"), fg=cdb.ColorList.getParam("PYFEC_VIEW_BUTTON_PUSH"), command=lambda logview=i[0]: self.setLogView(logview))
            col += 1
        row += 1
        self.Log = win.WinScrollList(self, 3, "Details", 35, 15, row, 0, [], mycolumnspan=4)

        col += 5
        row = row_init+1
        self.ResultsList = []
        fgcolor = cdb.ColorList.getParam("PYFEC_STATUS_INIT_FG")
        self.labelID = win.WinValueLabel(self, "labelID", -1, row, col)
        self.labelID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"), fg=fgcolor)
        self.labelID.setVal("Unknown")
        row += 1
        self.barDef = win.WinValueLabel(self, "Board Set", -1, row, col)
        self.barDef.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"), fg="#ffffff")
        self.barDef.setVal("Unknown")
        row += 1
        self.BoardID = win.WinValueLabel(self, "BoardID", -1, row, col)
        self.BoardID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"), fg=fgcolor)
        self.BoardID.setVal("Unknown")
        row += 1
        for i in ["Pwr", "Noise", "Pedestal", "SCA Info", "Gain 20x", "Gain 30x", "CrossTalk","CrossTalkVictim"]:
            VL = win.WinValueLabel(self, "{} Test".format(i), 15, row, col)
            VL.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"),fg="#000000")
            row += 1
            self.ResultsList.append([i,VL])
        win.WinSetDummyLabel(self,row,col)
        row += 1
        self.Mode = win.WinValueLabel(self, "Mode", 20, row, col)
        self.Mode.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"), fg=fgcolor)
        row += 1
        self.ScaID = win.WinValueLabel(self, "ScaID", -1, row, col)
        self.ScaID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"), fg=fgcolor)
        row += 1
        self.Timer = win.WinValueLabel(self, "Timer", -1, row, col)
        self.Timer.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"), fg=fgcolor)
        row += 1
        self.Status = win.WinValueLabel(self, "Status", -1, row, col)
        self.Status.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"), fg=fgcolor)
        row += 1
        col += 3
        win.WinSetDummyLabel(self,row+15,col+3)
#        self.TestLog = win.WinScrollList(self, 3, "Test Log", 15, 15, row_init, col, [])
        self.Clear()
        UpdateWindowsList.append(self)

    #
    # def defineOptions(self):
    #    if self.Log==0:
    #       return
    #
    #    mode = ""
    #    mainmenu = self.b1.menu
    #    mainmenu.delete(0,END)
    #    mainmenu.add_command(label="Clear Log",command=lambda cmd="clearlog":self.DoCmd(cmd))
    #    mainmenu.add_command(label="Run Basic Tests",command=lambda cmd="basictests":self.DoCmd(cmd))
    #    mainmenu.add_command(label="Run Pulser Tests",command=lambda cmd="pulsertests":self.DoCmd(cmd))
    #    mainmenu.add_separator()
    #    mainmenu.add_command(label="Exit (FEC remains powered)",command=self.doSysExit)
    #    mainmenu.add_command(label="Shutdown (FEC is powered down)",command=self.doSysShutdown)

    def updateLog(self):
        self.Log.delete()
        if self.ViewMode=="tests":
            for TL in self.TestLog:
                self.addToLog(TL[0], color=TL[1])
        elif self.ViewMode=="log":
            for TL in self.fecIntf.InitLog:
                self.addToLog(TL[0], color=TL[1])
        elif self.fecIntf != 0:
            TestStepDef = self.fecIntf.getTestStepInfo()
            for i in TestStepDef[FEC_IO.ATTR_DESCLIST]:
                words = i.split()
                mlen = 0
                msg = ""
                for w in words:
                    wlen = len(w)
                    if mlen + wlen >= 80:
                        self.addToLog(msg, color="#4040c0")
                        msg = "    "
                        mlen = 0
                    mlen += wlen + 1
                    if mlen > 0:
                        msg += " "
                    msg += w
                if mlen > 0:
                    self.addToLog(msg, color="#4040c0")
            if TestStepDef[FEC_IO.ATTR_EXESTATE] == "boardid":
                BCvalue = self.fecIntf.GetFecIDFromBarCode()
                if BCvalue!=-1:
                    self.addToLog("Exp => " + self.Value2Bits(BCvalue), color="#4040c0")
                else:
                    self.addToLog("Barcode not found", color="#ff4040")
                self.addToLog("Got => " + self.Value2Bits(self.fecIntf.boardID), color="#4040c0")

    def Clear(self):
        self.setLogView("opr")
        self.TestLog = []
        self.lastseconds = 0
        self.Mode.setVal("CLEARED")
        self.labelID.setVal("")
        self.barDef.setVal("")
        self.Mode.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))
        self.ScaID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))
        self.ScaID.setVal("Unknown")
        self.BoardID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))
        self.BoardID.setVal("Unknown")
        self.Timer.setVal("--")
        self.Timer.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))
        self.Status.setVal("Unknown")
        self.Status.entry.config(bg="#40ff40")
        for i in self.ResultsList:
            i[1].setVal("Unknown")
            i[1].entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"))

    def setStatus(self,msg,colorcode="PYFEC_STATUS_IDLE",log=0):
        self.Status.setVal(msg)
        self.Status.entry.config(bg=cdb.ColorList.getParam(colorcode))

    def setStep(self,TestStep):
        self.fecIntf.TestStep = TestStep
        self.fecIntf.NewMode = 1

    def setLogView(self,viewMode):
        self.ViewMode = viewMode
        for i in ViewModeList:
            if i[0]==viewMode:
                self.Log.label.configure(text=i[1])
                self.updateLog()
                return
    def addToLog(self,msg,color=0):
        self.Log.addLog(msg)
        if color!=0:
            self.Log.setColor(color)

    def setNewMode(self,newMode):
        self.fecMode = newMode

    def FindTestBT(self,testname):
        for i in self.ResultsList:
            if i[0] == testname:
                return i[1]
        return 0

    def TestMsg(self,msg,color="#40ff40"):
        self.TestLog.append([msg,color])

    def TextReportObj(self,got_data,testname,mList):
        max_yellow = FEC_DATA.get_max_yellows(testname)
        VL = self.FindTestBT(testname)

        if got_data==0:
            VL.setVal("Unknown")
            VL.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_INIT"),fg=cdb.ColorList.getParam("PYFEC_STATUS_INIT_FG"))
            return -1

        if VL==0:
            sys.stdout.write("Cannot find Button {}\n".format(testname))
            return 1

        if len(mList)==0:
            self.TestMsg("{} PASSED".format(testname),color="#40ff40")
            VL.setVal("PASSED")
            VL.entry.config(bg=cdb.ColorList.getParam("TEST_STATUS_PASSED"),fg="#000000")
            return 0

        cond = [0,0]
        for i in [[0,"RED","#c04040","FAILED"],[1,"YELLOW","#c0c040","WARNED"]]:
            for channel in mList:
                if channel[1]==i[1]:
                    cond[i[0]] += 1

            if cond[i[0]] > 0:
                index = 1
                self.TestMsg("",color="#4040ff")
                self.TestMsg("{} {}={}".format(testname,i[3],cond[i[0]]),color=i[2])
                self.TestMsg("",color="#000000")
                for channel in mList:
                    if channel[1]==i[1]:
                        try:
                            pname = "Ch{}".format(int(channel[0]))
                        except:
                            pname = channel[0]
                        self.TestMsg("[{}] {}={:.3f} {}={:.3f}".format(index,pname, channel[3], channel[2],channel[4]),i[2])
                        index += 1

        color = cdb.ColorList.getParam("TEST_STATUS_FAILED")
        if cond[0] > 0:
            VL.setVal("FAILED-{}".format(cond[0]))
            code = 1
        elif cond[1] > max_yellow:
            VL.setVal("FAILED-Y{}".format(cond[1]))
            self.TestMsg("{} YELLOW conditions {} beyond limit={}".format(testname, cond[1], max_yellow), color=color)
            code = 1
        else:
            VL.setVal("PASSED-Y{}".format(cond[1]))
            color = cdb.ColorList.getParam("TEST_STATUS_PASSED")
            self.TestMsg("{} Passed with {} YELLOW conditions < {}".format(testname, cond[1], max_yellow), color=color)
            code = 0
        VL.entry.config(bg=color,fg="#000000")
        return code

    def ReadFecSummary(self):
        if self.fecIntf.fid==-1:
            return
        pathList = []

        pathList.append("{}/fid{}/Summary".format(FEC_DATA.DATAPATH,self.fecIntf.fid.lower()))
        pathList.append("{}/fid{}".format(FEC_DATA.AUTOSUMPATH,self.fecIntf.fid.lower()))
        for path in pathList:
#            sys.stdout.write("ReadFecSummary={}\n".format(path))
            barfile = "{}/barcode_fid{}".format(path,self.fecIntf.fid.lower())
            self.fecIntf.ReadBarCodes(filename=barfile)
            code = FEC_DATA.readSummary(self.fecIntf.fecData, path)
            sys.stdout.write("ReadFecSummary {} = {}\n".format(path,code))
            if code == "GOT":
                break
        self.fecIntf.loadBarCodes()
        self.CheckData()

    def CheckData(self):
        self.TestLog = []
        code = 0
        found = 0
        if self.fecIntf.fecData.got_basic_data==1:
            found += 1
            code += self.TextReportObj(self.fecIntf.fecData.got_basic_data,"Pwr",self.fecIntf.fecData.checkPwr())
            code += self.TextReportObj(self.fecIntf.fecData.got_basic_data,"Noise",self.fecIntf.fecData.checkData(2,self.fecIntf.fecData.RmsLevels))
            code += self.TextReportObj(self.fecIntf.fecData.got_basic_data,"Pedestal",self.fecIntf.fecData.checkData(1,self.fecIntf.fecData.AvgLevels))
            code += self.TextReportObj(self.fecIntf.fecData.got_basic_data, "SCA Info",self.fecIntf.fecData.checkMeasurements())
        if self.fecIntf.fecData.got_pulse_data==1:
            found += 1
            code += self.TextReportObj(self.fecIntf.fecData.got_pulse_data,"Gain 20x",self.fecIntf.fecData.checkGain("20"))
            code += self.TextReportObj(self.fecIntf.fecData.got_pulse_data,"Gain 30x",self.fecIntf.fecData.checkGain("30"))
            code += self.TextReportObj(self.fecIntf.fecData.got_pulse_data,"CrossTalk",self.fecIntf.fecData.checkCrosstalk())
            code += self.TextReportObj(self.fecIntf.fecData.got_pulse_data,"CrossTalkVictim",self.fecIntf.fecData.checkCrosstalkVictum())

        if found!=2:
            status = "UNKNOWN"
            color = cdb.ColorList.getParam("PYFEC_STATUS_INIT")
        elif code > 0:
            status = "FAILED"
            color = cdb.ColorList.getParam("TEST_STATUS_FAILED")
        else:
            status = "PASSED"
            color = cdb.ColorList.getParam("TEST_STATUS_PASSED")

        self.ScaID.entry.config(bg=color,fg="#000000")
        self.BoardID.entry.config(bg=color,fg="#000000")
        self.Status.setVal(status)
        self.Status.entry.config(bg=color,fg="#000000")

    def RawValue2Bits(self,value):
        msg = ""
        ivalue = value
        for i in range(0,12):
            if value & 1:
                msg = "1"+msg
            else:
                msg = "0" + msg
            if i == 7:
                msg = " " + msg
            value = value >> 1
        return msg

    def Value2Bits(self,ivalue):
        msg = self.RawValue2Bits(ivalue)
        msg += " = {:0>3}".format(hex(ivalue)[2:])
        return msg

    def UpdateBarCodeInfo(self):
        boardID = self.fecIntf.GetFecIDFromBarCode()
        if boardID > 0:
            bID = "0x{:0>3},  {:0>4}".format(hex(boardID)[2:].upper(),boardID)
            self.labelID.setVal(bID)
            hc = self.RawValue2Bits(boardID)
            self.barDef.setVal(hc)
        else:
            self.labelID.setVal("")
            self.barDef.setVal("")
        self.fecIntf.NewMode = 0

    def Update(self):
        if self.fecIntf == 0:
            self.setStatus("FEC Interface is missing","PYFEC_STATUS_ERROR")
        else:
            while len(self.fecIntf.CmdExecuteList) > 0:
                cmdDef = self.fecIntf.CmdExecuteList.pop(0).split()
                cmd = cmdDef[0]
                # if cmd=="addlog":
                #     color = 0
                #     if len(cmdDef) > 2:
                #         color = cmdDef[2]
                #     self.addToLog(cmdDef[1], color)
                # el
                if cmd=="updatelog":
                    self.updateLog()
                elif cmd=="setmode":
                    self.Mode.setVal(cmdDef[1:])
                elif cmd=="checkdata":
                    self.CheckData()
                elif cmd == "clear":
                    self.Clear()
                    self.CheckData()
                elif cmd == "boardid":
                    if self.fecIntf.boardID!=-1:
                        val = "{:0>3}".format(hex(self.fecIntf.boardID)[2:]).upper()
                        self.labelID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"),fg="#000000")
                        self.BoardID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"),fg="#000000")
                    else:
                        val = "Unknown"
                        self.BoardID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_ERROR"), fg="#000000")
                        self.labelID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_ERROR"), fg="#000000")
                    self.BoardID.setVal(val)

                elif cmd == "scaid":
                    if self.fecIntf.ScaID!=-1:
                        val = "{:0>6}".format(hex(self.fecIntf.ScaID)[2:]).upper()
                        self.ScaID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_IDLE"),fg="#000000")
                    else:
                        val = "Unknown"
                        self.ScaID.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_ERROR"),fg="#000000")
                    self.ScaID.setVal(val)

                elif cmd == "getfecsummary":
                    self.ReadFecSummary()

#            self.Mode.setVal(self.fecIntf.TestMode)
            if self.fecIntf.WatchEnable == 1:
                seconds = int(time.time()-self.fecIntf.WatchTime)
                if seconds > self.lastseconds:
                    self.lastseconds = seconds
                    self.Timer.setVal(valueToTime(seconds))

            if self.fecIntf.TestStep < 0:
                self.Status.setVal("ERROR")
                self.Status.entry.config(bg=cdb.ColorList.getParam("PYFEC_STATUS_ERROR"),fg="#000000")
                self.OprDo.setVal("**Fix ERROR**",mode=DISABLED)

            if self.fecIntf.TestMode not in ["waiting","waitpwr"]:
                self.OprDo.setVal("Wait for Done",mode=DISABLED)

            if self.fecIntf.NewMode==1:
                TestStepDef = self.fecIntf.getTestStepInfo()
                self.OprDo.setVal(TestStepDef[FEC_IO.ATTR_NAME])
                self.OprDo.button.config(state=NORMAL)
                self.UpdateBarCodeInfo()
                self.updateLog()

    def Rescan(self):
        pass

    def runFecBarScanWindow(self):
        FecBarScanWindow(self,view_mode=1)
    def ClearBarCodes(self):
        self.fecIntf.ClearBarCodeScan()
        self.UpdateBarCodeInfo()

    def StepExecute(self):
        code = self.fecIntf.nextStep()
        if code == 0:
            self.fecIntf.setOprDetail("Wait for step to finish or stop the present step")
        else:
            if self.fecIntf.TestMode=="barscan":
                FecBarScanWindow(self)
            self.Log.delete()

    def doSysShutdown(self):
        if self.fecIntf.Supply != 0:
            self.fecIntf.Supply.Off()
        self.doSysExit()

    def doSysExit(self):
        global do_exit
        self.fecIntf.Supply.Off()

        # Does not hurt to turn off waveform generator
#        if waveGen.wavegen != 0:
#            waveGen.wavegen.Channel[0].Off()
        do_exit = 1
#        self.close()
        exit(0)


for i in [1]:
    winmain = WindowMain(i)
    winmain.fecIntf = FEC_IO.FEC(winmain, opticalChannel=i)
    _thread.start_new_thread(winmain.fecIntf.DoWork, (0.25,))

def TimerProc():
    global do_exit
    for i in UpdateWindowsList:
        i.Update()
    if do_exit==0:
        win._root.after(1, TimerProc)


win._root.after(1, TimerProc)
win._root.mainloop()
