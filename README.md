PyTFEC is a variation on pyFEC that uses the TRORC.

pyTFEC is python program that will:

1. Turn on the power supply.
2. Setup a bus pirate and configure the GBTX0 on the FEC board.
3. Configuring the FEC board. This script should be VERY similar to what CERN is using, but the program logs the results ~/TPC-FEC/data/fec<boardID>.
4. Configure a waveform generator.

capture
For processing the data, ViewFecData is 
