#!/usr/bin/env python3
#
# This file processes the .dat files created by pulsePrint
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
version = "2.0 01/16/2018"

sys.stdout.write("\npulsePlot version {}\n\n".format(version))
import projpath as mpath
mpath.projName = "TPC-FEC"
import errwarn as errwarn
errwarn.ErrWarn.setup(0)
import pulseReConstruct
import projmkrpt as mkrpt
import FEC_IO as fec_io

def getFiles():
	global resolution
	return glob.glob("amp_*PD{}.dat".format(resolution))

resolution = 16
ctbase = 0
i = 0
FREQINPUT = pulseReConstruct.FREQINPUT
outputName = "pulsePlot"
xminDef = xmaxDef = "None"
pChannels = range(0, 160)
while  i < len(sys.argv):
   cmd = sys.argv[i].lower()
   if cmd == "-freq":
	   FREQINPUT = int(sys.argv[i + 1])
	   i += 1
   elif cmd == "-freqk":
	   FREQINPUT = int(1000 * float(sys.argv[i + 1]))
	   i += 1
   elif cmd == "-res":
	   resolution = int(sys.argv[i + 1])
	   i += 1
   elif cmd == "-channel":
	   pChannels = [int(sys.argv[i + 1])]
	   i += 1
   elif cmd == "-ctbase":
	   ctbase = 1
   elif cmd == "-xmin":
       xminDef = float(sys.argv[i + 1])
       i += 1
   elif cmd == "-xmax":
	   xmaxDef = float(sys.argv[i + 1])
	   i += 1
   elif cmd == "-o":
	   outputName = sys.argv[i + 1].replace(".pdf","")
	   i += 1
   i+=1

pulseReConstruct.DoSetup(resolution,setFREQINPUT=FREQINPUT)
if xminDef=="None":
   xminDef = 0
else:
   xminDef = int(xminDef*pulseReConstruct.USTOPNTS)

if xmaxDef == "None":
   xmaxDef = pulseReConstruct.num_bins
else:
   xmaxDef = int(xmaxDef*pulseReConstruct.USTOPNTS)

#sys.stdout.write("xmin={} xmax={}\n".format(xminDef, xmaxDef))

files = getFiles()
DataList = []
for f in files:
	data = pulseReConstruct.LoadPulseData(f)
	sys.stdout.write("Reading {}\n".format(f))
	data.loadConstructedData(f)
	data.pulseAlign()
	data.infofind()
	if ctbase==1:
	   data.removeBaseLine()
	DataList.append(data)

pdfgen = mkrpt.makeReport("{}.pdf".format(outputName), "PulsePrint Output by Channels")
ColorList = [mkrpt.colors.red, mkrpt.colors.blue, mkrpt.colors.green, mkrpt.colors.purple, mkrpt.colors.brown, mkrpt.colors.cyan, mkrpt.colors.orange,
		 mkrpt.colors.darkolivegreen]

for index in pChannels:
	sys.stdout.write("Creating Channel {} plot\n".format(index))
	myData = []
	myDataZoom = []
	lineList = []
	cindex = 0
	myMax = []
	for dl in DataList:
		got_data = 0
		ymax = 0
		for dt in dl.ReconList[index]:
			if dt[1] > 5:
			   got_data = 1
			   break
		if got_data==1:
#			sys.stdout.write("Len={} xmax={}\n".format(len(dl.ReconList[index]),xmaxDef))
			myData.append(dl.ReconList[index][xminDef:xmaxDef])
			lineList.append(["{}".format(dl.FileDef),ColorList[cindex],None])
			cindex += 1
			if cindex >= len(ColorList):
			   cindex = 0
	if len(myData) > 0:
		xmin = 5000
		xmax = 0
		ymax = 0
		for i in myData:
			for k in i:
				if k[0] > xmax:
					xmax = k[0]
				if k[0] < xmin:
					xmin = k[0]
				if k[1] > ymax:
					ymax = k[1]
		xmax = 0.1 * int(10.1*xmax)
		ymax = 10 * (int(0.1 * ymax) + 1)
		pdfgen.addBasicMultiPlot(xmax,ymax,myData,"Channel {}".format(index),lineList,xpnts=16,plotsize=250,xdesc="Time (us)",ydesc="ADC value",xmin=xmin)

pdfgen.DoExit()

exit()
