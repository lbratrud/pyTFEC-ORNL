# mtcsreport.py
#
# Collection of routines used to generate the PDF forms for MTCS
# This entire class is based on Python::reportlab
#
# --------------------------------------------------------------------------------
# Created 1/1/2015 by Lloyd Clonts clontslg@yahoo.com
# Project started December 2013 for Gary W. Turner
# for implementing CTC system

import time
import math
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
import sys
import commondb as cdb
import projtime as mtime
import projpath as mpath
from projbase import *
sysname = "Need to define this"
from reportlab.graphics.shapes import Drawing, _DrawingEditorMixin, String
from reportlab.graphics.charts.lineplots import SimpleTimeSeriesPlot
from reportlab.lib.colors import purple, PCMYKColor, black, pink, green, blue, white
from reportlab.graphics import renderPDF

from reportlab.graphics.charts.lineplots import LinePlot, ScatterPlot
from reportlab.graphics.charts.legends import LineLegend
from reportlab.graphics.shapes import Drawing, _DrawingEditorMixin
from reportlab.graphics.widgets.markers import makeMarker
from reportlab.pdfbase.pdfmetrics import stringWidth, EmbeddedType1Face, registerTypeFace, Font, registerFont
from reportlab.graphics.charts.axes import XValueAxis, YValueAxis, AdjYValueAxis, NormalDateXValueAxis
from reportlab.graphics.widgets.markers import makeMarker
from reportlab.lib import colors

from reportlab import rl_settings

TC = cdb.TextConfigList.find("report")
rl_settings.canvas_basefontname = TC.getFont()

margin = 0.5*inch
WIDTH2SIZE = 0.575
# --------------------------------------------------------------------------------
# The basic reporting class that all other routines call!!
# --------------------------------------------------------------------------------
class makeReport:
    def __init__(self,filename,title):
        self.title1 = title
        self.title2 = filename
        self.columnXSize = 0 
        self.columnPnts = []
        self.columnList = []
        self.columnRowCnt = 0
        self.depth = 0
        self.columnMaxRow = 0
        self.canvas = canvas.Canvas(filename,pagesize=letter)
        self.y = letter[1]-margin
        self.TextConfig = cdb.TextConfigList.find("report")
        self.size = self.TextConfig.getSize()
        self.done = 0
        stylesheet=getSampleStyleSheet()
        self.normalStyle = stylesheet['Normal']
        self.normalStyle.fontSize = self.size
        self.normalStyle.fontName = self.TextConfig.getFont()
        self.add_header = 0
        self.page = 1
        self.columnPnts = []
        self.columnY = -1
        self.ColorList = [colors.red, colors.blue, colors.green, colors.purple, colors.brown, colors.cyan, colors.orange,
                      colors.darkolivegreen]
        self.MarkerList = [None]
        self.joinedLines = 1

    def pageLine(self,y):
        self.canvas.setStrokeColorRGB(0,0,0)
        self.canvas.line(margin,y,letter[0]-margin,y)

    def addHeader(self):
        self.add_header = 0
        yf = y = letter[1]-margin
        xl = margin
        xh = letter[0]-margin
        x = 0.5*(xl+xh)

        t = cdb.TextConfigList.find("title")
        size = t.getSize()

        self.canvas.setFont("Times-Roman", 0.5*size)
        msg = "Page={}".format(self.page)
        self.page += 1
        msize = self.getWidth(msg,size)
        self.canvas.drawString(letter[0]-margin-0.5*msize,y+0.1*inch,msg)
        self.pageLine(y)
        y -= 1.1*size


        self.canvas.setFont("Times-Roman", size)
        x = xl + 0.5*(xh-xl-self.getWidth(self.title1,size))
        if x < xl:
            x = xl
        self.canvas.drawString(x,y,self.title1)
        y -= 1.1*size
        x = xl + 0.5*(xh-xl-self.getWidth(self.title2,0.5*size))
        self.canvas.setFont("Times-Roman", 0.5*size)
        self.canvas.drawString(x,y,self.title2)
        y -= 0.3*size
        self.pageLine(y)
        self.canvas.line(xl,yf,xl,y)
        self.canvas.line(xh,yf,xh,y)
        self.y = y -1.1*size

        t = cdb.TextConfigList.find("title")
        size = t.getSize()
        y -= 1.1*size
        t = cdb.TextConfigList.find("report")
        self.canvas.setFont("Times-Roman", t.getSize())

    def newPage(self):
        self.canvas.showPage()
        self.y = letter[1]-margin
        self.done = 1
        if self.add_header == 1:
            self.addHeader()

    def doneText(self):
        self.y -= 1.1*self.size
        self.done = 0
        if self.y < margin:
            self.newPage()

    def addText(self,x,y,msg,size,color,rotate=0):
        self.canvas.setFont("Times-Roman", size)
        self.canvas.setFillColorRGB(color[0],color[1],color[2])
        if rotate!=0:
            self.canvas.saveState()
            self.canvas.translate(x,y)
            self.canvas.rotate(rotate)
            self.canvas.drawString(0,0,msg)
            self.canvas.restoreState()
        else:
            self.canvas.drawString(x, y, msg)

    def addCenteredText(self,x,y,msg,size,color,rotate=0):
        self.canvas.setFont("Times-Roman", size)
        self.canvas.setFillColorRGB(color[0],color[1],color[2])
        if rotate!=0:
            self.canvas.saveState()
            self.canvas.translate(x,y)
            self.canvas.rotate(rotate)
            self.canvas.drawCentredString(0,0,msg)
            self.canvas.restoreState()
        else:
            self.canvas.drawCentredString(x,y,msg)

    def getWidth(self,string,size):
        return self.canvas.stringWidth(string, self.TextConfig.getFont(), size)

    def DoExit(self):
        if self.done==0:
            self.canvas.showPage()
        self.canvas.save()

    def addMessage(self,x,msg,code,color):
        tcode = code
        if len(tcode) > 0:
            tcode += ","
        tcode += "LF"
        mwidth = letter[0]-2*margin
        rwidth = letter[0]-margin - x

        words = msg.split()
        nmsg = ""
        for i in words:
            isize = self.getWidth(i+" ",self.size)
            if len(nmsg) > 0:
                nmsg += " "
            if rwidth - isize <= 0:
               self.addComponent([x,nmsg,tcode,color])
               nmsg = ""
               rwidth = mwidth
               x = margin
            nmsg += i
            rwidth -= isize

        if rwidth > 0:
           return self.addComponent([margin,nmsg,code,color])
        return margin

    def decodeComponent(self,define):
       underline = 0
       size = self.size
       newline = 0
       centered = 0
       words = define.upper().translate(strmaketrans(","," ")).split()
       for j in words:
           if j=="-":
              size -= 1
           elif j=="+":
              size += 1
           elif j=="U":
              underline = 1
           elif j=="LF":
              newline += 1
           elif j=="CTR":
              centered = 1
       return [size,underline,newline,centered]

    def addComponent(self,define):
        if define[0]==-2:
           self.newPage()
           return 0
        elif define[0]==-1:
           self.doneText()
           return 0
        else:
           decode = self.decodeComponent(define[2])
           color = color2RGB(define[3])
           if decode[3]==1:
               self.addCenteredText(define[0],self.y,define[1],decode[0],color)
           else:
               self.addText(define[0],self.y,define[1],decode[0],color)
           xsize = self.getWidth(define[1],decode[0])
           if decode[1]==1:
              yu=self.y-decode[0]*0.2
              self.canvas.setStrokeColorRGB(color[0],color[1],color[2])
              self.canvas.line(define[0],yu,define[0]+xsize,yu)

           newline = decode[2]               
           while newline != 0:
              self.doneText()
              newline -= 1
           return xsize


    def columnLine(self,y):
        x1 = self.columnPnts[0]
        x2 = self.columnPnts[len(self.columnPnts)-1]
        self.canvas.line(x1,y,x2,y)

    def getColumnMaxWidth(self,cList,size):
        cnt = 0
        tmax = 0
        for j in cList:
            wsize = self.getWidth(j,size)
            if wsize > tmax:
                tmax = wsize
            cnt += 1
        return [cnt,tmax]

    def setupColumn(self,cList,mode):
        self.columnY = self.y
        self.columnRowCnt = 0
        self.addComponent([-1])
        self.columnPnts = []
        self.columnList = cList
        decode = self.decodeComponent(mode)
        tsize = 0
        mlen = 1
        self.depth = 0
        for i in cList:
            cnt,wsize = self.getColumnMaxWidth(i,decode[0])
            if cnt > self.depth:
                self.depth = cnt
            tsize += wsize
            mlen += 2

        self.columnXSize = (letter[0]-2*margin-tsize)/mlen
        if self.columnXSize < 0:
            self.columnXSize = 0

    def addColumnHdr(self,mode,color):
        decode = self.decodeComponent(mode)
        yinit = self.y
        x = margin
        for i in self.columnList:
            self.columnPnts.append(x)
            self.y = yinit
            cnt,wsize = self.getColumnMaxWidth(i,decode[0])
            xctr = x + 0.5*wsize +self.columnXSize
            for j in i:
                self.addComponent([xctr,j,mode+",CTR",color])
                self.addComponent([-1])
            x += wsize + 2*self.columnXSize
        self.columnPnts.append(x)
        self.columnLine(self.columnY)
        self.y -= self.size*0.5
        self.columnLine(self.columnY-self.size*(self.depth+0.5))
        self.y += 0.25*self.size

    def beginColumn(self,cList,mode,color,maxrowToAddLine):
        self.columnMaxRow = maxrowToAddLine
        self.setupColumn(cList,mode)
        self.addColumnHdr(mode,color)

##    def beginColumn(self,cList,mode,color,maxrowToAddLine):
##        self.columnRowCnt = 0
##        yinit = self.y
##        x = margin
##        for i in cList:
##            self.columnPnts.append(x)
##            x += self.columnXSize
##            self.y = yinit
##            for j in i:
##                wsize = self.addComponent([x,j,mode,color])
##                self.addComponent([-1])
##            x += self.columnXSize
##        self.columnPnts.append(x)
##        self.columnLine(self.columnY)
##        self.y -= self.size*(self.depth-0.5)
##        self.columnLine(self.y)
##        self.y += 0.25*self.size

    def addColumnRowLine(self):
        self.columnRowCnt = 0
        self.y -= 0.25*self.size
        self.columnLine(self.y)
        self.columnLineAdded = 1

    def setupColumnRow(self,colorDef):
        color = color2RGB(colorDef)
        self.canvas.setFillColorRGB(color[0],color[1],color[2])
        self.columnLineAdded = 0
        if self.depth > 0:
            self.addColumnRowLine()
        elif self.columnMaxRow > 0:
            self.columnRowCnt += 1
            if self.columnRowCnt > self.columnMaxRow:
                self.addColumnRowLine()
        self.addComponent([-1])

    def addDbRow(self,elem,color):
        cList = []
#        sys.stdout.write("addDbRow color={}\n".format(color))
        for i in self.columnList:
            subList = []
            for k in i:
                subList.append("{}".format(elem.getParam(k)))
            cList.append(subList)
        self.addColumnRow(cList,color)

    def addColumnRow(self,cList,colorDef):
        last = -1
        index = 0
        yinit = self.y
#        sys.stdout.write("addColumnRow {}\n".format(colorDef))
        color = color2RGB(colorDef)
        self.canvas.setFillColorRGB(color[0],color[1],color[2])
        for i in self.columnPnts:
            self.y = yinit
            if last!=-1:
                for j in cList[index]:
                    self.canvas.drawCentredString(0.5*(i+last),self.y,j)
                    self.addComponent([-1])
                index += 1
            last = i
        if self.depth > 1:
           self.columnLine(self.y+self.size/1.1)

    def endColumn(self):
        self.y += 0.75*self.size
        self.canvas.setStrokeColorRGB(0,0,0)
        for i in self.columnPnts:
            self.canvas.line(i,self.columnY,i,self.y)

        self.columnLine(self.y)
        if self.depth > 1:
            self.addComponent([-1])


    def range2List(self, start, stop, ranges, mtype=int):
        stepint = mtype((stop - start) / ranges)
        scale = 1
        if abs(stepint) > 1:
           while abs(stepint) > 1:
               scale *= 10
               stepint /= 9
           step = scale * 0.1*int(10*stepint)
        elif stepint > 1e-4:
            while abs(stepint) < 1:
                scale /= 10
                stepint *= 10
            step = scale * int(stepint)
        else:
            step = 1e-4

        mList = []
        i = 0
        if start < stop:
           val = start
           while (val < stop):
              mList.append(val)
              val += step
        else:
            val = start
            while (val > stop):
                mList.append(val)
                val += step
        mList.append(val)
        stop = val
        return [mList, stop]


    def addBasicMultiPlot(self, xmax,ymax, xyList, desc, lineList, plotsize=225,xpnts=16,ypnts=10,xmin=0,ymin=0,subdesc="",ydesc="",xdesc="",add_linedef=1):
           if len(xyList) == 0:
               sys.stdout.write("Nothing here!")
               return 0
           for i in range(0,len(xyList)):
               if len(xyList[i])==0:
                   sys.stdout.write("Missing data at index={}\n".format(i))
                   return 0
           self.canvas.setFillColorRGB(0,0,0)
           mList = desc.split("\n")
           dsize = 15
           plot_ysep = dsize*len(mList)
           txtsize = 10
           maxsize = 1
           hsize = 0.5 * (letter[0] - 2 * margin)
           for i in lineList:
               tsize = self.getWidth(i[0],txtsize)
               if tsize > maxsize:
                  maxsize = tsize

           MaxIndex = int((letter[0]-4*margin)/(maxsize+1))
           xstart = margin
           if MaxIndex==1:
              xstart += 0.5*(hsize-maxsize)

           plot_ysep += txtsize * (2+int(len(lineList)/MaxIndex))
           xstepsize = 2.0*hsize/MaxIndex

           ytest = self.y - plotsize - plot_ysep
           if ytest < 0:
               self.addComponent([-2])

           #sys.stdout.write("MAX x,y={},{} len={}\n".format(xmax,ymax,len(xyList)))
           x = 50
           self.y -= dsize
           y = self.y - 0.5*dsize
           txindex = 0
           rdsize = dsize
           radj = 3
           for i in mList:
               self.addCenteredText(hsize + margin, self.y,i,rdsize, [0, 0, 0])
               rdsize -= radj
               self.y -= dsize
               radj = 0
           if len(ydesc) > 0:
              self.addCenteredText(margin, self.y-plotsize*0.5 , ydesc,txtsize+2, [0, 0, 0],rotate=90)
           xinfo = [xmin,xmax,xpnts]
           yinfo = [ymin,ymax,ypnts]
           if add_linedef==1:
               self.y -= 0.5 * txtsize
               for i in lineList:
                   self.addText(x + xstart + xstepsize * txindex, self.y, i[0], txtsize, [i[1].red, i[1].green, i[1].blue])
                   txindex += 1
                   if txindex >= MaxIndex:
                       txindex = 0
                       self.y -= txtsize
               if txindex > 0:
                   self.y -= txtsize
           code = self.createMultiPlot(xyList, xinfo, yinfo, lineList,plotysize=plotsize, plotxsize=700, joinedLines=self.joinedLines)
           self.doneText()
           self.doneText()
           if len(xdesc) > 0:
              self.addCenteredText(hsize + margin, self.y, xdesc,txtsize+2, [0, 0, 0])
              self.doneText()
           self.doneText()
           return code


    def createMultiPlot(self, xyList, xinfo, yinfo, lineList,plotysize=320,plotxsize=700,joinedLines=1):
           if len(xyList) == 0:
               sys.stdout.write("Nothing here!")
               return 0
           for i in range(0,len(xyList)):
               if len(xyList[i])==0:
                   sys.stdout.write("Missing data at index={}\n".format(i))
                   return 0
           xmin = xinfo[0]
           xmax = xinfo[1]
           xpnts = xinfo[2]
           ymin = yinfo[0]
           ymax = yinfo[1]
           ypnts = yinfo[2]
           xList, xmax = self.range2List(xmin, xmax, xpnts, mtype=float)
           yList, ymax = self.range2List(ymin, ymax, ypnts, mtype=float)

           yinit = self.y - plotysize
           if yinit < 0:
               self.addComponent([-2])
               self.add_header = 1
               yinit = self.y - plotysize
           self.y = yinit
           if plotxsize+3*margin > letter[0]:
              plotxsize = letter[0]-3*margin

           drawing = Drawing(plotxsize, plotysize)
           lp = LinePlot()
           lp.x = 2*margin
           lp.y = yinit
           lp.width = plotxsize
           lp.height = plotysize
           lp.data = xyList
           index = 0
           lp.joinedLines = joinedLines
           for i in lineList:
               lp.lines[index].strokeColor = i[1]
               lp.lines[index].symbol = i[2]
               if len(i) > 3:
                   lp.lines[index].symbol.size = i[3]
               if len(i) > 4:
                   lp.lines[index].strokeDashArray = i[4]
               index += 1
           lp.strokeColor = colors.black
           lp.xValueAxis.valueMin = xmin
           lp.xValueAxis.valueMax = xmax
           lp.xValueAxis.valueSteps = xList

           lp.yValueAxis.valueMin = ymin
           lp.yValueAxis.valueMax = ymax
           lp.yValueAxis.valueSteps = yList
           drawing.add(lp)
           renderPDF.draw(drawing, self.canvas, 0, 0)
           return 1
