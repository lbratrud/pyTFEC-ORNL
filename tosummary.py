#! /usr/bin/env python3

# tosummary.py
#
# Command line version of the TPC capture routines from pyFEC.py
#
# ORNL - 12/02/2016
# --------------------------------------------------------------------------------
# Lloyd Clonts (clontslg@ornl.gov)

import sys
import string
import sys
import os
import glob
from scipy import stats

#from projbase import *
import projpath as mpath
mpath.projName = "TPC-FEC"
import FEC_DATA as fecDATA
import FEC_IO as fecIO
import pulseReConstruct
import math
sumPath = mpath.getPath("summary")
version = "1.1 6/26/2018"

pwd = os.getcwd()
pwdRef = pwd.replace("/home/daquser/TPC-FEC/data/","")
words = pwdRef.split("/")
fecname = words[0]

def addParam(pList,line):
    pwdRef = pwd.replace("/home/daquser/TPC-FEC/data/", "")
    words = line.replace("="," ").split()
#    sys.stdout.write("command={}\n".format(words))
    for i in pList:
        if i[0]==words[1]:
            i[1].append(words[2])
            return
    pList.append([words[1],[words[2]]])
#    sys.stdout.write("command={}\n".format(command[0]))
#    for i in pList:

gfiles = glob.glob("pyTFEC*.log")
if len(gfiles) > 0:
    paramList = []
    for i in gfiles:
        mList = fecIO.ExecuteCmdParse("grep parameter {}".format(i))
        for i in mList:
            addParam(paramList,i)

    sys.stdout.write("Processing LOG/Parameter files\n")
    pFile = open("{}/parameters_{}.txt".format(sumPath, fecname), 'w')
    pFile.write("# tosummary Version {}\n".format(version))
    pFile.write("# Parameters Data Directory {}\n".format(pwd))
    pFile.write("# Format: parameter kewords value\n")
    pFile.write("string parameterPath {}\n".format(pwd))
    for parm in paramList:
        if parm[0] in ["ScaID"]:
            val = -1
            for i in parm[1]:
                if val==-1:
                    val = i
                    pFile.write("string {} {}\n".format(parm[0],val))
                elif i!= val:
                    pFile.write("duplparam {} {}\n".format(parm[0], val))
        else:
            sum = 0
            sumsq = 0
            for i in parm[1]:
                val = float(i)
                sum += val
                sumsq += val*val
            size = len(parm[1])
            sum /= size
            delta = math.sqrt(sumsq/size-sum*sum)
            pFile.write("measure {} {} {}\n".format(parm[0], sum, delta))
    pFile.close()
    exit(0)

resolution=16
gfiles = glob.glob("amp_*_on_off_PD{}.dat".format(resolution))
gfiles += glob.glob("amp_*_off_on_PD{}.dat".format(resolution))
if len(gfiles) > 0:
    sys.stdout.write("Processing CROSSTALK files\n")
    pulseReConstruct.DoSetup(resolution)
    DataList = []
    chanList = []
    for i in range(0,160):
       chanList.append([0,0])
    for f in gfiles:
            data = pulseReConstruct.LoadPulseData(f)
            sys.stdout.write("Reading {}\n".format(f))
            data.loadConstructedData(f)
            data.pulseAlign()
            data.infofind()
            data.removeBaseLine()
            DataList.append(data)

    index=0
    for i in DataList:
        for j in range(0,160):
           val = i.getPeak(j)
           chanList[j][index] = val
        index += 1

#    sys.stdout.write("Path={}\n".format(sumPath))
    gainFile = open("{}/crosstalk_{}.txt".format(sumPath,fecname),'w')
    gainFile.write("# tosummary Version {}\n".format(version))
    gainFile.write("# Crosstalk Data Directory {}\n".format(pwd))
    gainFile.write("# Format: crosstalk Eon_Ooff Eoff_Oon\n")
    gainFile.write("string crosstalkPath {}\n".format(pwd))

    for i in range(0,160):
       gainFile.write("crosstalk {} {} {}\n".format(i, chanList[i][0],chanList[i][1]))
    gainFile.close()
    exit()

gfiles = glob.glob("amp_*PD{}.dat".format(resolution))
if len(gfiles) > 0:
    if len(gfiles) < 10:
        sys.stdout.write("No enough file for Gain Summary\n")
        exit(0)
    sys.stdout.write("Processing GAIN files\n")
    pulseReConstruct.DoSetup(resolution)
    DataList = []
    peakList = []
    peakData = []
    sizeList = []
    chanList = []
    ampmin = 5000
    ampmax = 0
    for i in range(0,160):
       chanList.append([])
    for f in gfiles:
            data = pulseReConstruct.LoadPulseData(f)
            sys.stdout.write("Reading {}\n".format(f))
            data.loadConstructedData(f)
            data.pulseAlign()
            data.infofind()
            data.removeBaseLine()
            DataList.append(data)
    for i in DataList:
        myData = []
        words = i.FileDef.upper().split("_")
        amp = int(words[1].upper().replace("MVPP",""))
        sizeList.append(amp)
        myData.append(amp)
        if amp < ampmin:
           ampmin = amp
        if amp > ampmax:
           ampmax = amp
        mChanList = []
        for j in range(0,160):
           val = i.getPeak(j)
           myData.append(val/amp)
           chanList[j].append([amp*2,val*2.2/1.024])
        peakList.append(myData)
        chanList.append(mChanList)

    npeakList = []
    for i in sizeList:
       for j in peakList:
          if j[0]==i:
             npeakList.append(j)

#    sys.stdout.write("Path={}\n".format(sumPath))
    gainFile = open("{}/gain_{}.txt".format(sumPath,fecname),'w')
    gainFile.write("# tosummary Version {}\n".format(version))
    gainFile.write("# Gain Data Directory {}\n".format(pwd))
    gainFile.write("# Format: changain <chan#> <slope> <intercept> <r_value> <p_value> <std_err>\n")
    gainFile.write("string gainPath {}\n".format(pwd))

    for i in range(0,160):
       x = []
       y = []
       for j in chanList[i]:
          x.append(j[0])
          y.append(j[1])
       slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
       gainFile.write("changain {} {} {} {} {} {}\n".format(i, slope,intercept,r_value,p_value,std_err))
    gainFile.close()
    exit()

gfiles = glob.glob("runinfo.txt")
if len(gfiles) > 0:
    sys.stdout.write("Processing NOISETEST files\n")
    d1 = fecDATA.FEC_data()
    d1.Read("runinfo.txt")
    d1.ReadAll()
    filename = "{}/chan_{}.txt".format(sumPath,words[0])
    d1.SummarySave(filename,version,pwd)
else:
    sys.stdout.write("Nothing to process in this directory!")

exit()
